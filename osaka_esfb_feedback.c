#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <mpi.h>
#include "allvars.h"
#include "proto.h"

#ifdef OSAKA
#ifdef OSAKA_ESFB

static struct esfbmaindata_in
{
  int NodeList[NODELISTLENGTH];
  double Pos[3];
  double Hsml;
  double RHII;
  double ESFBNgbNum;
  double ESFBNgbMass;
  double Masswk;
  double ESFBEnergy;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*ESFBDataIn, *ESFBDataGet;

static struct esfbmaindata_out
{
  double ESFBNgbNum;
  double ESFBNgbMass;
  double Masswk;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*ESFBDataResult, *ESFBDataOut;

static struct MASSWK
{
  double Masswk;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*MassWk;

void osaka_esfb_feedback(void)
{
  set_esfb_star_hsml_guess();
  esfb_star_density();
  
  esfb_feedback();
}

void calc_esfb_energy(int target, double *esfb_energy, int *nfb, double *next_esfb_deposit_time)
{
  int i, inow, inext, fbflagesfb;
  double dt, min_tesfb, desfb_energy;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
      stellarage = time - a2t(P[target].FormationTime);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
      stellarage = time - factor_time * P[target].FormationTime;
    }
    
  desfb_energy = All.ESFBEfficiency * All.ESFBEnergy * P[target].InitialStarMass * All.UnitMass_in_g / SOLAR_MASS / (double) All.ESFBEventNumber;
  fbflagesfb = P[target].FBflagESFB;
  min_tesfb = P[target].ESFBDepositedTime;
  dt = P[target].SNIIStartTime * TIME_GYR / (double) All.ESFBEventNumber;
  *nfb = (int) ((stellarage - min_tesfb) / dt) + 1;
  
  if(*nfb <= 0)
    {
      *esfb_energy = 0.0;
      
      return;
    }
    
  inow = fbflagesfb;
  inext = fbflagesfb + *nfb;
  
  if(inext > All.ESFBEventNumber)
    {
      inext = All.ESFBEventNumber;
      *nfb = inext - fbflagesfb;
    }
    
  *esfb_energy = desfb_energy * (double) (inext - inow);
  
  min_tesfb += dt * (double) *nfb;
  *next_esfb_deposit_time = min_tesfb;
}

void esfb_feedback(void)
{
  int i, j, k, ndone, ndone_flag, dummy;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  int nstar_local, nstar_total, nfb;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  double esfb_energy, esfb_energy_local, esfb_energy_total, next_esfb_deposit_time;
#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[N_SURFACE_TOTAL_LV1];
#endif

  if(ThisTask == 0)
    {
      printf("Start esfb feedback...\n");
      fflush(stdout);
    }
    
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  for(i = FirstActiveParticle, esfb_energy_local = 0.0, nstar_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagESFB < All.ESFBEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
           
          if(stellarage >= P[i].ESFBDepositedTime)
            {
              if(All.ESFBEventNumber == 1)
                esfb_energy = All.ESFBEfficiency * All.ESFBEnergy * P[i].InitialStarMass * All.UnitMass_in_g / SOLAR_MASS;
              else
                calc_esfb_energy(i, &esfb_energy, &nfb, &next_esfb_deposit_time);
            }
          else
            esfb_energy = 0.0;
            
          esfb_energy_local += esfb_energy;
          
          if(esfb_energy > 0.0)
            nstar_local++;
        }
    }
    
  MPI_Allreduce(&esfb_energy_local, &esfb_energy_total, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&nstar_local, &nstar_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(nstar_total > 0.0)
    {
      if(ThisTask == 0)
        {
          printf("Time = %e, esfb_energy_total = %e, nstar_total = %d\n", All.Time, esfb_energy_total, nstar_total);
          fflush(stdout);
        }
    }
  else
    {
      if(ThisTask == 0)
        {
          printf("This loop is no ESFB feedback...\n");
          fflush(stdout);
        }
        
      return;
    }
    
  // First loop : calculation for only wk //
  if(ThisTask == 0)
    {
      printf("Prepare distributing esfb energy...\n");
      fflush(stdout);
    }
    
  Ngblist = (int *) mymalloc(NumPart * sizeof(int));
  
  All.BunchSize = (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                    sizeof(struct esfbmaindata_in) + sizeof(struct esfbmaindata_out) + sizemax(sizeof(struct esfbmaindata_in),
                      sizeof(struct esfbmaindata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
  
  MassWk = (struct MASSWK *) mymalloc(NumPart * sizeof(struct MASSWK));
  
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  osaka_make_geodesic_dome_esfb(surface);
#endif

  i = FirstActiveParticle;
  
  do
    {
      for(j = 0; j < NTask; j++)
        {
          Send_count[j] = 0;
          Exportflag[j] = -1;
        }
        
      // do local particles and prepare export list //
      for(nexport = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagESFB < All.ESFBEventNumber && stellarage >= P[i].ESFBDepositedTime)
            {
              calc_HII_bubble_radius(i);
              
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
              if(esfb_feedback_evaluate(i, 0, 1, &nexport, Send_count) < 0)
                break;
#else
              osaka_assign_surface_esfb(MassWk[i].surface, surface);
              osaka_set_current_star_surface_esfb(P[i].Pos, MassWk[i].surface);
              
              if(esfb_feedback_evaluate_gdw(i, 0, 1, &nexport, Send_count) < 0)
                break;
#endif
            }
        }
        
#ifdef MYSORT
      mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
      qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
      
      MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
      
      for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
        {
          Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
          nimport += Recv_count[j];
          
          if(j > 0)
            {
              Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
              Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
            }
        }
        
      ESFBDataGet = (struct esfbmaindata_in *) mymalloc(nimport * sizeof(struct esfbmaindata_in));
      ESFBDataIn = (struct esfbmaindata_in *) mymalloc(nexport * sizeof(struct esfbmaindata_in));
            
      // prepare particle data for export //
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          ESFBDataIn[j].Pos[0] = P[place].Pos[0];
          ESFBDataIn[j].Pos[1] = P[place].Pos[1];
          ESFBDataIn[j].Pos[2] = P[place].Pos[2];
          ESFBDataIn[j].Hsml = P[place].Hsml;
          ESFBDataIn[j].RHII = P[place].RHII;
          ESFBDataIn[j].RminFeedbackInteraction = MassWk[place].RminFeedbackInteraction;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
          osaka_assign_surface_esfb(ESFBDataIn[j].surface, surface);
          osaka_set_current_star_surface_esfb(P[place].Pos, ESFBDataIn[j].surface);
#endif
          
          memcpy(ESFBDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
        }
        
      // exchange particle data //
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // get the particles //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&ESFBDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct esfbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&ESFBDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct esfbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&ESFBDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct esfbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, &ESFBDataGet[Recv_offset[recvTask]], 
                      Recv_count[recvTask] * sizeof(struct esfbmaindata_in), MPI_BYTE, recvTask, 
                        TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      myfree(ESFBDataIn);
      
      ESFBDataResult = (struct esfbmaindata_out *) mymalloc(nimport * sizeof(struct esfbmaindata_out));
      ESFBDataOut = (struct esfbmaindata_out *) mymalloc(nexport * sizeof(struct esfbmaindata_out));
      
      // now do the particles that were sent to us //
      for(j = 0; j < nimport; j++)
        {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          esfb_feedback_evaluate(j, 1, 1, &dummy, &dummy);
#else
          esfb_feedback_evaluate_gdw(j, 1, 1, &dummy, &dummy);
#endif
        }
        
      if(i < 0)
        ndone_flag = 1;
      else
        ndone_flag = 0;
        
      MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
      
      // get the result // 
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // send the results //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&ESFBDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct esfbmaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&ESFBDataOut[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct esfbmaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&ESFBDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct esfbmaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B, &ESFBDataOut[Send_offset[recvTask]],
                      Send_count[recvTask] * sizeof(struct esfbmaindata_out), MPI_BYTE, recvTask, 
                        TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      // add the result to the local particles // 
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          P[place].ESFBNgbNum += ESFBDataOut[j].ESFBNgbNum;
          P[place].ESFBNgbMass += ESFBDataOut[j].ESFBNgbMass;
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          MassWk[place].Masswk += ESFBDataOut[j].Masswk;
#else
          osaka_update_surface_nparticle_esfb(MassWk[place].surface, ESFBDataOut[j].surface);
#endif
          
          if(MassWk[place].RminFeedbackInteraction > ESFBDataOut[j].RminFeedbackInteraction)
            {
              MassWk[place].RminFeedbackInteraction = ESFBDataOut[j].RminFeedbackInteraction;
              MassWk[place].PID_Rmin = ESFBDataOut[j].PID_Rmin;
            }
        }
        
      myfree(ESFBDataOut);
      myfree(ESFBDataResult);
      myfree(ESFBDataGet);
    }
  while(ndone < NTask);
  
  for(i = FirstActiveParticle, nstar_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagESFB < All.ESFBEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
           
          if(stellarage >= P[i].ESFBDepositedTime)
            {
              if(P[i].ESFBNgbNum == 0.0)
                nstar_local++;
            }
        }
    }
    
  MPI_Allreduce(&nstar_local, &nstar_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(nstar_total > 0)
    {
      if(ThisTask == 0)
        {
          printf("*--- %d star particles can not find interaction gas particles. ---*\n", nstar_total);
          printf("*--- ESFB energy assign nearest gas particle. ---*\n");
          fflush(stdout);
        }
    }
    
  // Second loop : calculation for real part of ESFB feedback //
  if(ThisTask == 0)
    {
      printf("Distributing esfb energy...\n");
      fflush(stdout);
    }
    
  i = FirstActiveParticle;
  
  do
    {
      for(j = 0; j < NTask; j++)
        {
          Send_count[j] = 0;
          Exportflag[j] = -1;
        }
        
      // do local particles and prepare export list //
      for(nexport = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagESFB < All.ESFBEventNumber && stellarage >= P[i].ESFBDepositedTime)
            {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
              if(esfb_feedback_evaluate(i, 0, 2, &nexport, Send_count) < 0)
                break;
#else
              osaka_calc_surface_area_weight_esfb(MassWk[i].surface);
              
              if(esfb_feedback_evaluate_gdw(i, 0, 2, &nexport, Send_count) < 0)
                break;
#endif
            }
        }
        
#ifdef MYSORT
      mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
      qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
      
      MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
      
      for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
        {
          Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
          nimport += Recv_count[j];
          
          if(j > 0)
            {
              Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
              Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
            }
        }
        
      ESFBDataGet = (struct esfbmaindata_in *) mymalloc(nimport * sizeof(struct esfbmaindata_in));
      ESFBDataIn = (struct esfbmaindata_in *) mymalloc(nexport * sizeof(struct esfbmaindata_in));
      
      // prepare particle data for export //
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          ESFBDataIn[j].Pos[0] = P[place].Pos[0];
          ESFBDataIn[j].Pos[1] = P[place].Pos[1];
          ESFBDataIn[j].Pos[2] = P[place].Pos[2];
          ESFBDataIn[j].Hsml = P[place].Hsml;
          ESFBDataIn[j].RHII = P[place].RHII;
          ESFBDataIn[j].ESFBNgbNum = P[place].ESFBNgbNum;
          ESFBDataIn[j].ESFBNgbMass = P[place].ESFBNgbMass;
          ESFBDataIn[j].Masswk = MassWk[place].Masswk;
          ESFBDataIn[j].RminFeedbackInteraction = MassWk[place].RminFeedbackInteraction;
          ESFBDataIn[j].PID_Rmin = MassWk[place].PID_Rmin;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
          osaka_assign_surface_esfb(ESFBDataIn[j].surface, MassWk[place].surface);
#endif

          if(All.ESFBEventNumber == 1)
            esfb_energy = All.ESFBEfficiency * All.ESFBEnergy * P[place].InitialStarMass * All.UnitMass_in_g / SOLAR_MASS;
          else
            calc_esfb_energy(place, &esfb_energy, &nfb, &next_esfb_deposit_time);
            
          ESFBDataIn[j].ESFBEnergy = esfb_energy;
          
          memcpy(ESFBDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
        }
        
      // exchange particle data //
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // get the particles //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&ESFBDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct esfbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&ESFBDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct esfbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&ESFBDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct esfbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, &ESFBDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct esfbmaindata_in), 
                      MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      myfree(ESFBDataIn);
      
      // now do the particles that were sent to us //
      for(j = 0; j < nimport; j++)
        {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          esfb_feedback_evaluate(j, 1, 2, &dummy, &dummy);
#else
          esfb_feedback_evaluate_gdw(j, 1, 2, &dummy, &dummy);
#endif
        }
        
      if(i < 0)
        ndone_flag = 1;
      else
        ndone_flag = 0;
        
      MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
      
      myfree(ESFBDataGet);
    }
  while(ndone < NTask);
  
  myfree(MassWk);
  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagESFB < All.ESFBEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(stellarage >= P[i].ESFBDepositedTime)
            {
              if(All.ESFBEventNumber == 1)
                P[i].FBflagESFB = All.ESFBEventNumber;
              else
                {
                  calc_esfb_energy(i, &esfb_energy, &nfb, &next_esfb_deposit_time);
                  P[i].FBflagESFB += nfb;
                  P[i].ESFBDepositedTime = next_esfb_deposit_time;
                }
            }
        }
    }
}

int esfb_feedback_evaluate(int target, int mode, int loop, int *nexport, int *nsend_local)
{
  int i, j, k, n;
  int startnode, numngb_inbox, listindex = 0;
  int nfb;
  double h, h2, hinv, hinv3, hinv4, dx, dy, dz, r, r2, u, mass_j, wk, dwk;
  double ascale, a3inv, hubble, time, factor_time;
  double pos[3], ngb, ngbmass, masswk, rHII;
  double esfb_energy, esfb_energy_in, next_esfb_deposit_time, rminfeedbackinteraction, fac_HII_bubble_energy, HII_bubble_energy;
  MyIDType pid_rmin;
  
  fac_HII_bubble_energy = 3.0 / 2.0 / 0.6 / PROTONMASS * BOLTZMANN * All.TemperatureHIIBubble * All.UnitMass_in_g / All.UnitEnergy_in_cgs;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (All.Time * All.Time * All.Time);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
      factor_time = 1.0;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      factor_time = All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  if(mode == 0)
    {
      pos[0] = P[target].Pos[0];
      pos[1] = P[target].Pos[1];
      pos[2] = P[target].Pos[2];
      h = P[target].Hsml;
      rHII = P[target].RHII;
      
      if(loop == 1)
        {
          masswk = ngb = ngbmass = 0.0;
          rminfeedbackinteraction = 1.0e30;
        }
        
      if(loop == 2)
        {
          masswk = MassWk[target].Masswk;
          ngb = P[target].ESFBNgbNum;
          ngbmass = P[target].ESFBNgbMass;
          rminfeedbackinteraction = MassWk[target].RminFeedbackInteraction;
          pid_rmin = MassWk[target].PID_Rmin;
          
          if(All.ESFBEventNumber == 1)
            esfb_energy = All.ESFBEfficiency * All.ESFBEnergy * P[target].InitialStarMass * All.UnitMass_in_g / SOLAR_MASS;
          else
            calc_esfb_energy(target, &esfb_energy, &nfb, &next_esfb_deposit_time);
            
          esfb_energy /= All.UnitEnergy_in_cgs;
        }
    }
  else
    {
      pos[0] = ESFBDataGet[target].Pos[0];
      pos[1] = ESFBDataGet[target].Pos[1];
      pos[2] = ESFBDataGet[target].Pos[2];
      h = ESFBDataGet[target].Hsml;
      rHII = ESFBDataGet[target].RHII;
      
      if(loop == 1)
        {
          masswk = ngb = ngbmass = 0.0;
          rminfeedbackinteraction = ESFBDataGet[target].RminFeedbackInteraction;
        }
        
      if(loop == 2)
        {
          masswk = ESFBDataGet[target].Masswk;
          ngb = ESFBDataGet[target].ESFBNgbNum;
          ngbmass = ESFBDataGet[target].ESFBNgbMass;
          rminfeedbackinteraction = ESFBDataGet[target].RminFeedbackInteraction;
          pid_rmin = ESFBDataGet[target].PID_Rmin;
          esfb_energy = ESFBDataGet[target].ESFBEnergy / All.UnitEnergy_in_cgs;
        }
    }
    
#ifdef OSAKA_ESFB_HII_BUBBLE
  if(h < rHII)
    h = rHII;
#endif

  h2 = h * h;
  hinv = 1.0 / h;
#ifndef  TWODIMS
  hinv3 = hinv * hinv * hinv;
#else
  hinv3 = hinv * hinv / boxSize_Z;
#endif
  hinv4 = hinv3 * hinv;
  
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = ESFBDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  if(loop == 2 && ngb == 0.0)
    {
      if(rminfeedbackinteraction < 0.0)
        return 0;
        
      while(startnode >= 0)
        {
          while(startnode >= 0)
            {
              numngb_inbox = ngb_treefind_esfb(pos, h, target, &startnode, mode, nexport, nsend_local);
              
              if(numngb_inbox < 0)
                return -1;
                
              for(n = 0; n < numngb_inbox; n++)
                {
                  j = Ngblist[n];
                  
                  if(pid_rmin == P[j].ID)
                    {
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      
                      HII_bubble_energy = fac_HII_bubble_energy * P[j].Mass;
                      
                      if(esfb_energy > HII_bubble_energy)
                        esfb_energy = HII_bubble_energy;
                        
                      SphP[j].ThermalEnergyESFB += esfb_energy;
                        
                      if(mode == 0)
                        MassWk[target].RminFeedbackInteraction = - 1.0;
                        
                      return 0;
                    }
                }// end of a for-loop for ngb_inbox
            }
            
          if(mode == 1)
            {
              listindex++;
              
              if(listindex < NODELISTLENGTH)
                {
                  startnode = ESFBDataGet[target].NodeList[listindex];
                  
                  if(startnode >= 0)
                    startnode = Nodes[startnode].u.d.nextnode;// open it //
                }
            }
        }
    }
      
  if(mode == 0)
    {
      startnode = All.MaxPart;// root node //
    }
  else
    {
      startnode = ESFBDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  while(startnode >= 0)
    {
      while(startnode >= 0)
        {
          numngb_inbox = ngb_treefind_esfb(pos, h, target, &startnode, mode, nexport, nsend_local);
          
          if(numngb_inbox < 0)
            return -1;
            
          for(n = 0; n < numngb_inbox; n++)
            {
              j = Ngblist[n];
              dx = pos[0] - P[j].Pos[0];
              dy = pos[1] - P[j].Pos[1];
              dz = pos[2] - P[j].Pos[2];
              
              //  now find the closest image in the given box size  //
#ifdef PERIODIC
              if(dx > boxHalf_X)
                dx -= boxSize_X;
              if(dx < -boxHalf_X)
                dx += boxSize_X;
              if(dy > boxHalf_Y)
                dy -= boxSize_Y;
              if(dy < -boxHalf_Y)
                dy += boxSize_Y;
              if(dz > boxHalf_Z)
                dz -= boxSize_Z;
              if(dz < -boxHalf_Z)
                dz += boxSize_Z;
#endif
              r2 = dx * dx + dy * dy + dz * dz;
              
              if(r2 < h2)
                {
                  r = sqrt(r2);
                  u = r * hinv;
                  
                  if(loop == 1)
                    if(rminfeedbackinteraction > r)
                      {
                        rminfeedbackinteraction = r;
                        pid_rmin = P[j].ID;
                      }
                      
#if !defined(QUINTIC_KERNEL) && !defined(WENDLAND_C4_KERNEL)
                  if(u < 0.5)
                    wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1.0) * u * u);
                  else
                    wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
#elif QUINTIC_KERNEL
                  if(0.0 <= u && u < ONETHIRD)
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) + 15.0 * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u);
                  else if(ONETHIRD <= u && u < TWOTHIRD)
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u);
                  else
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u);
                    
                  wk *= KERNEL_NORM * hinv3;
#elif WENDLAND_C4_KERNEL
                  wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 + 6.0 * u + 35.0 / 3 * u * u);
                  wk *= KERNEL_NORM * hinv3;
#endif

                  mass_j = P[j].Mass;
                  
#ifdef OSAKA_HYDRO_INTERACTION_OFF
                  if(SphP[j].flagCoolingSNII == 1 || SphP[j].flagCoolingSNIa == 1)
                    continue;
#endif

                  if(loop == 1)
                    {
                      masswk += FLT(mass_j * wk);
                      ngb += 1.0;
                      ngbmass += mass_j;
                    }
                    
#ifndef OSAKA_ESFB_HII_BUBBLE
                  if(loop == 2)
                    {
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      
                      esfb_energy_in = FLT(esfb_energy * mass_j * wk / masswk);
                      HII_bubble_energy = fac_HII_bubble_energy * P[j].Mass;
                      
                      if(esfb_energy_in > HII_bubble_energy)
                        esfb_energy_in = HII_bubble_energy;
                        
                      SphP[j].ThermalEnergyESFB += esfb_energy_in;
                    }
#else
                  if(loop == 2 && r < rHII)
                    {
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      
                      SphP[j].ThermalEnergyESFB += fac_HII_bubble_energy * P[j].Mass;
                    }
#endif
                }
            }// end of a for-loop for ngb_inbox
        }
        
      if(mode == 1)
        {
          listindex++;
          
          if(listindex < NODELISTLENGTH)
            {
              startnode = ESFBDataGet[target].NodeList[listindex];
              
              if(startnode >= 0)
                startnode = Nodes[startnode].u.d.nextnode;// open it //
            }
        }
    } 
    
  if(loop == 1)
    {
      if(mode == 0)
        {
          MassWk[target].Masswk = masswk;
          P[target].ESFBNgbNum = ngb;
          P[target].ESFBNgbMass = ngbmass;
          MassWk[target].RminFeedbackInteraction = rminfeedbackinteraction;
          MassWk[target].PID_Rmin = pid_rmin;
        }
      else
        {
          ESFBDataResult[target].Masswk = masswk;
          ESFBDataResult[target].ESFBNgbNum = ngb;
          ESFBDataResult[target].ESFBNgbMass = ngbmass;
          ESFBDataResult[target].RminFeedbackInteraction = rminfeedbackinteraction;
          ESFBDataResult[target].PID_Rmin = pid_rmin;
        }
    }
    
  return 0;
}

int ngb_treefind_esfb(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local)
{
  int numngb, no, p, task, nexport_save;
  struct NODE *current;
  double dx, dy, dz, dist;
#ifdef PERIODIC
  MyDouble xtmp;
#endif
  
  nexport_save = *nexport;
  numngb = 0;
  no = *startnode;
  
  while(no >= 0)
    {
      if(no < All.MaxPart)// single particle //
        {
          p = no;
          no = Nextnode[no];
          
          if(P[p].Type != 0)
            continue;
            
          if(P[p].Ti_current != All.Ti_Current)
            drift_particle(p, All.Ti_Current);
            
          dist = hsml;
#ifdef PERIODIC
          dx = NGB_PERIODIC_LONG_X((double) P[p].Pos[0] - searchcenter[0]);
          
          if(dx > dist)
            continue;
            
          dy = NGB_PERIODIC_LONG_Y((double) P[p].Pos[1] - searchcenter[1]);
          
          if(dy > dist)
            continue;
            
          dz = NGB_PERIODIC_LONG_Z((double) P[p].Pos[2] - searchcenter[2]);
          
          if(dz > dist)
                continue;
#else
          dx = (double) P[p].Pos[0] - searchcenter[0];
          
          if(dx > dist)
            continue;
            
          dy = (double) P[p].Pos[1] - searchcenter[1];
          
          if(dy > dist)
            continue;
            
          dz = (double) P[p].Pos[2] - searchcenter[2];
          
          if(dz > dist)
            continue;
#endif
          if(dx * dx + dy * dy + dz * dz > dist * dist)
            continue;
            
          Ngblist[numngb++] = p;
        }
      else
        {
          if(no >= All.MaxPart + MaxNodes)// pseudo particle //
            {
              if(mode == 1)
                endrun(12312);
                
              if(target >= 0)// if no target is given, export will not occur //
                {
                  if(Exportflag[task = DomainTask[no - (All.MaxPart + MaxNodes)]] != target)
                    {
                      Exportflag[task] = target;
                      Exportnodecount[task] = NODELISTLENGTH;
                    }
                    
                  if(Exportnodecount[task] == NODELISTLENGTH)
                    {
                      if(*nexport >= All.BunchSize)
                        {
                          *nexport = nexport_save;
                          
                          if(nexport_save == 0)
                            endrun(13004);// in this case, the buffer is too small to process even a single particle //
                            
                          for(task = 0; task < NTask; task++)
                            nsend_local[task] = 0;
                            
                          for(no = 0; no < nexport_save; no++)
                            nsend_local[DataIndexTable[no].Task]++;
                            
                          return -1;
                        }
                        
                      Exportnodecount[task] = 0;
                      Exportindex[task] = *nexport;
                      DataIndexTable[*nexport].Task = task;
                      DataIndexTable[*nexport].Index = target;
                      DataIndexTable[*nexport].IndexGet = *nexport;
                      *nexport = *nexport + 1;
                      nsend_local[task]++;
                    }
                    
                  DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]++] =
                  DomainNodeIndex[no - (All.MaxPart + MaxNodes)];
                  
                  if(Exportnodecount[task] < NODELISTLENGTH)
                    DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]] = -1;
                }
                
              no = Nextnode[no - MaxNodes];
              continue;
            }
            
          current = &Nodes[no];
          
          if(mode == 1)
            {
              // we reached a top-level node again, which means that we are done with the branch //
              if(current->u.d.bitflags & (1 << BITFLAG_TOPLEVEL))
                {
                  *startnode = -1;
                  return numngb;
                }
            }
            
          if(current->Ti_current != All.Ti_Current)
            force_drift_node(no, All.Ti_Current);
            
          no = current->u.d.sibling;// in case the node can be discarded //
          
          dist = hsml + 0.5 * current->len;
#ifdef PERIODIC	  
          dx = NGB_PERIODIC_LONG_X((double) current->center[0] - searchcenter[0]);
          
          if(dx > dist)
            continue;
            
          dy = NGB_PERIODIC_LONG_Y((double) current->center[1] - searchcenter[1]);
          
          if(dy > dist)
            continue;
            
          dz = NGB_PERIODIC_LONG_Z((double) current->center[2] - searchcenter[2]);
          
          if(dz > dist)
            continue;
#else
          dx = (double) current->center[0] - searchcenter[0];
          
          if(dx > dist)
            continue;
            
          dy = (double) current->center[1] - searchcenter[1];
          
          if(dy > dist)
            continue;
            
          dz = (double) current->center[2] - searchcenter[2];
          
          if(dz > dist)
            continue;
#endif
          // now test against the minimal sphere enclosing everything //
          dist += FACT1 * (double) current->len;
          
          if(dx * dx + dy * dy + dz * dz > dist * dist)
            continue;
            
          no = current->u.d.nextnode;// ok, we need to open the node //
        }
    }
    
  *startnode = -1;
  
  return numngb;
}

void read_LyC_table(void)
{
  int i, j;
  double aa;
  char fin[256];
  FILE *fp;
  
  for(i = 0; i < NMETAL_BIN; i++)
    {
      sprintf(fin, "%s/LyC%d.txt", All.LyCTableDir, i);
      fp = fopen(fin, "rb");
      
      if(fp == NULL)
        {
          printf("can not open LyC table: %s\n", fin);
          exit(1);
        }
        
      for(j = 0; j < NAGE_LYC; j++)
        {
          fread(&aa, sizeof(double), 1, fp);
          LyC_table[i][j] = aa;
        }
        
      fclose(fp);
    }
}

void calc_HII_bubble_radius(int target)
{
  int imetal, iage;
  double stellarage, rHII, fac_rHII, nH, nion;
  double ascale, a3inv, time, hubble, factor_time;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  if(All.ComovingIntegrationOn)
    stellarage = time - a2t(P[target].FormationTime);
  else
    stellarage = time - factor_time * P[target].FormationTime;
    
  stellarage *= 1.0e3;
  iage = (int) stellarage;
  
  if(P[target].Metallicity <= pow(10.0, METAL_MIN))
    imetal = 0;
  else if(P[target].Metallicity > pow(10.0, METAL_MAX))
    imetal = NMETAL_BIN - 1;
  else
    imetal = (int) ((log10(P[target].Metallicity ) - (double) METAL_MIN) / (double) METAL_BIN);
    
  fac_rHII = 9.719550e+3;// (3/4/PI/alphaB)^1/3 //
  nion = LyC_table[imetal][iage] * P[target].InitialStarMass * All.UnitMass_in_g / SOLAR_MASS;
  nH = P[target].Density * All.InitAbundance_Hydrogen * All.UnitDensity_in_cgs / PROTONMASS;
  nH *= hubble * hubble * a3inv;
  rHII = fac_rHII * pow(nion / nH / nH, 1.0 / 3.0) / All.UnitLength_in_cm * hubble / ascale;
  
  P[target].RHII = rHII;
}












#ifdef OSAKA_GEODESIC_DOME_WEIGHT
int esfb_feedback_evaluate_gdw(int target, int mode, int loop, int *nexport, int *nsend_local)
{
  int i, j, k, n;
  int startnode, numngb_inbox, listindex = 0;
  int nfb;
  double h, h2, dx, dy, dz, r, r2, u, mass_j;
  double ascale, a3inv, hubble, time, factor_time;
  double pos[3], ngb, ngbmass, rHII;
  double esfb_energy, esfb_energy_in, next_esfb_deposit_time, rminfeedbackinteraction, fac_HII_bubble_energy, HII_bubble_energy;
  MyIDType pid_rmin;
  struct Surface surface[N_SURFACE_TOTAL_LV1];
  
  fac_HII_bubble_energy = 3.0 / 2.0 / 0.6 / PROTONMASS * BOLTZMANN * All.TemperatureHIIBubble * All.UnitMass_in_g / All.UnitEnergy_in_cgs;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (All.Time * All.Time * All.Time);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
      factor_time = 1.0;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      factor_time = All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  if(mode == 0)
    {
      osaka_assign_surface_esfb(surface, MassWk[target].surface);
      pos[0] = P[target].Pos[0];
      pos[1] = P[target].Pos[1];
      pos[2] = P[target].Pos[2];
      h = P[target].Hsml;
      rHII = P[target].RHII;
      
      if(loop == 1)
        {
          ngb = ngbmass = 0.0;
          rminfeedbackinteraction = 1.0e30;
        }
        
      if(loop == 2)
        {
          ngb = P[target].ESFBNgbNum;
          ngbmass = P[target].ESFBNgbMass;
          rminfeedbackinteraction = MassWk[target].RminFeedbackInteraction;
          pid_rmin = MassWk[target].PID_Rmin;
          
          if(All.ESFBEventNumber == 1)
            esfb_energy = All.ESFBEfficiency * All.ESFBEnergy * P[target].InitialStarMass * All.UnitMass_in_g / SOLAR_MASS;
          else
            calc_esfb_energy(target, &esfb_energy, &nfb, &next_esfb_deposit_time);
            
          esfb_energy /= All.UnitEnergy_in_cgs;
        }
    }
  else
    {
      osaka_assign_surface_esfb(surface, ESFBDataGet[target].surface);
      pos[0] = ESFBDataGet[target].Pos[0];
      pos[1] = ESFBDataGet[target].Pos[1];
      pos[2] = ESFBDataGet[target].Pos[2];
      h = ESFBDataGet[target].Hsml;
      rHII = ESFBDataGet[target].RHII;
      
      if(loop == 1)
        {
          ngb = ngbmass = 0.0;
          rminfeedbackinteraction = ESFBDataGet[target].RminFeedbackInteraction;
        }
        
      if(loop == 2)
        {
          ngb = ESFBDataGet[target].ESFBNgbNum;
          ngbmass = ESFBDataGet[target].ESFBNgbMass;
          rminfeedbackinteraction = ESFBDataGet[target].RminFeedbackInteraction;
          pid_rmin = ESFBDataGet[target].PID_Rmin;
          esfb_energy = ESFBDataGet[target].ESFBEnergy / All.UnitEnergy_in_cgs;
        }
    }
    
#ifdef OSAKA_ESFB_HII_BUBBLE
  if(h < rHII)
    h = rHII;
#endif

  h2 = h * h;
  
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = ESFBDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  if(loop == 2 && ngb == 0.0)
    {
      if(rminfeedbackinteraction < 0.0)
        return 0;
        
      while(startnode >= 0)
        {
          while(startnode >= 0)
            {
              numngb_inbox = ngb_treefind_esfb(pos, h, target, &startnode, mode, nexport, nsend_local);
              
              if(numngb_inbox < 0)
                return -1;
                
              for(n = 0; n < numngb_inbox; n++)
                {
                  j = Ngblist[n];
                  
                  if(pid_rmin == P[j].ID)
                    {
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      
                      HII_bubble_energy = fac_HII_bubble_energy * P[j].Mass;
                      
                      if(esfb_energy > HII_bubble_energy)
                        esfb_energy = HII_bubble_energy;
                        
                      SphP[j].ThermalEnergyESFB += esfb_energy;
                        
                      if(mode == 0)
                        MassWk[target].RminFeedbackInteraction = - 1.0;
                        
                      return 0;
                    }
                }// end of a for-loop for ngb_inbox
            }
            
          if(mode == 1)
            {
              listindex++;
              
              if(listindex < NODELISTLENGTH)
                {
                  startnode = ESFBDataGet[target].NodeList[listindex];
                  
                  if(startnode >= 0)
                    startnode = Nodes[startnode].u.d.nextnode;// open it //
                }
            }
        }
    }
      
  if(mode == 0)
    {
      startnode = All.MaxPart;// root node //
    }
  else
    {
      startnode = ESFBDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  while(startnode >= 0)
    {
      while(startnode >= 0)
        {
          numngb_inbox = ngb_treefind_esfb(pos, h, target, &startnode, mode, nexport, nsend_local);
          
          if(numngb_inbox < 0)
            return -1;
            
          for(n = 0; n < numngb_inbox; n++)
            {
              j = Ngblist[n];
              dx = pos[0] - P[j].Pos[0];
              dy = pos[1] - P[j].Pos[1];
              dz = pos[2] - P[j].Pos[2];
              
              //  now find the closest image in the given box size  //
#ifdef PERIODIC
              if(dx > boxHalf_X)
                dx -= boxSize_X;
              if(dx < -boxHalf_X)
                dx += boxSize_X;
              if(dy > boxHalf_Y)
                dy -= boxSize_Y;
              if(dy < -boxHalf_Y)
                dy += boxSize_Y;
              if(dz > boxHalf_Z)
                dz -= boxSize_Z;
              if(dz < -boxHalf_Z)
                dz += boxSize_Z;
#endif
              r2 = dx * dx + dy * dy + dz * dz;
              
              if(r2 < h2)
                {
                  r = sqrt(r2);
                  
                  if(loop == 1)
                    if(rminfeedbackinteraction > r)
                      {
                        rminfeedbackinteraction = r;
                        pid_rmin = P[j].ID;
                      }
                      
                  mass_j = P[j].Mass;
                  
#ifdef OSAKA_HYDRO_INTERACTION_OFF
                  if(SphP[j].flagCoolingSNII == 1 || SphP[j].flagCoolingSNIa == 1)
                    continue;
#endif

                  if(loop == 1)
                    {
                      osaka_search_particle_inside_surface_esfb(pos, P[j].Pos, surface);
                      ngb += 1.0;
                      ngbmass += mass_j;
                    }
                    
#ifndef OSAKA_ESFB_HII_BUBBLE
                  if(loop == 2)
                    {
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      
                      osaka_physical_value_assignment_esfb(j, mass_j, esfb_energy, pos, P[j].Pos, surface);
                    }
#else
                  if(loop == 2 && r < rHII)
                    {
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      
                      SphP[j].ThermalEnergyESFB += fac_HII_bubble_energy * P[j].Mass;
                    }
#endif
                }
            }// end of a for-loop for ngb_inbox
        }
        
      if(mode == 1)
        {
          listindex++;
          
          if(listindex < NODELISTLENGTH)
            {
              startnode = ESFBDataGet[target].NodeList[listindex];
              
              if(startnode >= 0)
                startnode = Nodes[startnode].u.d.nextnode;// open it //
            }
        }
    } 
    
  if(loop == 1)
    {
      if(mode == 0)
        {
          osaka_assign_surface_esfb(MassWk[target].surface, surface);
          P[target].ESFBNgbNum = ngb;
          P[target].ESFBNgbMass = ngbmass;
          MassWk[target].RminFeedbackInteraction = rminfeedbackinteraction;
          MassWk[target].PID_Rmin = pid_rmin;
        }
      else
        {
          osaka_assign_surface_esfb(ESFBDataResult[target].surface, surface);
          ESFBDataResult[target].ESFBNgbNum = ngb;
          ESFBDataResult[target].ESFBNgbMass = ngbmass;
          ESFBDataResult[target].RminFeedbackInteraction = rminfeedbackinteraction;
          ESFBDataResult[target].PID_Rmin = pid_rmin;
        }
    }
    
  return 0;
}

void osaka_physical_value_assignment_esfb(int target, double mass, double esfb_energy, double position_star[], double position_gas[], struct Surface surface[])
{
  int i, j, k;
  double v01[3], v02[3], vsp[3], vs0[3], re2[3], te1[3], re2e1, t, u, v;
  double esfb_energyin, fac_HII_bubble_energy, HII_bubble_energy;
#ifdef PERIODIC
  double vsptemp[3];
#endif

  fac_HII_bubble_energy = 3.0 / 2.0 / 0.6 / PROTONMASS * BOLTZMANN * All.TemperatureHIIBubble * All.UnitMass_in_g / All.UnitEnergy_in_cgs;
  HII_bubble_energy = fac_HII_bubble_energy * mass;
  
  for(k = 0; k < 3; k++)
    {
#ifdef PERIODIC
      vsptemp[0] = position_gas[k] - position_star[k];
      vsptemp[1] = vsptemp[0] + header.BoxSize;
      vsptemp[2] = vsptemp[0] - header.BoxSize;
      
      if(fabs(vsptemp[0]) < fabs(vsptemp[1]))
        {
          if(fabs(vsptemp[0]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[0];
          else
            vsp[k] = vsptemp[2];
        }
      else
        {
          if(fabs(vsptemp[1]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[1];
          else
            vsp[k] = vsptemp[2];
        }
#else
      vsp[k] = position_gas[k] - position_star[k];
#endif
    }
    
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle == 0)
        continue;
      if(surface[i].nparticle == 1)
        {
          for(k = 0; k < 3; k++)
            {
              v01[k] = surface[i].vertex[1][k] - surface[i].vertex[0][k];
              v02[k] = surface[i].vertex[2][k] - surface[i].vertex[0][k];
              vs0[k] = position_star[k] - surface[i].vertex[0][k];
            }
            
          osaka_cross_product_esfb(vsp, v02, re2);
          osaka_cross_product_esfb(vs0, v01, te1);
          re2e1 = osaka_inner_product_esfb(re2, v01);
          
          t = osaka_inner_product_esfb(te1, v02) / re2e1;
          u = osaka_inner_product_esfb(re2, vs0) / re2e1;
          v = osaka_inner_product_esfb(te1, vsp) / re2e1;
          
          if(t < 0.0)
            continue;
            
          if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
            {
              esfb_energyin = esfb_energy * surface[i].occupancy;
              
              if(esfb_energyin > HII_bubble_energy)
                esfb_energyin = HII_bubble_energy;
                
              SphP[target].ThermalEnergyESFB += esfb_energyin;
              
              return;
            }
        }
      else
        {
          for(j = 0; j < 4; j++)
            {
              if(surface[surface[i].children[j]].nparticle == 0)
                continue;
                
              for(k = 0; k < 3; k++)
                {
                  v01[k] = surface[surface[i].children[j]].vertex[1][k] - surface[surface[i].children[j]].vertex[0][k];
                  v02[k] = surface[surface[i].children[j]].vertex[2][k] - surface[surface[i].children[j]].vertex[0][k];
                  vs0[k] = position_star[k] - surface[surface[i].children[j]].vertex[0][k];
                }
                
              osaka_cross_product_esfb(vsp, v02, re2);
              osaka_cross_product_esfb(vs0, v01, te1);
              re2e1 = osaka_inner_product_esfb(re2, v01);
              
              t = osaka_inner_product_esfb(te1, v02) / re2e1;
              u = osaka_inner_product_esfb(re2, vs0) / re2e1;
              v = osaka_inner_product_esfb(te1, vsp) / re2e1;
              
              if(t < 0.0)
                continue;
                
              if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
                {
                  esfb_energyin = esfb_energy * surface[surface[i].children[j]].occupancy;
                  
                  if(esfb_energyin > HII_bubble_energy)
                    esfb_energyin = HII_bubble_energy;
                    
                  SphP[target].ThermalEnergyESFB += esfb_energyin;
                  
                  return;
                }
            }
        }
    }
}

void osaka_calc_surface_area_weight_esfb(struct Surface surface[])
{
  int i, j, k, l;
  int nin, surface_id;
  double vectord[3], vectordc[3], division_factor, occupancyp, occupancyc, occupancyd, occupancydc, occupancy_leftp, occupancy_leftc, norm;
  
  occupancyp = 1.0 / (double) N_ICOSAHEDRON_SURFACE;
  occupancyc = 0.25 * occupancyp;
  occupancy_leftp = 0.0;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    if(surface[i].nparticle == 0)
        surface[i].occupancy = 0.0;
        
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle == 1)
        continue;
        
      if(surface[i].nparticle > 1)
        {
          vectordc[0] = vectordc[1] = vectordc[2] = 0.0;
          
          for(j = 0, nin = 0; j < 4; j++)
            {
              surface_id = surface[i].children[j];
              
              if(surface[surface_id].nparticle == 0)
                {
                  vectordc[0] += surface[surface_id].vector[0];
                  vectordc[1] += surface[surface_id].vector[1];
                  vectordc[2] += surface[surface_id].vector[2];
                  nin++;
                }
            }
            
          division_factor = 1.0 / (double) (NDIVISION - nin);
          occupancydc = occupancyc * (double) nin * division_factor;
          vectordc[0] *= division_factor;
          vectordc[1] *= division_factor;
          vectordc[2] *= division_factor;
          
          for(j = 0; j < 4; j++)
            {
              surface_id = surface[i].children[j];
              
              if(surface[surface_id].nparticle > 0)
                {
                  surface[surface_id].occupancy += occupancydc;
                  surface[surface_id].vector[0] += vectordc[0];
                  surface[surface_id].vector[1] += vectordc[1];
                  surface[surface_id].vector[2] += vectordc[2];
                }
            }
        }
      else if(surface[i].nparticle == 0)
        {
          for(j = 0, nin = 0; j < 3; j++)
            {
              surface_id = surface[i].neighbour[j];
              
              if(surface[surface_id].nparticle > 0)
                nin++;
            }
            
           if(nin > 0)
             {
               division_factor = 1.0 / (double) nin;
               occupancyd = occupancyp * division_factor;
               vectord[0] = surface[i].vector[0] * division_factor;
               vectord[1] = surface[i].vector[1] * division_factor;
               vectord[2] = surface[i].vector[2] * division_factor;
               
               for(j = 0; j < 3; j++)
                 {
                   surface_id = surface[i].neighbour[j];
                   
                   if(surface[surface_id].nparticle > 0)
                     {
                       surface[surface_id].occupancy += occupancyd;
                       surface[surface_id].vector[0] += vectord[0];
                       surface[surface_id].vector[1] += vectord[1];
                       surface[surface_id].vector[2] += vectord[2];
                       
                       if(surface[surface_id].nparticle > 1)
                         {
                           for(k = 0, nin = 0; k < 4; k++)
                             {
                               surface_id = surface[surface[i].neighbour[j]].children[k];
                               
                               if(surface[surface_id].nparticle > 0)
                                 nin++;
                             }
                             
                           division_factor = 1.0 / (double) nin;
                           occupancydc = occupancyd * division_factor;
                           vectordc[0] = vectord[0] * division_factor;
                           vectordc[1] = vectord[1] * division_factor;
                           vectordc[2] = vectord[2] * division_factor;
                           
                           for(k = 0; k < 4; k++)
                             {
                               surface_id = surface[surface[i].neighbour[j]].children[k];
                               
                               if(surface[surface_id].nparticle > 0)
                                 {
                                   surface[surface_id].occupancy += occupancydc;
                                   surface[surface_id].vector[0] += vectordc[0];
                                   surface[surface_id].vector[1] += vectordc[1];
                                   surface[surface_id].vector[2] += vectordc[2];
                                 }
                             }
                         }
                     }
                 }
             }
           else
             {
               for(j = 0, nin = 0; j < 3; j++)
                 for(k = 0; k < 3; k++)
                   {
                     surface_id = surface[surface[i].neighbour[j]].neighbour[k];
                     
                     if(surface[surface_id].nparticle > 0)
                       nin++;
                   }
                   
               if(nin == 0)
                 occupancy_leftp += occupancyp;
                 
               if(nin > 0)
                 {
                   division_factor = 1.0 / (double) nin;
                   occupancyd = occupancyp * division_factor;
                   vectord[0] = surface[i].vector[0] * division_factor;
                   vectord[1] = surface[i].vector[1] * division_factor;
                   vectord[2] = surface[i].vector[2] * division_factor;
                   
                   for(j = 0; j < 3; j++)
                     for(k = 0; k < 3; k++)
                       {
                         surface_id = surface[surface[i].neighbour[j]].neighbour[k];
                         
                         if(surface[surface_id].nparticle > 0)
                           {
                             surface[surface_id].occupancy += occupancyd;
                             surface[surface_id].vector[0] += vectord[0];
                             surface[surface_id].vector[1] += vectord[1];
                             surface[surface_id].vector[2] += vectord[2];
                             
                             if(surface[surface_id].nparticle > 1)
                               {
                                 for(l = 0, nin = 0; l < 4; l++)
                                   {
                                     surface_id = surface[surface[surface[i].neighbour[j]].neighbour[k]].children[l];
                                     
                                     if(surface[surface_id].nparticle > 0)
                                       nin++;
                                   }
                                   
                                 division_factor = 1.0 / (double) nin;
                                 occupancydc = occupancyd * division_factor;
                                 vectordc[0] = vectord[0] * division_factor;
                                 vectordc[1] = vectord[1] * division_factor;
                                 vectordc[2] = vectord[2] * division_factor;
                                 
                                 for(l = 0; l < 4; l++)
                                   {
                                     surface_id = surface[surface[surface[i].neighbour[j]].neighbour[k]].children[l];
                                     
                                     if(surface[surface_id].nparticle > 0)
                                       {
                                         surface[surface_id].occupancy += occupancydc;
                                         surface[surface_id].vector[0] += vectordc[0];
                                         surface[surface_id].vector[1] += vectordc[1];
                                         surface[surface_id].vector[2] += vectordc[2];
                                       }
                                   }
                               }
                           }
                       }
                 }
             }
        }
    }
    
  for(i = 0, nin = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    if(surface[i].nparticle > 0)
      nin++;
      
  occupancy_leftp /= (double) nin;
  
  for(i = 0, nin = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle > 0)
        surface[i].occupancy += occupancy_leftp;
        
      if(surface[i].nparticle > 1)
        {
          for(j = 0, nin = 0; j < 4; j++)
            if(surface[surface[i].children[j]].nparticle > 0)
              nin++;
              
          occupancy_leftc = occupancy_leftp / (double) nin;
          
          for(j = 0, nin = 0; j < 4; j++)
            if(surface[surface[i].children[j]].nparticle > 0)
              surface[surface[i].children[j]].occupancy += occupancy_leftc;
        }
    }
    
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    if(surface[i].nparticle > 0)
      {
        norm = 0.0;
        norm += surface[i].vector[0] * surface[i].vector[0] + surface[i].vector[1] * surface[i].vector[1] + surface[i].vector[2] * surface[i].vector[2];
        norm = sqrt(norm);
        
        surface[i].vector[0] /= norm;
        surface[i].vector[1] /= norm;
        surface[i].vector[2] /= norm;
      }
    else
      {
        surface[i].vector[0] = 0.0;
        surface[i].vector[1] = 0.0;
        surface[i].vector[2] = 0.0;
      }
      
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    if(surface[i].nparticle > 1)
      for(j = 0; j < 4; j++)
        if(surface[surface[i].children[j]].nparticle > 1)
          surface[surface[i].children[j]].occupancy /= (double) surface[surface[i].children[j]].nparticle;
}

void osaka_set_current_star_surface_esfb(double position_star[], struct Surface surface[])
{
  int i, j, k;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    for(j = 0; j < 3; j++)
      for(k = 0; k < 3; k++)
        surface[i].vertex[j][k] += position_star[k];
}

void osaka_search_particle_inside_surface_esfb(double position_star[], double position_gas[], struct Surface surface[])
{
  int i, j, k;
  double v01[3], v02[3], vsp[3], vs0[3], re2[3], te1[3], re2e1, t, u, v;
#ifdef PERIODIC
  double vsptemp[3];
#endif

  for(k = 0; k < 3; k++)
    {
#ifdef PERIODIC
      vsptemp[0] = position_gas[k] - position_star[k];
      vsptemp[1] = vsptemp[0] + header.BoxSize;
      vsptemp[2] = vsptemp[0] - header.BoxSize;
      
      if(fabs(vsptemp[0]) < fabs(vsptemp[1]))
        {
          if(fabs(vsptemp[0]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[0];
          else
            vsp[k] = vsptemp[2];
        }
      else
        {
          if(fabs(vsptemp[1]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[1];
          else
            vsp[k] = vsptemp[2];
        }
#else
      vsp[k] = position_gas[k] - position_star[k];
#endif
    }
    
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      for(k = 0; k < 3; k++)
        {
          v01[k] = surface[i].vertex[1][k] - surface[i].vertex[0][k];
          v02[k] = surface[i].vertex[2][k] - surface[i].vertex[0][k];
          vs0[k] = position_star[k] - surface[i].vertex[0][k];
        }
        
      osaka_cross_product_esfb(vsp, v02, re2);
      osaka_cross_product_esfb(vs0, v01, te1);
      re2e1 = osaka_inner_product_esfb(re2, v01);
      
      t = osaka_inner_product_esfb(te1, v02) / re2e1;
      u = osaka_inner_product_esfb(re2, vs0) / re2e1;
      v = osaka_inner_product_esfb(te1, vsp) / re2e1;
      
      if(t < 0.0)
        continue;
        
      if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
        {
          surface[i].nparticle++;
          
          for(j = 0; j < 4; j++)
            {
              for(k = 0; k < 3; k++)
                {
                  v01[k] = surface[surface[i].children[j]].vertex[1][k] - surface[surface[i].children[j]].vertex[0][k];
                  v02[k] = surface[surface[i].children[j]].vertex[2][k] - surface[surface[i].children[j]].vertex[0][k];
                  vs0[k] = position_star[k] - surface[surface[i].children[j]].vertex[0][k];
                }
                
              osaka_cross_product_esfb(vsp, v02, re2);
              osaka_cross_product_esfb(vs0, v01, te1);
              re2e1 = osaka_inner_product_esfb(re2, v01);
              
              t = osaka_inner_product_esfb(te1, v02) / re2e1;
              u = osaka_inner_product_esfb(re2, vs0) / re2e1;
              v = osaka_inner_product_esfb(te1, vsp) / re2e1;
              
              if(t < 0.0)
                continue;
                
              if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
                {
                  surface[surface[i].children[j]].nparticle++;
                  break;
                }
            }
        }
    }
}

double osaka_inner_product_esfb(double aa[], double bb[])
{
  return aa[0] * bb[0] + aa[1] * bb[1] + aa[2] * bb[2];
}

void osaka_cross_product_esfb(double aa[], double bb[], double cc[])
{
  cc[0] = aa[1] * bb[2] - aa[2] * bb[1];
  cc[1] = aa[2] * bb[0] - aa[0] * bb[2];
  cc[2] = aa[0] * bb[1] - aa[1] * bb[0];
}

void osaka_assign_surface_esfb(struct Surface surface_out[], struct Surface surface_in[])
{
  int i;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    surface_out[i] = surface_in[i];
}

void osaka_update_surface_nparticle_esfb(struct Surface surface_out[], struct Surface surface_in[])
{
  int i;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    surface_out[i].nparticle += surface_in[i].nparticle;
}

void osaka_make_geodesic_dome_esfb(struct Surface surface[])
{
  int i, k;
  double norm;
  struct Point point[N_ICOSAHEDRON_VERTEX];
  
  osaka_set_icosahedron_vertex_esfb(point);
  osaka_set_icosahedron_surface_esfb(surface, point);
  osaka_surface_refinement_esfb(surface);
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    {
      for(k = 0; k < 3; k++)
        {
          norm = surface[i].vertex[k][0] * surface[i].vertex[k][0] + surface[i].vertex[k][1] * surface[i].vertex[k][1] + surface[i].vertex[k][2] * surface[i].vertex[k][2];
          norm = sqrt(norm);
          surface[i].vertex[k][0] /= norm;
          surface[i].vertex[k][1] /= norm;
          surface[i].vertex[k][2] /= norm;
        }
        
      for(k = 0, norm = 0.0; k < 3; k++)
        {
          surface[i].vector[k] = (surface[i].vertex[0][k] + surface[i].vertex[1][k] + surface[i].vertex[2][k]) / 3.0;
          norm += surface[i].vector[k] * surface[i].vector[k];
        }
        
      norm = sqrt(norm);
      surface[i].vector[0] /= norm;
      surface[i].vector[1] /= norm;
      surface[i].vector[2] /= norm;
      surface[i].nparticle = 0;
      
      /*for(k = 0; k < 3; k++)
        {
          surface[i].vertex[k][0] *= header.BoxSize;
          surface[i].vertex[k][1] *= header.BoxSize;
          surface[i].vertex[k][2] *= header.BoxSize;
        }*/
    }
}

void osaka_surface_refinement_esfb(struct Surface surface[])
{
  int i, k, istart, iend;
  double middle_point[3][3];
  
  istart = 0;
  iend = N_ICOSAHEDRON_SURFACE;
  
  for(i = istart; i < iend; i++)
    {
      for(k = 0; k < 4; k++)
        {
          surface[surface[i].children[k]].id = surface[i].children[k];
          surface[surface[i].children[k]].parent = surface[i].id;
          surface[surface[i].children[k]].occupancy = 0.25* surface[i].occupancy;
          
          surface[surface[i].children[k]].children[0] = - 1;
          surface[surface[i].children[k]].children[1] = - 1;
          surface[surface[i].children[k]].children[2] = - 1;
          surface[surface[i].children[k]].children[3] = - 1;
        }
        
      surface[surface[i].children[0]].neighbour[0] = surface[surface[i].children[1]].id;
      surface[surface[i].children[0]].neighbour[1] = surface[surface[i].children[2]].id;
      surface[surface[i].children[0]].neighbour[2] = surface[surface[i].children[3]].id;
      
      surface[surface[i].children[1]].neighbour[0] = surface[surface[i].children[2]].id;
      surface[surface[i].children[1]].neighbour[1] = surface[surface[i].children[3]].id;
      surface[surface[i].children[1]].neighbour[2] = surface[surface[i].children[0]].id;
      
      surface[surface[i].children[2]].neighbour[0] = surface[surface[i].children[3]].id;
      surface[surface[i].children[2]].neighbour[1] = surface[surface[i].children[0]].id;
      surface[surface[i].children[2]].neighbour[2] = surface[surface[i].children[1]].id;
      
      surface[surface[i].children[3]].neighbour[0] = surface[surface[i].children[0]].id;
      surface[surface[i].children[3]].neighbour[1] = surface[surface[i].children[1]].id;
      surface[surface[i].children[3]].neighbour[2] = surface[surface[i].children[2]].id;
      
      for(k = 0; k < 3; k++)
        {
          middle_point[0][k] = 0.5 * (surface[i].vertex[0][k] + surface[i].vertex[1][k]);
          middle_point[1][k] = 0.5 * (surface[i].vertex[1][k] + surface[i].vertex[2][k]);
          middle_point[2][k] = 0.5 * (surface[i].vertex[2][k] + surface[i].vertex[0][k]);
          
          surface[surface[i].children[0]].vertex[0][k] = surface[i].vertex[0][k];
          surface[surface[i].children[0]].vertex[1][k] = middle_point[0][k];
          surface[surface[i].children[0]].vertex[2][k] = middle_point[2][k];
          
          surface[surface[i].children[1]].vertex[0][k] = surface[i].vertex[1][k];
          surface[surface[i].children[1]].vertex[1][k] = middle_point[1][k];
          surface[surface[i].children[1]].vertex[2][k] = middle_point[0][k];
          
          surface[surface[i].children[2]].vertex[0][k] = surface[i].vertex[2][k];
          surface[surface[i].children[2]].vertex[1][k] = middle_point[2][k];
          surface[surface[i].children[2]].vertex[2][k] = middle_point[1][k];
          
          surface[surface[i].children[3]].vertex[0][k] = middle_point[0][k];
          surface[surface[i].children[3]].vertex[1][k] = middle_point[1][k];
          surface[surface[i].children[3]].vertex[2][k] = middle_point[2][k];
        }
    }
}

void osaka_set_icosahedron_vertex_esfb(struct Point point[])
{
  int i , k;
  double golden_ratio, norm;
  
  golden_ratio = 0.5 * (1.0 + sqrt(5));
  
  point[0].x[0] = 1.0;
  point[0].x[1] = golden_ratio;
  point[0].x[2] = 0.0;
  
  point[1].x[0] = - 1.0;
  point[1].x[1] = golden_ratio;
  point[1].x[2] = 0.0;
  
  point[2].x[0] = 1.0;
  point[2].x[1] = - golden_ratio;
  point[2].x[2] = 0.0;
  
  point[3].x[0] = - 1.0;
  point[3].x[1] = - golden_ratio;
  point[3].x[2] = 0.0;
  
  point[4].x[0] = 0.0;
  point[4].x[1] = 1.0;
  point[4].x[2] = golden_ratio;
  
  point[5].x[0] = 0.0;
  point[5].x[1] = - 1.0;
  point[5].x[2] = golden_ratio;
  
  point[6].x[0] = 0.0;
  point[6].x[1] = 1.0;
  point[6].x[2] = - golden_ratio;
  
  point[7].x[0] = 0.0;
  point[7].x[1] = - 1.0;
  point[7].x[2] = - golden_ratio;
  
  point[8].x[0] = golden_ratio;
  point[8].x[1] = 0.0;
  point[8].x[2] = 1.0;
  
  point[9].x[0] = golden_ratio;
  point[9].x[1] = 0.0;
  point[9].x[2] = - 1.0;
  
  point[10].x[0] = - golden_ratio;
  point[10].x[1] = 0.0;
  point[10].x[2] = 1.0;
  
  point[11].x[0] = - golden_ratio;
  point[11].x[1] = 0.0;
  point[11].x[2] = - 1.0;
  
  norm = sqrt(1.0 + golden_ratio * golden_ratio);
  
  for(i = 0; i < N_ICOSAHEDRON_VERTEX; i++)
    for(k = 0; k < 3; k++)
      point[i].x[k] /= norm;
}

void osaka_set_icosahedron_surface_esfb(struct Surface surface[], struct Point point[])
{
  int i, k, children_id_offset;
  double occupancy;
  
  occupancy = 1.0 / (double) N_ICOSAHEDRON_SURFACE;
  
  for(i = 0, children_id_offset = N_ICOSAHEDRON_SURFACE; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      surface[i].parent = - 1;
      surface[i].id = i;
      surface[i].children[0] = children_id_offset + 0;
      surface[i].children[1] = children_id_offset + 1;
      surface[i].children[2] = children_id_offset + 2;
      surface[i].children[3] = children_id_offset + 3;
      children_id_offset += NDIVISION;
      surface[i].occupancy = occupancy;
    }
    
  for(k = 0; k < 3; k++)
    {
      surface[0].vertex[0][k] = point[0].x[k];
      surface[0].vertex[1][k] = point[1].x[k];
      surface[0].vertex[2][k] = point[4].x[k];
      surface[0].vector[k] = (surface[0].vertex[0][k] + surface[0].vertex[1][k] + surface[0].vertex[2][k]) / 3.0;
      
      surface[1].vertex[0][k] = point[1].x[k];
      surface[1].vertex[1][k] = point[4].x[k];
      surface[1].vertex[2][k] = point[10].x[k];
      surface[1].vector[k] = (surface[1].vertex[0][k] + surface[1].vertex[1][k] + surface[1].vertex[2][k]) / 3.0;
      
      surface[2].vertex[0][k] = point[4].x[k];
      surface[2].vertex[1][k] = point[5].x[k];
      surface[2].vertex[2][k] = point[10].x[k];
      surface[2].vector[k] = (surface[2].vertex[0][k] + surface[2].vertex[1][k] + surface[2].vertex[2][k]) / 3.0;
      
      surface[3].vertex[0][k] = point[4].x[k];
      surface[3].vertex[1][k] = point[5].x[k];
      surface[3].vertex[2][k] = point[8].x[k];
      surface[3].vector[k] = (surface[3].vertex[0][k] + surface[3].vertex[1][k] + surface[3].vertex[2][k]) / 3.0;
      
      surface[4].vertex[0][k] = point[0].x[k];
      surface[4].vertex[1][k] = point[4].x[k];
      surface[4].vertex[2][k] = point[8].x[k];
      surface[4].vector[k] = (surface[4].vertex[0][k] + surface[4].vertex[1][k] + surface[4].vertex[2][k]) / 3.0;
      
      surface[5].vertex[0][k] = point[0].x[k];
      surface[5].vertex[1][k] = point[1].x[k];
      surface[5].vertex[2][k] = point[6].x[k];
      surface[5].vector[k] = (surface[5].vertex[0][k] + surface[5].vertex[1][k] + surface[5].vertex[2][k]) / 3.0;
      
      surface[6].vertex[0][k] = point[1].x[k];
      surface[6].vertex[1][k] = point[6].x[k];
      surface[6].vertex[2][k] = point[11].x[k];
      surface[6].vector[k] = (surface[6].vertex[0][k] + surface[6].vertex[1][k] + surface[6].vertex[2][k]) / 3.0;
      
      surface[7].vertex[0][k] = point[1].x[k];
      surface[7].vertex[1][k] = point[10].x[k];
      surface[7].vertex[2][k] = point[11].x[k];
      surface[7].vector[k] = (surface[7].vertex[0][k] + surface[7].vertex[1][k] + surface[7].vertex[2][k]) / 3.0;
      
      surface[8].vertex[0][k] = point[3].x[k];
      surface[8].vertex[1][k] = point[10].x[k];
      surface[8].vertex[2][k] = point[11].x[k];
      surface[8].vector[k] = (surface[8].vertex[0][k] + surface[8].vertex[1][k] + surface[8].vertex[2][k]) / 3.0;
      
      surface[9].vertex[0][k] = point[3].x[k];
      surface[9].vertex[1][k] = point[5].x[k];
      surface[9].vertex[2][k] = point[10].x[k];
      surface[9].vector[k] = (surface[9].vertex[0][k] + surface[9].vertex[1][k] + surface[9].vertex[2][k]) / 3.0;
      
      surface[10].vertex[0][k] = point[2].x[k];
      surface[10].vertex[1][k] = point[3].x[k];
      surface[10].vertex[2][k] = point[5].x[k];
      surface[10].vector[k] = (surface[10].vertex[0][k] + surface[10].vertex[1][k] + surface[10].vertex[2][k]) / 3.0;
      
      surface[11].vertex[0][k] = point[2].x[k];
      surface[11].vertex[1][k] = point[5].x[k];
      surface[11].vertex[2][k] = point[8].x[k];
      surface[11].vector[k] = (surface[11].vertex[0][k] + surface[11].vertex[1][k] + surface[11].vertex[2][k]) / 3.0;
      
      surface[12].vertex[0][k] = point[2].x[k];
      surface[12].vertex[1][k] = point[8].x[k];
      surface[12].vertex[2][k] = point[9].x[k];
      surface[12].vector[k] = (surface[12].vertex[0][k] + surface[12].vertex[1][k] + surface[12].vertex[2][k]) / 3.0;
      
      surface[13].vertex[0][k] = point[0].x[k];
      surface[13].vertex[1][k] = point[8].x[k];
      surface[13].vertex[2][k] = point[9].x[k];
      surface[13].vector[k] = (surface[13].vertex[0][k] + surface[13].vertex[1][k] + surface[13].vertex[2][k]) / 3.0;
      
      surface[14].vertex[0][k] = point[0].x[k];
      surface[14].vertex[1][k] = point[6].x[k];
      surface[14].vertex[2][k] = point[9].x[k];
      surface[14].vector[k] = (surface[14].vertex[0][k] + surface[14].vertex[1][k] + surface[14].vertex[2][k]) / 3.0;
      
      surface[15].vertex[0][k] = point[6].x[k];
      surface[15].vertex[1][k] = point[7].x[k];
      surface[15].vertex[2][k] = point[11].x[k];
      surface[15].vector[k] = (surface[15].vertex[0][k] + surface[15].vertex[1][k] + surface[15].vertex[2][k]) / 3.0;
      
      surface[16].vertex[0][k] = point[3].x[k];
      surface[16].vertex[1][k] = point[7].x[k];
      surface[16].vertex[2][k] = point[11].x[k];
      surface[16].vector[k] = (surface[16].vertex[0][k] + surface[16].vertex[1][k] + surface[16].vertex[2][k]) / 3.0;
      
      surface[17].vertex[0][k] = point[2].x[k];
      surface[17].vertex[1][k] = point[3].x[k];
      surface[17].vertex[2][k] = point[7].x[k];
      surface[17].vector[k] = (surface[17].vertex[0][k] + surface[17].vertex[1][k] + surface[17].vertex[2][k]) / 3.0;
      
      surface[18].vertex[0][k] = point[2].x[k];
      surface[18].vertex[1][k] = point[7].x[k];
      surface[18].vertex[2][k] = point[9].x[k];
      surface[18].vector[k] = (surface[18].vertex[0][k] + surface[18].vertex[1][k] + surface[18].vertex[2][k]) / 3.0;
      
      surface[19].vertex[0][k] = point[6].x[k];
      surface[19].vertex[1][k] = point[7].x[k];
      surface[19].vertex[2][k] = point[9].x[k];
      surface[19].vector[k] = (surface[19].vertex[0][k] + surface[19].vertex[1][k] + surface[19].vertex[2][k]) / 3.0;
    }
    
  surface[0].neighbour[0] = 1;
  surface[0].neighbour[1] = 4;
  surface[0].neighbour[2] = 5;
  
  surface[1].neighbour[0] = 0;
  surface[1].neighbour[1] = 2;
  surface[1].neighbour[2] = 7;
  
  surface[2].neighbour[0] = 1;
  surface[2].neighbour[1] = 3;
  surface[2].neighbour[2] = 9;
  
  surface[3].neighbour[0] = 2;
  surface[3].neighbour[1] = 4;
  surface[3].neighbour[2] = 11;
  
  surface[4].neighbour[0] = 0;
  surface[4].neighbour[1] = 3;
  surface[4].neighbour[2] = 13;
  
  surface[5].neighbour[0] = 0;
  surface[5].neighbour[1] = 14;
  surface[5].neighbour[2] = 6;
  
  surface[6].neighbour[0] = 5;
  surface[6].neighbour[1] = 7;
  surface[6].neighbour[2] = 15;
  
  surface[7].neighbour[0] = 1;
  surface[7].neighbour[1] = 6;
  surface[7].neighbour[2] = 8;
  
  surface[8].neighbour[0] = 7;
  surface[8].neighbour[1] = 9;
  surface[8].neighbour[2] = 16;
  
  surface[9].neighbour[0] = 2;
  surface[9].neighbour[1] = 8;
  surface[9].neighbour[2] = 10;
  
  surface[10].neighbour[0] = 9;
  surface[10].neighbour[1] = 11;
  surface[10].neighbour[2] = 17;
  
  surface[11].neighbour[0] = 3;
  surface[11].neighbour[1] = 10;
  surface[11].neighbour[2] = 12;
  
  surface[12].neighbour[0] = 11;
  surface[12].neighbour[1] = 13;
  surface[12].neighbour[2] = 18;
  
  surface[13].neighbour[0] = 4;
  surface[13].neighbour[1] = 12;
  surface[13].neighbour[2] = 14;
  
  surface[14].neighbour[0] = 5;
  surface[14].neighbour[1] = 13;
  surface[14].neighbour[2] = 19;
  
  surface[15].neighbour[0] = 6;
  surface[15].neighbour[1] = 16;
  surface[15].neighbour[2] = 19;
  
  surface[16].neighbour[0] = 8;
  surface[16].neighbour[1] = 15;
  surface[16].neighbour[2] = 17;
  
  surface[17].neighbour[0] = 10;
  surface[17].neighbour[1] = 16;
  surface[17].neighbour[2] = 18;
  
  surface[18].neighbour[0] = 12;
  surface[18].neighbour[1] = 17;
  surface[18].neighbour[2] = 19;
  
  surface[19].neighbour[0] = 14;
  surface[19].neighbour[1] = 15;
  surface[19].neighbour[2] = 18;
}
#endif

#endif
#endif
