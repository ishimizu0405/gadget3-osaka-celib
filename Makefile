######################################################################
#  Look at end of file for a brief guide to the compile-time options. #
#######################################################################

#--------------------------------------- Basic operation mode of code
OPT	+=  -DPERIODIC
OPT	+=  -DCOOLING
#OPT	+=  -DSFR
OPT	+=  -DUNEQUALSOFTENINGS

OPT	+= -DDENSITY_INDEPENDENT_SPH     # Hopkins '12 DISPH, requires: WAKEUP, QUINTIC_KERNEL, TIME_DEP_ART_VISC
#OPT	+= -DARTIFICIAL_CONDUCTIVITY     # Enables mixing entropy (Price-Monaghan conductivity with Cullen-Dehnen switches)

#--------------------------------------- TreePM Options
OPT	+=  -DPMGRID=256
OPT	+=  -DGRIDBOOST=2
OPT	+=  -DASMTH=1.25
OPT	+=  -DRCUT=4.5
OPT	+=  -DPLACEHIGHRESREGION=19
OPT	+=  -DENLARGEREGION=1.2


#--------------------------------------- Multi-Domain and Top-Level Tree options
#OPT	+=  -DMULTIPLEDOMAINS=8
#OPT	+=  -DTOPNODEFACTOR=4.0


#--------------------------------------- Things that are always recommended
OPT	+=  -DPEANOHILBERT
OPT	+=  -DWALLCLOCK
OPT	+=  -DMYSORT
OPT	+=  -DPEDANTIC_MEMORY_HANDLER   # this enables an internal memory handler (most recently allocated block needs to be freed first)
#OPT	+=  -DCPUSPEEDADJUSTMENT
#OPT    +=  -DAUTO_SWAP_ENDIAN_READIC        # Enables automatic ENDIAN swapping for reading ICs

#--------------------------------------- Timestep Limiter (set to ~4)
OPT	+=  -DWAKEUP=4.1

#---------------------------------------- Single/Double Precision
OPT	+=  -DDOUBLEPRECISION
OPT	+=  -DDOUBLEPRECISION_FFTW
#OPT	+=  -DOUTPUT_IN_DOUBLEPRECISION # snapshot files will be written in double precision
#OPT	+=  -DINPUT_IN_DOUBLEPRECISION  # initial conditions are in double precision

#OPT	+=  -DFLTROUNDOFFREDUCTION      # enables (expensive!) `double-double' round-off reduction in particle sums
#OPT	+=  -DSOFTDOUBLEDOUBLE          # needs to be set if a C++ software implementation of 128bit double-double precision should be used


#--------------------------------------- Alternative SPH Smoothing kernels (default is cubic spline)
#OPT	+=  -DWENDLAND_C4_KERNEL         # Dehnen & Aly 2012, DesNumNgb=200
OPT	+=  -DQUINTIC_KERNEL             # Implementation of the Morris 1996 quintic spline kernel, requires ~1.74x more neighbours !

#---------------------------------------- On the fly FOF groupfinder 
#OPT	+=  -DFOF                                # enable FoF output
#OPT	+=  -DFOF_PRIMARY_LINK_TYPES=2           # 2^type for the primary dark matter type
#OPT	+=  -DFOF_SECONDARY_LINK_TYPES=(1+16+32) # 2^type for the types linked to nearest primaries
#OPT	+=  -DSUBFIND                            # enables substructure finder
#OPT	+=  -DSO_VEL_DISPERSIONS                 # computes velocity dispersions for as part of FOF SO-properties
#OPT	+=  -DORDER_SNAPSHOTS_BY_ID

#--------------------------------------- SFR/feedback model
#OPT     +=  -DSOFTEREQS
#OPT     +=  -DMOREPARAMS
#OPT     +=  -DMETALS
#OPT     +=  -DMETALSMOOTHING            # IS add : calculation metal smoothing
#OPT     +=  -DSTELLARAGE
#OPT     +=  -DWINDS
#OPT     +=  -DOUT_WINDS_INFO            #snapshot contains winds information
#OPT     +=  -DQUICK_LYALPHA
#OPT     +=  -DISOTROPICWINDS
#OPT     +=  -DMHM

#OPT    +=  -DRESCALEPHYSDENSTHRESH     # Rescaling the SFR Density threshold
#OPT    +=  -DSCHAYE_PRESSURE_MULTI     # tsfr using pressure with multiphase (Schaye & Dalla Vecchia 2008)
#OPT    +=  -DBLITZ_PRESSURE            # tsfr using Pressure based SFR  (Blitz & Rosolowsky 2006)
#OPT	+=  -DH2REGSF			# KMT fH2 model - metallicity dependent SF threshold

OPT	+=  -DJEANSPRESSUREFLOOR

#-------------------------------------- AGN stuff
#OPT	+=  -DBLACK_HOLES             # enables Black-Holes (master switch)
#OPT	+=  -DBONDI                   # Bondi-Hoyle style accretion model
#OPT	+=  -DENFORCE_EDDINGTON_LIMIT # put a hard limit on the maximum accretion rate
#OPT	+=  -DBH_THERMALFEEDBACK      # couple a fraction of the BH luminosity into surrounding gas
#OPT	+=  -DBH_DRAG                 # Drag on black-holes due to accretion
#OPT	+=  -DSWALLOWGAS              # Enables stochastic accretion of gas particles consistent with growth rate of hole
#OPT	+=  -DEVALPOTENTIAL           # computes gravitational potential
#OPT	+=  -DREPOSITION_ON_POTMIN    # repositions hole on potential minimum (requires EVALPOTENTIAL)


#-------------------------------------- AGN-Bubble feedback
#OPT	+=  -DBUBBLES                 # generation of hot bubbles in an isolated halo or the the biggest halo in the run
#OPT	+=  -DMULTI_BUBBLES 	      # hot bubbles in all haloes above certain mass threshold (works only without FOF and BUBBLES)
#OPT	+=  -DEBUB_PROPTO_BHAR        # Energy content of the bubbles with cosmic time evolves as an integrated BHAR(z) over a Salpeter time (Di Matteo 2003 eq. [11])


#-------------------------------------- Viscous gas treatment 
#OPT	+=  -DNAVIERSTOKES            # Braginskii-Spitzer parametrization of the shear viscosity: mu = f x T^{5/2}
#OPT	+=  -DNAVIERSTOKES_CONSTANT   # Shear viscosity set constant for all gas particles
#OPT	+=  -DNAVIERSTOKES_BULK       # Bulk viscosity set constant for all gas particles. To run with bulk visocity only one has to set shear viscosity to zero in the parameterfile.
#OPT	+=  -DVISCOSITY_SATURATION    # Both shear and bulk viscosities are saturated, so that unphysical accelerations and entropy increases are avoided. Relevant for the cosmological simulations.
#OPT	+=  -DNS_TIMESTEP             # Enables timestep criterion based on entropy increase due to internal friction forces
#OPT	+=  -DOUTPUTSTRESS            # Outputs diagonal and offdiagonal components of viscous shear stress tensor
#OPT	+=  -DOUTPUTBULKSTRESS        # Outputs viscous bulk stress tensor
#OPT	+=  -DOUTPUTSHEARCOEFF        # Outputs variable shear viscosity coefficient in internal code units


#-------------------------------------------- Things for special behaviour
#OPT	+=  -DPEDANTIC_MEMORY_CEILING=400   # this should be set to define the memory ceiling in MByte
#OPT	+=  -DDOMAIN_MEMORY_CEILING=400
#OPT    +=  -DNO_ISEND_IRECV_IN_DOMAIN
#OPT	+=  -DFIX_PATHSCALE_MPI_STATUS_IGNORE_BUG
#OPT	+=  -DNOGRAVITY
#OPT	+=  -DNOACCEL
#OPT	+=  -DNOISMPRESSURE
#OPT	+=  -DNOVISCOSITYLIMITER
#OPT	+=  -DNOTREERND
OPT	+=  -DNOSTOP_WHEN_BELOW_MINTIMESTEP
#OPT	+=  -DNOPMSTEPADJUSTMENT
#OPT	+=  -DNOTYPEPREFIX_FFTW
#OPT	+=  -DNO_TREEDATA_IN_RESTART
#OPT	+=  -DNOWINDTIMESTEPPING            # Disable wind reducing timestep (not recommended)
#OPT	+=  -DISOTHERM=200                  # adds potential of an isothermal sphere
#OPT	+=  -DCOMPUTE_POTENTIAL_ENERGY
#OPT	+=  -DALLOWEXTRAPARAMS
#OPT	+=  -DLONGIDS
#OPT	+=  -DINHOMOG_GASDISTR_HINT         # if the gas is distributed very different from collisionless particles, this can helps to avoid problems in the domain decomposition
#OPT	+=  -DLONG_X=32.
#OPT	+=  -DLONG_Y=32.
#OPT	+=  -DLONG_Z=1.
#OPT	+=  -DTWODIMS
#OPT	+=  -DSPH_BND_PARTICLES
#OPT	+=  -DNEW_RATES                     # switches in updated cooling rates from Naoki
#OPT	+=  -DRADIATIVE_RATES               # used in non-equilibrium chemistry model
#OPT	+=  -DREAD_HSML                     # reads hsml from IC file
#OPT   	+=  -DADAPTIVE_GRAVSOFT_FORGAS      # allows variable softening length for gas particles (requires UNEQUALSOFTENINGLENGTH)
#OPT	+=  -DADAPTIVE_GRAVSOFT_FORGAS_HSML # this sets the gravitational softening for SPH particles equal to the SPH smoothing (requires ADAPTIVE_GRAVSOFT_FORGAS)
#OPT	+=  -DGENERATE_GAS_IN_ICS
#OPT    +=  -DNEUTRINOS                     # Option for special integration of light neutrino species 
#OPT   +=  -DSTART_WITH_EXTRA_NGBDEV        # Uses special MaxNumNgbDeviation for starting
#OPT   +=   -DHALO_MODEL         	    # Static host halo potential by JHC

#--------------------------------------- Time integration options
#OPT	+=  -DALTERNATIVE_VISCOUS_TIMESTEP


#--------------------------------------- Output/Input options
#OPT	+=  -DOUTPUTPOTENTIAL
#OPT	+=  -DRECOMPUTE_POTENTIAL_ON_OUTPUT # update potential every output even it EVALPOTENTIAL is set
#OPT	+=  -DOUTPUTACCELERATION
#OPT	+=  -DOUTPUTCHANGEOFENTROPY
#OPT	+=  -DOUTPUTTIMESTEP
#OPT	+=  -DOUTPUTCOOLRATE                # outputs cooling rate, and conduction rate if enabled
OPT	+=  -DHAVE_HDF5                     # needed when HDF5 I/O support is desired
OPT     +=  -DH5_USE_16_API		    # REQUIRED FOR HDF5 COMPATIBILITY
#OPT	+=  -DOUTPUTBSMOOTH
#OPT	+=  -DOUTPUTDENSNORM
#OPT	+=  -DXXLINFO                       # Enables additional output for viscosityand bfield
#OPT	+=  -DOUTPUTLINEOFSIGHT             # enables on-the-fly output of Ly-alpha absorption spectra
#OPT	+=  -DOUTPUTLINEOFSIGHT_SPECTRUM
#OPT	+=  -DOUTPUTLINEOFSIGHT_PARTICLES
#OPT	+=  -DOUTPUT_TIDALTENSOR            #  outputs tidal tensor (=matrix of second derivatives of grav. potential)


#--------------------------------------- Testing and Debugging options
#OPT	+=  -DFORCETEST=0.1
#OPT	+=  -DDEBUG                     # enables core-dumps and FPU exceptions
#OPT	+=  -DPARTICLE_DEBUG            # auxiliary communication of IDs
#OPT	+=  -DVERBOSE


#--------------------------------------- Static NFW Potential
#OPT	+=  -DSTATICNFW
#OPT	+=  -DNFW_C=12
#OPT	+=  -DNFW_M200=100.0
#OPT	+=  -DNFW_Eps=0.01
#OPT	+=  -DNFW_DARKFRACTION=0.87


#--------------------------------------- Static Hernquist Potential
#OPT	+=  -DSTATICHQ
#OPT	+=  -DHQ_M200=1.0
#OPT	+=  -DHQ_C=10
#OPT	+=  -DHQ_DARKFRACTION=0.9


#--------------------------------------- Thermal conduction
#OPT	+=  -DCONDUCTION
#OPT	+=  -DCONDUCTION_CONSTANT
#OPT	+=  -DCONDUCTION_SATURATION


#--------------------------------------- Dark energy
#OPT	+=  -DDARKENERGY # Enables Dark Energy
#OPT	+=  -DTIMEDEPDE  # read w(z) from a DE file
#OPT	+=  -DRESCALEVINI # rescale v_ini in read_ic / read_ic_cluster
#OPT    +=  -DEXTERNALHUBBLE # reads the hubble function from the DE file
#OPT    +=  -DTIMEDEPGRAV # resacles H and G according to DE model
#OPT    +=  -DDARKENERGY_DEBUG # enable writing of drift/kick table


#--------------------------------------- Long-range scalar field
#OPT	+=  -DSCALARFIELD


#--------------------------------------- SPH viscosity options
#OPT	+=  -DCONVENTIONAL_VISCOSITY     # enables the old viscosity
OPT	+=  -DTIME_DEP_ART_VISC          # Enables time dependend viscosity
#OPT	+=  -DNO_SHEAR_VISCOSITY_LIMITER # Turns of the shear viscosity supression
#OPT	+=  -DHIGH_ART_VISC_START        # Start with high rather than low viscosity
#OPT	+=  -DALTVISCOSITY               # enables alternative viscosity based on div(v)


#--------------------------------------- Magnetic Field options
#OPT   +=  -DMAGNETIC                   # Turns on B Field (including induction eqn) 
#OPT   +=  -DMAGFORCE                   # Turns on B force
#OPT   +=  -DMAGNETIC_SIGNALVEL         # Extend definition of signal velocity 
                                        # by the magneto sonic waves
#OPT   +=  -DALFVEN_VEL_LIMITER=10      # Limits the contribution of the Alfven waves
                                        # to the singal velocity 

#....................................... Induction equation stuff
#OPT   +=  -DDIVBINDUCTION              # Turns on divB term in induction eq.
#OPT   +=  -DCORRECTDB                  # Turns on dW/dh correction in induction eq.

#....................................... Force equation stuff
#OPT   +=  -DMU0_UNITY                  # Sets \mu_0 to unity
#OPT   +=  -DARTBPRES                   # Anti clumping term ala Morris
#OPT   +=  -DDIVBFORCE                  # Subtract div(B) force from M-tensor
#OPT   +=  -DCORRECTBFRC                # Turns on dW/dh correction for B force 

#....................................... Smoothing Options
#OPT   +=  -DBSMOOTH                    # Turns on B field smoothing

#....................................... Artificial magnetic dissipation options
#OPT   +=  -DMAGNETIC_DISSIPATION       # Turns on artificial magnetic dissipation 
#OPT   +=  -DMAGDISSIPATION_PERPEN      # Uses only the perpendicular magnetic 
                                        # field component for dissipation 
#OPT   +=  -DTIME_DEP_MAGN_DISP         # Enables time dependent coefficients
#OPT   +=  -DHIGH_MAGN_DISP_START       # Starts from high coefficient
#OPT   +=  -DROT_IN_MAG_DIS             # Adds the RotB term in dissipation 

#....................................... DivB cleaning options
#OPT   +=  -DDIVBCLEANING_DEDNER        # Turns on hyp/par cleaning (Dedner 2002)
#OPT   +=  -DSMOOTH_PHI                 # Turns on smoothing of Phi

#....................................... magnetic diffusion options
#OPT   +=  -DMAGNETIC_DIFFUSION         # Turns on magnetic diffusion
#OPT   +=  -DSMOOTH_ROTB                # Turns on smoothing of rot(B)

#....................................... Bebugging stuff
#OPT   +=  -DTRACEDIVB                  # Writes div(B) into snapshot
#OPT   +=  -DDBOUTPUT                   # Writes dB/dt into snapshot
#OPT   +=  -DOUTPUT_ROTB                # Writes rotB into snapshot
#OPT   +=  -DOUTPUT_SROTB               # Writes smoothed rotB into snapshot

#....................................... Initil condition stuff
#OPT   +=  -DBINISET                    # Allows to set Bx,By,Bz in parameter file
#OPT   +=  -DBFROMROTA                  # Allows to five vector potential instead of
                                        # B within the IC file
#OPT   +=  -DIGNORE_PERIODIC_IN_ROTA    # Don't use the periodic mapping when 
                                        # calculating rot(A)
                                        # Note A might not be periodic even if B is. 
#OPT   +=  -DBRIOWU                     # Extrapolate A outside simulation in case 
                                        # of Brio & Wu test problem


#--------------------------------------- Glass making
#OPT	+=  -DMAKEGLASS


#--------------------------------------- Raditive Transfer
#OPT	+=  -DRADTRANSFER


#--------------------------------------- Distortion Tensor
#OPT	+=  -DDISTORTIONTENSOR


#---------------------------------------- nonequilibrium proimodal chemisitry
#OPT	+=  -DNONEQUILIBRIUM
#OPT	+=  -DCHEMISTRY
#OPT	+=  -DCMB
#OPT	+=  -DRADIATION


#---------------------------------------- Cosmic Rays (Martin)
#OPT	+=  -DCOSMIC_RAYS               # Cosmic Rays Master Switch
#OPT	+=  -DCR_IC                     # IC files contain CR information
#OPT	+=  -DCR_IC_PHYSICAL
#OPT	+=  -DCR_DISSIPATION            # Catastrophic losses
#OPT	+=  -DCR_THERMALIZATION         # Coulomb cooling
#OPT	+=  -DCR_SHOCK=2                # Shock energy is directed into CR
			                # 2 = Mach-Number dependent shocks, Mach-number derived for thermal gas/CR composite
			                # 3 = Mach-Number dependent shocks, Mach-number derived for thermal gas
#OPT	+=  -DCR_DIFFUSION              # Cosmic Ray diffusion
#OPT	+=  -DCR_DIFFUSION_GREEN        # alternative diffusion model
#OPT	+=  -DUPDATE_PARANOIA=1         # 1 = Update on every predict, 2 = Update on every energy injection and on every predict
#OPT	+=  -DCR_OUTPUT_INJECTION       # Output energy injection rate in snapshots
#OPT	+=  -DCR_NO_CHANGE              # Compute changes to CR, but do not execute them, useful for estimating the size of effects
#OPT	+=  -DCOSMIC_RAY_TEST           # starts a test routine instead of the simulation
#OPT	+=  -DCR_NOPRESSURE             # computes CRs as usual, but ignores the pressure in the dynamics


#---------------------------------------- Mach number finder (Christoph)
#OPT	+=  -DMACHNUM                   # Mach number Master Switch
#OPT	+=  -DMACHSTATISTIC             # Dissipated thermal energy at shocks
#OPT	+=  -DCR_OUTPUT_JUMP_CONDITIONS # CR: density and thermal energy jump at shocks


#---------------------------------------- JHC's Metal cooling routine using SD93. All need to enable on METALS flag
# mc : turn on METAL_COOLING
# mv : turn on METAL_COOLING and ADJUSTPHYSDENSTHRESH (It only affects on the SFR)
#----------------------------------------

#OPT     +=  -DMETAL_COOLING             # Metal cooling routine using SD93.
#OPT     +=  -DPRIMODIAL_COOLING         # In case a simulation uses only SD93 primodial cooling. Need to enable on METAL_COOLING flag
                                         # Recommand use with disable ADJUSTPHYSDENSTHRESH flag.
#OPT    +=  -DADJUSTPHYSDENSTHRESH       # Adjust PhysDensThresh

#OPT    +=  -DDT_COOL                    # Add cooling time step criterion in timstep.c

#---------------------------------------- JHC's varibale wind model (need turn on WINDS and GROUPFIND)
#OPT     +=  -DGROUPFINDER
#OPT	 +=  -DGROUPFINDER_EASY
#OPT     +=  -DVWINDS     
#OPT	 +=  -DOUT_WINDS_INFO

#--------------------------------------- JHC's miscellary options
#OPT    +=  -DNOUV_J                   # Turn-off ionization back ground aka UV heating
#OPT    +=  -DOPTICALLYTHICK_UV        # Turn-off ionization back ground aka if the particle density is higher than threshold density

# -- Select ONE of the following, or none.  Selecting none defaults to TREECOOL_fg_dec11 file (215 lines reionization @ z=10.75)
#OPT	+= -DTREECOOL_FG_WMAP7		# FG June 2011 WMAP7 update (215 lines, reionization at z=10.75)
#OPT	+= -DTREECOOL_FG_JUNE11		# FG June 2011 data (236 lines, reionization @ z=13.96)
#OPT	+= -DTREECOOL_FG_09		# FG      2009 (209 lines, reionization @ z=9.96)
#OPT	+= -DTREECOOL_SPRINGEL		# Use the original TREECOOL file from Springel (171 lines, reionization @ z=6.08)

#--------------------------------------- Miscellaneous
#OPT    +=  -DUSE_ISEND_IRECV           # For a certain MPI works with MPI_Isend and MPI_Irecv instead of MPI_Sendrecv in some functions
					# Such as TACC machines (OBSOLETE?)

#--------------------------------------- KN: added "Relative Pressure SPH" by Abel (2010) arXiv:1003.0937
#OPT     +=  -DRPSPH

#----AGORA SF & Metal Floor by Bobby
#OPT	+=  -DAGORA_SF=0.01
#OPT	+=  -DAGORA_METAL_FLOOR=1.0	# assume gas to be at this*solarZ

#---------------------------------------- Osaka star formation, feedback and yields model
OPT     += -DOSAKA
OPT     += -DOSAKA_COOLING                      # using grackle : default version 3
#OPT     += -DOSAKA_GRACKLE_VERSION2             # using grackle version 2
OPT     += -DOSAKA_SFR
OPT     += -DOSAKA_ESFB
#OPT     += -DOSAKA_ESFB_HII_BUBBLE              # ESFB with HII bubble heating
OPT     += -DOSAKA_SNII
OPT     += -DOSAKA_SNIA
#OPT     += -DOSAKA_SNIA_THERMAL_FEEDBACK_ONLY
#OPT     += -DOSAKA_SNIA_NO_COOLING_STOP         # if on, cooling stop flag always off for SNIa feedback
OPT     += -DCHEVALIER1974                      # using Schevalier 1974 shock evolution model
OPT     += -DOSAKA_KICK_ENERGY_CONSERVATION     # if off, kick velocity is ST solution for Osaka model (S19).
#OPT     += -DOSAKA_ISOTROPIC_WIND
#OPT     += -DOSAKA_RADIAL_DIRECTION_WIND
#OPT     += -DOSAKA_STRONG_THERMAL_FEEDBACK      # based on Dalla Vecchia et al. 2012
#OPT     += -DOSAKA_CONSTANT_WIND                # based on Dalla Vecchia et al. 2008 (similar to SH03)
OPT     += -DOSAKA_MECHANICAL_FB                # based on Hopkins et al. 2014, Kimm & Cen 2014
#OPT     += -DOSAKA_USE_HSML_FOR_SN_FEEDBACK     # feedback affected region should be within hsml rather than rshock
#OPT     += -DDENSITY_WEIGHTED_VALUE             # use density weighted value rather than kernel weight
#OPT     += -DOSAKA_HYDRO_INTERACTION_OFF        # if on, hydro-interaction off during certain time
#OPT     += -DOSAKA_NO_COOLING_OFF               # if on, always cooling on
#OPT     += -DOSAKA_SN_ENERGY_LOWER_LIMIT        # at least, SN energy per 1 SN of a star particle is more than 1.0e51 erg/s
OPT     += -DOSAKA_AGB
OPT     += -DOSAKA_AGB_OUTFLOW                  # if on, all SPH particles affected by AGB FB have arbitrary outflow velocity
OPT     += -DOSAKA_CELIB
#OPT     += -DOSAKA_CELIB_POPIII                 # considering POP III star formation
#OPT     += -DOSAKA_METAL_ELEMENT_TRACE_SNII     # tracing metal element of SNII
#OPT     += -DOSAKA_METAL_ELEMENT_TRACE_SNIA     # tracing metal element of SNIa
#OPT     += -DOSAKA_METAL_ELEMENT_TRACE_AGB      # tracing metal element of AGB
OPT     += -DOSAKA_METALSMOOTHING
OPT     += -DOSAKA_METAL_FLOOR                  # if no feedback, I recomend this option on.
OPT     += -DOSAKA_GEODESIC_DOME_WEIGHT         # weighting method with geodesic dome
OPT     += -DOSAKA_MINMAX_NEIGHBOUR_NUMBER     # desire minimum number of neighbour particles for SN FB
#OPT     += -DOSAKA_SET_STAR_MINIMUM_HTML       # set minimum hsml for star particls as well as gas particle
#OPT     += -DOSAKA_OUTPUT_SMOOTHED_VALUE        # output smoothed CELib yield value
#OPT     += -DOSAKA_VELOCITY_COOLING_STOP        # Cooling stop in massive galaxy

#--------------------------------------- Select target Computer

SYSTYPE="CfCA-intel"
#SYSTYPE="CfCA-gnu"
#SYSTYPE="octopus-intel"
#SYSTYPE="octopus-gnu"
#SYSTYPE="xc40-intel"
#SYSTYPE="xc40-gnu"
#SYSTYPE="pluto"
#SYSTYPE="orion"


CC       = mpicc        # sets the C-compiler (default)
OPTIMIZE = -Wall  -g   # optimization and warning flags (default)

MPICHLIB = -lmpich

ifeq (SOFTDOUBLEDOUBLE,$(findstring SOFTDOUBLEDOUBLE,$(OPT)))
CC       =   mpiCC     # default C++ compiler
OPTIMIZE =   -g 
OPT     +=  -DX86FIX   # only needed for 32-bit intel/amd systems
endif

ifeq ($(SYSTYPE),"CfCA-intel")
CC       =  cc
OPTIMIZE = -O3 -Wall -static -g
#OPTIMIZE = -O3 -Wall -static -g
GSL_INCL = -I${GSL_DIR}/include
GSL_LIBS = -L${GSL_DIR}/lib -lgsl
FFTW_INCL = -I${FFTW_DIR}
FFTW_LIBS = -L${FFTW_DIR}
MPICHLIB = 
HDF5INCL = 
HDF5LIB = -lhdf5 -lz
RLIBS = 
ifeq (OSAKA_GRACKLE_VERSION2,$(findstring OSAKA_GRACKLE_VERSION2,$(OPT)))
GRACKLE_INCL = -I/work/shimzuik/local/intel/gracklev2/include
GRACKLE_LIBS = -L/work/shimzuik/local/intel/gracklev2/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
else
GRACKLE_INCL = -I/work/shimzuik/local/intel/gracklev3/include
GRACKLE_LIBS = -L/work/shimzuik/local/intel/gracklev3/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
endif
endif

ifeq ($(SYSTYPE),"CfCA-gnu")
CC       =  cc
OPTIMIZE = -O3 -Wall -static -g -fast
GSL_INCL = -I${GSL_DIR}/include
GSL_LIBS = -L${GSL_DIR}/lib -lgsl
FFTW_INCL = -I${FFTW_DIR}
FFTW_LIBS = -L${FFTW_DIR}
MPICHLIB = 
HDF5INCL = 
HDF5LIB = -static -lhdf5 -lz
RLIBS = 
ifeq (OSAKA_GRACKLE_VERSION2,$(findstring OSAKA_GRACKLE_VERSION2,$(OPT)))
GRACKLE_INCL = -I/work/shimzuik/local/gnu/gracklev2/include
GRACKLE_LIBS = -L/work/shimzuik/local/gnu/gracklev2/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
else
GRACKLE_INCL = -I/work/shimzuik/local/gnu/gracklev3/include
GRACKLE_LIBS = -L/work/shimzuik/local/gnu/gracklev3/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
endif
endif

ifeq ($(SYSTYPE),"octopus-intel")
INSTALL_DIR=$(HOME)/local/intel
CC       =  mpiicc
OPTIMIZE = -O3 -Wall -g -xCORE-AVX2 -mtune=skylake-avx512
GSL_INCL = -I$(INSTALL_DIR)/include
GSL_LIBS = -L$(INSTALL_DIR)/lib -lgsl
FFTW_INCL = -I${INSTALL_DIR}/include
FFTW_LIBS = -L${INSTALL_DIR}/lib
MPICHLIB = 
HDF5INCL = -I$(INSTALL_DIR)/include 
HDF5LIB = -L$(INSTALL_DIR)/lib -lhdf5 -lz
RLIBS = 
ifeq (OSAKA_GRACKLE_VERSION2,$(findstring OSAKA_GRACKLE_VERSION2,$(OPT)))
GRACKLE_INCL = -I$(INSTALL_DIR)/gracklev2/include
GRACKLE_LIBS = -L$(INSTALL_DIR)/gracklev2/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
else
GRACKLE_INCL = -I$(INSTALL_DIR)/gracklev3/include
GRACKLE_LIBS = -L$(INSTALL_DIR)/gracklev3/lib -lgrackle 
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
endif
endif

ifeq ($(SYSTYPE),"octopus-gnu")
INSTALL_DIR=$(HOME)/local/gnu
CC       =  mpicc
OPTIMIZE = -O3 -Wall -g
GSL_INCL = -I$(INSTALL_DIR)/include
GSL_LIBS = -L$(INSTALL_DIR)/lib -lgsl
FFTW_INCL = -I${INSTALL_DIR}/include
FFTW_LIBS = -L${INSTALL_DIR}/lib
MPICHLIB = 
HDF5INCL = -I$(INSTALL_DIR)/include 
HDF5LIB = -L$(INSTALL_DIR)/lib -lhdf5 -lz
RLIBS = 
ifeq (OSAKA_GRACKLE_VERSION2,$(findstring OSAKA_GRACKLE_VERSION2,$(OPT)))
GRACKLE_INCL = -I$(INSTALL_DIR)/gracklev2/include
GRACKLE_LIBS = -L$(INSTALL_DIR)/gracklev2/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
else
GRACKLE_INCL = -I$(INSTALL_DIR)/gracklev3/include
GRACKLE_LIBS = -L$(INSTALL_DIR)/gracklev3/lib -lgrackle 
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
endif
endif

ifeq ($(SYSTYPE),"xc40-intel")
INSTALL_DIR=$(HOME)/local/intel
CC       =  cc
OPTIMIZE = -O3 -Wall -g
GSL_INCL = -I$(INSTALL_DIR)/include
GSL_LIBS = -L$(INSTALL_DIR)/lib -lgsl
FFTW_INCL = -I${INSTALL_DIR}/include
FFTW_LIBS = -L${INSTALL_DIR}/lib
MPICHLIB = 
HDF5INCL = -I$(INSTALL_DIR)/include 
HDF5LIB = -L$(INSTALL_DIR)/lib -lhdf5 -lz -lsz
RLIBS = 
ifeq (OSAKA_GRACKLE_VERSION2,$(findstring OSAKA_GRACKLE_VERSION2,$(OPT)))
GRACKLE_INCL = -I$(INSTALL_DIR)/gracklev2/include
GRACKLE_LIBS = -L$(INSTALL_DIR)/gracklev2/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
else
GRACKLE_INCL = -I$(INSTALL_DIR)/gracklev3/include
GRACKLE_LIBS = -L$(INSTALL_DIR)/gracklev3/lib -lgrackle 
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
endif
endif

ifeq ($(SYSTYPE),"xc40-gnu")
INSTALL_DIR=$(HOME)/local/gnu
CC       =  cc
OPTIMIZE = -O3 -Wall -g
GSL_INCL = -I$(INSTALL_DIR)/include
GSL_LIBS = -L$(INSTALL_DIR)/lib -lgsl
FFTW_INCL = -I${INSTALL_DIR}/include
FFTW_LIBS = -L${INSTALL_DIR}/lib
MPICHLIB = 
HDF5INCL = -I$(INSTALL_DIR)/include 
HDF5LIB = -L$(INSTALL_DIR)/lib -lhdf5 -lz -lsz
RLIBS = 
ifeq (OSAKA_GRACKLE_VERSION2,$(findstring OSAKA_GRACKLE_VERSION2,$(OPT)))
GRACKLE_INCL = -I$(INSTALL_DIR)/gracklev2/include
GRACKLE_LIBS = -L$(INSTALL_DIR)/gracklev2/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
else
GRACKLE_INCL = -I$(INSTALL_DIR)/gracklev3/include
GRACKLE_LIBS = -L$(INSTALL_DIR)/gracklev3/lib -lgrackle 
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
endif
endif

ifeq ($(SYSTYPE),"pluto")
INSTALL_DIR=/home/ishimizu/local
CC           = mpicc
CXX          = mpiCC
OPTIMIZE     =  -O3  #-g -Wall
MPICHLIB     =
GSL_INCL     = -I${INSTALL_DIR}/include
GSL_LIBS     = -L${INSTALL_DIR}/lib 
FFTW_INCL    = -I${INSTALL_DIR}/include
FFTW_LIBS    = -L${INSTALL_DIR}/lib
HDF5INCL     = -I${INSTALL_DIR}/include
HDF5LIB      = -L${INSTALL_DIR}/lib -lhdf5
ifeq (GRACKLE,$(findstring GRACKLE,$(OPT)))
GRACKLE_INCL = -I${INSTALL_DIR}/include
GRACKLE_LIBS = -L${INSTALL_DIR}/lib -lgrackle
endif
endif

ifeq ($(SYSTYPE),"orion")
INSTALL_DIR=/home/ishimizu/local
CC           = mpicc
OPTIMIZE     =  -O3  #-g -Wall
MPICHLIB     =
GSL_INCL     = -I${INSTALL_DIR}/include/gsl
GSL_LIBS     = -L${INSTALL_DIR}/lib 
FFTW_INCL    = -I${INSTALL_DIR}/include
FFTW_LIBS    = -L${INSTALL_DIR}/lib
HDF5INCL     = -I${INSTALL_DIR}/include
HDF5LIB      = -L${INSTALL_DIR}/lib -lhdf5
ifeq (OSAKA_GRACKLE_VERSION2,$(findstring OSAKA_GRACKLE_VERSION2,$(OPT)))
GRACKLE_INCL = -I/work/shimzuik/local/intel/gracklev2/include
GRACKLE_LIBS = -L/work/shimzuik/local/intel/gracklev2/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
else
GRACKLE_INCL = -I${INSTALL_DIR}/gracklev3/include
GRACKLE_LIBS = -L${INSTALL_DIR}/gracklev3/lib -lgrackle
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
endif
endif

################## compile #######################

ifneq (HAVE_HDF5,$(findstring HAVE_HDF5,$(OPT)))
HDF5INCL =
HDF5LIB  =
endif

ifeq (GRACKLE,$(findstring GRACKLE,$(OPT)))
GRACKLE_OPT = -DLINUX -DH5_USE_16_API -fPIC -DLARGE_INTS  -DCONFIG_BFLOAT_8
endif

OPTIONS = $(OPTIMIZE) $(OPT) $(GRACKLE_OPT) -DCOMPILETIMESETTINGS=\""$(OPT)"\"

EXEC	= P-Gadget3_osaka


OBJS   = fof.o \
	 domain.o allvars.o main.o   \
	 kinfb_mhm.o sfr_mhm.o  \
	 run.o predict.o begrun.o endrun.o global.o  \
	 timestep.o init.o restart.o io.o sfr_eff.o \
	 accel.o read_ic.o cooling.o metalcooling.o ngb.o parallel_sort.o  \
	 system.o allocate.o density.o  \
	 gravtree.o hydra.o  driftfac.o darkenergy.o \
	 potential.o  forcetree.o  peano.o gravtree_forcetest.o \
	 pm_periodic.o pm_nonperiodic.o longrange.o mymalloc.o  \
	 groupfinder.o barydensity.o stars.o \
	 osaka_CELib_yield.o osaka_utilities.o osaka_cooling.o osaka_sfr.o osaka_star_density.o \
	 osaka_esfb_feedback.o osaka_snii_feedback.o osaka_snia_feedback.o osaka_agb_feedback.o osaka_dm_velocity_dispersion.o


INCL   = allvars.h proto.h forcetree.h cooling.h metalcooling.h domain.h groupfinder.h \
	 dd.h fof.h grackle_wrapper.h Makefile

CFLAGS = $(OPTIONS) $(GSL_INCL) $(FFTW_INCL) $(HDF5INCL) $(GRACKLE_INCL)

ifeq (NOTYPEPREFIX_FFTW,$(findstring NOTYPEPREFIX_FFTW,$(OPT)))    # fftw installed with type prefix?
  FFTW_LIB = $(FFTW_LIBS) -lrfftw_mpi -lfftw_mpi -lrfftw -lfftw
else
ifeq (DOUBLEPRECISION_FFTW,$(findstring DOUBLEPRECISION_FFTW,$(OPT)))
  FFTW_LIB = $(FFTW_LIBS) -ldrfftw_mpi -ldfftw_mpi -ldrfftw -ldfftw
else
  FFTW_LIB = $(FFTW_LIBS) -lsrfftw_mpi -lsfftw_mpi -lsrfftw -lsfftw
endif
endif


LIBS   = -lm $(HDF5LIB) -g $(MPICHLIB) $(GSL_LIBS) -lgsl -lgslcblas $(FFTW_LIB) $(GRACKLE_LIBS)


$(EXEC): $(OBJS)
	$(CC) $(OPTIONS) $(OBJS) $(LIBS) $(RLIBS) -o $(EXEC)

$(OBJS): $(INCL)

clean:
	rm -f $(OBJS) $(EXEC) *.o*




###############################################################################
#
# at compile-time. From the list below, please activate/deactivate the
# options that apply to your run. If you modify any of these options,
# make sure that you recompile the whole code by typing "make clean;
# make".
#
# Main code options:
#
#     These affect the physical model that is simulated.
#
#     - PERIODIC:   Set this if you want to have periodic boundary conditions.
#     - COOLING:    This enables radiative cooling and heating. It also enables
#                   an external UV background which is read from a file.
#     - SFR:        This enables star formation using an effective multiphase
#                   models. This option requires cooling.
#     - METALS:     This model activates the tracking of enrichment in gas and
#                   stars. Note that metal-line cooling is not included yet.
#     - STELLARAGE: This stores the formation redshift of each star particle.
#     - WINDS:      This activates galactic winds. Requires star formation.
#     - ISOTROPICWINDS: This makes the wind isotropic. If not set the wind is
#                       spawned in an axial way. Requires winds to be activated.
#     - NOGRAVITY:  This switches off gravity. Makes only sense for pure
#                   SPH simulations in non-expanding space.
#
# Options for SPH:
#
#     - NOFIXEDMASSINKERNEL:  If set, the number of SPH particles in the kernel
#                             is kept constant instead of the mass.
#     - NOGRADHSML:           If actived, an equation of motion without grad(h)
#                             terms is used.
#            Note: To have the default "entropy"-formulation of SPH (Springel &
#                  Hernquist), the switches NOFIXEDMASSINKERNEL and NOGRADHSML
#                  should *not* be set.
#     - NOVISCOSITYLIMITER:   If this is set, there is no explicit upper limit
#                             on the viscosity that tries to prevent particle
#                             'reflection' in case of poor timestepping.
#
# Numerical options:
#
#     - PMGRID:     This enables the TreePM method, i.e. the long-range force
#                   is computed with a PM-algoritthm, and the short range force
#                   with the tree. The parameter has to be set to the size of the
#                   mesh that should be used, (e.g. 64, 96, 128, etc). The mesh
#                   dimensions need not necessarily be a power of two.
#                   Note: If the simulation is not in a periodic box, then a FFT
#                   method for vacuum boundaries is employed, using a mesh with
#                   dimension twice that specified by PMGRID.
#     - PLACEHIGHRESREGION: If this option is set (will only work together
#                   with PMGRID), then the long range force is computed in two
#                   stages: One Fourier-grid is used to cover the whole simulation
#                   volume, allowing the computation of the large-scale force.
#                   A second Fourier mesh is placed on the region occupied by
#                   "high-resolution" particles, allowing the computation of an
#                   intermediate scale force. Finally, the force on very small
#                   scales is supplemented by the tree. This procedure can be useful
#                   for "zoom-simulations", where the majority of particles (the
#                   high-res particles) are occupying only a small fraction of the
#                   volume. To activate this option, the parameter needs to be set
#                   to an integer that encodes the particle types that represent the
#                   high-res particles in the form of a bit mask. For example, if
#                   types 0, 1, and 4 form the high-res particles, set the parameter
#                   to PLACEHIGHRESREGION=1+2+16. The spatial region covered by the
#                   high-res grid is determined automatically from the initial
#                   conditions. Note: If a periodic box is used, the high-res zone
#                   may not intersect the box boundaries.
#     - ENLARGEREGION: The spatial region covered by the high-res zone has a fixed
#                   size during the simulation, which initially is set to the
#                   smallest region that encompasses all high-res particles. Normally, the
#                   simulation will be interrupted, if high-res particles leave this
#                   region in the course of the run. However, by setting this parameter
#                   to a value larger than one, the high-res region can be expanded.
#                   For example, setting it to 1.4 will enlarge its side-length by
#                   40% (it remains centered on the high-res particles). Hence, with
#                   such a setting, the high-res region may expand or move by a
#                   limited amount. If in addition SYNCHRONIZATION is activated, then
#                   the code will be able to continue even if high-res particles
#                   leave the initial high-res grid. In this case, the code will
#                   update the size and position of the grid that is placed onto
#                   the high-resolution region automatically. To prevent that this
#                   potentially happens every single PM step, one should nevertheless
#                   assign a value slightly larger than 1 to ENLARGEREGION.
#     - DOUBLEPRECISION: This makes the code store and compute internal
#                        particle data in double precision. Note that output
#                        files are nevertheless written by converting to single
#                        precision.
#     - NOTREERND:       If this is not set, the tree construction will succeed
#                        even when there are a few particles at identical
#                        locations. This is done by `rerouting' particles once
#                        the node-size has fallen below 1.0e-3 of the softening
#                        length. When this option is activated, this will be
#                        surpressed and the tree construction will always fail
#                        if there are particles at extremely close coordinates.
#     - NOSTOP_WHEN_BELOW_MINTIMESTEP: If this is activated, the code will not
#                        terminate when the timestep falls below the value of
#                        MinSizeTimestep specified in the parameterfile. This
#                        is useful for runs where one wants to enforce a
#                        constant timestep for all particles. This can be done
#                        by activating this option, and by setting Min- and
#                        MaxSizeTimestep to an equal value.
#     - PSEUDOSYMMETRIC: When this option is set, the code will try to "anticipate"
#                        timestep changes by extrapolating the change of the
#                        acceleration into the future. This in general improves the
#                        long-term integration behaviour of periodic orbits.
#     - SYNCHRONIZATION: When this is set, particles may only increase their
#                        timestep if the new timestep will put them into
#                        synchronization with the higher time level. This typically
#                        means that only on half of the timesteps of a particle
#                        an increase of the step may occur.
#     - NOPMSTEPADJUSTMENT: When this is set, the long-range timestep for the
#                        PM force computation is always determined by MaxSizeTimeStep.
#                        Otherwise, it is set to the minimum of MaxSizeTimeStep and
#                        the timestep obtained for the maximum long-range force with
#                        an effective softening scale equal to the PM smoothing-scale.
# - LONG_X/Y/Z:
#     These options can be used together with PERIODIC and NOGRAVITY only.
#     When set, the options define numerical factors that can be used to
#     distorts the periodic simulation cube into a parallelepiped of
#     arbitrary aspect ratio. This can be useful for idealized SPH tests.
#
# - TWODIMS:
#     This effectively switches of one dimension in SPH, i.e. the code
#     follows only 2d hydrodynamics in the xy-, yz-, or xz-plane. This
#     only works with NOGRAVITY, and if all coordinates of the third
#     axis are exactly equal. Can be useful for idealized SPH tests.
#
# - SPH_BND_PARTICLES:
#     If this is set, particles with a particle-ID equal to zero do not
#     receive any SPH acceleration. This can be useful for idealized
#     SPH tests, where these particles represent fixed "walls".
#
#
# Architecture options:
#
#     - T3E:       The code assumes that sizeof(int)=4 holds. A few machines
#                  (like Cray T3E) have sizeof(int)=8. In this case, set the
#                  T3E flag.
#     - NOTYPEPREFIX_FFTW: If this is set, the fftw-header/libraries are accessed
#                  without type prefix (adopting whatever was chosen as default at compile
#                  of fftw). Otherwise, the type prefix 'd' for double is used.
#
# Input options:
#
#     - MOREPARAMS:  Activate this to allow a set of additional parameters in
#                    the parameterfile which control the star formation and
#                    feedback sector. This option must be activated when star
#                    formation is switched on.
#
# Output options:
#
#     - OUTPUTPOTENTIAL: This will force the code to compute gravitational
#                        potentials for all particles each time a snapshot file
#                        is generated. This values are then included in the
#                        snapshot file. Note that the computation of the
#                        values of the potential costs additional time.
#     - OUTPUTACCELERATION: This will include the physical acceleration of
#                        each particle in snapshot files.
#     - OUTPUTCHANGEOFENTROPY: This will include the rate of change of entropy
#                        of gas particles in snapshot files.
#     - OUTPUTTIMESTEP:  This will include an output of the timesteps actually
#                        taken by each particle.
#     - OUTPUT_D2POT: This will output the second derivatives of the potential
#                     for each particle in the snapshot file.
#
# Miscellaneous options:
#
#     - PEANOHILBERT:    This is a tuning option. When set, the code will bring
#                        the particles after each domain decomposition into
#                        Peano-Hilbert order. This improves cache utilization
#                        and performance.
#     - WALLCLOCK:       If set, a wallclock timer is used by the code to
#                        measure internal time consumption (see cpu-log file).
#                        Otherwise a timer that measures consumed processor
#                        ticks is used.
#
# Debugging/testing options:
#
#     - FORCETEST:       This can be set to check the force accuracy of the
#                        code. The option needs to be set to a number between
#                        0 and 1 (e.g. 0.01), which is taken to specify a
#                        random fraction of particles for which at each
#                        timestep forces by direct summation are computed. The
#                        normal tree-forces and the "correct" direct summation
#                        forces are collected in a file. Note that the
#                        simulation itself is unaffected by this option, but it
#                        will of course run much(!) slower
#                        if FORCETEST*NumPart*NumPart >> NumPart. Note: Particle
#                        IDs must be set to numbers >=1 for this to work.
#
###############################################################################




# - FLTROUNDOFFREDUCTION     enables round off reduction in particle sums
#		             if DOUBLEPRECISION is set, these sums are done in 'long double'
#                            if single precision is used, they are done in 'double'
#                            This should in principle allow to make computations
#                            *exactly* invariant to different numbers of CPUs.
#
# - SOFTDOUBLEDOUBLE         when this is set, a software implementation of
#                            128bit double-double addition is used, implemented as a c++ class.
#                            Hence, this option requires compilation with a c++ compiler
#


#     - QUICK_LYALPHA:   This only works for cosmological simulations in periodic boxes
#                        with COOLING & SFR. (WINDS, METALS should be deselected).
#                        It will simply convert all gas particles above overdensity
#                        CritPhysOverdensity and with Temperature below 10^5 K to stars.
#                        This should still leave the Ly-Alpha forest largely unaffected,
#                        but should be faster. It is recommended to set GENERATIONS equal
#                        to 1 for maximum speed-up.
#
#     - TIDALTENSOR:     Calculates the tidal tensor for each particle. 
