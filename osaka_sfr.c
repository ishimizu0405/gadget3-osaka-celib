#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "allvars.h"
#include "proto.h"
#include "forcetree.h"

// This routine does cooling and star formation for the effective multi-phase model. //
#ifdef OSAKA
#ifdef COOLING
#ifdef OSAKA_COOLING
#include <grackle.h>

// cooling routine when star formation is enabled //
#ifdef OSAKA_SFR
void cooling_and_starformation(void)
{
  int i, j, bin, flag, stars_spawned, tot_spawned, stars_converted, tot_converted, number_of_stars_generated, flagcooling;
  unsigned int bits;
  double dt, dtime, ascale, hubble_a, ainv, a3inv, ne;
  double time_hubble_a, unew, mass_of_star;
  double sum_sm, total_sm, sm, rate, sum_mass_stars, total_sum_mass_stars;
  double p, prob;
  double t_ff, tsfr, rate_in_msunperyear;
  double sfrrate, totsfrrate, dmax1, dmax2;
  double time_factor, time_next, time;
  double temperature, u_to_temp_fac;
  double HI, HII, HeI, HeII, HeIII;
  double HM, H2I, H2II, DI, DII, HDI;
  double density, metallicity;
  double sigma_th, rho_virial, rho_th, rho_z, fbaryon;
  
#ifdef OSAKA_CELIB
  double snii_explosion_time, snia_explosion_time, agb_explosion_time;
#endif

  if(ThisTask == 0)
    {
      printf("Cooling and star formation\n"); 
      fflush(stdout);
    }
    
  u_to_temp_fac = (4.0 / (8.0 - 5.0 * (1.0 - HYDROGEN_MASSFRAC))) * PROTONMASS / BOLTZMANN * GAMMA_MINUS1 * All.UnitEnergy_in_cgs / All.UnitMass_in_g;
  
  for(bin = 0; bin < TIMEBINS; bin++)
    if(TimeBinActive[bin])
      TimeBinSfr[bin] = 0;
      
  if(All.ComovingIntegrationOn)
    {
      // Factors for comoving integration of hydro //
      ainv = 1.0 / All.Time;
      a3inv = ainv * ainv * ainv;
      hubble_a = hubble_function(All.Time);
      time_hubble_a = All.Time * hubble_a;
      ascale = All.Time;
      time_factor = All.UnitTime_in_s  / All.HubbleParam / SEC_PER_GIGAYEAR;
      time = a2t(All.Time);
    }
  else
    {
      ainv = a3inv = ascale = time_hubble_a = 1.0;
      time_factor = All.UnitTime_in_s / All.HubbleParam / SEC_PER_GIGAYEAR;
      time = All.Time * time_factor;
    }
    
  stars_spawned = stars_converted = 0;
  sum_sm = sum_mass_stars = 0;
  
#ifdef OSAKA_VELOCITY_COOLING_STOP
  fbaryon = All.OmegaBaryon / All.Omega0;
  rho_virial = virial_density(&rho_z, ascale);
  rho_th = rho_virial * (1.0 - fbaryon) / 3.0;
  sigma_th = All.CoolingStopVelocity * pow(ainv, All.CoolingStopAlpha);
#endif
  
  for(bits = 0; All.Nspawn > (1 << bits); bits++);
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 0)
        {
#ifndef WAKEUP
          dt = (P[i].TimeBin ? (1 << P[i].TimeBin) : 0) * All.Timebase_interval;
#else
          dt = P[i].dt_step * All.Timebase_interval;
#endif

          if(All.ComovingIntegrationOn)
            dtime = All.Time * dt / time_hubble_a;
          else
            dtime = dt;
            
          SphP[i].Sfr = 0.0;
          density = SphP[i].d.Density;
          t_ff = sqrt((3.0 * M_PI) / (32.0 * density * a3inv * All.G));
          
          // check whether conditions for star formation are fulfilled. (flag=1:normal cooling, flag=0:star formation)
          flag = 1;
          flagcooling = 0;
          
#ifdef OSAKA_SNII
          if(SphP[i].flagCoolingSNII > 0)
            {
              time_next = time + dtime * time_factor;
              
              if(time_next >= SphP[i].timeSNII)
                {
                  SphP[i].flagCoolingSNII = 0;
                  flagcooling = 0;
                }
              else
                flagcooling = 1;
            }
#endif

#ifdef OSAKA_SNIA
          if(SphP[i].flagCoolingSNIa > 0)
            {
              time_next = time + dtime * time_factor;
              
              if(time_next >= SphP[i].timeSNIa)
                {
                  SphP[i].flagCoolingSNIa = 0;
                  flagcooling = 0;
                }
              else
                flagcooling = 1;
            }
#endif

          ne = SphP[i].Ne; // electron abundance (gives ionization state and mean molecular weight) //
          unew = DMAX(All.MinEgySpec, (SphP[i].Entropy + SphP[i].e.DtEntropy * dt) / GAMMA_MINUS1 * pow(density * a3inv, GAMMA_MINUS1));
          
#ifdef OSAKA_ESFB
          if(SphP[i].ThermalEnergyESFB)
            {
              if(P[i].Mass == 0.0)
                SphP[i].ThermalEnergyESFB = 0.0;
              else
                unew += SphP[i].ThermalEnergyESFB / P[i].Mass;
                
              //temperature = u_to_temp_fac * unew;
              temperature = GetTemperature_grackle(i, unew, ascale);
              
              if(temperature > 2.0e4 && SphP[i].ThermalEnergySNII == 0.0 && SphP[i].ThermalEnergySNIa == 0.0)
                unew = 2.0e4 / u_to_temp_fac;
               
              SphP[i].ThermalEnergyESFB = 0.0;
            }
#endif

#ifdef OSAKA_SNII
          if(SphP[i].ThermalEnergySNII)
            {
              if(P[i].Mass == 0.0)
                SphP[i].ThermalEnergySNII = 0.0;
              else
                unew += SphP[i].ThermalEnergySNII / P[i].Mass;
                
              //temperature = u_to_temp_fac * unew;
              temperature = GetTemperature_grackle(i, unew, ascale);
              
              if(temperature > 5.0e9)
                unew = 5.0e9 / u_to_temp_fac;
                
              SphP[i].ThermalEnergySNII = 0.0;
            }
#endif

#ifdef OSAKA_SNIA
          if(SphP[i].ThermalEnergySNIa)
            {
              if(P[i].Mass == 0.0)
                SphP[i].ThermalEnergySNIa = 0.0;
              else
                unew += SphP[i].ThermalEnergySNIa / P[i].Mass;
                
              //temperature = u_to_temp_fac * unew;
              temperature = GetTemperature_grackle(i, unew, ascale);
              
              if(temperature > 5.0e9)
                unew = 5.0e9 / u_to_temp_fac;
                
              SphP[i].ThermalEnergySNIa = 0.0;
            }
#endif

#ifdef OSAKA_VELOCITY_COOLING_STOP
          if(SphP[i].VelDisp > sigma_th && SphP[i].DMDensity * a3inv > rho_th)
            flagcooling = 1;
#endif

          if(flagcooling == 0)
            unew = DoCooling_grackle(i, unew, dtime, ascale);
            
          if(P[i].TimeBin)
            {
              // upon start-up, we need to protect against dt==0 //
              // note: the adiabatic rate has been already added in ! //
              if(dt > 0.0)
                {
                  SphP[i].e.DtEntropy = (unew * GAMMA_MINUS1 / pow(density * a3inv, GAMMA_MINUS1) - SphP[i].Entropy) / dt;
                  
                  if(SphP[i].e.DtEntropy < - 0.5 * SphP[i].Entropy / dt)
                    SphP[i].e.DtEntropy = - 0.5 * SphP[i].Entropy / dt;
                }
            }
            
          //temperature = u_to_temp_fac * unew;
          temperature = GetTemperature_grackle(i, unew, ascale);
          
          if(All.ComovingIntegrationOn)
            {
              if(density >= All.OverDensThresh && density * a3inv >= All.PhysDensThresh && temperature <= All.TemperatureThresh)
                flag = 0;
              else
                flag = 1; // adiabatic expansion without radiative cooling //
            }
          else
            {
              if(density * a3inv >= All.PhysDensThresh && temperature <= All.TemperatureThresh)
                flag = 0;
              else
                flag = 1; // adiabatic expansion without radiative cooling //
            }
            
#ifdef OSAKA_HYDRO_INTERACTION_OFF
          if(SphP[i].flagCoolingSNII == 1 || SphP[i].flagCoolingSNIa == 1)
            flag = 1;
#endif

          if(flag == 0) // active star formation //
            {
              tsfr = t_ff / All.SFEfficiency;
              
              if(tsfr < dtime)
                tsfr = dtime;
                
              p = dtime / tsfr;
              sm = p * P[i].Mass; // amount of stars expect to form //
              sum_sm += P[i].Mass * (1.0 - exp(- p));
              
              // the upper bits of the gas particle ID store how man stars this gas. particle gas already generated  //
              mass_of_star = P[i].Mass / ((double) (All.Nspawn - SphP[i].nStarSpawn));
              prob = P[i].Mass / mass_of_star * (1 - exp(-p));
              
              SphP[i].Sfr = P[i].Mass / tsfr * (All.UnitMass_in_g / SOLAR_MASS) / (All.UnitTime_in_s / SEC_PER_YEAR);
	      TimeBinSfr[P[i].TimeBin] += SphP[i].Sfr;
	      
              if(get_random_number(P[i].ID + SphP[i].nStarSpawn + 1) < prob)	// ok, make a star //
                {
                  if(SphP[i].nStarSpawn == All.Nspawn - 1)
                    {
                      // here we turn the gas particle itself into a star //
                      Stars_converted++;
                      stars_converted++;
                      sum_mass_stars += P[i].Mass;
                      
                      P[i].InitialStarMass = P[i].Mass;
                      P[i].FormationTime = All.Time;
                      P[i].Hsml = SphP[i].Hsml;
                      P[i].Type = 4;
                      
#ifdef OSAKA_CELIB
                      calc_feedback_duration_time(i, P[i].Metallicity);
#ifdef OSAKA_SNII
                      if(All.SNIIEventNumber == 1)
                        calc_instantaneous_snii_feedback_time(i, P[i].ID, P[i].Metallicity, CELib_yield, CELib_yield_dif, &snii_explosion_time);
                      else
                        snii_explosion_time = P[i].SNIIStartTime * TIME_GYR;
                        
                      P[i].SNIIExplosionTime = snii_explosion_time;
#endif

#ifdef OSAKA_SNIA
                      if(All.SNIaEventNumber == 1)
                        calc_instantaneous_snia_feedback_time(i, P[i].ID, P[i].Metallicity, CELib_yield, CELib_yield_dif, &snia_explosion_time);
                      else
                        snia_explosion_time = P[i].SNIaStartTime * TIME_GYR;
                        
                      P[i].SNIaExplosionTime = snia_explosion_time;
#endif

#ifdef OSAKA_AGB
                      if(All.AGBEventNumber == 1)
                        calc_instantaneous_agb_feedback_time(i, P[i].ID, P[i].Metallicity, CELib_yield, CELib_yield_dif, &agb_explosion_time);
                      else
                        agb_explosion_time = P[i].AGBStartTime * TIME_GYR;
                        
                      P[i].AGBExplosionTime = agb_explosion_time;
#endif
#endif

                      P[i].ESFBNgbNum = 0.0;
                      P[i].SNIINgbNum = 0.0;
                      P[i].SNIaNgbNum = 0.0;
                      P[i].AGBNgbNum = 0.0;
                      P[i].ESFBNgbMass = 0.0;
                      P[i].SNIINgbMass = 0.0;
                      P[i].SNIaNgbMass = 0.0;
                      P[i].AGBNgbMass = 0.0;
                      P[i].FBflagESFB = 0;
                      P[i].FBflagSNII = 0;
                      P[i].FBflagSNIa = 0;
                      P[i].FBflagAGB = 0;
                      P[i].TimeSNII = 0.0;
                      P[i].TimeSNIa = 0.0;

#ifdef OSAKA_ESFB
                      if(All.ESFBEventNumber == 1)
                        P[i].ESFBDepositedTime = P[i].SNIIStartTime * TIME_GYR * get_random_number(P[i].ID + 3);
                      else
                        P[i].ESFBDepositedTime = 0.0;
                        
                      if(All.ESFBEfficiency == 0.0)
                        P[i].FBflagESFB = All.ESFBEventNumber;
#endif

                      TimeBinCountSph[P[i].TimeBin]--;
                      TimeBinSfr[P[i].TimeBin] -= SphP[i].Sfr;
		    }
		  else
		    {
                      // here we spawn a new star particle //
                      if(NumPart + stars_spawned >= All.MaxPart)
                        {
                          printf("On Task=%d with NumPart=%d we try to spawn %d particles. Sorry, no space left...(All.MaxPart = %d)\n", ThisTask, NumPart, stars_spawned, All.MaxPart);
                          fflush(stdout);
                          endrun(8888);
                        }
                        
                      SphP[i].nStarSpawn ++;
                      P[NumPart + stars_spawned] = P[i];
                      P[NumPart + stars_spawned].Hsml = SphP[i].Hsml;
                      P[NumPart + stars_spawned].Type = 4;
                      P[NumPart + stars_spawned].Mass = mass_of_star;
                      P[NumPart + stars_spawned].InitialStarMass = mass_of_star;
                      P[NumPart + stars_spawned].FormationTime = All.Time;
                      P[NumPart + stars_spawned].ID += SphP[i].nStarSpawn * All.MaxID;
                      //P[NumPart + stars_spawned].ID += (1 << (32 - bits));
                      //P[i].ID += (1 << (32 - bits));
                      
#ifdef OSAKA_CELIB
                      calc_feedback_duration_time(NumPart + stars_spawned, P[NumPart + stars_spawned].Metallicity);
                      
                      for(j = 0; j < N_YIELD_ELEMENT_CELIB; j++)
                        {
                          P[NumPart + stars_spawned].Metals[j] = P[i].Metals[j] * mass_of_star / P[i].Mass;
                          P[i].Metals[j] = P[i].Metals[j] * (1.0 - mass_of_star / P[i].Mass);
#ifdef OSAKA_METALSMOOTHING
                          P[NumPart + stars_spawned].MetalsSmoothed[j] = P[i].MetalsSmoothed[j] * mass_of_star / P[i].Mass;
                          P[i].MetalsSmoothed[j] = P[i].MetalsSmoothed[j] * (1.0 - mass_of_star / P[i].Mass);
#endif
                        }
                        
#ifdef OSAKA_SNII
                      if(All.SNIIEventNumber == 1)
                        calc_instantaneous_snii_feedback_time(NumPart + stars_spawned, P[NumPart + stars_spawned].ID, P[NumPart + stars_spawned].Metallicity, CELib_yield, CELib_yield_dif, &snii_explosion_time);
                      else
                        snii_explosion_time = P[NumPart + stars_spawned].SNIIStartTime * TIME_GYR;
                        
                      P[NumPart + stars_spawned].SNIIExplosionTime = snii_explosion_time;
#endif

#ifdef OSAKA_SNIA
                      if(All.SNIaEventNumber == 1)
                        calc_instantaneous_snia_feedback_time(NumPart + stars_spawned, P[NumPart + stars_spawned].ID, P[NumPart + stars_spawned].Metallicity, CELib_yield, CELib_yield_dif, &snia_explosion_time);
                      else
                        snia_explosion_time = P[NumPart + stars_spawned].SNIaStartTime * TIME_GYR;
                        
                      P[NumPart + stars_spawned].SNIaExplosionTime = snia_explosion_time;
#endif

#ifdef OSAKA_AGB
                      if(All.AGBEventNumber == 1)
                        calc_instantaneous_agb_feedback_time(NumPart + stars_spawned, P[NumPart + stars_spawned].ID, P[NumPart + stars_spawned].Metallicity, CELib_yield, CELib_yield_dif, &agb_explosion_time);
                      else
                        agb_explosion_time = P[NumPart + stars_spawned].AGBStartTime * TIME_GYR;
                        
                      P[NumPart + stars_spawned].AGBExplosionTime = agb_explosion_time;
#endif
#endif
                      P[i].Mass -= mass_of_star;
                      
                      P[NumPart + stars_spawned].ESFBNgbNum = 0.0;
                      P[NumPart + stars_spawned].SNIINgbNum = 0.0;
                      P[NumPart + stars_spawned].SNIaNgbNum = 0.0;
                      P[NumPart + stars_spawned].AGBNgbNum = 0.0;
                      P[NumPart + stars_spawned].ESFBNgbMass = 0.0;
                      P[NumPart + stars_spawned].SNIINgbMass = 0.0;
                      P[NumPart + stars_spawned].SNIaNgbMass = 0.0;
                      P[NumPart + stars_spawned].AGBNgbMass = 0.0;
                      P[NumPart + stars_spawned].FBflagESFB = 0;
                      P[NumPart + stars_spawned].FBflagSNII = 0;
                      P[NumPart + stars_spawned].FBflagSNIa = 0;
                      P[NumPart + stars_spawned].FBflagAGB = 0;
                      P[NumPart + stars_spawned].TimeSNII = 0.0;
                      P[NumPart + stars_spawned].TimeSNIa = 0.0;
                      
#ifdef OSAKA_ESFB
                      if(All.ESFBEventNumber == 1)
                        P[NumPart + stars_spawned].ESFBDepositedTime = P[NumPart + stars_spawned].SNIIStartTime * TIME_GYR * get_random_number(P[NumPart + stars_spawned].ID + 4);
                      else
                        P[NumPart + stars_spawned].ESFBDepositedTime = 0.0;
                        
                      if(All.ESFBEfficiency == 0.0)
                        P[NumPart + stars_spawned].FBflagESFB = All.ESFBEventNumber;
#endif
                      
                      NextActiveParticle[NumPart + stars_spawned] = FirstActiveParticle;
                      FirstActiveParticle = NumPart + stars_spawned;
                      NumForceUpdate++;
                      
                      TimeBinCount[P[NumPart + stars_spawned].TimeBin]++;
                      
                      PrevInTimeBin[NumPart + stars_spawned] = i;
                      NextInTimeBin[NumPart + stars_spawned] = NextInTimeBin[i];
                      
                      if(NextInTimeBin[i] >= 0)
                        PrevInTimeBin[NextInTimeBin[i]] = NumPart + stars_spawned;
                        
                      NextInTimeBin[i] = NumPart + stars_spawned;
                      
                      if(LastInTimeBin[P[i].TimeBin] == i)
                        LastInTimeBin[P[i].TimeBin] = NumPart + stars_spawned;
			
                      sum_mass_stars += P[NumPart + stars_spawned].Mass;
                      
                      force_add_star_to_tree(i, NumPart + stars_spawned);
                      
                      stars_spawned++;
                    }
                }
            }
        }
    } // end of main loop over active particles //
    
  MPI_Allreduce(&stars_spawned, &tot_spawned, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&stars_converted, &tot_converted, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(tot_spawned > 0 || tot_converted > 0)
    {
      if(ThisTask == 0)
        {
          printf("SFR: spawned %d stars, converted %d gas particles into stars\n", tot_spawned, tot_converted);
          fflush(stdout);
        }
        
      All.TotNumPart += tot_spawned;
      All.TotN_gas -= tot_converted;
      NumPart += stars_spawned;
      
      // Note: N_gas is only reduced once rearrange_particle_sequence is called //
      // Note: New tree construction can be avoided because of  `force_add_star_to_tree()' //
    }
    
  for(bin = 0, sfrrate = 0; bin < TIMEBINS; bin++)
    if(TimeBinCount[bin])
      sfrrate += TimeBinSfr[bin];
      
  MPI_Allreduce(&sfrrate, &totsfrrate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Reduce(&sum_sm, &total_sm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Allreduce(&sum_mass_stars, &total_sum_mass_stars, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  
  if(ThisTask == 0)
    {
      if(All.TimeStep > 0.0)
        rate = total_sm / (All.TimeStep / time_hubble_a);
      else
        rate = 0.0;
        
      // convert to solar masses per yr //
      rate_in_msunperyear = rate * (All.UnitMass_in_g / SOLAR_MASS) / (All.UnitTime_in_s / SEC_PER_YEAR);
      
      fprintf(FdSfr, "%g %g %g %g %g\n", All.Time, total_sm, totsfrrate, rate_in_msunperyear, total_sum_mass_stars);
      fflush(FdSfr);
    }
}

void rearrange_particle_sequence(void)
{
  int i, j, flag = 0, flag_sum;
  struct particle_data psave;
  
  if(Stars_converted)
    {
      N_gas -= Stars_converted;
      Stars_converted = 0;
      
      for(i = 0; i < N_gas; i++)
        if(P[i].Type != 0)
          {
            for(j = N_gas; j < NumPart; j++)
              if(P[j].Type == 0)
                break;
                
            if(j >= NumPart)
              endrun(181170);
              
            psave = P[i];
            P[i] = P[j];
            SphP[i] = SphP[j];
            P[j] = psave;
          }
      flag = 1;
    }
    
  MPI_Allreduce(&flag, &flag_sum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(flag_sum)
    reconstruct_timebins();
}

void set_units_sfr(void)
{
  All.OverDensThresh = All.CritOverDensity * All.OmegaBaryon * 3.0 * All.Hubble * All.Hubble / (8.0 * M_PI * All.G);
  All.PhysDensThresh = All.CritPhysDensity * PROTONMASS / HYDROGEN_MASSFRAC / All.UnitDensity_in_cgs;
  All.MetallicityFloor = All.MetallicityFloorCoefficient * All.SolarMetallicity;
}

double virial_density(double *rho_z, double ascale)
{
  double H_z, rho_c_z, Om_z, kappa, delta, rho_v, delta_v;
  
  H_z = All.Hubble * sqrt(All.Omega0 / ascale / ascale / ascale + All.OmegaLambda); 
  rho_c_z = 3.0 * H_z * H_z / (8.0 * M_PI * All.G);
  *rho_z = (3.0 * All.Hubble * All.Hubble / (8.0 * M_PI * All.G)) * All.Omega0 / ascale / ascale / ascale;
  Om_z = (*rho_z)/rho_c_z;
  kappa = Om_z - 1.0;
  delta = 18.0 * M_PI * M_PI + 82.0 * kappa - 39.0 * kappa * kappa;
  rho_v = delta * (*rho_z) / Om_z;
  delta_v = rho_v / (*rho_z);
  
  return rho_v;
}

#endif // closes OSAKA_SFR //
#endif // closes OSAKA_COOLING //
#endif // clises COOLING //
#endif // closes OSAKA //
