#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_math.h>

#include "allvars.h"
#include "proto.h"

#ifdef GROUPFINDER
#include "groupfinder.h"

/*! Structure for communication during the density computation. 
 *  Holds data that is sent to other processors.
 */
static struct bdensdata_in
{
  MyDouble Pos[3];
  MyFloat Hsml;
  int NodeList[NODELISTLENGTH];
}
 *BDensDataIn, *BDensDataGet;


static struct bdensdata_out
{
  MyLongDouble Rho;
  MyLongDouble DhsmlDensity;
  MyLongDouble Ngb;
}
 *BDensDataResult, *BDensDataOut;


/*! Does almost the same calulcation as density.c, 
 * but including star particles and even wind particles.
 */
void baryondensity(void)
{
  MyFloat *Left, *Right;
  int i, j, ndone, ndone_flag, npleft, dummy, iter = 0;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  long long ntot;
  double dmax1, dmax2, fac;
  double desnumngb;

#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif

  Left = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  Right = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));

  for(i = 0; i < NumPart; i++)
    {
      if(P[i].GroupIn == 1)
	Left[i] = Right[i] = 0;
      else
	Left[i] = Right[i] = -1;
    }

  /* allocate buffers to arrange communication */


  Ngblist = (int *) mymalloc(NumPart * sizeof(int));

  All.BunchSize =
    (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
					     sizeof(struct bdensdata_in) + sizeof(struct bdensdata_out) +
					     sizemax(sizeof(struct bdensdata_in),
						     sizeof(struct bdensdata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));

  desnumngb = All.DesNumNgb;

  /* we will repeat the whole thing for those particles where we didn't find enough neighbours */
  do
    {
      do
	{
	  for(j = 0; j < NTask; j++)
	    {
	      Send_count[j] = 0;
	      Exportflag[j] = -1;
	    }

	  /* do local particles and prepare export list */
	  for(i = 0, nexport = 0; i < NumPart; i++)
	    {
	      if(P[i].GroupIn == 1 && Left[i] >= 0)
		{
#ifdef GROUPFINDER_EASY
		  baryondensity_evaluate(i, 0, &nexport, Send_count);
#else
		  if(baryondensity_evaluate(i, 0, &nexport, Send_count) < 0)
		    break;
#endif
		}
	    }

#ifdef MYSORT
	  mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
	  qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif

	  MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);

	  for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
	    {
	      Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
	      nimport += Recv_count[j];

	      if(j > 0)
		{
		  Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
		  Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
		}
	    }

	  BDensDataGet = (struct bdensdata_in *) mymalloc(nimport * sizeof(struct bdensdata_in));
	  BDensDataIn = (struct bdensdata_in *) mymalloc(nexport * sizeof(struct bdensdata_in));

	  /* prepare particle data for export */
	  for(j = 0; j < nexport; j++)
	    {
	      place = DataIndexTable[j].Index;

	      BDensDataIn[j].Pos[0] = P[place].Pos[0];
	      BDensDataIn[j].Pos[1] = P[place].Pos[1];
	      BDensDataIn[j].Pos[2] = P[place].Pos[2];
	      BDensDataIn[j].Hsml = P[place].gHsml;

	      memcpy(BDensDataIn[j].NodeList,
		     DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
	    }

	  /* exchange particle data */
	  for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
	    {
	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
		{
		  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
		    {
		      /* get the particles */
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&BDensDataIn[Send_offset[recvTask]],
				Send_count[recvTask] * sizeof(struct bdensdata_in), MPI_BYTE,
				recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&BDensDataGet[Recv_offset[recvTask]],
				Recv_count[recvTask] * sizeof(struct bdensdata_in), MPI_BYTE,
				recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
		      MPI_Sendrecv(&BDensDataIn[Send_offset[recvTask]],
				   Send_count[recvTask] * sizeof(struct bdensdata_in), MPI_BYTE,
				   recvTask, TAG_DENS_A,
				   &BDensDataGet[Recv_offset[recvTask]],
				   Recv_count[recvTask] * sizeof(struct bdensdata_in), MPI_BYTE,
				   recvTask, TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
		    }
		}
	    }

	  myfree(BDensDataIn);
	  BDensDataResult = (struct bdensdata_out *) mymalloc(nimport * sizeof(struct bdensdata_out));
	  BDensDataOut = (struct bdensdata_out *) mymalloc(nexport * sizeof(struct bdensdata_out));

	  /* now do the particles that were sent to us */
	  for(j = 0; j < nimport; j++)
	    baryondensity_evaluate(j, 1, &dummy, &dummy);

	  if(i >= NumPart)
	    ndone_flag = 1;
	  else
	    ndone_flag = 0;

	  MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

	  /* get the result */
	  for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
	    {
	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;
	      if(recvTask < NTask)
		{
		  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
		    {
		      /* send the results */
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&BDensDataResult[Recv_offset[recvTask]],
				Recv_count[recvTask] * sizeof(struct bdensdata_out), MPI_BYTE,
				recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&BDensDataOut[Send_offset[recvTask]],
				Send_count[recvTask] * sizeof(struct bdensdata_out), MPI_BYTE,
				recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
		      MPI_Sendrecv(&BDensDataResult[Recv_offset[recvTask]],
				   Recv_count[recvTask] * sizeof(struct bdensdata_out),
				   MPI_BYTE, recvTask, TAG_DENS_B,
				   &BDensDataOut[Send_offset[recvTask]],
				   Send_count[recvTask] * sizeof(struct bdensdata_out),
				   MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
		    }
		}

	    }

	  /* add the result to the local particles */
	  for(j = 0; j < nexport; j++)
	    {
	      place = DataIndexTable[j].Index;

	      P[place].gn.dNumNgb += BDensDataOut[j].Ngb;

	      if(P[place].GroupIn == 1)
		{
		  P[place].gd.dDensity += BDensDataOut[j].Rho;
		  P[place].gh.dDhsmlDensityFactor += BDensDataOut[j].DhsmlDensity;
		}
	    }

	  myfree(BDensDataOut);
	  myfree(BDensDataResult);
	  myfree(BDensDataGet);
	}
      while(ndone < NTask);

      for(i = 0, npleft = 0; i < NumPart; i++)
	{
	  if(P[i].GroupIn == 1 && Left[i] >= 0)
	    {
	      if(P[i].gd.Density > 0)
		{
		  P[i].gh.DhsmlDensityFactor *= P[i].gHsml / (NUMDIMS * P[i].gd.Density);
		  if(P[i].gh.DhsmlDensityFactor > -0.9)   /* note: this would be -1 if only a single particle at zero lag is found */
		    P[i].gh.DhsmlDensityFactor = 1 / (1 + P[i].gh.DhsmlDensityFactor);
		  else
		    P[i].gh.DhsmlDensityFactor = 1;
		}
	      /* now check whether we had enough neighbours */
	      if(P[i].gn.NumNgb < (desnumngb - All.MaxNumNgbDeviation) ||
		 P[i].gn.NumNgb > (desnumngb + All.MaxNumNgbDeviation))
		{
		  /* need to redo this particle */
		  npleft++;

		  if(Left[i] > 0 && Right[i] > 0)
		    if((Right[i] - Left[i]) < 1.0e-3 * Left[i])
		      {
			/* this one should be ok */
			npleft--;
			Left[i] = -1;               /* Mark as inactive */
			continue;
		      }

		  if(P[i].gn.NumNgb < (desnumngb - All.MaxNumNgbDeviation))
		    Left[i] = DMAX(P[i].gHsml, Left[i]);
		  else
		    {
		      if(Right[i] != 0)
			{
			  if(P[i].gHsml < Right[i])
			    Right[i] = P[i].gHsml;
			}
		      else
			Right[i] = P[i].gHsml;
		    }

		  if(iter >= MAXITER - 10)
		    {
		      printf
			("i=%d task=%d ID=%d Hsml=%g Left=%g Right=%g Ngbs=%g Right-Left=%g\n   pos=(%g|%g|%g)\n",
			 i, ThisTask, (int) P[i].ID, P[i].gHsml, Left[i], Right[i],
			 (float) P[i].gn.NumNgb, Right[i] - Left[i], P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
		      fflush(stdout);
		    }

		  if(Right[i] > 0 && Left[i] > 0)
		    P[i].gHsml = pow(0.5 * (pow(Left[i], 3) + pow(Right[i], 3)), 1.0 / 3);
		  else
		    {
		      if(Right[i] == 0 && Left[i] == 0)
			endrun(8188);	/* can't occur */

		      if(Right[i] == 0 && Left[i] > 0)
			{
			  if(P[i].GroupIn == 1 && fabs(P[i].gn.NumNgb - desnumngb) < 0.5 * desnumngb)
			    {
			      fac = 1 - (P[i].gn.NumNgb -
					 desnumngb) / (NUMDIMS * P[i].gn.NumNgb) *
				P[i].gh.DhsmlDensityFactor;

			      if(fac < 1.26)
				P[i].gHsml *= fac;
			      else
				P[i].gHsml *= 1.26;
			    }
			  else
			    P[i].gHsml *= 1.26;
			}

		      if(Right[i] > 0 && Left[i] == 0)
			{
			  if(P[i].GroupIn == 1 && fabs(P[i].gn.NumNgb - desnumngb) < 0.5 * desnumngb)
			    {
			      fac = 1 - (P[i].gn.NumNgb -
					 desnumngb) / (NUMDIMS * P[i].gn.NumNgb) *
				P[i].gh.DhsmlDensityFactor;

			      if(fac > 1 / 1.26)
				P[i].gHsml *= fac;
			      else
				P[i].gHsml /= 1.26;
			    }
			  else
			    P[i].gHsml /= 1.26;
			}
		    }
		}
	      else
		Left[i] = -1;  /* Mark as inactive */
	    }
	}

      sumup_large_ints(1, &npleft, &ntot);

      if(ntot > 0)
	{
	  iter++;

	  if(iter > 0 && ThisTask == 0)
	    {
	      printf("ngb iteration %d: need to repeat for %d%09d particles.\n", iter,
		     (int) (ntot / 1000000000), (int) (ntot % 1000000000));
	      fflush(stdout);
	    }

	  if(iter > MAXITER)
	    {
	      printf("failed to converge in neighbour iteration in baryondensity()\n");
	      fflush(stdout);
	      endrun(1155);
	    }
	}
    }
  while(ntot > 0);


  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  myfree(Right);
  myfree(Left);

}


/*! This function represents the core of the SPH density computation. The
 *  target particle may either be local, or reside in the communication
 *  buffer.
 */
int baryondensity_evaluate(int target, int mode, int *nexport, int *nsend_local)
{
  int j, n;
  int startnode, numngb, numngb_inbox, listindex = 0;
  double h, h2, hinv, hinv3, hinv4;
  MyLongDouble rho;
  double wk, dwk;
  double dx, dy, dz, r, r2, u, mass_j;
  MyLongDouble weighted_numngb;
  MyLongDouble dhsmlrho;

  MyDouble *pos;

  rho = weighted_numngb = dhsmlrho = 0;

  if(mode == 0)
    {
      pos = P[target].Pos;
      h = P[target].gHsml;
    }
  else
    {
      pos = BDensDataGet[target].Pos;
      h = BDensDataGet[target].Hsml;
    }


  h2 = h * h;
  hinv = 1.0 / h;
#ifndef  TWODIMS
  hinv3 = hinv * hinv * hinv;
#else
  hinv3 = hinv * hinv / boxSize_Z;
#endif
  hinv4 = hinv3 * hinv;


  if(mode == 0)
    {
      startnode = All.MaxPart;	/* root node */
    }
  else
    {
      startnode = BDensDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;	/* open it */
    }

  numngb = 0;

  while(startnode >= 0)
    {
      while(startnode >= 0)
	{
	  numngb_inbox = ngb_treefind_baryon(pos, h, target, &startnode, mode, nexport, nsend_local);

	  if(numngb_inbox < 0)
	    return -1;

	  for(n = 0; n < numngb_inbox; n++)
	    {
	      j = Ngblist[n];

	      dx = pos[0] - P[j].Pos[0];
	      dy = pos[1] - P[j].Pos[1];
	      dz = pos[2] - P[j].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
	      if(dx > boxHalf_X)
		dx -= boxSize_X;
	      if(dx < -boxHalf_X)
		dx += boxSize_X;
	      if(dy > boxHalf_Y)
		dy -= boxSize_Y;
	      if(dy < -boxHalf_Y)
		dy += boxSize_Y;
	      if(dz > boxHalf_Z)
		dz -= boxSize_Z;
	      if(dz < -boxHalf_Z)
		dz += boxSize_Z;
#endif
	      r2 = dx * dx + dy * dy + dz * dz;

	      if(r2 < h2)
		{
		  numngb++;

		  r = sqrt(r2);

		  u = r * hinv;

		  if(u < 0.5)
		    {
		      wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1) * u * u);
		      dwk = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
		    }
		  else
		    {
		      wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
		      dwk = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
		    }

		  mass_j = P[j].Mass;

		  rho += FLT(mass_j * wk);

		  weighted_numngb += FLT(NORM_COEFF * wk / hinv3);	/* 4.0/3 * PI = 4.188790204786 */

                  dhsmlrho += FLT(-mass_j * (NUMDIMS * hinv * wk + u * dwk));
		}
	    }
	}

      if(mode == 1)
	{
	  listindex++;
	  if(listindex < NODELISTLENGTH)
	    {
	      startnode = BDensDataGet[target].NodeList[listindex];
	      if(startnode >= 0)
		startnode = Nodes[startnode].u.d.nextnode;	/* open it */
	    }
	}
    }

  if(mode == 0)
    {
      P[target].gn.dNumNgb = weighted_numngb;
      if(P[target].GroupIn == 1)
	{
	  P[target].gd.dDensity = rho;
	  P[target].gh.dDhsmlDensityFactor = dhsmlrho;
	}
    }
  else
    {
      BDensDataResult[target].Rho = rho;
      BDensDataResult[target].Ngb = weighted_numngb;
      BDensDataResult[target].DhsmlDensity = dhsmlrho;
    }

  return 0;
}

void set_hsml_guess(void)
{
  int n, m, no;
  float hsml;

  for(n = 0; n < NumPart; n++)
    {
      if(P[n].Type == 4)
	{
	  no = Father[n];

          while(no >= 0)
            {
              if(Nodes[no].u.d.mass > All.DesNumNgb * P[n].Mass)
                break;
              no = Nodes[no].u.d.father;
            }

          hsml = P[n].gHsml;

          if(no >= 0)
            {
              if(Nodes[no].u.d.mass > All.DesNumNgb * P[n].Mass)
                hsml = pow(3/(4*M_PI) * All.DesNumNgb * P[n].Mass / Nodes[no].u.d.mass, 1.0/3) * Nodes[no].len;
            }

          if(hsml < P[n].gHsml)
            P[n].gHsml = hsml;

	  if(P[n].gHsml < All.MinGasHsml)
	    P[n].gHsml = All.MinGasHsml;
        }
    }
}

#endif

