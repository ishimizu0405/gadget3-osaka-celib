#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifdef GRACKLE
#include <grackle.h>

#include "grackle_wrapper.h"

// Set grid dimension and size.
/* 
   grid_start and grid_end are used to ignore ghost zones.
   If grid rank is less than 3, set the other dimensions, 
   start indices, and end indices to 0.
*/


#define griddim 3
int grid_rank = griddim;
int grid_dimension[griddim], grid_start[griddim], grid_end[griddim];

// arrays passed to grackle as input and to be filled
#define field_size  1
gr_float Density[field_size];
gr_float energy[field_size];
gr_float x_velocity[field_size];
gr_float y_velocity[field_size];
gr_float z_velocity[field_size];
// for primordial_chemistry >= 1
gr_float HI_density[field_size];
gr_float HII_density[field_size];
gr_float HeI_density[field_size];
gr_float HeII_density[field_size];
gr_float HeIII_density[field_size];
gr_float e_density[field_size];
// for primordial_chemistry >= 2
gr_float HM_density[field_size];
gr_float H2I_density[field_size];
gr_float H2II_density[field_size];
//for primordial_chemistry >= 3
gr_float DI_density[field_size];
gr_float DII_density[field_size];
gr_float HDI_density[field_size];
// for metal_cooling = 1
gr_float metal_density[field_size];
// calculated
gr_float cooling_time[field_size];

static chemistry_data my_chemistry;
static code_units my_units;


int wrap_init_cooling(double udensity, double ulength, double utime) 
{
  /*********************************************************************
  / Initial setup of units and chemistry objects.
  / This should be done at simulation start.
  *********************************************************************/
  int i;
  // First, set up the units system.
  // These are conversions from code units to cgs.
  my_units.comoving_coordinates = 0;
  my_units.density_units = udensity;
  my_units.length_units = ulength;
  my_units.time_units = utime;
  my_units.a_units = 1.0; // units for the expansion factor (1/1+zi)
  fprintf(stdout,"units %g %g %g\n",my_units.density_units,my_units.length_units,my_units.time_units);

  // Second, create a chemistry object for parameters and rate data.
  if (set_default_chemistry_parameters() == 0) {
    fprintf(stderr, "Error in set_default_chemistry_parameters.\n");
    return 0;
  }

  // Set parameter values for chemistry.
  grackle_data.use_grackle = 1;          
  grackle_data.with_radiative_cooling = 1;
  grackle_data.primordial_chemistry = GRACKLE_CHEMISTRY;   // 0: tablized, 1: 6 species, 2: 9 species, 3: 12 species
  grackle_data.metal_cooling = 1;          // metal cooling on
  grackle_data.UVbackground = 1;          
  grackle_data.grackle_data_file = "CloudyData/CloudyData_UVB=HM2012.h5"; 
  //grackle_data.grackle_data_file = "CloudyData_noUVB.h5"; 
  //fprintf(stdout,"Chemistry %d\n",grackle_data.primordial_chemistry);

  // Finally, initialize the chemistry object.
  gr_float initial_redshift = 0.; 
  gr_float a_value;
  a_value = 1. / (1. + initial_redshift);

  //check the following
  if(my_units.comoving_coordinates)
    my_units.density_units *= (a_value*a_value*a_value);

  // Finally, initialize the chemistry object.
  if (initialize_chemistry_data(&my_units, a_value) == 0) {
    fprintf(stderr, "Error in initialize_chemistry_data.\n");
    return 0;
  }

  
  for (i = 0;i < griddim;i++) {
	  grid_dimension[i] = 1; 
	  grid_start[i] = 0;
	  grid_end[i] = 0;
  }
  grid_dimension[0] = field_size;
  grid_end[0] = field_size - 1;

  return 1;
}

int wrap_update_UVbackground_rates(double a) {
  // The UV background rates must be updated before 
  // calling the other functions.

  /**
  if (update_UVbackground_rates(&grackle_data, &my_units, a) == 0)
    return 0;
  
  return 1;
  **/
  //For grackle >2.0, we do not need to call this function. I do not like it.
  return 1;
}

double wrap_get_alow_uvb()
{
  double a;

  a = 1.0/(grackle_data.UVbackground_table.zmax+1);
  a /= 0.1; // for safety

  return a;
}

int wrap_do_cooling(double rho, double *u, double dt, double vx, double vy, double vz,
		    double *HI, double *HII, double *HeI, double *HeII, double *HeIII,
		    double *HM, double *H2I, double *H2II, double *DI, double *DII, double *HDI,
		    double *ne, double Z, double a_now)
{
  int i;
  gr_float den_factor, u_factor;
  gr_float tiny_number = 1.0e-20;  

  if(field_size != 1){
    fprintf(stderr,"field_size must currently be set to 1.\n");
    return 0;
  } 

  // passed density and energy are proper
  if(my_units.comoving_coordinates){
    den_factor = pow(a_now, 3);
    u_factor   = pow(a_now, 0);
    my_units.velocity_units = my_units.length_units / my_units.time_units;
  } else {
    den_factor = 1.0;
    u_factor   = 1.0;
    my_units.velocity_units = my_units.length_units / my_units.time_units;
  }


  for (i = 0; i < field_size; i++) {
    Density[i]     =  rho * den_factor; 
    HI_density[i]  = *HI * Density[i];
    HII_density[i] = *HII * Density[i];
    
    HeI_density[i]   = *HeI * Density[i];
    HeII_density[i]  = *HeII * Density[i];
    HeIII_density[i] = *HeIII * Density[i];

    HM_density[i]   = *HM * Density[i];
    H2I_density[i]  = *H2I * Density[i];
    H2II_density[i] = *H2II * Density[i];
    
    DI_density[i]   = *DI * Density[i];
    DII_density[i]  = *DII * Density[i];
    HDI_density[i]  = *HDI * Density[i];

    e_density[i]     = *ne * Density[i]; 
    metal_density[i] = Z * Density[i];
    
    x_velocity[i] = vx;
    y_velocity[i] = vy;
    z_velocity[i] = vz;
    
    energy[i] = *u * u_factor; 

  }

  if (grackle_data.primordial_chemistry)
    {
      if (solve_chemistry(&my_units,
			  a_now, dt,
			  grid_rank, grid_dimension,
			  grid_start, grid_end,
			  Density, energy,
			  x_velocity, y_velocity, z_velocity,
			  HI_density, HII_density, HM_density,
			  HeI_density, HeII_density, HeIII_density,
			  H2I_density, H2II_density,
			  DI_density, DII_density, HDI_density,
			  e_density, metal_density) == 0) {
	fprintf(stderr,"Error in solve_chemistry.\n");
	return 0;
      }
    }
  else
    {
      if (solve_chemistry_table(&my_units,
				a_now, dt,
				grid_rank, grid_dimension,
				grid_start, grid_end,
				Density, energy,
				x_velocity, y_velocity, z_velocity,
				metal_density) == 0) {
	fprintf(stderr,"Error in solve_chemistry.\n");
	return 0;
      }
    }


  // return updated chemistry and energy
  for (i = 0;i < field_size;i++) {
    *HI     =  HI_density[i]/Density[i];
    *HII    =  HII_density[i]/Density[i];

    *HeI    =  HeI_density[i]/Density[i];
    *HeII   =  HeII_density[i]/Density[i];
    *HeIII  =  HeIII_density[i]/Density[i];

    *HM     =  HM_density[i]/Density[i];
    *H2I    =  H2I_density[i]/Density[i];
    *H2II   =  H2II_density[i]/Density[i];

    *DI     =  DI_density[i]/Density[i];
    *DII    =  DII_density[i]/Density[i];
    *HDI    =  HDI_density[i]/Density[i];    

    *ne     =  e_density[i]/Density[i];
    *u      =  energy[i]/u_factor;
  }

  return 1;
}

int wrap_get_cooling_time(double rho, double u, double vx, double vy, double vz,
			  double HI, double HII, double HeI, double HeII, double HeIII,
			  double HM, double H2I, double H2II, double DI, double DII, double HDI,
			  double ne, double Z, double a_now, double *coolingtime)
{
  int i;
  gr_float den_factor, u_factor;
  gr_float tiny_number = 1.0e-20;

  if(field_size != 1){
    fprintf(stderr,"field_size must currently be set to 1.\n");
    return 0;
  } 

  if(my_units.comoving_coordinates){
    den_factor = pow(a_now, 3);
    u_factor   = pow(a_now, 0);
    my_units.velocity_units = my_units.length_units / my_units.time_units;
  } else {
    den_factor = 1.0;
    u_factor   = 1.0;
    my_units.velocity_units = my_units.length_units / my_units.time_units;
  }
  for (i = 0;i < field_size;i++) {
      Density[i]     =  rho * den_factor; 
      HI_density[i]  = HI * Density[i];
      HII_density[i] = HII * Density[i];
        
      HeI_density[i]   = HeI * Density[i];
      HeII_density[i]  = HeII * Density[i];
      HeIII_density[i] = HeIII * Density[i];

      HM_density[i]   = HM * Density[i];
      H2I_density[i]  = H2I * Density[i];
      H2II_density[i] = H2II * Density[i];

      DI_density[i]   = DI * Density[i];
      DII_density[i]  = DII * Density[i];
      HDI_density[i]  = HDI * Density[i];

      e_density[i]     = ne * Density[i]; 
      metal_density[i] = Z * Density[i];
      
      x_velocity[i] = vx;
      y_velocity[i] = vy;
      z_velocity[i] = vz;
        
      energy[i] = u;
  }

  if (grackle_data.primordial_chemistry)
    {
      if (calculate_cooling_time(&my_units,
				 a_now,
				 grid_rank, grid_dimension,
				 grid_start, grid_end,
				 Density, energy,
				 x_velocity, y_velocity, z_velocity,
				 HI_density, HII_density, HM_density,
				 HeI_density, HeII_density, HeIII_density,
				 H2I_density, H2II_density,
				 DI_density, DII_density, HDI_density,
				 e_density, metal_density, 
				 cooling_time) == 0) {
	fprintf(stderr,"Error in calculate_cooling_time.\n");
	return 0;
      }
    }
  else
    {
      if (calculate_cooling_time_table(&my_units,
				       a_now,
				       grid_rank, grid_dimension,
				       grid_start, grid_end,
				       Density, energy,
				       x_velocity, y_velocity, z_velocity,
				       metal_density, 
				       cooling_time) == 0) {
	fprintf(stderr,"Error in calculate_cooling_time.\n");
	return 0;
      }
    }
  // return updated chemistry and energy
  for (i = 0;i < field_size;i++)
    *coolingtime = cooling_time[i] ;

  return 1;
}
#endif
