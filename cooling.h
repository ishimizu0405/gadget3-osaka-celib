#ifndef INLINE_FUNC
#ifdef INLINE
#define INLINE_FUNC inline
#else
#define INLINE_FUNC
#endif
#endif

#ifdef METAL_COOLING
double AbundanceRatios(double u, double rho, double *ne_guess, double *nH0_pointer, double *nHeII_pointer, double Z);
double convert_u_to_temp(double u, double rho, double *ne_guess, double Z);
void   find_abundances_and_rates(double logT, double rho, double *ne_guess, double Z);
#else
double AbundanceRatios(double u, double rho, double *ne_guess, double *nH0_pointer, double *nHeII_pointer);
double convert_u_to_temp(double u, double rho, double *ne_guess);
void   find_abundances_and_rates(double logT, double rho, double *ne_guess);
#endif

#if defined(LT_METAL_COOLING) || defined(METAL_COOLING)
double GetCoolingTime(double u_old, double rho,  double *ne_guess, double Z);
double CoolingRateFromU(double u, double rho, double *ne_guess, double Z);
double DoCooling(double u_old, double rho, double dt, double *ne_guess, double Z);
double CoolingRate(double logT, double rho, double *nelec, double Z);
double DoInstabilityCooling(double m_old, double u, double rho, double dt, double fac, double *ne_guess, double Z);
#else
double CoolingRate(double logT, double rho, double *nelec);
double CoolingRateFromU(double u, double rho, double *ne_guess);
double DoCooling(double u_old, double rho, double dt, double *ne_guess);
double GetCoolingTime(double u_old, double rho,  double *ne_guess);
double DoInstabilityCooling(double m_old, double u, double rho, double dt, double fac, double *ne_guess);
#endif

void   InitCool(void);
void   InitCoolMemory(void);
void   IonizeParams(void);
void   IonizeParamsFunction(void);
void   IonizeParamsTable(void);
double INLINE_FUNC LogTemp(double u, double ne);
void   MakeCoolingTable(void);
void   *mymalloc(size_t size);
void   ReadIonizeParams(char *fname);
void   SetZeroIonization(void);
void   TestCool(void);

#ifdef GRACKLE
#if (GRACKLE_CHEMISTRY ==0)
double GetCoolingTime_GRACKLE(double energy, double rho, double ne, double Z, double a_now);
double DoCooling_GRACKLE(double energy, double rho, double dt, double *ne, double Z, double a_now);
#endif
#if (GRACKLE_CHEMISTRY ==1)
double GetCoolingTime_GRACKLE(double energy, double rho, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double Z, double a_now);
double DoCooling_GRACKLE(double energy, double rho, double dt, double *ne, double *HI, double *HII, double *HeI, double *HeII, double *HeIII, double Z, double a_now);
#endif
#if (GRACKLE_CHEMISTRY ==2)
double GetCoolingTime_GRACKLE(double energy, double rho, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double Z, double a_now);
double DoCooling_GRACKLE(double energy, double rho, double dt, double *ne, double *HI, double *HII, double *HeI, double *HeII, double *HeIII, double *HM, double *H2I, double *H2II, double Z, double a_now);
#endif
#if (GRACKLE_CHEMISTRY ==3)
double GetCoolingTime_GRACKLE(double energy, double rho, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI, double Z, double a_now);
double DoCooling_GRACKLE(double energy, double rho, double dt, double *ne, double *HI, double *HII, double *HeI, double *HeII, double *HeIII, double *HM, double *H2I, double *H2II, double *DI, double *DII, double *HDI, double Z, double a_now);
#endif
#endif
