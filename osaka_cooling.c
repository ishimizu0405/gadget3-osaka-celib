#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "allvars.h"
#include "proto.h"

#ifdef OSAKA_COOLING
#include <grackle.h>

void osaka_grackle_initialization(void)
{
  int ret;
  double unit_density, unit_length, unit_time, unit_velocity;
  
  unit_density = All.UnitDensity_in_cgs * All.HubbleParam * All.HubbleParam;
  unit_length = All.UnitLength_in_cm / All.HubbleParam;
  unit_time = All.UnitTime_in_s / All.HubbleParam;
  unit_velocity = unit_length / unit_time;
  //unit_velocity = All.UnitVelocity_in_cm_per_s;
  
  ret = grackle_initialization_wrapper(unit_density, unit_length, unit_time, unit_velocity);
  
  if(ret != 0)
    {
      printf("grackle initialization fails.\n");
      fflush(stdout);
      
      endrun(100);
    }
}

double DoCooling_grackle(int target, double uin, double dt, double anow)
{
  int i;
  double metallicity, density, u, uout;
  double ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI;
  double ain, a3inv;
  
  if(dt == 0.0)
    return uin;
    
  if(All.ComovingIntegrationOn)
    {
      a3inv = 1.0 / anow / anow / anow;
      ain = anow;
    }
  else
    {
      a3inv = 1.0;
      ain = 1.0 / (1.0 + All.UVbackground_redshift_in);
    }
    
  density = SphP[target].d.Density * a3inv;
  u = uin;
  
#ifdef OSAKA_METALSMOOTHING
  metallicity = P[target].MetallicitySmoothed;
#else
  metallicity = P[target].Metallicity;
#endif

#ifdef OSAKA_METAL_FLOOR
  if(metallicity < All.MetallicityFloor)
    metallicity = All.MetallicityFloor;
#endif

  ne = SphP[target].Ne;
  HI = SphP[target].HI;
  HII = SphP[target].HII;
  HeI = SphP[target].HeI;
  HeII = SphP[target].HeII;
  HeIII = SphP[target].HeIII;
  HM = SphP[target].HM;
  H2I = SphP[target].H2I;
  H2II = SphP[target].H2II;
  DI = SphP[target].DI;
  DII = SphP[target].DII;
  HDI = SphP[target].HDI;
  
  if(docooling_grackle_wrapper(metallicity, dt, ain, density, &u, &ne, &HI, &HII, &HeI, &HeII, &HeIII, &HM, &H2I, &H2II, &DI, &DII, &HDI) == 0)
    {
      printf("Error in docooling_grackle_wrapper.\n");
      fflush(stdout);
      
      return 0;
    }
    
  SphP[target].Ne = ne;
  SphP[target].HI = HI;
  SphP[target].HII = HII;
  SphP[target].HeI = HeI;
  SphP[target].HeII = HeII;
  SphP[target].HeIII = HeIII;
  SphP[target].HM = HM;
  SphP[target].H2I = H2I;
  SphP[target].H2II = H2II;
  SphP[target].DI = DI;
  SphP[target].DII = DII;
  SphP[target].HDI = HDI;
  
  uout = u;
  
  return uout;
}

double GetCoolingTime_grackle(int target, double uin, double anow)
{
  int i;
  double metallicity, density, cooling_time;
  double ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI;
  double ain, a3inv;
  
  if(All.ComovingIntegrationOn)
    {
      a3inv = 1.0 / anow / anow / anow;
      ain = anow;
    }
  else
    {
      a3inv = 1.0;
      ain = 1.0 / (1.0 + All.UVbackground_redshift_in);
    }
    
  density = SphP[target].d.Density * a3inv;
  
#ifdef OSAKA_METALSMOOTHING
  metallicity = P[target].MetallicitySmoothed;
#else
  metallicity = P[target].Metallicity;
#endif

#ifdef OSAKA_METAL_FLOOR
  if(metallicity < All.MetallicityFloor)
    metallicity = All.MetallicityFloor;
#endif

  ne = SphP[target].Ne;
  HI = SphP[target].HI;
  HII = SphP[target].HII;
  HeI = SphP[target].HeI;
  HeII = SphP[target].HeII;
  HeIII = SphP[target].HeIII;
  HM = SphP[target].HM;
  H2I = SphP[target].H2I;
  H2II = SphP[target].H2II;
  DI = SphP[target].DI;
  DII = SphP[target].DII;
  HDI = SphP[target].HDI;
  
  if(getcoolingtime_grackle_wrapper(metallicity, ain, density, &cooling_time, uin, ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI) == 0)
    {
      printf("Error in getcoolingtime_grackle_wrapper.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return cooling_time;
}

double GetTemperature_grackle(int target, double uin, double anow)
{
  int i;
  double metallicity, density, temperature;
  double ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI;
  double ain, a3inv;
  
  if(All.ComovingIntegrationOn)
    {
      a3inv = 1.0 / anow / anow / anow;
      ain = anow;
    }
  else
    {
      a3inv = 1.0;
      ain = 1.0 / (1.0 + All.UVbackground_redshift_in);
    }
    
  density = SphP[target].d.Density * a3inv;
  
#ifdef OSAKA_METALSMOOTHING
  metallicity = P[target].MetallicitySmoothed;
#else
  metallicity = P[target].Metallicity;
#endif

#ifdef OSAKA_METAL_FLOOR
  if(metallicity < All.MetallicityFloor)
    metallicity = All.MetallicityFloor;
#endif

  ne = SphP[target].Ne;
  HI = SphP[target].HI;
  HII = SphP[target].HII;
  HeI = SphP[target].HeI;
  HeII = SphP[target].HeII;
  HeIII = SphP[target].HeIII;
  HM = SphP[target].HM;
  H2I = SphP[target].H2I;
  H2II = SphP[target].H2II;
  DI = SphP[target].DI;
  DII = SphP[target].DII;
  HDI = SphP[target].HDI;
  
  if(gettemperature_grackle_wrapper(metallicity, ain, density, &temperature, uin, ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI) == 0)
    {
      printf("Error in gettemperature_grackle_wrapper.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return temperature;
}

double GetPressure_grackle(int target, double uin, double anow)
{
  int i;
  double metallicity, density, pressure;
  double ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI;
  double ain, a3inv;
  
  if(All.ComovingIntegrationOn)
    {
      a3inv = 1.0 / anow / anow / anow;
      ain = anow;
    }
  else
    {
      a3inv = 1.0;
      ain = 1.0 / (1.0 + All.UVbackground_redshift_in);
    }
    
  density = SphP[target].d.Density * a3inv;
  
#ifdef OSAKA_METALSMOOTHING
  metallicity = P[target].MetallicitySmoothed;
#else
  metallicity = P[target].Metallicity;
#endif

#ifdef OSAKA_METAL_FLOOR
  if(metallicity < All.MetallicityFloor)
    metallicity = All.MetallicityFloor;
#endif

  ne = SphP[target].Ne;
  HI = SphP[target].HI;
  HII = SphP[target].HII;
  HeI = SphP[target].HeI;
  HeII = SphP[target].HeII;
  HeIII = SphP[target].HeIII;
  HM = SphP[target].HM;
  H2I = SphP[target].H2I;
  H2II = SphP[target].H2II;
  DI = SphP[target].DI;
  DII = SphP[target].DII;
  HDI = SphP[target].HDI;
  
  if(getpressure_grackle_wrapper(metallicity, ain, density, &pressure, uin, ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI) == 0)
    {
      printf("Error in getpressure_grackle_wrapper.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return pressure;
}

double GetGamma_grackle(int target, double uin, double anow)
{
  int i;
  double metallicity, density, gamma;
  double ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI;
  double ain, a3inv;
  
  if(All.ComovingIntegrationOn)
    {
      a3inv = 1.0 / anow / anow / anow;
      ain = anow;
    }
  else
    {
      a3inv = 1.0;
      ain = 1.0 / (1.0 + All.UVbackground_redshift_in);
    }
    
  density = SphP[target].d.Density * a3inv;
  
#ifdef OSAKA_METALSMOOTHING
  metallicity = P[target].MetallicitySmoothed;
#else
  metallicity = P[target].Metallicity;
#endif

#ifdef OSAKA_METAL_FLOOR
  if(metallicity < All.MetallicityFloor)
    metallicity = All.MetallicityFloor;
#endif

  ne = SphP[target].Ne;
  HI = SphP[target].HI;
  HII = SphP[target].HII;
  HeI = SphP[target].HeI;
  HeII = SphP[target].HeII;
  HeIII = SphP[target].HeIII;
  HM = SphP[target].HM;
  H2I = SphP[target].H2I;
  H2II = SphP[target].H2II;
  DI = SphP[target].DI;
  DII = SphP[target].DII;
  HDI = SphP[target].HDI;
  
  if(getgamma_grackle_wrapper(metallicity, ain, density, &gamma, uin, ne, HI, HII, HeI, HeII, HeIII, HM, H2I, H2II, DI, DII, HDI) == 0)
    {
      printf("Error in getgamma_grackle_wrapper.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return gamma;
}

#ifndef OSAKA_GRACKLE_VERSION2
#define griddim    3
#define field_size 1
#define tiny_number  1.e-20

static chemistry_data *my_grackle_data;
static code_units my_units;
static grackle_field_data my_fields;

int grackle_initialization_wrapper(double unit_density, double unit_length, double unit_time, double unit_velocity)
{
  int i;
  
  grackle_verbose = 1;
  
  if(field_size != 1 || griddim != 3)
    {
      printf("field_size and griddim must currently be set to 1 and 3.\n");
      fflush(stdout);
      
      endrun(110);
    }
    
  my_units.density_units = unit_density;
  my_units.length_units = unit_length;
  my_units.time_units = unit_time;
  my_units.velocity_units = unit_velocity;
  
  if(All.ComovingIntegrationOn)
    {
      my_units.comoving_coordinates = 0; // 1 if cosmological sim, 0 if not
      my_units.a_units = 1.0; // units for the expansion factor
      my_units.a_value = All.TimeBegin; // Set expansion factor to 1 for non-cosmological simulation.
      //my_units.a_value = 1.0; // Set expansion factor to 1 for non-cosmological simulation.
    }
  else
    {
      my_units.comoving_coordinates = 0; // 1 if cosmological sim, 0 if not
      my_units.a_units = 1.0; // units for the expansion factor
      my_units.a_value = 1.0; // Set expansion factor to 1 for non-cosmological simulation.
    }
    
  // Second, create a chemistry object for parameters.  This needs to be a pointer.
  my_grackle_data = malloc(sizeof(chemistry_data));
  
  if(set_default_chemistry_parameters(my_grackle_data) == 0)
    {
      printf("Error in set_default_chemistry_parameters.\n");
      fflush(stdout);
      
      endrun(111);
    }
    
  // Set parameter values for chemistry.
  grackle_data->use_grackle = 1; // chemistry on
  grackle_data->with_radiative_cooling = 1; // cooling on
  grackle_data->primordial_chemistry = All.GrackleChemistry; // molecular network with H, He, D; 0: tablized, 1: 6 species, 2: 9 species, 3: 12 species
  grackle_data->metal_cooling = All.MetalCooling; // metal cooling on
  grackle_data->UVbackground = All.UVBackground; // UV background on
  grackle_data->grackle_data_file = All.GrackleUVBackgroundData; // data file
  grackle_data->h2_on_dust = All.H2onDust; // H2 formation on dust grains, dust cooling, and dust-gas heat transfer follow Omukai (2000). 
  grackle_data->self_shielding_method = All.self_shielding_method;
  grackle_data->H2_self_shielding = All.H2_self_shielding;
  grackle_data->UVbackground_redshift_on = All.UVbackground_redshift_on;
  grackle_data->UVbackground_redshift_off = All.UVbackground_redshift_off;
  grackle_data->UVbackground_redshift_fullon = All.UVbackground_redshift_fullon;
  grackle_data->UVbackground_redshift_drop = All.UVbackground_redshift_drop;
  grackle_data->SolarMetalFractionByMass = All.SolarMetalFractionByMass;
  
  // Finally, initialize the chemistry object.
  if(initialize_chemistry_data(&my_units) == 0)
    {
      printf("Error in initialize_chemistry_data.\n");
      fflush(stdout);
      
      endrun(112);
    }
    
  my_fields.grid_rank = griddim;
  my_fields.grid_dimension = malloc(griddim * sizeof(int));
  my_fields.grid_start = malloc(griddim * sizeof(int));
  my_fields.grid_end = malloc(griddim * sizeof(int));
  
  if(my_fields.grid_dimension == NULL || my_fields.grid_start == NULL || my_fields.grid_end == NULL)
    {
      printf("Can not allocate my_fields.grid\n");
      fflush(stdout);
      
      endrun(113);
    }
    
  for(i = 0; i < griddim; i++)
    {
      my_fields.grid_dimension[i] = 1; // the active dimension not including ghost zones.
      my_fields.grid_start[i] = 0;
      my_fields.grid_end[i] = 0;
    }
    
  my_fields.grid_dimension[0] = field_size;
  my_fields.grid_end[0] = field_size - 1;
  
  my_fields.density = malloc(field_size * sizeof(gr_float));
  my_fields.internal_energy = malloc(field_size * sizeof(gr_float));
  my_fields.x_velocity = malloc(field_size * sizeof(gr_float));
  my_fields.y_velocity = malloc(field_size * sizeof(gr_float));
  my_fields.z_velocity = malloc(field_size * sizeof(gr_float));
  
  if(my_fields.density == NULL || my_fields.internal_energy == NULL || my_fields.x_velocity == NULL || my_fields.y_velocity == NULL || my_fields.z_velocity == NULL)
    {
      printf("Can not allocate my_fields 0\n");
      fflush(stdout);
      
      endrun(114);
    }
    
  // for primordial_chemistry >= 1
  my_fields.HI_density = malloc(field_size * sizeof(gr_float));
  my_fields.HII_density = malloc(field_size * sizeof(gr_float));
  my_fields.HeI_density = malloc(field_size * sizeof(gr_float));
  my_fields.HeII_density = malloc(field_size * sizeof(gr_float));
  my_fields.HeIII_density = malloc(field_size * sizeof(gr_float));
  my_fields.e_density = malloc(field_size * sizeof(gr_float));
  
  if(my_fields.HI_density == NULL || my_fields.HII_density == NULL || my_fields.HeI_density == NULL || my_fields.HeII_density == NULL || my_fields.HeIII_density == NULL || my_fields.e_density == NULL)
    {
      printf("Can not allocate my_fields 1\n");
      fflush(stdout);
      
      endrun(115);
    }
    
  // for primordial_chemistry >= 2
  my_fields.HM_density = malloc(field_size * sizeof(gr_float));
  my_fields.H2I_density = malloc(field_size * sizeof(gr_float));
  my_fields.H2II_density = malloc(field_size * sizeof(gr_float));
  
  if(my_fields.HM_density == NULL || my_fields.H2I_density == NULL || my_fields.H2II_density == NULL)
    {
      printf("Can not allocate my_fields 2\n");
      fflush(stdout);
      
      endrun(116);
    }
    
  // for primordial_chemistry >= 3
  my_fields.DI_density = malloc(field_size * sizeof(gr_float));
  my_fields.DII_density = malloc(field_size * sizeof(gr_float));
  my_fields.HDI_density = malloc(field_size * sizeof(gr_float));
  
  if(my_fields.DI_density == NULL || my_fields.DII_density == NULL || my_fields.HDI_density == NULL)
    {
      printf("Can not allocate my_fields 3\n");
      fflush(stdout);
      
      endrun(117);
    }
    
  // for metal_cooling = 1
  my_fields.metal_density = malloc(field_size * sizeof(gr_float));
  
  if(my_fields.metal_density == NULL)
    {
      printf("Can not allocate my_fields 4\n");
      fflush(stdout);
      
      endrun(118);
    }
    
  // volumetric heating rate (provide in units [erg s^-1 cm^-3])
  my_fields.volumetric_heating_rate = malloc(field_size * sizeof(gr_float));
  // specific heating rate (provide in units [egs s^-1 g^-1]
  my_fields.specific_heating_rate = malloc(field_size * sizeof(gr_float));
  
  if(my_fields.volumetric_heating_rate == NULL || my_fields.specific_heating_rate == NULL)
    {
      printf("Can not allocate my_fields 5\n");
      fflush(stdout);
      
      endrun(119);
    }
    
  return 0;
}

int docooling_grackle_wrapper(double metallicity, double dt, double anow, double density, double *u, double *ne, double *HI, double *HII, double *HeI, double *HeII, double *HeIII, double *HM, double *H2I, double *H2II, double *DI, double *DII, double *HDI)
{
  int i;
  
  my_units.a_value = anow;
  
  /*if(All.ComovingIntegrationOn)
    {
      my_units.a_value = anow;
    }*/
    
  for(i = 0; i < field_size; i++)
    {
      my_fields.density[i] = density;
      my_fields.HI_density[i] = *HI * my_fields.density[i];
      my_fields.HII_density[i] = *HII * my_fields.density[i];
      my_fields.HeI_density[i] = *HeI * my_fields.density[i];
      my_fields.HeII_density[i] = *HeII * my_fields.density[i];
      my_fields.HeIII_density[i] = *HeIII * my_fields.density[i];
      my_fields.HM_density[i] = *HM * my_fields.density[i];
      my_fields.H2I_density[i] = *H2I * my_fields.density[i];
      my_fields.H2II_density[i] = *H2II * my_fields.density[i];
      my_fields.DI_density[i] = *DI * my_fields.density[i];
      my_fields.DII_density[i] = *DII * my_fields.density[i];
      my_fields.HDI_density[i] = *HDI * my_fields.density[i];
      my_fields.e_density[i] = *ne * my_fields.density[i];
      my_fields.metal_density[i] = metallicity * my_fields.density[i];
      my_fields.x_velocity[i] = 0.0;
      my_fields.y_velocity[i] = 0.0;
      my_fields.z_velocity[i] = 0.0;
      my_fields.internal_energy[i] = *u;
      
      my_fields.volumetric_heating_rate[i] = 0.0;
      my_fields.specific_heating_rate[i] = 0.0;
    }
    
  if(solve_chemistry(&my_units, &my_fields, dt) == 0)
    {
      printf("Error in solve_chemistry.\n");
      fflush(stdout);
      
      return 0;
    }
    
  // return updated chemistry and energy
  for(i = 0; i < field_size; i++)
    {
      *HI = my_fields.HI_density[i] / my_fields.density[i];
      *HII = my_fields.HII_density[i] / my_fields.density[i];
      *HeI = my_fields.HeI_density[i] / my_fields.density[i];
      *HeII = my_fields.HeII_density[i] / my_fields.density[i];
      *HeIII = my_fields.HeIII_density[i] / my_fields.density[i];
      *HM = my_fields.HM_density[i] / my_fields.density[i];
      *H2I = my_fields.H2I_density[i] / my_fields.density[i];
      *H2II = my_fields.H2II_density[i] / my_fields.density[i];
      *DI = my_fields.DI_density[i] / my_fields.density[i];
      *DII = my_fields.DII_density[i] / my_fields.density[i];
      *HDI = my_fields.HDI_density[i] / my_fields.density[i];
      *ne = my_fields.e_density[i] / my_fields.density[i];
      *u = my_fields.internal_energy[0];
    }
    
  return 1;
}

int getcoolingtime_grackle_wrapper(double metallicity, double anow, double density, double *cooling_time, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI)
{
  int i;
  
  my_units.a_value = anow;
  
  /*if(All.ComovingIntegrationOn)
    {
      my_units.a_value = anow;
    }*/
    
  for(i = 0; i < field_size; i++)
    {
      my_fields.density[i] = density;
      my_fields.HI_density[i] = HI * my_fields.density[i];
      my_fields.HII_density[i] = HII * my_fields.density[i];
      my_fields.HeI_density[i] = HeI * my_fields.density[i];
      my_fields.HeII_density[i] = HeII * my_fields.density[i];
      my_fields.HeIII_density[i] = HeIII * my_fields.density[i];
      my_fields.HM_density[i] = HM * my_fields.density[i];
      my_fields.H2I_density[i] = H2I * my_fields.density[i];
      my_fields.H2II_density[i] = H2II * my_fields.density[i];
      my_fields.DI_density[i] = DI * my_fields.density[i];
      my_fields.DII_density[i] = DII * my_fields.density[i];
      my_fields.HDI_density[i] = HDI * my_fields.density[i];
      my_fields.e_density[i] = ne * my_fields.density[i];
      my_fields.metal_density[i] = metallicity * my_fields.density[i];
      my_fields.x_velocity[i] = 0.0;
      my_fields.y_velocity[i] = 0.0;
      my_fields.z_velocity[i] = 0.0;
      my_fields.internal_energy[i] = u;
      
      my_fields.volumetric_heating_rate[i] = 0.0;
      my_fields.specific_heating_rate[i] = 0.0;
    }
    
  if(calculate_cooling_time(&my_units, &my_fields, cooling_time) == 0)
    {
      printf("Error in calculate_cooling_time.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return 1;
}

int gettemperature_grackle_wrapper(double metallicity, double anow, double density, double *temperature, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI)
{
  int i;
  
  my_units.a_value = anow;
  
  /*if(All.ComovingIntegrationOn)
    {
      my_units.a_value = anow;
    }*/
    
  for(i = 0; i < field_size; i++)
    {
      my_fields.density[i] = density;
      my_fields.HI_density[i] = HI * my_fields.density[i];
      my_fields.HII_density[i] = HII * my_fields.density[i];
      my_fields.HeI_density[i] = HeI * my_fields.density[i];
      my_fields.HeII_density[i] = HeII * my_fields.density[i];
      my_fields.HeIII_density[i] = HeIII * my_fields.density[i];
      my_fields.HM_density[i] = HM * my_fields.density[i];
      my_fields.H2I_density[i] = H2I * my_fields.density[i];
      my_fields.H2II_density[i] = H2II * my_fields.density[i];
      my_fields.DI_density[i] = DI * my_fields.density[i];
      my_fields.DII_density[i] = DII * my_fields.density[i];
      my_fields.HDI_density[i] = HDI * my_fields.density[i];
      my_fields.e_density[i] = ne * my_fields.density[i];
      my_fields.metal_density[i] = metallicity * my_fields.density[i];
      my_fields.x_velocity[i] = 0.0;
      my_fields.y_velocity[i] = 0.0;
      my_fields.z_velocity[i] = 0.0;
      my_fields.internal_energy[i] = u;
      
      my_fields.volumetric_heating_rate[i] = 0.0;
      my_fields.specific_heating_rate[i] = 0.0;
    }
    
  if(calculate_temperature(&my_units, &my_fields, temperature) == 0)
    {
      printf("Error in calculate_temperature.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return 1;
}

int getpressure_grackle_wrapper(double metallicity, double anow, double density, double *pressure, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI)
{
  int i;
  
  my_units.a_value = anow;
  
  /*if(All.ComovingIntegrationOn)
    {
      my_units.a_value = anow;
    }*/
    
  for(i = 0; i < field_size; i++)
    {
      my_fields.density[i] = density;
      my_fields.HI_density[i] = HI * my_fields.density[i];
      my_fields.HII_density[i] = HII * my_fields.density[i];
      my_fields.HeI_density[i] = HeI * my_fields.density[i];
      my_fields.HeII_density[i] = HeII * my_fields.density[i];
      my_fields.HeIII_density[i] = HeIII * my_fields.density[i];
      my_fields.HM_density[i] = HM * my_fields.density[i];
      my_fields.H2I_density[i] = H2I * my_fields.density[i];
      my_fields.H2II_density[i] = H2II * my_fields.density[i];
      my_fields.DI_density[i] = DI * my_fields.density[i];
      my_fields.DII_density[i] = DII * my_fields.density[i];
      my_fields.HDI_density[i] = HDI * my_fields.density[i];
      my_fields.e_density[i] = ne * my_fields.density[i];
      my_fields.metal_density[i] = metallicity * my_fields.density[i];
      my_fields.x_velocity[i] = 0.0;
      my_fields.y_velocity[i] = 0.0;
      my_fields.z_velocity[i] = 0.0;
      my_fields.internal_energy[i] = u;
      
      my_fields.volumetric_heating_rate[i] = 0.0;
      my_fields.specific_heating_rate[i] = 0.0;
    }
    
  if(calculate_pressure(&my_units, &my_fields, pressure) == 0)
    {
      printf("Error in calculate_pressure.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return 1;
}

int getgamma_grackle_wrapper(double metallicity, double anow, double density, double *gamma, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI)
{
  int i;
  
  my_units.a_value = anow;
  
  /*if(All.ComovingIntegrationOn)
    {
      my_units.a_value = anow;
    }*/
    
  for(i = 0; i < field_size; i++)
    {
      my_fields.density[i] = density;
      my_fields.HI_density[i] = HI * my_fields.density[i];
      my_fields.HII_density[i] = HII * my_fields.density[i];
      my_fields.HeI_density[i] = HeI * my_fields.density[i];
      my_fields.HeII_density[i] = HeII * my_fields.density[i];
      my_fields.HeIII_density[i] = HeIII * my_fields.density[i];
      my_fields.HM_density[i] = HM * my_fields.density[i];
      my_fields.H2I_density[i] = H2I * my_fields.density[i];
      my_fields.H2II_density[i] = H2II * my_fields.density[i];
      my_fields.DI_density[i] = DI * my_fields.density[i];
      my_fields.DII_density[i] = DII * my_fields.density[i];
      my_fields.HDI_density[i] = HDI * my_fields.density[i];
      my_fields.e_density[i] = ne * my_fields.density[i];
      my_fields.metal_density[i] = metallicity * my_fields.density[i];
      my_fields.x_velocity[i] = 0.0;
      my_fields.y_velocity[i] = 0.0;
      my_fields.z_velocity[i] = 0.0;
      my_fields.internal_energy[i] = u;
      
      my_fields.volumetric_heating_rate[i] = 0.0;
      my_fields.specific_heating_rate[i] = 0.0;
    }
    
  if(calculate_gamma(&my_units, &my_fields, gamma) == 0)
    {
      printf("Error in calculate_gamma.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return 1;
}
#else
#define griddim 3
#define field_size  1

int grid_rank = griddim;
int grid_dimension[griddim], grid_start[griddim], grid_end[griddim];
static gr_float Density[field_size], energy[field_size], x_velocity[field_size], y_velocity[field_size], z_velocity[field_size];
static gr_float HI_density[field_size], HII_density[field_size], HeI_density[field_size], HeII_density[field_size], HeIII_density[field_size], e_density[field_size];
static gr_float HM_density[field_size], H2I_density[field_size], H2II_density[field_size];
static gr_float DI_density[field_size], DII_density[field_size], HDI_density[field_size];
static gr_float metal_density[field_size];

static chemistry_data my_chemistry;
static code_units my_units;

int grackle_initialization_wrapper(double unit_density, double unit_length, double unit_time, double unit_velocity)
{
  int i;
  gr_float a_value;
  
  grackle_verbose = 1;
  
  if(field_size != 1 || griddim != 3)
    {
      printf("field_size and griddim must currently be set to 1 and 3.\n");
      fflush(stdout);
      
      endrun(110);
    }
    
  // First, set up the units system. These are conversions from code units to cgs. //
  my_units.density_units = unit_density;
  my_units.length_units = unit_length;
  my_units.time_units = unit_time;
  my_units.velocity_units = unit_velocity;
  
  if(All.ComovingIntegrationOn)
    {
      my_units.comoving_coordinates = 0; // 1 if cosmological sim, 0 if not //
      my_units.a_units = 1.0; // units for the expansion factor
      a_value = All.TimeBegin;
    }
  else
    {
      my_units.comoving_coordinates = 0; // 1 if cosmological sim, 0 if not //
      my_units.a_units = 1.0; // units for the expansion factor
      a_value = 1.0;
    }
    
  // Second, create a chemistry object for parameters and rate data. //
  if(set_default_chemistry_parameters() == 0)
    {
      printf("Error in set_default_chemistry_parameters.\n");
      fflush(stdout);
      
      endrun(111);
    }
    
  // Set parameter values for chemistry.
  grackle_data.use_grackle = 1;          
  grackle_data.with_radiative_cooling = 1;
  grackle_data.primordial_chemistry = All.GrackleChemistry;     // 0: tablized, 1: 6 species, 2: 9 species, 3: 12 species
  grackle_data.metal_cooling = All.MetalCooling;                // metal cooling on
  grackle_data.UVbackground = All.UVBackground;
  grackle_data.grackle_data_file = All.GrackleUVBackgroundData; // data file
  grackle_data.UVbackground_redshift_on = All.UVbackground_redshift_on;
  grackle_data.UVbackground_redshift_off = All.UVbackground_redshift_off;
  grackle_data.UVbackground_redshift_fullon = All.UVbackground_redshift_fullon;
  grackle_data.UVbackground_redshift_drop = All.UVbackground_redshift_drop;
  
  // Finally, initialize the chemistry object.
  if(initialize_chemistry_data(&my_units, a_value) == 0)
    {
      printf("Error in initialize_chemistry_data.\n");
      fflush(stdout);
      
      endrun(112);
    }
    
  // Set grid dimension and size. grid_start and grid_end are used to ignore ghost zones. //
  for(i = 0; i < 3; i++)
    {
      grid_dimension[i] = 1; // the active dimension not including ghost zones.
      grid_start[i] = 0;
      grid_end[i] = 0;
    }
    
  grid_dimension[0] = field_size;
  grid_end[0] = field_size - 1;
  
  return 0;
}

int docooling_grackle_wrapper(double metallicity, double dt, double anow, double density, double *u, double *ne, double *HI, double *HII, double *HeI, double *HeII, double *HeIII, double *HM, double *H2I, double *H2II, double *DI, double *DII, double *HDI)
{
  int i;
  
  for(i = 0; i < field_size; i++)
    {
      Density[i] = density;
      HI_density[i] = *HI * Density[i];
      HII_density[i] = *HII * Density[i];
      HeI_density[i] = *HeI * Density[i];
      HeII_density[i] = *HeII * Density[i];
      HeIII_density[i] = *HeIII * Density[i];
      HM_density[i] = *HM * Density[i];
      H2I_density[i] = *H2I * Density[i];
      H2II_density[i] = *H2II * Density[i];
      DI_density[i] = *DI * Density[i];
      DII_density[i] = *DII * Density[i];
      HDI_density[i] = *HDI * Density[i];
      e_density[i] = *ne * Density[i];
      metal_density[i] = metallicity * Density[i];
      x_velocity[i] = 0.0;
      y_velocity[i] = 0.0;
      z_velocity[i] = 0.0;
      energy[i] = *u;
    }
    
  if(grackle_data.primordial_chemistry)
    {
      if(solve_chemistry(&my_units, anow, dt, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, x_velocity, y_velocity, z_velocity, HI_density, HII_density, HM_density, HeI_density, HeII_density, HeIII_density, H2I_density, H2II_density, DI_density, DII_density, HDI_density, e_density, metal_density) == 0)
        {
	  printf("Error in solve_chemistry.\n");
          fflush(stdout);
          
          return 0;
        }
    }
  else
    {
      if(solve_chemistry_table(&my_units, anow, dt, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, x_velocity, y_velocity, z_velocity, metal_density) == 0)
        {
	  printf("Error in solve_chemistry.\n");
          fflush(stdout);
          
          return 0;
        }
    }
    
  // return updated chemistry and energy
  for(i = 0; i < field_size; i++)
    {
      *HI = HI_density[i] / Density[i];
      *HII = HII_density[i] / Density[i];
      *HeI = HeI_density[i] / Density[i];
      *HeII = HeII_density[i] / Density[i];
      *HeIII = HeIII_density[i] / Density[i];
      *HM = HM_density[i] / Density[i];
      *H2I = H2I_density[i] / Density[i];
      *H2II = H2II_density[i] / Density[i];
      *DI = DI_density[i] / Density[i];
      *DII = DII_density[i] / Density[i];
      *HDI = HDI_density[i] / Density[i];
      *ne = e_density[i] / Density[i];
      *u = energy[i];
    }
    
  return 1;
}

int getcoolingtime_grackle_wrapper(double metallicity, double anow, double density, double *cooling_time, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI)
{
  int i;
  
  for(i = 0; i < field_size; i++)
    {
      Density[i] = density;
      HI_density[i] = HI * Density[i];
      HII_density[i] = HII * Density[i];
      HeI_density[i] = HeI * Density[i];
      HeII_density[i] = HeII * Density[i];
      HeIII_density[i] = HeIII * Density[i];
      HM_density[i] = HM * Density[i];
      H2I_density[i] = H2I * Density[i];
      H2II_density[i] = H2II * Density[i];
      DI_density[i] = DI * Density[i];
      DII_density[i] = DII * Density[i];
      HDI_density[i] = HDI * Density[i];
      e_density[i] = ne * Density[i];
      metal_density[i] = metallicity * Density[i];
      x_velocity[i] = 0.0;
      y_velocity[i] = 0.0;
      z_velocity[i] = 0.0;
      energy[i] = u;
    }
    
  if(grackle_data.primordial_chemistry)
    {
      if(calculate_cooling_time(&my_units, anow, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, x_velocity, y_velocity, z_velocity, HI_density, HII_density, HM_density, HeI_density, HeII_density, HeIII_density, H2I_density, H2II_density, DI_density, DII_density, HDI_density, e_density, metal_density, cooling_time) == 0)
        {
          printf("Error in calculate_cooling_time.\n");
          fflush(stdout);
          
          return 0;
        }
    }
  else
    {
      if(calculate_cooling_time_table(&my_units, anow, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, x_velocity, y_velocity, z_velocity, metal_density, cooling_time) == 0)
        {
          printf("Error in calculate_cooling_time.\n");
          fflush(stdout);
          
          return 0;
        }
    }
    
  return 1;
}

int gettemperature_grackle_wrapper(double metallicity, double anow, double density, double *temperature, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI)
{
  int i;
  
  for(i = 0; i < field_size; i++)
    {
      Density[i] = density;
      HI_density[i] = HI * Density[i];
      HII_density[i] = HII * Density[i];
      HeI_density[i] = HeI * Density[i];
      HeII_density[i] = HeII * Density[i];
      HeIII_density[i] = HeIII * Density[i];
      HM_density[i] = HM * Density[i];
      H2I_density[i] = H2I * Density[i];
      H2II_density[i] = H2II * Density[i];
      DI_density[i] = DI * Density[i];
      DII_density[i] = DII * Density[i];
      HDI_density[i] = HDI * Density[i];
      e_density[i] = ne * Density[i];
      metal_density[i] = metallicity * Density[i];
      energy[i] = u;
    }
    
  if(grackle_data.primordial_chemistry)
    {
      if(calculate_temperature(&my_units, anow, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, HI_density, HII_density, HM_density, HeI_density, HeII_density, HeIII_density, H2I_density, H2II_density, DI_density, DII_density, HDI_density, e_density, metal_density, temperature) == 0)
        {
          printf("Error in calculate_temperature.\n");
          fflush(stdout);
          
          return 0;
        }
    }
  else
    {
      if(calculate_temperature_table(&my_units, anow, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, metal_density, temperature) == 0)
        {
          printf("Error in calculate_temperature.\n");
          fflush(stdout);
          
          return 0;
        }
    }
    
  return 1;
}

int getpressure_grackle_wrapper(double metallicity, double anow, double density, double *pressure, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI)
{
  int i;
  
  for(i = 0; i < field_size; i++)
    {
      Density[i] = density;
      HI_density[i] = HI * Density[i];
      HII_density[i] = HII * Density[i];
      HeI_density[i] = HeI * Density[i];
      HeII_density[i] = HeII * Density[i];
      HeIII_density[i] = HeIII * Density[i];
      HM_density[i] = HM * Density[i];
      H2I_density[i] = H2I * Density[i];
      H2II_density[i] = H2II * Density[i];
      DI_density[i] = DI * Density[i];
      DII_density[i] = DII * Density[i];
      HDI_density[i] = HDI * Density[i];
      e_density[i] = ne * Density[i];
      metal_density[i] = metallicity * Density[i];
      energy[i] = u;
    }
    
  if(grackle_data.primordial_chemistry)
    {
      if(calculate_pressure(&my_units, anow, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, HI_density, HII_density, HM_density, HeI_density, HeII_density, HeIII_density, H2I_density, H2II_density, DI_density, DII_density, HDI_density, e_density, metal_density, pressure) == 0)
        {
          printf("Error in calculate_pressure.\n");
          fflush(stdout);
          
          return 0;
        }
    }
  else
    {
      if(calculate_pressure_table(&my_units, anow, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, pressure) == 0)
        {
          printf("Error in calculate_pressure.\n");
          fflush(stdout);
          
          return 0;
        }
    }
    
  return 1;
}

int getgamma_grackle_wrapper(double metallicity, double anow, double density, double *gamma, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI)
{
  int i;
  
  for(i = 0; i < field_size; i++)
    {
      Density[i] = density;
      HI_density[i] = HI * Density[i];
      HII_density[i] = HII * Density[i];
      HeI_density[i] = HeI * Density[i];
      HeII_density[i] = HeII * Density[i];
      HeIII_density[i] = HeIII * Density[i];
      HM_density[i] = HM * Density[i];
      H2I_density[i] = H2I * Density[i];
      H2II_density[i] = H2II * Density[i];
      DI_density[i] = DI * Density[i];
      DII_density[i] = DII * Density[i];
      HDI_density[i] = HDI * Density[i];
      e_density[i] = ne * Density[i];
      metal_density[i] = metallicity * Density[i];
      energy[i] = u;
    }
    
  if(calculate_gamma(&my_units, anow, grid_rank, grid_dimension, grid_start, grid_end, Density, energy, HI_density, HII_density, HM_density, HeI_density, HeII_density, HeIII_density, H2I_density, H2II_density, DI_density, DII_density, HDI_density, e_density, metal_density, gamma) == 0)
    {
      printf("Error in calculate_gamma.\n");
      fflush(stdout);
      
      return 0;
    }
    
  return 1;
}
#endif
#endif
