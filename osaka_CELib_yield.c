#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <mpi.h>
#include "allvars.h"
#include "proto.h"

#ifdef OSAKA
#ifdef OSAKA_CELIB

#ifdef OSAKA_SNII
void calc_snii_yield(int target, double snii_energy_table[][N_MAX_FEEDBACK], double snii_mass_released_table[][N_MAX_FEEDBACK], double snii_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK], double *snii_energy, double *snii_mass_released, double snii_metal_yield[], int *nfb, double *next_snii_explosion_time)
{
  int i;
  int inow, inext;
  int id_metallicity, fbflagsnii;
  double dt, min_tsnii;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  double metallicity, metallicity_log, dmetallicity_log, metallicity_min_log, a, b;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
      stellarage = time - a2t(P[target].FormationTime);
      stellarage /= TIME_GYR;
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
      stellarage = factor_time * All.Time - factor_time * P[target].FormationTime;
      stellarage /= TIME_GYR;
    }
    
  metallicity = P[target].Metallicity;
  fbflagsnii = P[target].FBflagSNII;
  min_tsnii = log10(P[target].SNIIExplosionTime / TIME_GYR);
  
  dt = log10(P[target].SNIIEndTime / P[target].SNIIStartTime) / (double) All.SNIIEventNumber;
  *nfb = (int) ((log10(stellarage) - min_tsnii) / dt) + 1;
  
  if(*nfb <= 0)
    {
      *snii_energy = 0.0;
      *snii_mass_released = 0.0;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snii_metal_yield[i] = 0.0;
        }
        
      return;
    }
    
  inow = fbflagsnii;
  inext = fbflagsnii + *nfb;
  
  if(inext > All.SNIIEventNumber)
    {
      inext = All.SNIIEventNumber;
      *nfb = inext - fbflagsnii;
    }
    
  if(metallicity < METALIICITY_MIN_CELIB)
    {
#ifndef OSAKA_CELIB_POPIII
      *snii_energy = snii_energy_table[1][inext] - snii_energy_table[1][inow];
      *snii_mass_released = snii_mass_released_table[1][inext] - snii_mass_released_table[1][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snii_metal_yield[i] = snii_metal_yield_table[1][i][inext] - snii_metal_yield_table[1][i][inow];
        }
#else
      *snii_energy = snii_energy_table[0][inext] - snii_energy_table[0][inow];
      *snii_mass_released = snii_mass_released_table[0][inext] - snii_mass_released_table[0][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snii_metal_yield[i] = snii_metal_yield_table[0][i][inext] - snii_metal_yield_table[0][i][inow];
        }
#endif
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      *snii_energy = snii_energy_table[N_METALLCITY_BIN_CELIB - 1][inext] - snii_energy_table[N_METALLCITY_BIN_CELIB - 1][inow];
      *snii_mass_released = snii_mass_released_table[N_METALLCITY_BIN_CELIB - 1][inext] - snii_mass_released_table[N_METALLCITY_BIN_CELIB - 1][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snii_metal_yield[i] = snii_metal_yield_table[N_METALLCITY_BIN_CELIB - 1][i][inext] - snii_metal_yield_table[N_METALLCITY_BIN_CELIB - 1][i][inow];
        }
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_POPIII_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      b = snii_energy_table[id_metallicity][inext] - snii_energy_table[id_metallicity][inow];
      a = (snii_energy_table[id_metallicity + 1][inext] - snii_energy_table[id_metallicity + 1][inow] - b) / METALLICITY_BIN_LOG;
      *snii_energy = a * dmetallicity_log + b;
      
      b = snii_mass_released_table[id_metallicity][inext] - snii_mass_released_table[id_metallicity][inow];
      a = (snii_mass_released_table[id_metallicity + 1][inext] - snii_mass_released_table[id_metallicity + 1][inow] - b) / METALLICITY_BIN_LOG;
      *snii_mass_released = a * dmetallicity_log + b;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          b = snii_metal_yield_table[id_metallicity][i][inext] - snii_metal_yield_table[id_metallicity][i][inow];
          a = (snii_metal_yield_table[id_metallicity + 1][i][inext] - snii_metal_yield_table[id_metallicity + 1][i][inow] - b) / METALLICITY_BIN_LOG;
          snii_metal_yield[i] = a * dmetallicity_log + b;
        }
    }
    
  min_tsnii += dt * (double) *nfb;
  min_tsnii = pow(10.0, min_tsnii) * TIME_GYR;
  *next_snii_explosion_time = min_tsnii;
}

void calc_snii_yield_table(struct CELIB_YIELD CELib_yield, double snii_energy_table[][N_MAX_FEEDBACK], double snii_mass_released_table[][N_MAX_FEEDBACK], double snii_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK])
{
  int i, j, k;
  int imin_tsnii, imax_tsnii, itnow;
  double dt, min_tsnii, max_tsnii;
  double a, b, tnow, tdif;
  
#ifdef OSAKA_SN_ENERGY_LOWER_LIMIT
  double nsnii_min, fac_nsnii, nsnii;
#endif

  for(i = 0; i < N_METALLCITY_BIN_CELIB; i++)
    {
      imin_tsnii = (int) (log10(CELib_yield.snii_duration_time[i][0] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      imax_tsnii = (int) (log10(CELib_yield.snii_duration_time[i][1] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      min_tsnii = log10(CELib_yield.stellar_age[imin_tsnii]);
      max_tsnii = log10(CELib_yield.stellar_age[imax_tsnii]);
      dt = (max_tsnii - min_tsnii) / (double) All.SNIIEventNumber;
      
      snii_energy_table[i][0] = 0.0;
      snii_energy_table[i][All.SNIIEventNumber] = CELib_yield.snii_energy_cum[i][N_TIME_CELIB - 1];
      
      for(j = 1; j < All.SNIIEventNumber; j++)
        {
          tnow = min_tsnii + dt * (double) j;
          itnow = (int) ((tnow - log10(CELib_yield.stellar_age[0])) / DT_LOG_CELIB);
          tdif = tnow - log10(CELib_yield.stellar_age[itnow]);
          
          a = (CELib_yield.snii_energy_cum[i][itnow + 1] - CELib_yield.snii_energy_cum[i][itnow]) / DT_LOG_CELIB;
          b = a * tdif + CELib_yield.snii_energy_cum[i][itnow];
          snii_energy_table[i][j] = b;
        }
    }
    
#ifdef OSAKA_SN_ENERGY_LOWER_LIMIT
  fac_nsnii = All.InitialGasMass / (double) All.Nspawn * All.UnitMass_in_g / SOLAR_MASS / All.Energy_per_SNII / (double) All.SNIIEventNumber;
  
  for(i = 0, nsnii_min = 1.0e33; i < N_METALLCITY_BIN_CELIB; i++)
    {
      nsnii = fac_nsnii * snii_energy_table[i][All.SNIIEventNumber];
      
      if(nsnii_min > nsnii)
        nsnii_min = nsnii;
    }
    
  if(ThisTask == 0)
    printf("Minimum number of SNII is %e\n", nsnii_min);
    
  if(nsnii_min <= NSN_MINIMUM)
    {
      if(ThisTask == 0)
        printf("All.SNIIEventNumber is too large. Be small this value!\n");
        
      endrun(0405);
    }
#endif

  if(ThisTask == 0)
    {
      printf("CELib SNII energy table\n");
      fflush(stdout);
      
      for(j = 0; j <= All.SNIIEventNumber; j++)
        {
          tnow = min_tsnii + dt * (double) j;
          printf("%e %e %e %e %e %e %e %e\n", tnow, snii_energy_table[0][j], snii_energy_table[1][j], snii_energy_table[2][j], snii_energy_table[3][j], snii_energy_table[4][j], snii_energy_table[5][j], snii_energy_table[6][j]);
          fflush(stdout);
        }
    }
    
  for(i = 0; i < N_METALLCITY_BIN_CELIB; i++)
    {
      imin_tsnii = (int) (log10(CELib_yield.snii_duration_time[i][0] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      imax_tsnii = (int) (log10(CELib_yield.snii_duration_time[i][1] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      min_tsnii = log10(CELib_yield.stellar_age[imin_tsnii]);
      max_tsnii = log10(CELib_yield.stellar_age[imax_tsnii]);
      dt = (max_tsnii - min_tsnii) / (double) All.SNIIEventNumber;
      
      snii_mass_released_table[i][0] = 0.0;
      snii_mass_released_table[i][All.SNIIEventNumber] = CELib_yield.snii_mass_released[i][N_TIME_CELIB - 1];
      
      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
        {
          snii_metal_yield_table[i][k][0] = 0.0;
          snii_metal_yield_table[i][k][All.SNIIEventNumber] = CELib_yield.snii_yield_cum[i][k][N_TIME_CELIB - 1];
        }
        
      for(j = 1; j < All.SNIIEventNumber; j++)
        {
          tnow = min_tsnii + dt * (double) j;
          itnow = (int) ((tnow - log10(CELib_yield.stellar_age[0])) / DT_LOG_CELIB);
          tdif = tnow - log10(CELib_yield.stellar_age[itnow]);
          
          a = (CELib_yield.snii_mass_released[i][itnow + 1] - CELib_yield.snii_mass_released[i][itnow]) / DT_LOG_CELIB;
          b = a * tdif + CELib_yield.snii_mass_released[i][itnow];
          snii_mass_released_table[i][j] = b;
          
          b = 0;
          
          for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
            {
              a = (CELib_yield.snii_yield_cum[i][k][itnow + 1] - CELib_yield.snii_yield_cum[i][k][itnow]) / DT_LOG_CELIB;
              b = a * tdif + CELib_yield.snii_yield_cum[i][k][itnow];
              snii_metal_yield_table[i][k][j] = b;
            }
        }
    }
    
  if(ThisTask == 0)
    {
      printf("CELib SNII mass released table\n");
      fflush(stdout);
      
      for(j = 0; j <= All.SNIIEventNumber; j++)
        {
          tnow = min_tsnii + dt * (double) j;
          printf("%e %e %e %e %e %e %e %e\n", tnow, snii_mass_released_table[0][j], snii_mass_released_table[1][j], snii_mass_released_table[2][j], snii_mass_released_table[3][j], snii_mass_released_table[4][j], snii_mass_released_table[5][j], snii_mass_released_table[6][j]);
          fflush(stdout);
        }
    }
}

void calc_instantaneous_snii_yield(double metallicity, struct CELIB_YIELD CELib_yield, double *snii_energy, double *snii_mass_released, double snii_metal_yield[])
{
  int i;
  int id_metallicity;
  double metallicity_log, dmetallicity_log, metallicity_min_log, a, b;
  
  if(metallicity < METALIICITY_MIN_CELIB)
    {
#ifndef OSAKA_CELIB_POPIII
      *snii_energy = CELib_yield.snii_energy_cum[1][N_TIME_CELIB - 1];
      *snii_mass_released = CELib_yield.snii_mass_released[1][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snii_metal_yield[i] = CELib_yield.snii_yield_cum[1][i][N_TIME_CELIB - 1];
        }
#else
      *snii_energy = CELib_yield.snii_energy_cum[0][N_TIME_CELIB - 1];
      *snii_mass_released = CELib_yield.snii_mass_released[0][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snii_metal_yield[i] = CELib_yield.snii_yield_cum[0][i][N_TIME_CELIB - 1];
        }
#endif
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      *snii_energy = CELib_yield.snii_energy_cum[N_METALLCITY_BIN_CELIB - 1][N_TIME_CELIB - 1];
      *snii_mass_released = CELib_yield.snii_mass_released[N_METALLCITY_BIN_CELIB - 1][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snii_metal_yield[i] = CELib_yield.snii_yield_cum[N_METALLCITY_BIN_CELIB - 1][i][N_TIME_CELIB - 1];
        }
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_POPIII_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      a = (CELib_yield.snii_energy_cum[id_metallicity + 1][N_TIME_CELIB - 1] - CELib_yield.snii_energy_cum[id_metallicity][N_TIME_CELIB - 1]) / METALLICITY_BIN_LOG;
      b = CELib_yield.snii_energy_cum[id_metallicity][N_TIME_CELIB - 1];
      *snii_energy = a * dmetallicity_log + b;
      
      a = (CELib_yield.snii_mass_released[id_metallicity + 1][N_TIME_CELIB - 1] - CELib_yield.snii_mass_released[id_metallicity][N_TIME_CELIB - 1]) / METALLICITY_BIN_LOG;
      b = CELib_yield.snii_mass_released[id_metallicity][N_TIME_CELIB - 1];
      *snii_mass_released = a * dmetallicity_log + b;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          a = (CELib_yield.snii_yield_cum[id_metallicity + 1][i][N_TIME_CELIB - 1] - CELib_yield.snii_yield_cum[id_metallicity][i][N_TIME_CELIB - 1]) / METALLICITY_BIN_LOG;
          b = CELib_yield.snii_yield_cum[id_metallicity][i][N_TIME_CELIB - 1];
          snii_metal_yield[i] = a * dmetallicity_log + b;
        }
    }
}

void calc_instantaneous_snii_feedback_time(int target, int id_particle, double metallicity, struct CELIB_YIELD CELib_yield, struct CELIB_YIELD_DIF CELib_yield_dif, double *snii_explosion_time)
{
  int i, id_metallicity, itemp;
  double metallicity_log, dmetallicity_log, metallicity_min_log, time_min_log, time_now_log, dt_dif_log, a, b, prob;
  double snii_event_rate[N_TIME_CELIB], snii_time_min_log, snii_time_max_log, snii_duration_time_log, snii_event_now;
  
  if(metallicity < METALIICITY_MIN_CELIB)
    {
      for(i = 0; i < N_TIME_CELIB; i++)
        snii_event_rate[i] = CELib_yield_dif.snii_energy_dif[1][i];
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      for(i = 0; i < N_TIME_CELIB; i++)
        snii_event_rate[i] = CELib_yield_dif.snii_energy_dif[N_METALLCITY_BIN_CELIB - 1][i];
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      for(i = 0; i < N_TIME_CELIB; i++)
        {
          a = (CELib_yield_dif.snii_energy_dif[id_metallicity + 1][i] - CELib_yield_dif.snii_energy_dif[id_metallicity][i]) / METALLICITY_BIN_LOG;
          b = CELib_yield_dif.snii_energy_dif[id_metallicity][i];
          snii_event_rate[i] = a * dmetallicity_log + b;
        }
    }
    
  init_genrand(id_particle + 1);
  time_min_log = log10(TIME_MIN_CELIB);
  
  snii_time_min_log = log10(P[target].SNIIStartTime);
  snii_time_max_log = log10(P[target].SNIIEndTime);
  snii_duration_time_log = snii_time_max_log - snii_time_min_log;
  
  for(; ;)
    {
      time_now_log = snii_time_min_log + genrand_real2() * snii_duration_time_log;
      itemp = (int) ((time_now_log - time_min_log) / DT_LOG_CELIB);
      a = (snii_event_rate[itemp + 1] - snii_event_rate[itemp]) / DT_LOG_CELIB;
      b = snii_event_rate[itemp];
      dt_dif_log = time_now_log - log10(CELib_yield.stellar_age[itemp]);
      snii_event_now = a * dt_dif_log + b;
      prob = 1.1 * genrand_real2();
      
      if(prob < snii_event_now)
        {
          *snii_explosion_time = pow(10.0, time_now_log) * TIME_GYR;
          break;
        }
    }
}
#endif

#ifdef OSAKA_SNIA
void calc_snia_yield(int target, double snia_energy_table[][N_MAX_FEEDBACK], double snia_mass_released_table[][N_MAX_FEEDBACK], double snia_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK], double *snia_energy, double *snia_mass_released, double snia_metal_yield[], int *nfb, double *next_snia_explosion_time)
{
  int i;
  int inow, inext;
  int id_metallicity, fbflagsnia;
  double dt, min_tsnia;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  double metallicity, metallicity_log, dmetallicity_log, metallicity_min_log, a, b;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
      stellarage = time - a2t(P[target].FormationTime);
      stellarage /= TIME_GYR;
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
      stellarage = factor_time * All.Time - factor_time * P[target].FormationTime;
      stellarage /= TIME_GYR;
    }
    
  metallicity = P[target].Metallicity;
  fbflagsnia = P[target].FBflagSNIa;
  min_tsnia = log10(P[target].SNIaExplosionTime / TIME_GYR);
  
  dt = log10(P[target].SNIaEndTime / P[target].SNIaStartTime) / (double) All.SNIaEventNumber;
  *nfb = (int) ((log10(stellarage) - min_tsnia) / dt) + 1;
  
  if(*nfb <= 0)
    {
      *snia_energy = 0.0;
      *snia_mass_released = 0.0;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snia_metal_yield[i] = 0.0;
        }
        
      return;
    }
    
  inow = fbflagsnia;
  inext = fbflagsnia + *nfb;
  
  if(inext > All.SNIaEventNumber)
    {
      inext = All.SNIaEventNumber;
      *nfb = inext - fbflagsnia;
    }
    
  if(metallicity < METALIICITY_MIN_CELIB)
    {
#ifndef OSAKA_CELIB_POPIII
      *snia_energy = snia_energy_table[1][inext] - snia_energy_table[1][inow];
      *snia_mass_released = snia_mass_released_table[1][inext] - snia_mass_released_table[1][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snia_metal_yield[i] = snia_metal_yield_table[1][i][inext] - snia_metal_yield_table[1][i][inow];
        }
#else
      *snia_energy = snia_energy_table[0][inext] - snia_energy_table[0][inow];
      *snia_mass_released = snia_mass_released_table[0][inext] - snia_mass_released_table[0][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snia_metal_yield[i] = snia_metal_yield_table[0][i][inext] - snia_metal_yield_table[0][i][inow];
        }
#endif
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      *snia_energy = snia_energy_table[N_METALLCITY_BIN_CELIB - 1][inext] - snia_energy_table[N_METALLCITY_BIN_CELIB - 1][inow];
      *snia_mass_released = snia_mass_released_table[N_METALLCITY_BIN_CELIB - 1][inext] - snia_mass_released_table[N_METALLCITY_BIN_CELIB - 1][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snia_metal_yield[i] = snia_metal_yield_table[N_METALLCITY_BIN_CELIB - 1][i][inext] - snia_metal_yield_table[N_METALLCITY_BIN_CELIB - 1][i][inow];
        }
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_POPIII_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      b = snia_energy_table[id_metallicity][inext] - snia_energy_table[id_metallicity][inow];
      a = (snia_energy_table[id_metallicity + 1][inext] - snia_energy_table[id_metallicity + 1][inow] - b) / METALLICITY_BIN_LOG;
      *snia_energy = a * dmetallicity_log + b;
      
      b = snia_mass_released_table[id_metallicity][inext] - snia_mass_released_table[id_metallicity][inow];
      a = (snia_mass_released_table[id_metallicity + 1][inext] - snia_mass_released_table[id_metallicity + 1][inow] - b) / METALLICITY_BIN_LOG;
      *snia_mass_released = a * dmetallicity_log + b;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          b = snia_metal_yield_table[id_metallicity][i][inext] - snia_metal_yield_table[id_metallicity][i][inow];
          a = (snia_metal_yield_table[id_metallicity + 1][i][inext] - snia_metal_yield_table[id_metallicity + 1][i][inow] - b) / METALLICITY_BIN_LOG;
          snia_metal_yield[i] = a * dmetallicity_log + b;
        }
    }
    
  min_tsnia += dt * (double) *nfb;
  min_tsnia = pow(10.0, min_tsnia) * TIME_GYR;
  *next_snia_explosion_time = min_tsnia;
}

void calc_snia_yield_table(struct CELIB_YIELD CELib_yield, double snia_energy_table[][N_MAX_FEEDBACK], double snia_mass_released_table[][N_MAX_FEEDBACK], double snia_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK])
{
  int i, j, k;
  int imin_tsnia, imax_tsnia, itnow;
  double dt, min_tsnia, max_tsnia;
  double a, b, tnow, tdif;

#ifdef OSAKA_SN_ENERGY_LOWER_LIMIT
  double nsnia_min, fac_nsnia, nsnia;
#endif

  for(i = 0; i < N_METALLCITY_BIN_CELIB; i++)
    {
      imin_tsnia = (int) (log10(CELib_yield.snia_duration_time[i][0] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      imax_tsnia = (int) (log10(CELib_yield.snia_duration_time[i][1] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      min_tsnia = log10(CELib_yield.stellar_age[imin_tsnia]);
      max_tsnia = log10(CELib_yield.stellar_age[imax_tsnia]);
      dt = (max_tsnia - min_tsnia) / (double) All.SNIaEventNumber;
      
      snia_energy_table[i][0] = 0.0;
      snia_energy_table[i][All.SNIaEventNumber] = CELib_yield.snia_energy_cum[i][N_TIME_CELIB - 1];
      
      for(j = 1; j < All.SNIaEventNumber; j++)
        {
          tnow = min_tsnia + dt * (double) j;
          itnow = (int) ((tnow - log10(CELib_yield.stellar_age[0])) / DT_LOG_CELIB);
          tdif = tnow - log10(CELib_yield.stellar_age[itnow]);
          
          a = (CELib_yield.snia_energy_cum[i][itnow + 1] - CELib_yield.snia_energy_cum[i][itnow]) / DT_LOG_CELIB;
          b = a * tdif + CELib_yield.snia_energy_cum[i][itnow];
          snia_energy_table[i][j] = b;
        }
    }
    
#ifdef OSAKA_SN_ENERGY_LOWER_LIMIT
  fac_nsnia = All.InitialGasMass / (double) All.Nspawn * All.UnitMass_in_g / SOLAR_MASS / All.Energy_per_SNIa / (double) All.SNIaEventNumber;
    
  for(i = 0, nsnia_min = 1.0e33; i < N_METALLCITY_BIN_CELIB; i++)
    {
      nsnia = fac_nsnia * snia_energy_table[i][All.SNIaEventNumber];
      
      if(nsnia_min > nsnia)
        nsnia_min = nsnia;
    }
    
  if(ThisTask == 0)
    printf("Minimum number of SNIa is %e\n", nsnia_min);
    
  if(nsnia_min <= NSN_MINIMUM)
    {
      if(ThisTask == 0)
        printf("All.SNIaEventNumber is too large. Be small this value!\n");
        
      endrun(0405);
    }
#endif

  if(ThisTask == 0)
    {
      printf("CELib SNIa energy table\n");
      fflush(stdout);
      
      for(j = 0; j <= All.SNIaEventNumber; j++)
        {
          tnow = min_tsnia + dt * (double) j;
          printf("%e %e %e %e %e %e %e %e\n", tnow, snia_energy_table[0][j], snia_energy_table[1][j], snia_energy_table[2][j], snia_energy_table[3][j], snia_energy_table[4][j], snia_energy_table[5][j], snia_energy_table[6][j]);
          fflush(stdout);
        }
    }
    
  for(i = 0; i < N_METALLCITY_BIN_CELIB; i++)
    {
      imin_tsnia = (int) (log10(CELib_yield.snia_duration_time[i][0] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      imax_tsnia = (int) (log10(CELib_yield.snia_duration_time[i][1] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      min_tsnia = log10(CELib_yield.stellar_age[imin_tsnia]);
      max_tsnia = log10(CELib_yield.stellar_age[imax_tsnia]);
      dt = (max_tsnia - min_tsnia) / (double) All.SNIaEventNumber;
      
      snia_mass_released_table[i][0] = 0.0;
      snia_mass_released_table[i][All.SNIaEventNumber] = CELib_yield.snia_mass_released[i][N_TIME_CELIB - 1];
      
      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
        {
          snia_metal_yield_table[i][k][0] = 0.0;
          snia_metal_yield_table[i][k][All.SNIaEventNumber] = CELib_yield.snia_yield_cum[i][k][N_TIME_CELIB - 1];
        }
        
      for(j = 1; j < All.SNIaEventNumber; j++)
        {
          tnow = min_tsnia + dt * (double) j;
          itnow = (int) ((tnow - log10(CELib_yield.stellar_age[0])) / DT_LOG_CELIB);
          tdif = tnow - log10(CELib_yield.stellar_age[itnow]);
          
          a = (CELib_yield.snia_mass_released[i][itnow + 1] - CELib_yield.snia_mass_released[i][itnow]) / DT_LOG_CELIB;
          b = a * tdif + CELib_yield.snia_mass_released[i][itnow];
          snia_mass_released_table[i][j] = b;
          
          b = 0;
          
          for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
            {
              a = (CELib_yield.snia_yield_cum[i][k][itnow + 1] - CELib_yield.snia_yield_cum[i][k][itnow]) / DT_LOG_CELIB;
              b = a * tdif + CELib_yield.snia_yield_cum[i][k][itnow];
              snia_metal_yield_table[i][k][j] = b;
            }
        }
    }
    
  if(ThisTask == 0)
    {
      printf("CELib SNIa mass released table\n");
      fflush(stdout);
      
      for(j = 0; j <= All.SNIaEventNumber; j++)
        {
          tnow = min_tsnia + dt * (double) j;
          printf("%e %e %e %e %e %e %e %e\n", tnow, snia_mass_released_table[0][j], snia_mass_released_table[1][j], snia_mass_released_table[2][j], snia_mass_released_table[3][j], snia_mass_released_table[4][j], snia_mass_released_table[5][j], snia_mass_released_table[6][j]);
          fflush(stdout);
        }
    }
}

void calc_instantaneous_snia_yield(double metallicity, struct CELIB_YIELD CELib_yield, double *snia_energy, double *snia_mass_released, double snia_metal_yield[])
{
  int i;
  int id_metallicity;
  double metallicity_log, dmetallicity_log, metallicity_min_log, a, b;
  
  if(metallicity < METALIICITY_MIN_CELIB)
    {
#ifndef OSAKA_CELIB_POPIII
      *snia_energy = CELib_yield.snia_energy_cum[1][N_TIME_CELIB - 1];
      *snia_mass_released = CELib_yield.snia_mass_released[1][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snia_metal_yield[i] = CELib_yield.snia_yield_cum[1][i][N_TIME_CELIB - 1];
        }
#else
      *snia_energy = CELib_yield.snia_energy_cum[0][N_TIME_CELIB - 1];
      *snia_mass_released = CELib_yield.snia_mass_released[0][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snia_metal_yield[i] = CELib_yield.snia_yield_cum[0][i][N_TIME_CELIB - 1];
        }
#endif
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      *snia_energy = CELib_yield.snia_energy_cum[N_METALLCITY_BIN_CELIB - 1][N_TIME_CELIB - 1];
      *snia_mass_released = CELib_yield.snia_mass_released[N_METALLCITY_BIN_CELIB - 1][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          snia_metal_yield[i] = CELib_yield.snia_yield_cum[N_METALLCITY_BIN_CELIB - 1][i][N_TIME_CELIB - 1];
        }
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_POPIII_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      a = (CELib_yield.snia_energy_cum[id_metallicity + 1][N_TIME_CELIB - 1] - CELib_yield.snia_energy_cum[id_metallicity][N_TIME_CELIB - 1]) / METALLICITY_BIN_LOG;
      b = CELib_yield.snia_energy_cum[id_metallicity][N_TIME_CELIB - 1];
      *snia_energy = a * dmetallicity_log + b;
      
      a = (CELib_yield.snia_mass_released[id_metallicity + 1][N_TIME_CELIB - 1] - CELib_yield.snia_mass_released[id_metallicity][N_TIME_CELIB - 1]) / METALLICITY_BIN_LOG;
      b = CELib_yield.snia_mass_released[id_metallicity][N_TIME_CELIB - 1];
      *snia_mass_released = a * dmetallicity_log + b;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          a = (CELib_yield.snia_yield_cum[id_metallicity + 1][i][N_TIME_CELIB - 1] - CELib_yield.snia_yield_cum[id_metallicity][i][N_TIME_CELIB - 1]) / METALLICITY_BIN_LOG;
          b = CELib_yield.snia_yield_cum[id_metallicity][i][N_TIME_CELIB - 1];
          snia_metal_yield[i] = a * dmetallicity_log + b;
        }
    }
}

void calc_instantaneous_snia_feedback_time(int target, int id_particle, double metallicity, struct CELIB_YIELD CELib_yield, struct CELIB_YIELD_DIF CELib_yield_dif, double *snia_explosion_time)
{
  int i, id_metallicity, itemp;
  double metallicity_log, dmetallicity_log, metallicity_min_log, time_min_log, time_now_log, dt_dif_log, a, b, prob;
  double snia_event_rate[N_TIME_CELIB], snia_time_min_log, snia_time_max_log, snia_duration_time_log, snia_event_now;
  
  if(metallicity < METALIICITY_MIN_CELIB)
    {
      for(i = 0; i < N_TIME_CELIB; i++)
        snia_event_rate[i] = CELib_yield_dif.snia_energy_dif[1][i];
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      for(i = 0; i < N_TIME_CELIB; i++)
        snia_event_rate[i] = CELib_yield_dif.snia_energy_dif[N_METALLCITY_BIN_CELIB - 1][i];
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      for(i = 0; i < N_TIME_CELIB; i++)
        {
          a = (CELib_yield_dif.snia_energy_dif[id_metallicity + 1][i] - CELib_yield_dif.snia_energy_dif[id_metallicity][i]) / METALLICITY_BIN_LOG;
          b = CELib_yield_dif.snia_energy_dif[id_metallicity][i];
          snia_event_rate[i] = a * dmetallicity_log + b;
        }
    }
    
  init_genrand(id_particle + 1);
  time_min_log = log10(TIME_MIN_CELIB);
  
  snia_time_min_log = log10(P[target].SNIaStartTime);
  snia_time_max_log = log10(P[target].SNIaEndTime);
  snia_duration_time_log = snia_time_max_log - snia_time_min_log;
  
  for(; ;)
    {
      time_now_log = snia_time_min_log + genrand_real2() * snia_duration_time_log;
      itemp = (int) ((time_now_log - time_min_log) / DT_LOG_CELIB);
      a = (snia_event_rate[itemp + 1] - snia_event_rate[itemp]) / DT_LOG_CELIB;
      b = snia_event_rate[itemp];
      dt_dif_log = time_now_log - log10(CELib_yield.stellar_age[itemp]);
      snia_event_now = a * dt_dif_log + b;
      prob = 1.1 * genrand_real2();
      
      if(prob < snia_event_now)
        {
          *snia_explosion_time = pow(10.0, time_now_log) * TIME_GYR;
          break;
        }
    }
}
#endif

#ifdef OSAKA_AGB
void calc_agb_yield(int target, double agb_mass_released_table[][N_MAX_FEEDBACK], double agb_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK], double *agb_mass_released, double agb_metal_yield[], int *nfb, double *next_agb_explosion_time)
{
  int i;
  int inow, inext;
  int id_metallicity, fbflagagb;
  double dt, min_tagb;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  double metallicity, metallicity_log, dmetallicity_log, metallicity_min_log, a, b;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
      stellarage = time - a2t(P[target].FormationTime);
      stellarage /= TIME_GYR;
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
      stellarage = factor_time * All.Time - factor_time * P[target].FormationTime;
      stellarage /= TIME_GYR;
    }
    
  metallicity = P[target].Metallicity;
  fbflagagb = P[target].FBflagAGB;
  min_tagb = log10(P[target].AGBExplosionTime / TIME_GYR);
  
  dt = log10(P[target].AGBEndTime / P[target].AGBStartTime) / (double) All.SNIIEventNumber;
  *nfb = (int) ((log10(stellarage) - min_tagb) / dt) + 1;
  
  if(*nfb <= 0)
    {
      *agb_mass_released = 0.0;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          agb_metal_yield[i] = 0.0;
        }
        
      return;
    }
    
  inow = fbflagagb;
  inext = fbflagagb + *nfb;
  
  if(inext > All.AGBEventNumber)
    {
      inext = All.AGBEventNumber;
      *nfb = inext - fbflagagb;
    }
    
  if(metallicity < METALIICITY_MIN_CELIB)
    {
#ifndef OSAKA_CELIB_POPIII
      *agb_mass_released = agb_mass_released_table[1][inext] - agb_mass_released_table[1][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          agb_metal_yield[i] = agb_metal_yield_table[1][i][inext] - agb_metal_yield_table[1][i][inow];
        }
#else
      *agb_mass_released = agb_mass_released_table[0][inext] - agb_mass_released_table[0][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          agb_metal_yield[i] = agb_metal_yield_table[0][i][inext] - agb_metal_yield_table[0][i][inow];
        }
#endif
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      *agb_mass_released = agb_mass_released_table[N_METALLCITY_BIN_CELIB - 1][inext] - agb_mass_released_table[N_METALLCITY_BIN_CELIB - 1][inow];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          agb_metal_yield[i] = agb_metal_yield_table[N_METALLCITY_BIN_CELIB - 1][i][inext] - agb_metal_yield_table[N_METALLCITY_BIN_CELIB - 1][i][inow];
        }
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_POPIII_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      b = agb_mass_released_table[id_metallicity][inext] - agb_mass_released_table[id_metallicity][inow];
      a = (agb_mass_released_table[id_metallicity + 1][inext] - agb_mass_released_table[id_metallicity + 1][inow] - b) / METALLICITY_BIN_LOG;
      *agb_mass_released = a * dmetallicity_log + b;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          b = agb_metal_yield_table[id_metallicity][i][inext] - agb_metal_yield_table[id_metallicity][i][inow];
          a = (agb_metal_yield_table[id_metallicity + 1][i][inext] - agb_metal_yield_table[id_metallicity + 1][i][inow] - b) / METALLICITY_BIN_LOG;
          agb_metal_yield[i] = a * dmetallicity_log + b;
        }
    }
    
  min_tagb += dt * (double) *nfb;
  min_tagb = pow(10.0, min_tagb) * TIME_GYR;
  *next_agb_explosion_time = min_tagb;
}

void calc_agb_yield_table(struct CELIB_YIELD CELib_yield, double agb_mass_released_table[][N_MAX_FEEDBACK], double agb_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK])
{
  int i, j, k;
  int imin_tagb, imax_tagb, itnow;
  double dt, min_tagb, max_tagb;
  double a, b, tnow, tdif;
  
  for(i = 0; i < N_METALLCITY_BIN_CELIB; i++)
    {
      imin_tagb = (int) (log10(CELib_yield.agb_duration_time[i][0] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      imax_tagb = (int) (log10(CELib_yield.agb_duration_time[i][1] / CELib_yield.stellar_age[0]) / DT_LOG_CELIB);
      min_tagb = log10(CELib_yield.stellar_age[imin_tagb]);
      max_tagb = log10(CELib_yield.stellar_age[imax_tagb]);
      dt = (max_tagb - min_tagb) / (double) All.AGBEventNumber;
      
      agb_mass_released_table[i][0] = 0.0;
      agb_mass_released_table[i][All.AGBEventNumber] = CELib_yield.agb_mass_released[i][N_TIME_CELIB - 1];
      
      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
        {
          agb_metal_yield_table[i][k][0] = 0.0;
          agb_metal_yield_table[i][k][All.AGBEventNumber] = CELib_yield.agb_yield_cum[i][k][N_TIME_CELIB - 1];
        }
        
      for(j = 1; j < All.AGBEventNumber; j++)
        {
          tnow = min_tagb + dt * (double) j;
          itnow = (int) ((tnow - log10(CELib_yield.stellar_age[0])) / DT_LOG_CELIB);
          tdif = tnow - log10(CELib_yield.stellar_age[itnow]);
          
          a = (CELib_yield.agb_mass_released[i][itnow + 1] - CELib_yield.agb_mass_released[i][itnow]) / DT_LOG_CELIB;
          b = a * tdif + CELib_yield.agb_mass_released[i][itnow];
          agb_mass_released_table[i][j] = b;
          
          b = 0.0;
          
          for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
            {
              a = (CELib_yield.agb_yield_cum[i][k][itnow + 1] - CELib_yield.agb_yield_cum[i][k][itnow]) / DT_LOG_CELIB;
              b = a * tdif + CELib_yield.agb_yield_cum[i][k][itnow];
              agb_metal_yield_table[i][k][j] = b;
            }
        }
    }
    
  if(ThisTask == 0)
    {
      printf("CELib AGB mass released table\n");
      fflush(stdout);
      
      for(j = 0; j <= All.AGBEventNumber; j++)
        {
          tnow = min_tagb + dt * (double) j;
          printf("%e %e %e %e %e %e %e %e\n", tnow, agb_mass_released_table[0][j], agb_mass_released_table[1][j], agb_mass_released_table[2][j], agb_mass_released_table[3][j], agb_mass_released_table[4][j], agb_mass_released_table[5][j], agb_mass_released_table[6][j]);
          fflush(stdout);
        }
    }
}

void calc_instantaneous_agb_yield(double metallicity, struct CELIB_YIELD CELib_yield, double *agb_mass_released, double agb_metal_yield[])
{
  int i;
  int id_metallicity;
  double metallicity_log, dmetallicity_log, metallicity_min_log, a, b;
  
  if(metallicity < METALIICITY_MIN_CELIB)
    {
#ifndef OSAKA_CELIB_POPIII
      *agb_mass_released = CELib_yield.agb_mass_released[1][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          agb_metal_yield[i] = CELib_yield.agb_yield_cum[1][i][N_TIME_CELIB - 1];
        }
#else
      *agb_mass_released = CELib_yield.agb_mass_released[0][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          agb_metal_yield[i] = CELib_yield.agb_yield_cum[0][i][N_TIME_CELIB - 1];
        }
#endif
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      *agb_mass_released = CELib_yield.agb_mass_released[N_METALLCITY_BIN_CELIB - 1][N_TIME_CELIB - 1];
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          agb_metal_yield[i] = CELib_yield.agb_yield_cum[N_METALLCITY_BIN_CELIB - 1][i][N_TIME_CELIB - 1];
        }
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_POPIII_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      a = (CELib_yield.agb_mass_released[id_metallicity + 1][N_TIME_CELIB - 1] - CELib_yield.agb_mass_released[id_metallicity][N_TIME_CELIB - 1]) / METALLICITY_BIN_LOG;
      b = CELib_yield.agb_mass_released[id_metallicity][N_TIME_CELIB - 1];
      *agb_mass_released = a * dmetallicity_log + b;
      
      for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
        {
          a = (CELib_yield.agb_yield_cum[id_metallicity + 1][i][N_TIME_CELIB - 1] - CELib_yield.agb_yield_cum[id_metallicity][i][N_TIME_CELIB - 1]) / METALLICITY_BIN_LOG;
          b = CELib_yield.agb_yield_cum[id_metallicity][i][N_TIME_CELIB - 1];
          agb_metal_yield[i] = a * dmetallicity_log + b;
        }
    }
}

void calc_instantaneous_agb_feedback_time(int target, int id_particle, double metallicity, struct CELIB_YIELD CELib_yield, struct CELIB_YIELD_DIF CELib_yield_dif, double *agb_explosion_time)
{
  int i, id_metallicity, itemp;
  double metallicity_log, dmetallicity_log, metallicity_min_log, time_min_log, time_now_log, dt_dif_log, a, b, prob;
  double agb_event_rate[N_TIME_CELIB], agb_time_min_log, agb_time_max_log, agb_duration_time_log, agb_event_now;
  
  if(metallicity < METALIICITY_MIN_CELIB)
    {
      for(i = 0; i < N_TIME_CELIB; i++)
        agb_event_rate[i] = CELib_yield_dif.agb_yield_dif[1][i];
    }
  else if(metallicity >= METALLICITY_MAX_CELIB)
    {
      for(i = 0; i < N_TIME_CELIB; i++)
        agb_event_rate[i] = CELib_yield_dif.agb_yield_dif[N_METALLCITY_BIN_CELIB - 1][i];
    }
  else
    {
      metallicity_log = log10(metallicity);
      metallicity_min_log = log10(METALIICITY_MIN_CELIB);
      id_metallicity = (int) ((metallicity_log - metallicity_min_log) / METALLICITY_BIN_LOG);
      dmetallicity_log = metallicity_log - METALLICITY_BIN_LOG * (double) id_metallicity - metallicity_min_log;
      
      for(i = 0; i < N_TIME_CELIB; i++)
        {
          a = (CELib_yield_dif.agb_yield_dif[id_metallicity + 1][i] - CELib_yield_dif.agb_yield_dif[id_metallicity][i]) / METALLICITY_BIN_LOG;
          b = CELib_yield_dif.agb_yield_dif[id_metallicity][i];
          agb_event_rate[i] = a * dmetallicity_log + b;
        }
    }
    
  init_genrand(id_particle + 1);
  time_min_log = log10(TIME_MIN_CELIB);
  
  agb_time_min_log = log10(P[target].AGBStartTime);
  agb_time_max_log = log10(P[target].AGBEndTime);
  agb_duration_time_log = agb_time_max_log - agb_time_min_log;
  
  for(; ;)
    {
      time_now_log = agb_time_min_log + genrand_real2() * agb_duration_time_log;
      itemp = (int) ((time_now_log - time_min_log) / DT_LOG_CELIB);
      a = (agb_event_rate[itemp + 1] - agb_event_rate[itemp]) / DT_LOG_CELIB;
      b = agb_event_rate[itemp];
      dt_dif_log = time_now_log - log10(CELib_yield.stellar_age[itemp]);
      agb_event_now = a * dt_dif_log + b;
      prob = 1.1 * genrand_real2();
      
      if(prob < agb_event_now)
        {
          *agb_explosion_time = pow(10.0, time_now_log) * TIME_GYR;
          break;
        }
    }
}
#endif

void read_yield_data(struct CELIB_YIELD *CELib_yield, struct CELIB_YIELD_DIF *CELib_yield_dif)
{
  int i, j, k;
  double aa, bb[3], cc[3][14];
  double dt, max_snii, max_snia, max_agb;
  char fenergy[N_METALLCITY_BIN_CELIB][256], fyield[N_METALLCITY_BIN_CELIB][256], fsniitime[256];
  FILE *fp, *gp, *hp;
  
  if(ThisTask == 0)
    {
      sprintf(fenergy[0], "CELib_yield_table/CELib_energy_Z1.0e-7.txt");
      sprintf(fenergy[1], "CELib_yield_table/CELib_energy_Z1.0e-6.txt");
      sprintf(fenergy[2], "CELib_yield_table/CELib_energy_Z1.0e-5.txt");
      sprintf(fenergy[3], "CELib_yield_table/CELib_energy_Z0.0001.txt");
      sprintf(fenergy[4], "CELib_yield_table/CELib_energy_Z0.001.txt");
      sprintf(fenergy[5], "CELib_yield_table/CELib_energy_Z0.01.txt");
      sprintf(fenergy[6], "CELib_yield_table/CELib_energy_Z0.1.txt");
      
      sprintf(fyield[0], "CELib_yield_table/CELib_yield_Z1.0e-7.txt");
      sprintf(fyield[1], "CELib_yield_table/CELib_yield_Z1.0e-6.txt");
      sprintf(fyield[2], "CELib_yield_table/CELib_yield_Z1.0e-5.txt");
      sprintf(fyield[3], "CELib_yield_table/CELib_yield_Z0.0001.txt");
      sprintf(fyield[4], "CELib_yield_table/CELib_yield_Z0.001.txt");
      sprintf(fyield[5], "CELib_yield_table/CELib_yield_Z0.01.txt");
      sprintf(fyield[6], "CELib_yield_table/CELib_yield_Z0.1.txt");
      
      sprintf(fsniitime, "CELib_yield_table/CELib_feedback_duration_time.txt");
      
      for(i = 0; i < N_METALLCITY_BIN_CELIB; i++)
        {
          fp = fopen(fenergy[i], "r");
          gp = fopen(fyield[i], "r");
          
          if(fp == NULL || gp == NULL)
            {
              printf("can not open inputfile: %s or %s\n", fenergy[i], fyield[i]);
              exit(1);
            }
            
          for(j = 0; j < N_TIME_CELIB; j++)
            {
              fscanf(fp, "%lf %lf %lf %lf", &aa, &bb[0], &bb[1], &bb[2]);
              
              for(k = 0; k < 3; k++)
                {
                  if(bb[k] < 0.0)
                    {
                      bb[k] = 0.0;
                    }
                }
                
              CELib_yield->stellar_age[j] = aa;
              CELib_yield->snii_energy_cum[i][j]= bb[0];// SNII //
              CELib_yield->snia_energy_cum[i][j]= bb[1];// SNIA //
              
              fscanf(gp, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", &aa, 
                &cc[0][0], &cc[0][1], &cc[0][2], &cc[0][3], &cc[0][4], &cc[0][5], &cc[0][6], &cc[0][7], &cc[0][8], &cc[0][9], &cc[0][10], &cc[0][11], &cc[0][12], &cc[0][13], 
                  &cc[1][0], &cc[1][1], &cc[1][2], &cc[1][3], &cc[1][4], &cc[1][5], &cc[1][6], &cc[1][7], &cc[1][8], &cc[1][9], &cc[1][10], &cc[1][11], &cc[1][12], &cc[1][13], 
                    &cc[2][0], &cc[2][1], &cc[2][2], &cc[2][3], &cc[2][4], &cc[2][5], &cc[2][6], &cc[2][7], &cc[2][8], &cc[2][9], &cc[2][10], &cc[2][11], &cc[2][12], &cc[2][13]);
                    
              for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                {
                  if(cc[0][k] < 0.0)
                    {
                      cc[0][k] = 0.0;
                    }
                    
                  if(cc[1][k] < 0.0)
                    {
                      cc[1][k] = 0.0;
                    }
                    
                  if(cc[2][k] < 0.0)
                    {
                      cc[2][k] = 0.0;
                    }
                }
                
              // SNII yield //
              CELib_yield->snii_mass_released[i][j] = cc[0][0];
              CELib_yield->snii_yield_cum[i][0][j] = cc[0][1];
              CELib_yield->snii_yield_cum[i][1][j] = cc[0][2];
              CELib_yield->snii_yield_cum[i][2][j] = cc[0][3];
              CELib_yield->snii_yield_cum[i][3][j] = cc[0][4];
              CELib_yield->snii_yield_cum[i][4][j] = cc[0][5];
              CELib_yield->snii_yield_cum[i][5][j] = cc[0][6];
              CELib_yield->snii_yield_cum[i][6][j] = cc[0][7];
              CELib_yield->snii_yield_cum[i][7][j] = cc[0][8];
              CELib_yield->snii_yield_cum[i][8][j] = cc[0][9];
              CELib_yield->snii_yield_cum[i][9][j] = cc[0][10];
              CELib_yield->snii_yield_cum[i][10][j] = cc[0][11];
              CELib_yield->snii_yield_cum[i][11][j] = cc[0][12];
              CELib_yield->snii_yield_cum[i][12][j] = cc[0][13];
              
              // SNIa yield //
              CELib_yield->snia_mass_released[i][j] = cc[1][0];
              CELib_yield->snia_yield_cum[i][0][j] = cc[1][1];
              CELib_yield->snia_yield_cum[i][1][j] = cc[1][2];
              CELib_yield->snia_yield_cum[i][2][j] = cc[1][3];
              CELib_yield->snia_yield_cum[i][3][j] = cc[1][4];
              CELib_yield->snia_yield_cum[i][4][j] = cc[1][5];
              CELib_yield->snia_yield_cum[i][5][j] = cc[1][6];
              CELib_yield->snia_yield_cum[i][6][j] = cc[1][7];
              CELib_yield->snia_yield_cum[i][7][j] = cc[1][8];
              CELib_yield->snia_yield_cum[i][8][j] = cc[1][9];
              CELib_yield->snia_yield_cum[i][9][j] = cc[1][10];
              CELib_yield->snia_yield_cum[i][10][j] = cc[1][11];
              CELib_yield->snia_yield_cum[i][11][j] = cc[1][12];
              CELib_yield->snia_yield_cum[i][12][j] = cc[1][13];
              
              // AGB yield //
              CELib_yield->agb_mass_released[i][j] = cc[2][0];
              CELib_yield->agb_yield_cum[i][0][j] = cc[2][1];
              CELib_yield->agb_yield_cum[i][1][j] = cc[2][2];
              CELib_yield->agb_yield_cum[i][2][j] = cc[2][3];
              CELib_yield->agb_yield_cum[i][3][j] = cc[2][4];
              CELib_yield->agb_yield_cum[i][4][j] = cc[2][5];
              CELib_yield->agb_yield_cum[i][5][j] = cc[2][6];
              CELib_yield->agb_yield_cum[i][6][j] = cc[2][7];
              CELib_yield->agb_yield_cum[i][7][j] = cc[2][8];
              CELib_yield->agb_yield_cum[i][8][j] = cc[2][9];
              CELib_yield->agb_yield_cum[i][9][j] = cc[2][10];
              CELib_yield->agb_yield_cum[i][10][j] = cc[2][11];
              CELib_yield->agb_yield_cum[i][11][j] = cc[2][12];
              CELib_yield->agb_yield_cum[i][12][j] = cc[2][13];
            }
            
          fclose(fp);
          fclose(gp);
        }
        
      hp = fopen(fsniitime, "r");
      
      if(hp == NULL)
        {
          printf("can not open inputfile: %s\n", fsniitime);
          exit(1);
        }
        
      for(i = 0; i < N_METALLCITY_BIN_CELIB; i++)
        {
          fscanf(hp, "%lf %lf %lf", &bb[0], &bb[1], &bb[2]);
          
          CELib_yield->snii_duration_time[i][0] = bb[1];
          CELib_yield->snii_duration_time[i][1] = bb[2];
          
          CELib_yield->snia_duration_time[i][0] = TIME_MIN_SNIA_CELIB;
          CELib_yield->snia_duration_time[i][1] = TIME_MAX_SNIA_CELIB;
          
          CELib_yield->agb_duration_time[i][0] = TIME_MIN_AGB_CELIB;
          CELib_yield->agb_duration_time[i][1] = TIME_MAX_AGB_CELIB;
        }
        
      fclose(hp);
    }
    
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Bcast(CELib_yield, sizeof(struct CELIB_YIELD), MPI_BYTE, 0, MPI_COMM_WORLD);
  
  for(i = 0; i < N_METALLCITY_BIN_CELIB; i++)
    {
      max_snii = - 1.0e30;
      max_snia = - 1.0e30;
      max_agb = - 1.0e30;
      
      for(j = 0; j < N_TIME_CELIB - 1; j++)
        {
          dt = CELib_yield->stellar_age[j + 1] - CELib_yield->stellar_age[j];
          CELib_yield_dif->snii_energy_dif[i][j] = fabs(CELib_yield->snii_energy_cum[i][j + 1] - CELib_yield->snii_energy_cum[i][j]) / dt;
          CELib_yield_dif->snia_energy_dif[i][j] = fabs(CELib_yield->snia_energy_cum[i][j + 1] - CELib_yield->snia_energy_cum[i][j]) / dt;
          CELib_yield_dif->agb_yield_dif[i][j] = fabs(CELib_yield->agb_mass_released[i][j + 1] - CELib_yield->agb_mass_released[i][j]) / dt;
          
          if(CELib_yield->stellar_age[j] <= CELib_yield->snii_duration_time[i][0] || CELib_yield->stellar_age[j] > CELib_yield->snii_duration_time[i][1])
            {
              CELib_yield_dif->snii_energy_dif[i][j] = 0.0;
            }
            
          if(CELib_yield->stellar_age[j] <= CELib_yield->snia_duration_time[i][0] || CELib_yield->stellar_age[j] > CELib_yield->snia_duration_time[i][1])
            {
              CELib_yield_dif->snia_energy_dif[i][j] = 0.0;
            }
            
          if(CELib_yield->stellar_age[j] <= CELib_yield->agb_duration_time[i][0] || CELib_yield->stellar_age[j] > CELib_yield->agb_duration_time[i][1])
            {
              CELib_yield_dif->agb_yield_dif[i][j] = 0.0;
            }
            
          if(max_snii < CELib_yield_dif->snii_energy_dif[i][j])
            {
              max_snii = CELib_yield_dif->snii_energy_dif[i][j];
            }
            
          if(max_snia < CELib_yield_dif->snia_energy_dif[i][j])
            {
              max_snia = CELib_yield_dif->snia_energy_dif[i][j];
            }
            
          if(max_agb < CELib_yield_dif->agb_yield_dif[i][j])
            {
              max_agb = CELib_yield_dif->agb_yield_dif[i][j];
            }
        }
        
      for(j = 0; j < N_TIME_CELIB; j++)
        {
          CELib_yield_dif->snii_energy_dif[i][j] /= max_snii;
          CELib_yield_dif->snia_energy_dif[i][j] /= max_snia;
          CELib_yield_dif->agb_yield_dif[i][j] /= max_agb;
        }
    }
}


void calc_feedback_duration_time(int target, double metallicity)
{
  int i, itemp;
  double zmin, znow, dz, a, b, aa, bb;
  
#ifndef OSAKA_CELIB_POPIII
  if(metallicity < METALIICITY_MIN_CELIB)
    metallicity = METALIICITY_MIN_CELIB;
#else
  if(metallicity < METALIICITY_MIN_POPIII_CELIB)
    metallicity = METALIICITY_MIN_POPIII_CELIB;
#endif

  if(metallicity >= METALLICITY_MAX_CELIB)
    {
      P[target].SNIIStartTime = CELib_yield.snii_duration_time[N_METALLCITY_BIN_CELIB - 1][0];
      P[target].SNIIEndTime = CELib_yield.snii_duration_time[N_METALLCITY_BIN_CELIB - 1][1];
      
      P[target].SNIaStartTime = CELib_yield.snia_duration_time[N_METALLCITY_BIN_CELIB - 1][0];
      P[target].SNIaEndTime = CELib_yield.snia_duration_time[N_METALLCITY_BIN_CELIB - 1][1];
      
      P[target].AGBStartTime = CELib_yield.agb_duration_time[N_METALLCITY_BIN_CELIB - 1][0];
      P[target].AGBEndTime = CELib_yield.agb_duration_time[N_METALLCITY_BIN_CELIB - 1][1];
      
      return;
    }
    
  zmin = log10(METALIICITY_MIN_POPIII_CELIB);
  znow = log10(metallicity);
  dz = znow - floor(znow);
  itemp = (int) ((znow - zmin) / (double) METALLICITY_BIN_LOG);
  
  a = (log10(CELib_yield.snii_duration_time[itemp + 1][0]) - log10(CELib_yield.snii_duration_time[itemp][0])) / (double) METALLICITY_BIN_LOG;
  b = log10(CELib_yield.snii_duration_time[itemp][0]);
  P[target].SNIIStartTime = pow(10.0, a * dz + b);
  aa = (log10(CELib_yield.snii_duration_time[itemp + 1][1]) - log10(CELib_yield.snii_duration_time[itemp][1])) / (double) METALLICITY_BIN_LOG;
  bb = log10(CELib_yield.snii_duration_time[itemp][1]);
  P[target].SNIIEndTime = pow(10.0, aa * dz + bb);
  
  a = (log10(CELib_yield.snia_duration_time[itemp + 1][0]) - log10(CELib_yield.snia_duration_time[itemp][0])) / (double) METALLICITY_BIN_LOG;
  b = log10(CELib_yield.snia_duration_time[itemp][0]);
  P[target].SNIaStartTime = pow(10.0, a * dz + b);
  aa = (log10(CELib_yield.snia_duration_time[itemp + 1][1]) - log10(CELib_yield.snia_duration_time[itemp][1])) / (double) METALLICITY_BIN_LOG;
  bb = log10(CELib_yield.snia_duration_time[itemp][1]);
  P[target].SNIaEndTime = pow(10.0, aa * dz + bb);
  
  a = (log10(CELib_yield.agb_duration_time[itemp + 1][0]) - log10(CELib_yield.agb_duration_time[itemp][0])) / (double) METALLICITY_BIN_LOG;
  b = log10(CELib_yield.agb_duration_time[itemp][0]);
  P[target].AGBStartTime = pow(10.0, a * dz + b);
  aa = (log10(CELib_yield.agb_duration_time[itemp + 1][1]) - log10(CELib_yield.agb_duration_time[itemp][1])) / (double) METALLICITY_BIN_LOG;
  bb = log10(CELib_yield.agb_duration_time[itemp][1]);
  P[target].AGBEndTime = pow(10.0, aa * dz + bb);
}

#endif
#endif
