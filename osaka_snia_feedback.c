#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <mpi.h>
#include "allvars.h"
#include "proto.h"

#ifdef OSAKA
#ifdef OSAKA_SNIA

static struct sniamaindata_in
{
  int NodeList[NODELISTLENGTH];
  double Pos[3];
  double Hsml;
  double SNIaNgbNum;
  double SNIaNgbMass;
  double Masswk;
  double Rshock;
  double Mass;
  double StarDensity;
  double InitialStarMass;
  double SNIaEnergy;
  double TimeSNIa;
  double MassReleased;
  double Metals[N_YIELD_ELEMENT_CELIB];
  double SNIaExplosionTime;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
  MyIDType IDStar;
  int FBflagSNIa;
#ifdef OSAKA_MECHANICAL_FB
  double Metallicity;
#endif
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*SNIaDataIn, *SNIaDataGet;

static struct sniamaindata_out
{
  double SNIaNgbNum;
  double SNIaNgbMass;
  double Masswk;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*SNIaDataResult, *SNIaDataOut;

static struct MASSWK
{
  double Masswk;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*MassWk;

void osaka_snia_feedback(void)
{
  set_snia_star_hsml_guess();
  snia_star_density();
  
  snia_feedback();
  
#ifdef OSAKA_STRONG_THERMAL_FEEDBACK
  snia_feedback_hot_bubble();
#elif OSAKA_CONSTANT_WIND
  snia_constant_wind_kick();
#else
  snia_feedback_kick();
#endif
}

void snia_feedback(void)
{
  int i, j, k, ndone, ndone_flag, dummy;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  int nfb, nstar_local, nstar_total;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  double snia_energy, snia_mass_released, next_snia_explosion_time, mass_fraction;
  static double snia_metal_yield[N_YIELD_ELEMENT_CELIB];
  double nsnia_local, nsnia_total, snia_mass_released_local, snia_mass_released_total;
#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[N_SURFACE_TOTAL_LV1];
#endif

  if(ThisTask == 0)
    {
      printf("Start snia feedback...\n");
      fflush(stdout);
    }
    
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  for(i = FirstActiveParticle, nsnia_local = 0.0, snia_mass_released_local = 0.0, nstar_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagSNIa < All.SNIaEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(All.SNIaEventNumber == 1)
            {
              if(stellarage > P[i].SNIaExplosionTime)
                calc_instantaneous_snia_yield(P[i].Metallicity, CELib_yield, &snia_energy, &snia_mass_released, snia_metal_yield);
              else
                {
                  snia_energy = 0.0;
                  snia_mass_released = 0.0;
                }
            }
          else
            if(stellarage > P[i].SNIaExplosionTime)
              calc_snia_yield(i, snia_energy_table, snia_mass_released_table, snia_metal_yield_table, &snia_energy, &snia_mass_released, snia_metal_yield, &nfb, &next_snia_explosion_time);
            else
              {
                snia_energy = 0.0;
                snia_mass_released = 0.0;
              }
              
          nsnia_local += P[i].InitialStarMass * snia_energy;
          snia_mass_released_local += snia_mass_released;
          
          if(snia_energy > 0.0)
            nstar_local++;
        }
    }
    
  nsnia_local *= All.UnitMass_in_g / SOLAR_MASS / All.Energy_per_SNIa;
  MPI_Allreduce(&nsnia_local, &nsnia_total, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&snia_mass_released_local, &snia_mass_released_total, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&nstar_local, &nstar_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(nsnia_total > 0.0)
    {
      if(ThisTask == 0)
        {
          printf("Time = %e, nsnia_total = %e, snia_mass_released_total = %e, nstar_total = %d\n", All.Time, nsnia_total, snia_mass_released_total, nstar_total);
          fflush(stdout);
        }
    }
  else
    {
      if(ThisTask == 0)
        {
          printf("This loop is no SNIa feedback...\n");
          fflush(stdout);
        }
        
      return;
    }
    
  // First loop : calculation for only wk //
  if(ThisTask == 0)
    {
      printf("Prepare distributing snia energy and metals...\n");
      fflush(stdout);
    }
    
  Ngblist = (int *) mymalloc(NumPart * sizeof(int));
  
  All.BunchSize = (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                    sizeof(struct sniamaindata_in) + sizeof(struct sniamaindata_out) + sizemax(sizeof(struct sniamaindata_in),
                      sizeof(struct sniamaindata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
  
  MassWk = (struct MASSWK *) mymalloc(NumPart * sizeof(struct MASSWK));
  
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  osaka_make_geodesic_dome_snia(surface);
#endif
  
  i = FirstActiveParticle;
  
  do
    {
      for(j = 0; j < NTask; j++)
        {
          Send_count[j] = 0;
          Exportflag[j] = -1;
        }
        
      // do local particles and prepare export list //
      for(nexport = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagSNIa < All.SNIaEventNumber && stellarage > P[i].SNIaExplosionTime)
            {
              calc_snia_shock_radius(i);
              
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
              if(snia_feedback_evaluate(i, 0, 1, &nexport, Send_count) < 0)
                break;
#else
              osaka_assign_surface_snia(MassWk[i].surface, surface);
              osaka_set_current_star_surface_snia(P[i].Pos, MassWk[i].surface);
              
              if(snia_feedback_evaluate_gdw(i, 0, 1, &nexport, Send_count) < 0)
                break;
#endif
            }
        }
        
#ifdef MYSORT
      mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
      qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
      
      MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
      
      for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
        {
          Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
          nimport += Recv_count[j];
          
          if(j > 0)
            {
              Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
              Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
            }
        }
        
      SNIaDataGet = (struct sniamaindata_in *) mymalloc(nimport * sizeof(struct sniamaindata_in));
      SNIaDataIn = (struct sniamaindata_in *) mymalloc(nexport * sizeof(struct sniamaindata_in));
      
      // prepare particle data for export //
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          SNIaDataIn[j].Pos[0] = P[place].Pos[0];
          SNIaDataIn[j].Pos[1] = P[place].Pos[1];
          SNIaDataIn[j].Pos[2] = P[place].Pos[2];
          SNIaDataIn[j].Mass = P[place].Mass;
          SNIaDataIn[j].InitialStarMass = P[place].InitialStarMass;
          SNIaDataIn[j].Rshock = P[place].Rshock;
          SNIaDataIn[j].Hsml = P[place].Hsml;
          SNIaDataIn[j].SNIaExplosionTime = P[place].SNIaExplosionTime;
          SNIaDataIn[j].RminFeedbackInteraction = MassWk[place].RminFeedbackInteraction;
          SNIaDataIn[j].IDStar = P[place].ID;
          SNIaDataIn[j].FBflagSNIa = P[place].FBflagSNIa;
#ifdef OSAKA_MECHANICAL_FB
          SNIaDataIn[j].Metallicity = P[place].Metallicity;
#endif
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
          osaka_assign_surface_snia(SNIaDataIn[j].surface, surface);
          osaka_set_current_star_surface_esfb(P[place].Pos, SNIaDataIn[j].surface);
#endif

          memcpy(SNIaDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
        }
        
      // exchange particle data //
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // get the particles //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&SNIaDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct sniamaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&SNIaDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct sniamaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&SNIaDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct sniamaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, &SNIaDataGet[Recv_offset[recvTask]], 
                      Recv_count[recvTask] * sizeof(struct sniamaindata_in), MPI_BYTE, recvTask, 
                        TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      myfree(SNIaDataIn);
      
      SNIaDataResult = (struct sniamaindata_out *) mymalloc(nimport * sizeof(struct sniamaindata_out));
      SNIaDataOut = (struct sniamaindata_out *) mymalloc(nexport * sizeof(struct sniamaindata_out));
      
      // now do the particles that were sent to us //
      for(j = 0; j < nimport; j++)
        {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          snia_feedback_evaluate(j, 1, 1, &dummy, &dummy);
#else
          snia_feedback_evaluate_gdw(j, 1, 1, &dummy, &dummy);
#endif
        }
        
      if(i < 0)
        ndone_flag = 1;
      else
        ndone_flag = 0;
        
      MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
      
      // get the result // 
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // send the results //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&SNIaDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct sniamaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&SNIaDataOut[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct sniamaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&SNIaDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct sniamaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B, &SNIaDataOut[Send_offset[recvTask]],
                      Send_count[recvTask] * sizeof(struct sniamaindata_out), MPI_BYTE, recvTask, 
                        TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      // add the result to the local particles // 
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          P[place].SNIaNgbNum += SNIaDataOut[j].SNIaNgbNum;
          P[place].SNIaNgbMass += SNIaDataOut[j].SNIaNgbMass;
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          MassWk[place].Masswk += SNIaDataOut[j].Masswk;
#else
          osaka_update_surface_nparticle_snia(MassWk[place].surface, SNIaDataOut[j].surface);
#endif

          if(MassWk[place].RminFeedbackInteraction > SNIaDataOut[j].RminFeedbackInteraction)
            {
              MassWk[place].RminFeedbackInteraction = SNIaDataOut[j].RminFeedbackInteraction;
              MassWk[place].PID_Rmin = SNIaDataOut[j].PID_Rmin;
            }
        }
        
      myfree(SNIaDataOut);
      myfree(SNIaDataResult);
      myfree(SNIaDataGet);
    }
  while(ndone < NTask);
  
  for(i = FirstActiveParticle, nstar_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagSNIa < All.SNIaEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
           
          if(stellarage > P[i].SNIaExplosionTime)
            {
              if(P[i].SNIaNgbNum == 0.0)
                nstar_local++;
            }
        }
    }
    
  MPI_Allreduce(&nstar_local, &nstar_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(nstar_total > 0)
    {
      if(ThisTask == 0)
        {
          printf("*--- %d star particles can not find interaction gas particles. ---*\n", nstar_total);
          printf("*--- SNIa energy and metal assign nearest gas particle. ---*\n");
          fflush(stdout);
        }
    }
    
  // Second loop : calculation for real part of SNIa feedback //
  if(ThisTask == 0)
    {
      printf("Distributing snia energy and metals...\n");
      fflush(stdout);
    }
    
  i = FirstActiveParticle;
  
  do
    {
      for(j = 0; j < NTask; j++)
        {
          Send_count[j] = 0;
          Exportflag[j] = -1;
        }
        
      // do local particles and prepare export list //
      for(nexport = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagSNIa < All.SNIaEventNumber && stellarage > P[i].SNIaExplosionTime)
            {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
              if(snia_feedback_evaluate(i, 0, 2, &nexport, Send_count) < 0)
                break;
#else
              osaka_calc_surface_area_weight_snia(MassWk[i].surface);
              
              if(snia_feedback_evaluate_gdw(i, 0, 2, &nexport, Send_count) < 0)
                break;
#endif
            }
        }
        
#ifdef MYSORT
      mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
      qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
      
      MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
      
      for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
        {
          Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
          nimport += Recv_count[j];
          
          if(j > 0)
            {
              Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
              Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
            }
        }
        
      SNIaDataGet = (struct sniamaindata_in *) mymalloc(nimport * sizeof(struct sniamaindata_in));
      SNIaDataIn = (struct sniamaindata_in *) mymalloc(nexport * sizeof(struct sniamaindata_in));
      
      // prepare particle data for export //
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          SNIaDataIn[j].Pos[0] = P[place].Pos[0];
          SNIaDataIn[j].Pos[1] = P[place].Pos[1];
          SNIaDataIn[j].Pos[2] = P[place].Pos[2];
          SNIaDataIn[j].Mass = P[place].Mass;
          SNIaDataIn[j].StarDensity = P[place].Density;
          SNIaDataIn[j].InitialStarMass = P[place].InitialStarMass;
          SNIaDataIn[j].Rshock = P[place].Rshock;
          SNIaDataIn[j].Hsml = P[place].Hsml;
          SNIaDataIn[j].SNIaNgbNum = P[place].SNIaNgbNum;
          SNIaDataIn[j].SNIaNgbMass = P[place].SNIaNgbMass;
          SNIaDataIn[j].Masswk = MassWk[place].Masswk;
          SNIaDataIn[j].TimeSNIa = P[place].TimeSNIa;
          SNIaDataIn[j].SNIaExplosionTime = P[place].SNIaExplosionTime;
          SNIaDataIn[j].RminFeedbackInteraction = MassWk[place].RminFeedbackInteraction;
          SNIaDataIn[j].PID_Rmin = MassWk[place].PID_Rmin;
          SNIaDataIn[j].IDStar = P[place].ID;
          SNIaDataIn[j].FBflagSNIa = P[place].FBflagSNIa;
#ifdef OSAKA_MECHANICAL_FB
          SNIaDataIn[j].Metallicity = P[place].Metallicity;
#endif
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
          osaka_assign_surface_snia(SNIaDataIn[j].surface, MassWk[place].surface);
#endif

#ifdef OSAKA_CELIB
          if(All.SNIaEventNumber == 1)
            calc_instantaneous_snia_yield(P[place].Metallicity, CELib_yield, &snia_energy, &snia_mass_released, snia_metal_yield);
          else
            calc_snia_yield(place, snia_energy_table, snia_mass_released_table, snia_metal_yield_table, &snia_energy, &snia_mass_released, snia_metal_yield, &nfb, &next_snia_explosion_time);
            
          SNIaDataIn[j].SNIaEnergy = snia_energy;
          SNIaDataIn[j].MassReleased = P[place].InitialStarMass * snia_mass_released;
          
          for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
            SNIaDataIn[j].Metals[k] = P[place].InitialStarMass * snia_metal_yield[k];
#endif
          memcpy(SNIaDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
        }
        
      // exchange particle data //
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // get the particles //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&SNIaDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct sniamaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&SNIaDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct sniamaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&SNIaDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct sniamaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, &SNIaDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct sniamaindata_in), 
                      MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      myfree(SNIaDataIn);
      
      // now do the particles that were sent to us //
      for(j = 0; j < nimport; j++)
        {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          snia_feedback_evaluate(j, 1, 2, &dummy, &dummy);
#else
          snia_feedback_evaluate_gdw(j, 1, 2, &dummy, &dummy);
#endif
        }
        
      if(i < 0)
        ndone_flag = 1;
      else
        ndone_flag = 0;
        
      MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
      
      myfree(SNIaDataGet);
    }
  while(ndone < NTask);
  
  myfree(MassWk);
  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagSNIa < All.SNIaEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(stellarage > P[i].SNIaExplosionTime)
            {
#ifdef OSAKA_CELIB
              if(All.SNIaEventNumber == 1)
                {
                  calc_instantaneous_snia_yield(P[i].Metallicity, CELib_yield, &snia_energy, &snia_mass_released, snia_metal_yield);
                  P[i].FBflagSNIa = All.SNIaEventNumber;
                }
              else
                {
                  calc_snia_yield(i, snia_energy_table, snia_mass_released_table, snia_metal_yield_table, &snia_energy, &snia_mass_released, snia_metal_yield, &nfb, &next_snia_explosion_time);
                  P[i].FBflagSNIa += nfb;
                  P[i].SNIaExplosionTime = next_snia_explosion_time;
                }
                
              mass_fraction = 1.0 - snia_mass_released * P[i].InitialStarMass / P[i].Mass;
              P[i].Mass -= P[i].InitialStarMass * snia_mass_released;
              
              for(j = 0; j < N_YIELD_ELEMENT_CELIB; j++)
                P[i].Metals[j] *= mass_fraction;
#endif
            }
        }
    }
}

int snia_feedback_evaluate(int target, int mode, int loop, int *nexport, int *nsend_local)
{
  int i, j, k, n, nfb, fbflagSNIa;
  int startnode, numngb_inbox, listindex;
  double h, h2, hinv, hinv3, hinv4, dx, dy, dz, r, r2, u, mass_j, mjwk, wk, dwk;
  double ascale, a3inv, hubble, time, factor_time;
  double pos[3], mass, stardensity, initmass, ngb, ngbmass, masswk, rshock, rminfeedbackinteraction;
  double timesnia, snia_explosion_time, timenocoolingsnia;
  double snia_mass_released, metal_mass_element[N_YIELD_ELEMENT_CELIB], next_snia_explosion_time;
  double snia_energy, sniath_energy, sniak_energy;
  static double snia_metal_yield[N_YIELD_ELEMENT_CELIB];
  MyIDType pid_rmin, IDStar;
#ifdef OSAKA_MECHANICAL_FB
  double ejecta_momentum, terminal_momentum, densityin, snia_energyin, starmetallicity, pejin, massin;
  double boost_factor, ptoverpej, SNIamomentum, energymomentum, theta, phi, norm, dir[3];
#endif
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  double hmin, fac_hmin, hmax, fac_hmax, rshock_real;
#endif

#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  fac_hmin = pow((double) All.MinNumNgbSNIa / (double) All.DesNumNgbSNIa, 0.333333);
  fac_hmax = pow((double) All.MaxNumNgbSNIa / (double) All.DesNumNgbSNIa, 0.333333);
#endif

  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (All.Time * All.Time * All.Time);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
      factor_time = 1.0;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      factor_time = All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  if(mode == 0)
    {
      pos[0] = P[target].Pos[0];
      pos[1] = P[target].Pos[1];
      pos[2] = P[target].Pos[2];
      h = P[target].Hsml;
      mass = P[target].Mass;
      initmass = P[target].InitialStarMass;
      rshock = P[target].Rshock;
      snia_explosion_time = P[target].SNIaExplosionTime;
#ifdef OSAKA_MECHANICAL_FB
      starmetallicity = P[target].Metallicity;
#endif
      
      if(loop == 1)
        {
          masswk = ngb = ngbmass = 0.0;
          rminfeedbackinteraction = 1.0e30;
        }
        
      if(loop == 2)
        {
          masswk = MassWk[target].Masswk;
          ngb = P[target].SNIaNgbNum;
          ngbmass = P[target].SNIaNgbMass;
          stardensity = P[target].Density;
          timesnia = time + P[target].TimeSNIa;
          timenocoolingsnia = P[target].TimeSNIa;
          rminfeedbackinteraction = MassWk[target].RminFeedbackInteraction;
          pid_rmin = MassWk[target].PID_Rmin;
          IDStar = P[target].ID;
          fbflagSNIa = P[target].FBflagSNIa;
          
#ifdef OSAKA_NO_COOLING_OFF
          timesnia = time;
#endif

#ifdef OSAKA_CELIB
          if(All.SNIaEventNumber == 1)
            calc_instantaneous_snia_yield(P[target].Metallicity, CELib_yield, &snia_energy, &snia_mass_released, snia_metal_yield);
          else
            calc_snia_yield(target, snia_energy_table, snia_mass_released_table, snia_metal_yield_table, &snia_energy, &snia_mass_released, snia_metal_yield, &nfb, &next_snia_explosion_time);
            
          snia_mass_released = P[target].InitialStarMass * snia_mass_released;
          
          for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
            metal_mass_element[i] = P[target].InitialStarMass * snia_metal_yield[i];
#endif
        }
    }
  else
    {
      pos[0] = SNIaDataGet[target].Pos[0];
      pos[1] = SNIaDataGet[target].Pos[1];
      pos[2] = SNIaDataGet[target].Pos[2];
      h = SNIaDataGet[target].Hsml;
      mass = SNIaDataGet[target].Mass;
      initmass = SNIaDataGet[target].InitialStarMass;
      rshock = SNIaDataGet[target].Rshock;
      snia_explosion_time = SNIaDataGet[target].SNIaExplosionTime;
#ifdef OSAKA_MECHANICAL_FB
      starmetallicity = SNIaDataGet[target].Metallicity;
#endif
      
      if(loop == 1)
        {
          masswk = ngb = ngbmass = 0.0;
          rminfeedbackinteraction = SNIaDataGet[target].RminFeedbackInteraction;
        }
        
      if(loop == 2)
        {
          masswk = SNIaDataGet[target].Masswk;
          ngb = SNIaDataGet[target].SNIaNgbNum;
          ngbmass = SNIaDataGet[target].SNIaNgbMass;
          stardensity = SNIaDataGet[target].StarDensity;
          timesnia = time + SNIaDataGet[target].TimeSNIa;
          timenocoolingsnia = SNIaDataGet[target].TimeSNIa;
          rminfeedbackinteraction = SNIaDataGet[target].RminFeedbackInteraction;
          pid_rmin = SNIaDataGet[target].PID_Rmin;
          IDStar = SNIaDataGet[target].IDStar;
          fbflagSNIa = SNIaDataGet[target].FBflagSNIa;
          
#ifdef OSAKA_NO_COOLING_OFF
          timesnia = time;
#endif

#ifdef OSAKA_CELIB
          snia_energy = SNIaDataGet[target].SNIaEnergy;
          snia_mass_released = SNIaDataGet[target].MassReleased;
          
          for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
            metal_mass_element[i] = SNIaDataGet[target].Metals[i];
#endif
        }
    }
    
  if(loop == 2)
    {
      snia_energy = snia_energy * initmass * All.UnitMass_in_g / SOLAR_MASS;
#ifndef OSAKA_MECHANICAL_FB
      sniath_energy = All.SNIaThermalFBEfficiency * snia_energy / All.UnitEnergy_in_cgs;
      sniak_energy = All.SNIaKineticFBEfficiency * snia_energy / All.UnitEnergy_in_cgs;
#else
      sniath_energy = snia_energy / All.UnitEnergy_in_cgs;
      sniak_energy = 0.0;
      ejecta_momentum = sqrt(2.0 * snia_mass_released / hubble * All.UnitMass_in_g * snia_energy) / All.UnitMass_in_g / All.UnitVelocity_in_cm_per_s;
      snia_energyin = snia_energy / All.Energy_per_SNIa;
      densityin = stardensity * All.UnitDensity_in_cgs / PROTONMASS * hubble * hubble * a3inv;
      terminal_momentum = 9.55e43 * pow(snia_energyin, 13.0 / 14.0) * pow(densityin, - 1.0 / 7.0) * pow(fz_SNIa(starmetallicity), 1.5) / All.UnitMass_in_g / All.UnitVelocity_in_cm_per_s;
#endif
    }
    
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  rshock_real = rshock;
#endif

#if !defined(OSAKA_STRONG_THERMAL_FEEDBACK) && !defined(OSAKA_CONSTANT_WIND) && !defined(OSAKA_USE_HSML_FOR_SN_FEEDBACK)
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  hmax = fac_hmax * h;
  
  if(hmax < rshock)
    {
      rshock_real = rshock;
      rshock = hmax;
    }
    
#endif
  if(h < rshock)
    h = rshock;
#endif
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  hmin = fac_hmin * h;
  
  if(rshock < hmin)
    {
      rshock_real = rshock;
      rshock = hmin;
    }
#endif

  h2 = h * h;
  hinv = 1.0 / h;
#ifndef  TWODIMS
  hinv3 = hinv * hinv * hinv;
#else
  hinv3 = hinv * hinv / boxSize_Z;
#endif
  hinv4 = hinv3 * hinv;
  
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = SNIaDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  if(loop == 2 && ngb == 0.0)
    {
      if(rminfeedbackinteraction < 0.0)
        return 0;
        
      while(startnode >= 0)
        {
          while(startnode >= 0)
            {
              numngb_inbox = ngb_treefind_snia(pos, h, target, &startnode, mode, nexport, nsend_local);
              
              if(numngb_inbox < 0)
                return -1;
                
              for(n = 0; n < numngb_inbox; n++)
                {
                  j = Ngblist[n];
                  
                  if(pid_rmin == P[j].ID)
                    {
                      dx = pos[0] - P[j].Pos[0];
                      dy = pos[1] - P[j].Pos[1];
                      dz = pos[2] - P[j].Pos[2];
                      
                      //  now find the closest image in the given box size  //
#ifdef PERIODIC
                      if(dx > boxHalf_X)
                        dx -= boxSize_X;
                      if(dx < -boxHalf_X)
                        dx += boxSize_X;
                      if(dy > boxHalf_Y)
                        dy -= boxSize_Y;
                      if(dy < -boxHalf_Y)
                        dy += boxSize_Y;
                      if(dz > boxHalf_Z)
                        dz -= boxSize_Z;
                      if(dz < -boxHalf_Z)
                        dz += boxSize_Z;
#endif
                      r2 = dx * dx + dy * dy + dz * dz;
                      r = sqrt(r2);

#ifndef OSAKA_MECHANICAL_FB
                      SphP[j].KineticEnergySNIa += sniak_energy;
                      SphP[j].ThermalEnergySNIa += sniath_energy;
#else
                      boost_factor = sqrt(1.0 + P[j].Mass / snia_mass_released);
                      ptoverpej = terminal_momentum / ejecta_momentum;
                      
                      if(boost_factor < ptoverpej)
                        SNIamomentum = boost_factor * ejecta_momentum;
                      else
                        SNIamomentum = terminal_momentum;
                        
                      energymomentum = 0.5 * ejecta_momentum * ejecta_momentum / snia_mass_released;
                      snia_energyin = sniath_energy + energymomentum;
                      
                      if(r > rshock)
                        snia_energyin *= pow(r/ rshock, - 6.5);
                        
                      SphP[j].ThermalEnergySNIa += snia_energyin;
                      
#ifdef OSAKA_ISOTROPIC_WIND
                      theta = acos(2 * get_random_number(IDStar + P[j].ID + fbflagSNIa + 3) - 1);
                      phi = 2 * M_PI * get_random_number(IDStar + P[j].ID + fbflagSNIa + 4);
                      
                      dir[0] = sin(theta) * cos(phi);
                      dir[1] = sin(theta) * sin(phi);
                      dir[2] = cos(theta);
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagSNIa + 5) < 0.5)
                        norm = -norm;
                        
#elif OSAKA_RADIAL_DIRECTION_WIND
                      dir[0] = P[j].Pos[0] - pos[0];
                      dir[1] = P[j].Pos[1] - pos[1];
                      dir[2] = P[j].Pos[2] - pos[2];
                      
#ifdef PERIODIC
                      if(dir[0] > boxHalf_X)
                        dir[0] -= boxSize_X;
                      if(dir[0] < -boxHalf_X)
                        dir[0] += boxSize_X;
                      if(dir[1] > boxHalf_Y)
                        dir[1] -= boxSize_Y;
                      if(dir[1] < -boxHalf_Y)
                        dir[1] += boxSize_Y;
                      if(dir[2] > boxHalf_Z)
                        dir[2] -= boxSize_Z;
                      if(dir[2] < -boxHalf_Z)
                        dir[2] += boxSize_Z;
#endif

                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
#else
                      dir[0] = P[j].g.GravAccel[1] * P[j].Vel[2] - P[j].g.GravAccel[2] * P[j].Vel[1];
                      dir[1] = P[j].g.GravAccel[2] * P[j].Vel[0] - P[j].g.GravAccel[0] * P[j].Vel[2];
                      dir[2] = P[j].g.GravAccel[0] * P[j].Vel[1] - P[j].g.GravAccel[1] * P[j].Vel[0];
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagSNIa + 5) < 0.5)
                        norm = -norm;
#endif

                      if(norm != 0)
                        {
                          for(k = 0; k < 3; k++)
                            dir[k] /= norm;
                            
                          for(k = 0; k < 3; k++)
                            SphP[j].MomentumSNIa[k] += SNIamomentum * dir[k];
                        }
#endif

#ifndef OSAKA_MINMAX_NEIGHBOUR_NUMBER
                      SphP[j].Rshock = rshock;
#else
                      SphP[j].Rshock = rshock_real;
#endif
                      SphP[j].timeSNIa = timesnia;
                      SphP[j].timeNoCoolingSNIa = timenocoolingsnia;
                      
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      SphP[j].MassStar = initmass;
                      SphP[j].IDStar= IDStar;
                      P[j].FBflagSNIa = fbflagSNIa;
                      P[j].SNIaNgbMass = P[j].Mass;
                      
                      SphP[j].StarSNIaDensity = stardensity;
                      
#ifdef OSAKA_CELIB
                      P[j].Mass += snia_mass_released;
                      
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals[k] += metal_mass_element[k];
                        
                      P[j].Metallicity = (P[j].Mass - P[j].Metals[0] - P[j].Metals[1]) / P[j].Mass;
                      
#ifdef OSAKA_METAL_ELEMENT_TRACE_SNIa
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals_SNIa[k] += metal_mass_element[k];
#endif
#endif

                      if(mode == 0)
                        MassWk[target].RminFeedbackInteraction = - 1.0;
                        
                      return 0;
                    }
                }// end of a for-loop for ngb_inbox
            }
            
          if(mode == 1)
            {
              listindex++;
              
              if(listindex < NODELISTLENGTH)
                {
                  startnode = SNIaDataGet[target].NodeList[listindex];
                  
                  if(startnode >= 0)
                    startnode = Nodes[startnode].u.d.nextnode;// open it //
                }
            }
        }
    }
    
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = SNIaDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  while(startnode >= 0)
    {
      while(startnode >= 0)
        {
          numngb_inbox = ngb_treefind_snia(pos, h, target, &startnode, mode, nexport, nsend_local);
          
          if(numngb_inbox < 0)
            return -1;
            
          for(n = 0; n < numngb_inbox; n++)
            {
              j = Ngblist[n];
              
#ifdef OSAKA_HYDRO_INTERACTION_OFF
              if(SphP[j].flagCoolingSNIa == 1 || SphP[j].flagCoolingSNIa == 1)
                continue;
#endif

              dx = pos[0] - P[j].Pos[0];
              dy = pos[1] - P[j].Pos[1];
              dz = pos[2] - P[j].Pos[2];
              
              //  now find the closest image in the given box size  //
#ifdef PERIODIC
              if(dx > boxHalf_X)
                dx -= boxSize_X;
              if(dx < -boxHalf_X)
                dx += boxSize_X;
              if(dy > boxHalf_Y)
                dy -= boxSize_Y;
              if(dy < -boxHalf_Y)
                dy += boxSize_Y;
              if(dz > boxHalf_Z)
                dz -= boxSize_Z;
              if(dz < -boxHalf_Z)
                dz += boxSize_Z;
#endif
              r2 = dx * dx + dy * dy + dz * dz;
              
              if(r2 < h2)
                {
                  r = sqrt(r2);
                  u = r * hinv;
                  
                  if(loop == 1)
                    if(rminfeedbackinteraction > r)
                      {
                        rminfeedbackinteraction = r;
                        pid_rmin = P[j].ID;
                      }
                      
#ifndef DENSITY_WEIGHTED_VALUE
#if !defined(QUINTIC_KERNEL) && !defined(WENDLAND_C4_KERNEL)
                  if(u < 0.5)
                    wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1.0) * u * u);
                  else
                    wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
#elif QUINTIC_KERNEL
                  if(0.0 <= u && u < ONETHIRD)
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) + 15.0 * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u);
                  else if(ONETHIRD <= u && u < TWOTHIRD)
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u);
                  else
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u);
                    
                  wk *= KERNEL_NORM * hinv3;
#elif WENDLAND_C4_KERNEL
                  wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 + 6.0 * u + 35.0 / 3 * u * u);
                  wk *= KERNEL_NORM * hinv3;
#endif
#else
                  wk = pow(SphP[j].d.Density, All.SNIaDensityWeightFactor);
#endif

                  mass_j = P[j].Mass;
                  
#if !defined(OSAKA_STRONG_THERMAL_FEEDBACK) && !defined(OSAKA_CONSTANT_WIND) && !defined(OSAKA_USE_HSML_FOR_SN_FEEDBACK)
                  if(loop == 1 && r < rshock)
#else
                  if(loop == 1)
#endif
                    {
#ifndef DENSITY_WEIGHTED_VALUE
                      masswk += FLT(mass_j * wk);
#else
                      masswk += FLT(wk);
#endif
                      ngb += 1.0;
                      ngbmass += mass_j;
                    }
                    
#if !defined(OSAKA_STRONG_THERMAL_FEEDBACK) && !defined(OSAKA_CONSTANT_WIND) && !defined(OSAKA_USE_HSML_FOR_SN_FEEDBACK)
                  if(loop == 2 && r < rshock)
#else
                  if(loop == 2)
#endif
                    {
#ifndef DENSITY_WEIGHTED_VALUE
                      mjwk = mass_j * wk;
#else
                      mjwk = wk;
#endif

#ifndef OSAKA_MECHANICAL_FB
                      SphP[j].KineticEnergySNIa += FLT(sniak_energy * mjwk / masswk);
                      SphP[j].ThermalEnergySNIa += FLT(sniath_energy * mjwk / masswk);
#else

                      pejin = FLT(ejecta_momentum * mjwk / masswk);
                      massin = FLT(snia_mass_released * mjwk / masswk);
                      boost_factor = sqrt(1.0 + P[j].Mass / massin);
                      ptoverpej = terminal_momentum / ejecta_momentum;
                      
                      if(boost_factor < ptoverpej)
                        SNIamomentum = boost_factor * pejin;
                      else
                        SNIamomentum = FLT(terminal_momentum * mjwk / masswk);
                        
                      energymomentum = 0.5 * pejin * pejin / massin;
                      snia_energyin = FLT(sniath_energy * mjwk / masswk) + energymomentum;
                      
                      if(r > rshock)
                        snia_energyin *= pow(r / rshock, - 6.5);
                        
                      SphP[j].ThermalEnergySNIa += snia_energyin;
                      
#ifdef OSAKA_ISOTROPIC_WIND
                      theta = acos(2 * get_random_number(IDStar + P[j].ID + fbflagSNIa + 3) - 1);
                      phi = 2 * M_PI * get_random_number(IDStar + P[j].ID + fbflagSNIa + 4);
                      
                      dir[0] = sin(theta) * cos(phi);
                      dir[1] = sin(theta) * sin(phi);
                      dir[2] = cos(theta);
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagSNIa + 5) < 0.5)
                        norm = -norm;
                        
#elif OSAKA_RADIAL_DIRECTION_WIND
                      dir[0] = P[j].Pos[0] - pos[0];
                      dir[1] = P[j].Pos[1] - pos[1];
                      dir[2] = P[j].Pos[2] - pos[2];
                      
#ifdef PERIODIC
                      if(dir[0] > boxHalf_X)
                        dir[0] -= boxSize_X;
                      if(dir[0] < -boxHalf_X)
                        dir[0] += boxSize_X;
                      if(dir[1] > boxHalf_Y)
                        dir[1] -= boxSize_Y;
                      if(dir[1] < -boxHalf_Y)
                        dir[1] += boxSize_Y;
                      if(dir[2] > boxHalf_Z)
                        dir[2] -= boxSize_Z;
                      if(dir[2] < -boxHalf_Z)
                        dir[2] += boxSize_Z;
#endif

                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
#else
                      dir[0] = P[j].g.GravAccel[1] * P[j].Vel[2] - P[j].g.GravAccel[2] * P[j].Vel[1];
                      dir[1] = P[j].g.GravAccel[2] * P[j].Vel[0] - P[j].g.GravAccel[0] * P[j].Vel[2];
                      dir[2] = P[j].g.GravAccel[0] * P[j].Vel[1] - P[j].g.GravAccel[1] * P[j].Vel[0];
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagSNIa + 5) < 0.5)
                        norm = -norm;
#endif

                      if(norm != 0)
                        {
                          for(k = 0; k < 3; k++)
                            dir[k] /= norm;
                            
                          for(k = 0; k < 3; k++)
                            SphP[j].MomentumSNIa[k] += SNIamomentum * dir[k];
                        }
#endif

#ifndef OSAKA_MINMAX_NEIGHBOUR_NUMBER
                      SphP[j].Rshock = rshock;
#else
                      SphP[j].Rshock = rshock_real;
#endif
                      SphP[j].timeSNIa = timesnia;
                      SphP[j].timeNoCoolingSNIa = timenocoolingsnia;
                      
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      SphP[j].MassStar = initmass;
                      SphP[j].IDStar = IDStar;
                      P[j].FBflagSNIa = fbflagSNIa;
                      P[j].SNIaNgbMass = ngbmass;
                      
                      SphP[j].StarSNIaDensity = stardensity;
                      
#ifdef OSAKA_CELIB
                      P[j].Mass += FLT(snia_mass_released * mjwk / masswk);
                      
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals[k] += FLT(metal_mass_element[k] * mjwk / masswk);
                        
                      P[j].Metallicity = (P[j].Mass - P[j].Metals[0] - P[j].Metals[1]) / P[j].Mass;
                      
#ifdef OSAKA_METAL_ELEMENT_TRACE_SNIa
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals_SNIa[k] += FLT(metal_mass_element[k] * mjwk / masswk);
#endif
#endif
                    }
                }
            }// end of a for-loop for ngb_inbox
        }
        
      if(mode == 1)
        {
          listindex++;
          
          if(listindex < NODELISTLENGTH)
            {
              startnode = SNIaDataGet[target].NodeList[listindex];
              
              if(startnode >= 0)
                startnode = Nodes[startnode].u.d.nextnode;// open it //
            }
        }
    }
    
  if(loop == 1)
    {
      if(mode == 0)
        {
          MassWk[target].Masswk = masswk;
          P[target].SNIaNgbNum = ngb;
          P[target].SNIaNgbMass = ngbmass;
          MassWk[target].RminFeedbackInteraction = rminfeedbackinteraction;
          MassWk[target].PID_Rmin = pid_rmin;
        }
      else
        {
          SNIaDataResult[target].Masswk = masswk;
          SNIaDataResult[target].SNIaNgbNum = ngb;
          SNIaDataResult[target].SNIaNgbMass = ngbmass;
          SNIaDataResult[target].RminFeedbackInteraction = rminfeedbackinteraction;
          SNIaDataResult[target].PID_Rmin = pid_rmin;
        }
    }
    
  return 0;
}

void snia_feedback_kick(void)
{
  int i, k;
  int ngas_local, ngas_total;
  double vshock, norm, dir[3];
  double sniak_energy, density;
  double time, ascale, a3inv, hubble;
#ifdef OSAKA_ISOTROPIC_WIND
  double theta, phi;
#endif
#ifdef OSAKA_MECHANICAL_FB
  double vpre, dv[3], momentnow;
#endif

  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      time = All.UnitTime_in_s / SEC_PER_GIGAYEAR * All.Time;
    }
    
  for(i = FirstActiveParticle, ngas_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 0)
        {
#ifndef OSAKA_MECHANICAL_FB
          // SNIa kick velocity calculation //
          if(SphP[i].KineticEnergySNIa > 0.0)
            {
              sniak_energy = SphP[i].KineticEnergySNIa * All.UnitEnergy_in_cgs;
              density = SphP[i].StarSNIaDensity * All.UnitDensity_in_cgs / PROTONMASS;
              density *= hubble * hubble * a3inv;
              
#ifdef OSAKA_KICK_ENERGY_CONSERVATION
              // velocity kick form energy conservation //
              vshock = sqrt(2.0 * sniak_energy / P[i].Mass / All.UnitMass_in_g) / All.UnitVelocity_in_cm_per_s;
#else
              // SEDOV-TAYLOR velocity kick //
              vshock = 151.41 * 1.0e5 * pow(All.STCoolingFactor, - 0.2) * pow(sniak_energy / All.Energy_per_SNIa, 0.07) * pow(density, 0.14)  / All.UnitVelocity_in_cm_per_s;
#endif

#ifndef OSAKA_GEODESIC_DOME_WEIGHT
#ifdef OSAKA_ISOTROPIC_WIND
              theta = acos(2 * get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa + 3) - 1);
              phi = 2 * M_PI * get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa + 4);
              
              dir[0] = sin(theta) * cos(phi);
              dir[1] = sin(theta) * sin(phi);
              dir[2] = cos(theta);
              
              for(k = 0, norm = 0; k < 3; k++)
                norm += dir[k] * dir[k];
                
              norm = sqrt(norm);
              
              if(get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa + 5) < 0.5)
                norm = -norm;
                
#elif OSAKA_RADIAL_DIRECTION_WIND
              dir[0] = P[i].Pos[0] - SphP[i].PosStar[0];
              dir[1] = P[i].Pos[1] - SphP[i].PosStar[1];
              dir[2] = P[i].Pos[2] - SphP[i].PosStar[2];
              
#ifdef PERIODIC
                      if(dir[0] > boxHalf_X)
                        dir[0] -= boxSize_X;
                      if(dir[0] < -boxHalf_X)
                        dir[0] += boxSize_X;
                      if(dir[1] > boxHalf_Y)
                        dir[1] -= boxSize_Y;
                      if(dir[1] < -boxHalf_Y)
                        dir[1] += boxSize_Y;
                      if(dir[2] > boxHalf_Z)
                        dir[2] -= boxSize_Z;
                      if(dir[2] < -boxHalf_Z)
                        dir[2] += boxSize_Z;
#endif

              for(k = 0, norm = 0; k < 3; k++)
                norm += dir[k] * dir[k];
                
              norm = sqrt(norm);
#else
              dir[0] = P[i].g.GravAccel[1] * P[i].Vel[2] - P[i].g.GravAccel[2] * P[i].Vel[1];
              dir[1] = P[i].g.GravAccel[2] * P[i].Vel[0] - P[i].g.GravAccel[0] * P[i].Vel[2];
              dir[2] = P[i].g.GravAccel[0] * P[i].Vel[1] - P[i].g.GravAccel[1] * P[i].Vel[0];
              
              for(k = 0, norm = 0; k < 3; k++)
                norm += dir[k] * dir[k];
                
              norm = sqrt(norm);
              
              if(get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa + 5) < 0.5)
                norm = -norm;
#endif
#else
              dir[0] = SphP[i].MomentumSNIa[0];
              dir[1] = SphP[i].MomentumSNIa[1];
              dir[2] = SphP[i].MomentumSNIa[2];
              
              SphP[i].MomentumSNIa[0] = 0.0;
              SphP[i].MomentumSNIa[1] = 0.0;
              SphP[i].MomentumSNIa[2] = 0.0;
#endif
              if(norm != 0)
                {
                  for(k = 0; k < 3; k++)
                    dir[k] /= norm;
                    
                  for(k = 0; k < 3; k++)
                    {
                      P[i].Vel[k] += vshock * ascale * dir[k];
                      SphP[i].VelPred[k] += vshock * ascale * dir[k];
                    }
                }
                
              SphP[i].WindVelocity = vshock;
              SphP[i].KineticEnergySNIa = 0.0;
              SphP[i].flagCoolingSNIa = 1;
              
              ngas_local++;
            }
          else
            {
              if(SphP[i].ThermalEnergySNIa > 0.0)
                {
                  SphP[i].KineticEnergySNIa = 0.0;
                  SphP[i].flagCoolingSNIa = 1;
                }
            }
#else
          if(SphP[i].MomentumSNIa[0] != 0.0 || SphP[i].MomentumSNIa[1] != 0.0 || SphP[i].MomentumSNIa[2] != 0.0)
            {
              for(k = 0; k < 3; k++)
                {
                  vpre = P[i].Vel[k];
                  momentnow = P[i].Mass * P[i].Vel[k] / ascale + SphP[i].MomentumSNIa[k];
                  dv[k] =  momentnow / P[i].Mass * ascale - vpre;
                  P[i].Vel[k] += dv[k];
                  SphP[i].VelPred[k] += dv[k];
                  SphP[i].MomentumSNIa[k] = 0.0;
                }
                
              vshock = sqrt(dv[0] * dv[0] + dv[1] * dv[1] + dv[2] * dv[2]) / ascale;
              SphP[i].WindVelocity = vshock;
              SphP[i].KineticEnergySNIa = 0.0;
              SphP[i].flagCoolingSNIa = 0;
              
              ngas_local++;
            }
#endif
        }
    }
    
  MPI_Allreduce(&ngas_local, &ngas_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(ThisTask == 0)
    if(ngas_total > 0)
      printf("%d gas particles go into wind\n", ngas_total);
}

#ifdef OSAKA_STRONG_THERMAL_FEEDBACK
void snia_feedback_hot_bubble(void)
{
  int i, k;
  int ngas_local, ngas_total;
  double fac_hot_bubble_energy, hot_bubble_energy, snia_energy, prob;
  double time, ascale, a3inv, hubble;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      time = All.UnitTime_in_s / SEC_PER_GIGAYEAR * All.Time;
    }
    
  fac_hot_bubble_energy = 3.0 / 2.0 / 0.6 / PROTONMASS * BOLTZMANN * pow(10.0, All.TemperatureSNIaFeedback) * All.UnitMass_in_g / All.UnitEnergy_in_cgs;
  
  for(i = FirstActiveParticle, ngas_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 0)
        {
          if(SphP[i].ThermalEnergySNIa > 0.0)
            {
              hot_bubble_energy = fac_hot_bubble_energy * P[i].Mass;
              
              snia_energy = SphP[i].SNIaEnergyStorage + SphP[i].ThermalEnergySNIa;
              prob = snia_energy / hot_bubble_energy;
              
              if(prob >= get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa))
                {
                  SphP[i].ThermalEnergySNIa = hot_bubble_energy;
                  SphP[i].SNIaEnergyStorage = 0.0;
                  SphP[i].flagCoolingSNIa = 0;
                  ngas_local++;
                }
              else
                {
                  SphP[i].SNIaEnergyStorage = 0.0;
                  //SphP[i].SNIaEnergyStorage = snia_energy;
                  SphP[i].ThermalEnergySNIa = 0.0;
                  SphP[i].flagCoolingSNIa = 0;
                }
            }
        }
    }
    
  MPI_Allreduce(&ngas_local, &ngas_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(ThisTask == 0)
    if(ngas_total > 0)
      printf("%d gas particles become hot bubble\n", ngas_total);
}
#endif

#ifdef OSAKA_CONSTANT_WIND
void snia_constant_wind_kick(void)
{
  int i, k;
  int ngas_local, ngas_total;
  double vshock, norm, dir[3];
  double fac_wind_energy, wind_energy, snia_energy, prob;
  double time, ascale, a3inv, hubble;
#ifdef OSAKA_ISOTROPIC_WIND
  double theta, phi;
#endif
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      time = All.UnitTime_in_s / SEC_PER_GIGAYEAR * All.Time;
    }
    
  fac_wind_energy = 0.5 * All.WindVelocitySNIa * All.WindVelocitySNIa * All.UnitVelocity_in_cm_per_s * All.UnitVelocity_in_cm_per_s * All.UnitMass_in_g / All.UnitEnergy_in_cgs;
  
  for(i = FirstActiveParticle, ngas_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 0)
        {
          if(SphP[i].KineticEnergySNIa > 0.0)
            {
              wind_energy = fac_wind_energy * P[i].Mass;
              
              snia_energy = SphP[i].SNIaEnergyStorage + SphP[i].KineticEnergySNIa;
              prob = All.EtaSNIa * SphP[i].MassStar / P[i].SNIaNgbMass * snia_energy / wind_energy;
              //prob = All.EtaSNIa * SphP[i].MassStar / P[i].SNIaNgbMass / (double) All. SNIaEventNumber;
              //prob = All.EtaSNIa * SphP[i].MassStar / P[i].SNIaNgbMass / (double) All. SNIaEventNumber * snia_energy / wind_energy;
              
              if(prob >= get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa))
                {
                  vshock = All.WindVelocitySNIa;
                  
#ifdef OSAKA_ISOTROPIC_WIND
                  theta = acos(2 * get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa + 3) - 1);
                  phi = 2 * M_PI * get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa + 4);
                  
                  dir[0] = sin(theta) * cos(phi);
                  dir[1] = sin(theta) * sin(phi);
                  dir[2] = cos(theta);
                  
                  for(k = 0, norm = 0; k < 3; k++)
                    norm += dir[k] * dir[k];
                    
                  norm = sqrt(norm);
                  
                  if(get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa + 5) < 0.5)
                    norm = -norm;
                    
#elif OSAKA_RADIAL_DIRECTION_WIND
                  dir[0] = P[i].Pos[0] - SphP[i].PosStar[0];
                  dir[1] = P[i].Pos[1] - SphP[i].PosStar[1];
                  dir[2] = P[i].Pos[2] - SphP[i].PosStar[2];
                  
                  for(k = 0, norm = 0; k < 3; k++)
                    norm += dir[k] * dir[k];
                    
                  norm = sqrt(norm);
#else
                  dir[0] = P[i].g.GravAccel[1] * P[i].Vel[2] - P[i].g.GravAccel[2] * P[i].Vel[1];
                  dir[1] = P[i].g.GravAccel[2] * P[i].Vel[0] - P[i].g.GravAccel[0] * P[i].Vel[2];
                  dir[2] = P[i].g.GravAccel[0] * P[i].Vel[1] - P[i].g.GravAccel[1] * P[i].Vel[0];
                  
                  for(k = 0, norm = 0; k < 3; k++)
                    norm += dir[k] * dir[k];
                    
                  norm = sqrt(norm);
                  
                  if(get_random_number(SphP[i].IDStar + P[i].ID + P[i].FBflagSNIa + 5) < 0.5)
                    norm = -norm;
#endif

                  if(norm != 0)
                    {
                      for(k = 0; k < 3; k++)
                        dir[k] /= norm;
                        
                      for(k = 0; k < 3; k++)
                        {
                          P[i].Vel[k] += vshock * ascale * dir[k];
                          SphP[i].VelPred[k] += vshock * ascale * dir[k];
                        }
                    }
                    
                  SphP[i].WindVelocity = vshock;
                  SphP[i].KineticEnergySNIa = 0.0;
                  SphP[i].SNIaEnergyStorage = 0.0;
                  SphP[i].flagCoolingSNIa = 0;
                  ngas_local++;
                }
              else
                {
                  SphP[i].SNIaEnergyStorage = 0.0;
                  //SphP[i].SNIaEnergyStorage = snia_energy;
                  SphP[i].KineticEnergySNIa = 0.0;
                  SphP[i].flagCoolingSNIa = 0;
                }
            }
        }
    }
    
  MPI_Allreduce(&ngas_local, &ngas_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(ThisTask == 0)
    if(ngas_total > 0)
      printf("%d gas particles go into wind\n", ngas_total);
}
#endif

void calc_snia_shock_radius(int target)
{
  int nfb;
  double density, snia_energy, pressure, cs, rshock, timesnia;
  double snia_mass_released, next_snia_explosion_time;
  static double snia_metal_yield[N_YIELD_ELEMENT_CELIB + 1];
  double ascale, a3inv, time, hubble;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (All.Time * All.Time * All.Time);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
    }
  else
    {
      time = All.Time;
      ascale = a3inv = hubble = 1.0;
    }
    
#ifdef OSAKA_CELIB
  if(All.SNIaEventNumber == 1)
    calc_instantaneous_snia_yield(P[target].Metallicity, CELib_yield, &snia_energy, &snia_mass_released, snia_metal_yield);
  else
    calc_snia_yield(target, snia_energy_table, snia_mass_released_table, snia_metal_yield_table, &snia_energy, &snia_mass_released, snia_metal_yield, &nfb, &next_snia_explosion_time);
#endif
  snia_energy = snia_energy * P[target].InitialStarMass * All.UnitMass_in_g / SOLAR_MASS;
  density = P[target].Density * All.UnitDensity_in_cgs / PROTONMASS;
  density *= hubble * hubble * a3inv;
  
#ifdef OSAKA_MECHANICAL_FB
#ifndef CHEVALIER1974
  rshock = 8.763e19 * pow(snia_energy / All.Energy_per_SNIa, 0.28571428571) * pow(density, - 0.4285714) * fz_SNIa(P[target].Metallicity) / All.UnitLength_in_cm * hubble / ascale;
#else
  pressure = P[target].Pressure * All.UnitPressure_in_cgs * 1.0e-4 / BOLTZMANN * a3inv / ascale / ascale * hubble * hubble;
  rshock = 1.6957e20 * pow(snia_energy / All.Energy_per_SNIa, 0.32) *  pow(density, - 0.16) * pow(pressure, - 0.20) / All.UnitLength_in_cm * hubble / ascale;
#endif
  timesnia = 0.0;
#else
#ifndef CHEVALIER1974
  pressure = P[target].Pressure * All.UnitPressure_in_cgs * a3inv / ascale / ascale * hubble * hubble;
  cs = sqrt(GAMMA * pressure / density / PROTONMASS) / 1.0e6; // in 10 km/s //
  rshock = 8.462e19 * pow(All.STCoolingFactor, 0.1315) * pow(snia_energy / All.Energy_per_SNIa, 0.29) * pow(density, - 0.42) / All.UnitLength_in_cm * hubble / ascale;
  timesnia = 7.08e4 * pow(All.STCoolingFactor, 0.33) * pow(snia_energy / All.Energy_per_SNIa, 0.22) * pow(density, - 0.55) * TIME_GYR;
#else
  pressure = P[target].Pressure * All.UnitPressure_in_cgs * 1.0e-4 / BOLTZMANN * a3inv / ascale / ascale * hubble * hubble;
  rshock = 1.6957e20 * pow(snia_energy / All.Energy_per_SNIa, 0.32) *  pow(density, - 0.16) * pow(pressure, - 0.20) / All.UnitLength_in_cm * hubble / ascale;
  timesnia = 8.31763e5 * pow(snia_energy / All.Energy_per_SNIa, 0.31) *  pow(density, 0.27) * pow(pressure, - 0.64) * TIME_GYR;
#endif
#endif
  
  P[target].Rshock = rshock;
  P[target].TimeSNIa = timesnia;
  
#ifdef OSAKA_STRONG_THERMAL_FEEDBACK
  P[target].TimeSNIa = 0.0;
#endif
}

double fz_SNIa(double metallicity)
{
  double z, fz;
  
  z = metallicity / All.SolarMetallicity;
  
  if(z < 0.01)
    z = 0.01;
    
  fz = pow(z, - 0.14);
  
  return fz;
}

int ngb_treefind_snia(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local)
{
  int numngb, no, p, task, nexport_save;
  struct NODE *current;
  double dx, dy, dz, dist;
#ifdef PERIODIC
  MyDouble xtmp;
#endif
  
  nexport_save = *nexport;
  numngb = 0;
  no = *startnode;
  
  while(no >= 0)
    {
      if(no < All.MaxPart)// single particle //
        {
          p = no;
          no = Nextnode[no];
          
          if(P[p].Type != 0)
            continue;
            
          if(P[p].Ti_current != All.Ti_Current)
            drift_particle(p, All.Ti_Current);
            
          dist = hsml;
#ifdef PERIODIC
          dx = NGB_PERIODIC_LONG_X((double) P[p].Pos[0] - searchcenter[0]);
          
          if(dx > dist)
            continue;
            
          dy = NGB_PERIODIC_LONG_Y((double) P[p].Pos[1] - searchcenter[1]);
          
          if(dy > dist)
            continue;
            
          dz = NGB_PERIODIC_LONG_Z((double) P[p].Pos[2] - searchcenter[2]);
          
          if(dz > dist)
                continue;
#else
          dx = (double) P[p].Pos[0] - searchcenter[0];
          
          if(dx > dist)
            continue;
            
          dy = (double) P[p].Pos[1] - searchcenter[1];
          
          if(dy > dist)
            continue;
            
          dz = (double) P[p].Pos[2] - searchcenter[2];
          
          if(dz > dist)
            continue;
#endif
          if(dx * dx + dy * dy + dz * dz > dist * dist)
            continue;
            
          Ngblist[numngb++] = p;
        }
      else
        {
          if(no >= All.MaxPart + MaxNodes)// pseudo particle //
            {
              if(mode == 1)
                endrun(12312);
                
              if(target >= 0)// if no target is given, export will not occur //
                {
                  if(Exportflag[task = DomainTask[no - (All.MaxPart + MaxNodes)]] != target)
                    {
                      Exportflag[task] = target;
                      Exportnodecount[task] = NODELISTLENGTH;
                    }
                    
                  if(Exportnodecount[task] == NODELISTLENGTH)
                    {
                      if(*nexport >= All.BunchSize)
                        {
                          *nexport = nexport_save;
                          
                          if(nexport_save == 0)
                            endrun(13004);// in this case, the buffer is too small to process even a single particle //
                            
                          for(task = 0; task < NTask; task++)
                            nsend_local[task] = 0;
                            
                          for(no = 0; no < nexport_save; no++)
                            nsend_local[DataIndexTable[no].Task]++;
                            
                          return -1;
                        }
                        
                      Exportnodecount[task] = 0;
                      Exportindex[task] = *nexport;
                      DataIndexTable[*nexport].Task = task;
                      DataIndexTable[*nexport].Index = target;
                      DataIndexTable[*nexport].IndexGet = *nexport;
                      *nexport = *nexport + 1;
                      nsend_local[task]++;
                    }
                    
                  DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]++] =
                  DomainNodeIndex[no - (All.MaxPart + MaxNodes)];
                  
                  if(Exportnodecount[task] < NODELISTLENGTH)
                    DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]] = -1;
                }
                
              no = Nextnode[no - MaxNodes];
              continue;
            }
            
          current = &Nodes[no];
          
          if(mode == 1)
            {
              // we reached a top-level node again, which means that we are done with the branch //
              if(current->u.d.bitflags & (1 << BITFLAG_TOPLEVEL))
                {
                  *startnode = -1;
                  return numngb;
                }
            }
            
          if(current->Ti_current != All.Ti_Current)
            force_drift_node(no, All.Ti_Current);
            
          no = current->u.d.sibling;// in case the node can be discarded //
          
          dist = hsml + 0.5 * current->len;
#ifdef PERIODIC	  
          dx = NGB_PERIODIC_LONG_X((double) current->center[0] - searchcenter[0]);
          
          if(dx > dist)
            continue;
            
          dy = NGB_PERIODIC_LONG_Y((double) current->center[1] - searchcenter[1]);
          
          if(dy > dist)
            continue;
            
          dz = NGB_PERIODIC_LONG_Z((double) current->center[2] - searchcenter[2]);
          
          if(dz > dist)
            continue;
#else
          dx = (double) current->center[0] - searchcenter[0];
          
          if(dx > dist)
            continue;
            
          dy = (double) current->center[1] - searchcenter[1];
          
          if(dy > dist)
            continue;
            
          dz = (double) current->center[2] - searchcenter[2];
          
          if(dz > dist)
            continue;
#endif
          // now test against the minimal sphere enclosing everything //
          dist += FACT1 * (double) current->len;
          
          if(dx * dx + dy * dy + dz * dz > dist * dist)
            continue;
            
          no = current->u.d.nextnode;// ok, we need to open the node //
        }
    }
    
  *startnode = -1;
  
  return numngb;
}

#ifdef OSAKA_GEODESIC_DOME_WEIGHT
int snia_feedback_evaluate_gdw(int target, int mode, int loop, int *nexport, int *nsend_local)
{
  int i, j, k, n, nfb, fbflagSNIa;
  int startnode, numngb_inbox, listindex;
  double h, h2, dx, dy, dz, r, r2, mass_j;
  double ascale, a3inv, hubble, time, factor_time;
  double pos[3], mass, stardensity, initmass, ngb, ngbmass, rshock, rshock2, rminfeedbackinteraction;
  double timesnia, snia_explosion_time, timenocoolingsnia;
  double snia_mass_released, metal_mass_element[N_YIELD_ELEMENT_CELIB], next_snia_explosion_time;
  double snia_energy, sniath_energy, sniak_energy;
  static double snia_metal_yield[N_YIELD_ELEMENT_CELIB];
  MyIDType pid_rmin, IDStar;
  struct Surface surface[N_SURFACE_TOTAL_LV1];
#ifdef OSAKA_MECHANICAL_FB
  double ejecta_momentum, terminal_momentum, densityin, snia_energyin, starmetallicity, pejin, massin;
  double boost_factor, ptoverpej, SNIamomentum, energymomentum, theta, phi, norm, dir[3];
#endif
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  double hmin, fac_hmin, hmax, fac_hmax, rshock_real;
#endif

#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  fac_hmin = pow((double) All.MinNumNgbSNIa / (double) All.DesNumNgbSNIa, 0.333333);
  fac_hmax = pow((double) All.MaxNumNgbSNIa / (double) All.DesNumNgbSNIa, 0.333333);
#endif

  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (All.Time * All.Time * All.Time);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
      factor_time = 1.0;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      factor_time = All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  if(mode == 0)
    {
      osaka_assign_surface_snia(surface, MassWk[target].surface);
      pos[0] = P[target].Pos[0];
      pos[1] = P[target].Pos[1];
      pos[2] = P[target].Pos[2];
      h = P[target].Hsml;
      mass = P[target].Mass;
      initmass = P[target].InitialStarMass;
      rshock = P[target].Rshock;
      snia_explosion_time = P[target].SNIaExplosionTime;
#ifdef OSAKA_MECHANICAL_FB
      starmetallicity = P[target].Metallicity;
#endif
      
      if(loop == 1)
        {
          ngb = ngbmass = 0.0;
          rminfeedbackinteraction = 1.0e30;
        }
        
      if(loop == 2)
        {
          ngb = P[target].SNIaNgbNum;
          ngbmass = P[target].SNIaNgbMass;
          stardensity = P[target].Density;
          timesnia = time + P[target].TimeSNIa;
          timenocoolingsnia = P[target].TimeSNIa;
          rminfeedbackinteraction = MassWk[target].RminFeedbackInteraction;
          pid_rmin = MassWk[target].PID_Rmin;
          IDStar = P[target].ID;
          fbflagSNIa = P[target].FBflagSNIa;
          
#ifdef OSAKA_NO_COOLING_OFF
          timesnia = time;
#endif

#ifdef OSAKA_CELIB
          if(All.SNIaEventNumber == 1)
            calc_instantaneous_snia_yield(P[target].Metallicity, CELib_yield, &snia_energy, &snia_mass_released, snia_metal_yield);
          else
            calc_snia_yield(target, snia_energy_table, snia_mass_released_table, snia_metal_yield_table, &snia_energy, &snia_mass_released, snia_metal_yield, &nfb, &next_snia_explosion_time);
            
          snia_mass_released = P[target].InitialStarMass * snia_mass_released;
          
          for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
            metal_mass_element[i] = P[target].InitialStarMass * snia_metal_yield[i];
#endif
        }
    }
  else
    {
      osaka_assign_surface_snia(surface, SNIaDataGet[target].surface);
      pos[0] = SNIaDataGet[target].Pos[0];
      pos[1] = SNIaDataGet[target].Pos[1];
      pos[2] = SNIaDataGet[target].Pos[2];
      h = SNIaDataGet[target].Hsml;
      mass = SNIaDataGet[target].Mass;
      initmass = SNIaDataGet[target].InitialStarMass;
      rshock = SNIaDataGet[target].Rshock;
      snia_explosion_time = SNIaDataGet[target].SNIaExplosionTime;
#ifdef OSAKA_MECHANICAL_FB
      starmetallicity = SNIaDataGet[target].Metallicity;
#endif
      
      if(loop == 1)
        {
          ngb = ngbmass = 0.0;
          rminfeedbackinteraction = SNIaDataGet[target].RminFeedbackInteraction;
        }
        
      if(loop == 2)
        {
          ngb = SNIaDataGet[target].SNIaNgbNum;
          ngbmass = SNIaDataGet[target].SNIaNgbMass;
          stardensity = SNIaDataGet[target].StarDensity;
          timesnia = time + SNIaDataGet[target].TimeSNIa;
          timenocoolingsnia = SNIaDataGet[target].TimeSNIa;
          rminfeedbackinteraction = SNIaDataGet[target].RminFeedbackInteraction;
          pid_rmin = SNIaDataGet[target].PID_Rmin;
          IDStar = SNIaDataGet[target].IDStar;
          fbflagSNIa = SNIaDataGet[target].FBflagSNIa;
          
#ifdef OSAKA_NO_COOLING_OFF
          timesnia = time;
#endif

#ifdef OSAKA_CELIB
          snia_energy = SNIaDataGet[target].SNIaEnergy;
          snia_mass_released = SNIaDataGet[target].MassReleased;
          
          for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
            metal_mass_element[i] = SNIaDataGet[target].Metals[i];
#endif
        }
    }
    
  if(loop == 2)
    {
      snia_energy = snia_energy * initmass * All.UnitMass_in_g / SOLAR_MASS;
#ifndef OSAKA_MECHANICAL_FB
      sniath_energy = All.SNIaThermalFBEfficiency * snia_energy / All.UnitEnergy_in_cgs;
      sniak_energy = All.SNIaKineticFBEfficiency * snia_energy / All.UnitEnergy_in_cgs;
#else
      sniath_energy = snia_energy / All.UnitEnergy_in_cgs;
      sniak_energy = 0.0;
      ejecta_momentum = sqrt(2.0 * snia_mass_released / hubble * All.UnitMass_in_g * snia_energy) / All.UnitMass_in_g / All.UnitVelocity_in_cm_per_s;
      snia_energyin = snia_energy / All.Energy_per_SNIa;
      densityin = stardensity * All.UnitDensity_in_cgs / PROTONMASS * hubble * hubble * a3inv;
      terminal_momentum = 9.55e43 * pow(snia_energyin, 13.0 / 14.0) * pow(densityin, - 1.0 / 7.0) * pow(fz_SNIa(starmetallicity), 1.5) / All.UnitMass_in_g / All.UnitVelocity_in_cm_per_s;
#endif
    }
    
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  rshock_real = rshock;
#endif

#if !defined(OSAKA_STRONG_THERMAL_FEEDBACK) && !defined(OSAKA_CONSTANT_WIND) && !defined(OSAKA_USE_HSML_FOR_SN_FEEDBACK)
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  hmax = fac_hmax * h;
  
  if(hmax < rshock)
    {
      rshock_real = rshock;
      rshock = hmax;
    }
    
#endif
  if(h < rshock)
    h = rshock;
#endif
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  hmin = fac_hmin * h;
  
  if(rshock < hmin)
    {
      rshock_real = rshock;
      rshock = hmin;
    }
#endif

  h2 = h * h;
  rshock2 = rshock * rshock;
  
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = SNIaDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  if(loop == 2 && ngb == 0.0)
    {
      if(rminfeedbackinteraction < 0.0)
        return 0;
        
      while(startnode >= 0)
        {
          while(startnode >= 0)
            {
              numngb_inbox = ngb_treefind_snia(pos, h, target, &startnode, mode, nexport, nsend_local);
              
              if(numngb_inbox < 0)
                return -1;
                
              for(n = 0; n < numngb_inbox; n++)
                {
                  j = Ngblist[n];
                  
                  if(pid_rmin == P[j].ID)
                    {
                      dx = pos[0] - P[j].Pos[0];
                      dy = pos[1] - P[j].Pos[1];
                      dz = pos[2] - P[j].Pos[2];
                      
                      //  now find the closest image in the given box size  //
#ifdef PERIODIC
                      if(dx > boxHalf_X)
                        dx -= boxSize_X;
                      if(dx < -boxHalf_X)
                        dx += boxSize_X;
                      if(dy > boxHalf_Y)
                        dy -= boxSize_Y;
                      if(dy < -boxHalf_Y)
                        dy += boxSize_Y;
                      if(dz > boxHalf_Z)
                        dz -= boxSize_Z;
                      if(dz < -boxHalf_Z)
                        dz += boxSize_Z;
#endif
                      r2 = dx * dx + dy * dy + dz * dz;
                      r = sqrt(r2);

#ifndef OSAKA_MECHANICAL_FB
                      SphP[j].KineticEnergySNIa += sniak_energy;
                      SphP[j].ThermalEnergySNIa += sniath_energy;
#else
                      boost_factor = sqrt(1.0 + P[j].Mass / snia_mass_released);
                      ptoverpej = terminal_momentum / ejecta_momentum;
                      
                      if(boost_factor < ptoverpej)
                        SNIamomentum = boost_factor * ejecta_momentum;
                      else
                        SNIamomentum = terminal_momentum;
                        
                      energymomentum = 0.5 * ejecta_momentum * ejecta_momentum / snia_mass_released;
                      snia_energyin = sniath_energy + energymomentum;
                      
                      if(r > rshock)
                        snia_energyin *= pow(r/ rshock, - 6.5);
                        
                      SphP[j].ThermalEnergySNIa += snia_energyin;
                      
                      dir[0] = P[j].Pos[0] - pos[0];
                      dir[1] = P[j].Pos[1] - pos[1];
                      dir[2] = P[j].Pos[2] - pos[2];
                      
#ifdef PERIODIC
                      if(dir[0] > boxHalf_X)
                        dir[0] -= boxSize_X;
                      if(dir[0] < -boxHalf_X)
                        dir[0] += boxSize_X;
                      if(dir[1] > boxHalf_Y)
                        dir[1] -= boxSize_Y;
                      if(dir[1] < -boxHalf_Y)
                        dir[1] += boxSize_Y;
                      if(dir[2] > boxHalf_Z)
                        dir[2] -= boxSize_Z;
                      if(dir[2] < -boxHalf_Z)
                        dir[2] += boxSize_Z;
#endif

                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(norm != 0)
                        {
                          for(k = 0; k < 3; k++)
                            dir[k] /= norm;
                            
                          for(k = 0; k < 3; k++)
                            SphP[j].MomentumSNIa[k] += SNIamomentum * dir[k];
                        }
#endif

#ifndef OSAKA_MINMAX_NEIGHBOUR_NUMBER
                      SphP[j].Rshock = rshock;
#else
                      SphP[j].Rshock = rshock_real;
#endif
                      SphP[j].timeSNIa = timesnia;
                      SphP[j].timeNoCoolingSNIa = timenocoolingsnia;
                      
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      SphP[j].MassStar = initmass;
                      SphP[j].IDStar= IDStar;
                      P[j].FBflagSNIa = fbflagSNIa;
                      P[j].SNIaNgbMass = P[j].Mass;
                      
                      SphP[j].StarSNIaDensity = stardensity;
                      
#ifdef OSAKA_CELIB
                      P[j].Mass += snia_mass_released;
                      
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals[k] += metal_mass_element[k];
                        
                      P[j].Metallicity = (P[j].Mass - P[j].Metals[0] - P[j].Metals[1]) / P[j].Mass;
                      
#ifdef OSAKA_METAL_ELEMENT_TRACE_SNIa
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals_SNIa[k] += metal_mass_element[k];
#endif
#endif

                      if(mode == 0)
                        MassWk[target].RminFeedbackInteraction = - 1.0;
                        
                      return 0;
                    }
                }// end of a for-loop for ngb_inbox
            }
            
          if(mode == 1)
            {
              listindex++;
              
              if(listindex < NODELISTLENGTH)
                {
                  startnode = SNIaDataGet[target].NodeList[listindex];
                  
                  if(startnode >= 0)
                    startnode = Nodes[startnode].u.d.nextnode;// open it //
                }
            }
        }
    }
    
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = SNIaDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  while(startnode >= 0)
    {
      while(startnode >= 0)
        {
          numngb_inbox = ngb_treefind_snia(pos, h, target, &startnode, mode, nexport, nsend_local);
          
          if(numngb_inbox < 0)
            return -1;
            
          for(n = 0; n < numngb_inbox; n++)
            {
              j = Ngblist[n];
              
#ifdef OSAKA_HYDRO_INTERACTION_OFF
              if(SphP[j].flagCoolingSNIa == 1 || SphP[j].flagCoolingSNIa == 1)
                continue;
#endif

              dx = pos[0] - P[j].Pos[0];
              dy = pos[1] - P[j].Pos[1];
              dz = pos[2] - P[j].Pos[2];
              
              //  now find the closest image in the given box size  //
#ifdef PERIODIC
              if(dx > boxHalf_X)
                dx -= boxSize_X;
              if(dx < -boxHalf_X)
                dx += boxSize_X;
              if(dy > boxHalf_Y)
                dy -= boxSize_Y;
              if(dy < -boxHalf_Y)
                dy += boxSize_Y;
              if(dz > boxHalf_Z)
                dz -= boxSize_Z;
              if(dz < -boxHalf_Z)
                dz += boxSize_Z;
#endif
              r2 = dx * dx + dy * dy + dz * dz;
              
              if(r2 < h2)
                {
                  r = sqrt(r2);
                  
                  if(loop == 1)
                    if(rminfeedbackinteraction > r)
                      {
                        rminfeedbackinteraction = r;
                        pid_rmin = P[j].ID;
                      }
                      
                  mass_j = P[j].Mass;
                  
#if !defined(OSAKA_STRONG_THERMAL_FEEDBACK) && !defined(OSAKA_CONSTANT_WIND) && !defined(OSAKA_USE_HSML_FOR_SN_FEEDBACK)
                  if(loop == 1 && r < rshock)
#else
                  if(loop == 1)
#endif
                    {
                      osaka_search_particle_inside_surface_snia(pos, P[j].Pos, surface);
                      ngb += 1.0;
                      ngbmass += mass_j;
                    }
                    
#if !defined(OSAKA_STRONG_THERMAL_FEEDBACK) && !defined(OSAKA_CONSTANT_WIND) && !defined(OSAKA_USE_HSML_FOR_SN_FEEDBACK)
                  if(loop == 2 && r < rshock)
#else
                  if(loop == 2)
#endif
                    {
#ifndef OSAKA_MECHANICAL_FB
                      osaka_physical_value_assignment_snia(j, r, rshock, 0.0, 0.0, snia_mass_released, metal_mass_element, sniak_energy, sniath_energy, pos, P[j].Pos, surface);
#else
                      osaka_physical_value_assignment_snia(j, r, rshock, ejecta_momentum, terminal_momentum, snia_mass_released, metal_mass_element, sniak_energy, sniath_energy, pos, P[j].Pos, surface);
#endif

#ifndef OSAKA_MINMAX_NEIGHBOUR_NUMBER
                      SphP[j].Rshock = rshock;
#else
                      SphP[j].Rshock = rshock_real;
#endif
                      SphP[j].timeSNIa = timesnia;
                      SphP[j].timeNoCoolingSNIa = timenocoolingsnia;
                      
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      SphP[j].MassStar = initmass;
                      SphP[j].IDStar = IDStar;
                      P[j].FBflagSNIa = fbflagSNIa;
                      P[j].SNIaNgbMass = ngbmass;
                      SphP[j].StarSNIaDensity = stardensity;
                    }
                }
            }// end of a for-loop for ngb_inbox
        }
        
      if(mode == 1)
        {
          listindex++;
          
          if(listindex < NODELISTLENGTH)
            {
              startnode = SNIaDataGet[target].NodeList[listindex];
              
              if(startnode >= 0)
                startnode = Nodes[startnode].u.d.nextnode;// open it //
            }
        }
    }
    
  if(loop == 1)
    {
      if(mode == 0)
        {
          osaka_assign_surface_snia(MassWk[target].surface, surface);
          P[target].SNIaNgbNum = ngb;
          P[target].SNIaNgbMass = ngbmass;
          MassWk[target].RminFeedbackInteraction = rminfeedbackinteraction;
          MassWk[target].PID_Rmin = pid_rmin;
        }
      else
        {
          osaka_assign_surface_snia(SNIaDataResult[target].surface, surface);
          SNIaDataResult[target].SNIaNgbNum = ngb;
          SNIaDataResult[target].SNIaNgbMass = ngbmass;
          SNIaDataResult[target].RminFeedbackInteraction = rminfeedbackinteraction;
          SNIaDataResult[target].PID_Rmin = pid_rmin;
        }
    }
    
  return 0;
}

void osaka_physical_value_assignment_snia(int target, double r, double rshock, double ejecta_momentum, double terminal_momentum, double snia_mass_released, double metal_mass_element[], double sniak_energy, double sniath_energy, double position_star[], double position_gas[], struct Surface surface[])
{
  int i, j, k;
  double v01[3], v02[3], vsp[3], vs0[3], re2[3], te1[3], re2e1, t, u, v;
  double norm, pejin, massin, boost_factor, ptoverpej, SNIamomentum, energymomentum, snia_energyin;
#ifdef PERIODIC
  double vsptemp[3];
#endif

  for(k = 0; k < 3; k++)
    {
#ifdef PERIODIC
      vsptemp[0] = position_gas[k] - position_star[k];
      vsptemp[1] = vsptemp[0] + header.BoxSize;
      vsptemp[2] = vsptemp[0] - header.BoxSize;
      
      if(fabs(vsptemp[0]) < fabs(vsptemp[1]))
        {
          if(fabs(vsptemp[0]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[0];
          else
            vsp[k] = vsptemp[2];
        }
      else
        {
          if(fabs(vsptemp[1]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[1];
          else
            vsp[k] = vsptemp[2];
        }
#else
      vsp[k] = position_gas[k] - position_star[k];
#endif
    }
    
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle == 0)
        continue;
      if(surface[i].nparticle == 1)
        {
          for(k = 0; k < 3; k++)
            {
              v01[k] = surface[i].vertex[1][k] - surface[i].vertex[0][k];
              v02[k] = surface[i].vertex[2][k] - surface[i].vertex[0][k];
              vs0[k] = position_star[k] - surface[i].vertex[0][k];
            }
            
          osaka_cross_product_snia(vsp, v02, re2);
          osaka_cross_product_snia(vs0, v01, te1);
          re2e1 = osaka_inner_product_snia(re2, v01);
          
          t = osaka_inner_product_snia(te1, v02) / re2e1;
          u = osaka_inner_product_snia(re2, vs0) / re2e1;
          v = osaka_inner_product_snia(te1, vsp) / re2e1;
          
          if(t < 0.0)
            continue;
            
          if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
            {
#ifndef OSAKA_MECHANICAL_FB
              SphP[target].KineticEnergySNIa += sniak_energy * surface[i].occupancy;
              SphP[target].ThermalEnergySNIa += sniath_energy * surface[i].occupancy;
#else
              pejin = ejecta_momentum * surface[i].occupancy;
              massin = snia_mass_released * surface[i].occupancy;
              boost_factor = sqrt(1.0 + P[target].Mass / massin);
              ptoverpej = terminal_momentum / ejecta_momentum;
              
              if(boost_factor < ptoverpej)
                SNIamomentum = boost_factor * pejin;
              else
                SNIamomentum = terminal_momentum * surface[i].occupancy;
                
              energymomentum = 0.5 * pejin * pejin / massin;
              snia_energyin = sniath_energy * surface[i].occupancy + energymomentum;
              
              if(r > rshock)
                snia_energyin *= pow(r / rshock, - 6.5);
                
              SphP[target].ThermalEnergySNIa += snia_energyin;
#endif

              norm = surface[i].vector[0] * surface[i].vector[0] + surface[i].vector[1] * surface[i].vector[1] + surface[i].vector[2] * surface[i].vector[2];
              norm = sqrt(norm);
              
              if(norm != 0)
                {
                  for(k = 0; k < 3; k++)
                    {
                      surface[i].vector[k] /= norm;
#ifdef OSAKA_MECHANICAL_FB
                      SphP[target].MomentumSNIa[k] += SNIamomentum * surface[i].vector[k];
#else
                      SphP[target].MomentumSNIa[k] += surface[i].vector[k];
#endif
                    }
                }
                
#ifdef OSAKA_CELIB
              P[target].Mass += snia_mass_released * surface[i].occupancy;
              
              for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                P[target].Metals[k] += metal_mass_element[k] * surface[i].occupancy;
                
              P[target].Metallicity = (P[target].Mass - P[target].Metals[0] - P[target].Metals[1]) / P[target].Mass;
              
#ifdef OSAKA_METAL_ELEMENT_TRACE_SNIa
              for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                P[target].Metals_SNIa[k] += metal_mass_element[k] * surface[i].occupancy;
#endif
#endif

              return;
            }
        }
      else
        {
          for(j = 0; j < 4; j++)
            {
              if(surface[surface[i].children[j]].nparticle == 0)
                continue;
                
              for(k = 0; k < 3; k++)
                {
                  v01[k] = surface[surface[i].children[j]].vertex[1][k] - surface[surface[i].children[j]].vertex[0][k];
                  v02[k] = surface[surface[i].children[j]].vertex[2][k] - surface[surface[i].children[j]].vertex[0][k];
                  vs0[k] = position_star[k] - surface[surface[i].children[j]].vertex[0][k];
                }
                
              osaka_cross_product_snia(vsp, v02, re2);
              osaka_cross_product_snia(vs0, v01, te1);
              re2e1 = osaka_inner_product_snia(re2, v01);
              
              t = osaka_inner_product_snia(te1, v02) / re2e1;
              u = osaka_inner_product_snia(re2, vs0) / re2e1;
              v = osaka_inner_product_snia(te1, vsp) / re2e1;
              
              if(t < 0.0)
                continue;
                
              if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
                {
#ifndef OSAKA_MECHANICAL_FB
                  SphP[target].KineticEnergySNIa += sniak_energy * surface[surface[i].children[j]].occupancy;
                  SphP[target].ThermalEnergySNIa += sniath_energy * surface[surface[i].children[j]].occupancy;
#else
                  pejin = ejecta_momentum * surface[surface[i].children[j]].occupancy;
                  massin = snia_mass_released * surface[surface[i].children[j]].occupancy;
                  boost_factor = sqrt(1.0 + P[target].Mass / massin);
                  ptoverpej = terminal_momentum / ejecta_momentum;
                  
                  if(boost_factor < ptoverpej)
                    SNIamomentum = boost_factor * pejin;
                  else
                    SNIamomentum = terminal_momentum * surface[surface[i].children[j]].occupancy;
                    
                  energymomentum = 0.5 * pejin * pejin / massin;
                  snia_energyin = sniath_energy * surface[surface[i].children[j]].occupancy + energymomentum;
                  
                  if(r > rshock)
                    snia_energyin *= pow(r / rshock, - 6.5);
                    
                  SphP[target].ThermalEnergySNIa += snia_energyin;
#endif

                  norm = surface[surface[i].children[j]].vector[0] * surface[surface[i].children[j]].vector[0] + surface[surface[i].children[j]].vector[1] * surface[surface[i].children[j]].vector[1] + surface[surface[i].children[j]].vector[2] * surface[surface[i].children[j]].vector[2];
                  norm = sqrt(norm);
                  
                  if(norm != 0)
                    {
                      for(k = 0; k < 3; k++)
                        {
                          surface[surface[i].children[j]].vector[k] /= norm;
#ifdef OSAKA_MECHANICAL_FB
                          SphP[target].MomentumSNIa[k] += SNIamomentum * surface[i].vector[k];
#else
                          SphP[target].MomentumSNIa[k] += surface[i].vector[k];
#endif
                        }
                    }
                    
#ifdef OSAKA_CELIB
                  P[target].Mass += snia_mass_released * surface[surface[i].children[j]].occupancy;
                  
                  for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                    P[target].Metals[k] += metal_mass_element[k] * surface[surface[i].children[j]].occupancy;
                    
                  P[target].Metallicity = (P[target].Mass - P[target].Metals[0] - P[target].Metals[1]) / P[target].Mass;
                  
#ifdef OSAKA_METAL_ELEMENT_TRACE_SNIa
                  for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                    P[target].Metals_SNIa[k] += metal_mass_element[k] * surface[surface[i].children[j]].occupancy;
#endif
#endif

                  return;
                }
            }
        }
    }
}

void osaka_calc_surface_area_weight_snia(struct Surface surface[])
{
  int i, j, k, l;
  int nin, surface_id;
  double vectord[3], vectordc[3], division_factor, occupancyp, occupancyc, occupancyd, occupancydc, occupancy_leftp, occupancy_leftc, norm;
  
  occupancyp = 1.0 / (double) N_ICOSAHEDRON_SURFACE;
  occupancyc = 0.25 * occupancyp;
  occupancy_leftp = 0.0;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    if(surface[i].nparticle == 0)
        surface[i].occupancy = 0.0;
        
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle == 1)
        continue;
        
      if(surface[i].nparticle > 1)
        {
          vectordc[0] = vectordc[1] = vectordc[2] = 0.0;
          
          for(j = 0, nin = 0; j < 4; j++)
            {
              surface_id = surface[i].children[j];
              
              if(surface[surface_id].nparticle == 0)
                {
                  vectordc[0] += surface[surface_id].vector[0];
                  vectordc[1] += surface[surface_id].vector[1];
                  vectordc[2] += surface[surface_id].vector[2];
                  nin++;
                }
            }
            
          division_factor = 1.0 / (double) (NDIVISION - nin);
          occupancydc = occupancyc * (double) nin * division_factor;
          vectordc[0] *= division_factor;
          vectordc[1] *= division_factor;
          vectordc[2] *= division_factor;
          
          for(j = 0; j < 4; j++)
            {
              surface_id = surface[i].children[j];
              
              if(surface[surface_id].nparticle > 0)
                {
                  surface[surface_id].occupancy += occupancydc;
                  surface[surface_id].vector[0] += vectordc[0];
                  surface[surface_id].vector[1] += vectordc[1];
                  surface[surface_id].vector[2] += vectordc[2];
                }
            }
        }
      else if(surface[i].nparticle == 0)
        {
          for(j = 0, nin = 0; j < 3; j++)
            {
              surface_id = surface[i].neighbour[j];
              
              if(surface[surface_id].nparticle > 0)
                nin++;
            }
            
           if(nin > 0)
             {
               division_factor = 1.0 / (double) nin;
               occupancyd = occupancyp * division_factor;
               vectord[0] = surface[i].vector[0] * division_factor;
               vectord[1] = surface[i].vector[1] * division_factor;
               vectord[2] = surface[i].vector[2] * division_factor;
               
               for(j = 0; j < 3; j++)
                 {
                   surface_id = surface[i].neighbour[j];
                   
                   if(surface[surface_id].nparticle > 0)
                     {
                       surface[surface_id].occupancy += occupancyd;
                       surface[surface_id].vector[0] += vectord[0];
                       surface[surface_id].vector[1] += vectord[1];
                       surface[surface_id].vector[2] += vectord[2];
                       
                       if(surface[surface_id].nparticle > 1)
                         {
                           for(k = 0, nin = 0; k < 4; k++)
                             {
                               surface_id = surface[surface[i].neighbour[j]].children[k];
                               
                               if(surface[surface_id].nparticle > 0)
                                 nin++;
                             }
                             
                           division_factor = 1.0 / (double) nin;
                           occupancydc = occupancyd * division_factor;
                           vectordc[0] = vectord[0] * division_factor;
                           vectordc[1] = vectord[1] * division_factor;
                           vectordc[2] = vectord[2] * division_factor;
                           
                           for(k = 0; k < 4; k++)
                             {
                               surface_id = surface[surface[i].neighbour[j]].children[k];
                               
                               if(surface[surface_id].nparticle > 0)
                                 {
                                   surface[surface_id].occupancy += occupancydc;
                                   surface[surface_id].vector[0] += vectordc[0];
                                   surface[surface_id].vector[1] += vectordc[1];
                                   surface[surface_id].vector[2] += vectordc[2];
                                 }
                             }
                         }
                     }
                 }
             }
           else
             {
               for(j = 0, nin = 0; j < 3; j++)
                 for(k = 0; k < 3; k++)
                   {
                     surface_id = surface[surface[i].neighbour[j]].neighbour[k];
                     
                     if(surface[surface_id].nparticle > 0)
                       nin++;
                   }
                   
               if(nin == 0)
                 occupancy_leftp += occupancyp;
                 
               if(nin > 0)
                 {
                   division_factor = 1.0 / (double) nin;
                   occupancyd = occupancyp * division_factor;
                   vectord[0] = surface[i].vector[0] * division_factor;
                   vectord[1] = surface[i].vector[1] * division_factor;
                   vectord[2] = surface[i].vector[2] * division_factor;
                   
                   for(j = 0; j < 3; j++)
                     for(k = 0; k < 3; k++)
                       {
                         surface_id = surface[surface[i].neighbour[j]].neighbour[k];
                         
                         if(surface[surface_id].nparticle > 0)
                           {
                             surface[surface_id].occupancy += occupancyd;
                             surface[surface_id].vector[0] += vectord[0];
                             surface[surface_id].vector[1] += vectord[1];
                             surface[surface_id].vector[2] += vectord[2];
                             
                             if(surface[surface_id].nparticle > 1)
                               {
                                 for(l = 0, nin = 0; l < 4; l++)
                                   {
                                     surface_id = surface[surface[surface[i].neighbour[j]].neighbour[k]].children[l];
                                     
                                     if(surface[surface_id].nparticle > 0)
                                       nin++;
                                   }
                                   
                                 division_factor = 1.0 / (double) nin;
                                 occupancydc = occupancyd * division_factor;
                                 vectordc[0] = vectord[0] * division_factor;
                                 vectordc[1] = vectord[1] * division_factor;
                                 vectordc[2] = vectord[2] * division_factor;
                                 
                                 for(l = 0; l < 4; l++)
                                   {
                                     surface_id = surface[surface[surface[i].neighbour[j]].neighbour[k]].children[l];
                                     
                                     if(surface[surface_id].nparticle > 0)
                                       {
                                         surface[surface_id].occupancy += occupancydc;
                                         surface[surface_id].vector[0] += vectordc[0];
                                         surface[surface_id].vector[1] += vectordc[1];
                                         surface[surface_id].vector[2] += vectordc[2];
                                       }
                                   }
                               }
                           }
                       }
                 }
             }
        }
    }
    
  for(i = 0, nin = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    if(surface[i].nparticle > 0)
      nin++;
      
  occupancy_leftp /= (double) nin;
  
  for(i = 0, nin = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle > 0)
        surface[i].occupancy += occupancy_leftp;
        
      if(surface[i].nparticle > 1)
        {
          for(j = 0, nin = 0; j < 4; j++)
            if(surface[surface[i].children[j]].nparticle > 0)
              nin++;
              
          occupancy_leftc = occupancy_leftp / (double) nin;
          
          for(j = 0, nin = 0; j < 4; j++)
            if(surface[surface[i].children[j]].nparticle > 0)
              surface[surface[i].children[j]].occupancy += occupancy_leftc;
        }
    }
    
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    if(surface[i].nparticle > 0)
      {
        norm = 0.0;
        norm += surface[i].vector[0] * surface[i].vector[0] + surface[i].vector[1] * surface[i].vector[1] + surface[i].vector[2] * surface[i].vector[2];
        norm = sqrt(norm);
        
        surface[i].vector[0] /= norm;
        surface[i].vector[1] /= norm;
        surface[i].vector[2] /= norm;
      }
    else
      {
        surface[i].vector[0] = 0.0;
        surface[i].vector[1] = 0.0;
        surface[i].vector[2] = 0.0;
      }
      
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    if(surface[i].nparticle > 1)
      for(j = 0; j < 4; j++)
        if(surface[surface[i].children[j]].nparticle > 1)
          surface[surface[i].children[j]].occupancy /= (double) surface[surface[i].children[j]].nparticle;
}

void osaka_set_current_star_surface_snia(double position_star[], struct Surface surface[])
{
  int i, j, k;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    for(j = 0; j < 3; j++)
      for(k = 0; k < 3; k++)
        surface[i].vertex[j][k] += position_star[k];
}

void osaka_search_particle_inside_surface_snia(double position_star[], double position_gas[], struct Surface surface[])
{
  int i, j, k;
  double v01[3], v02[3], vsp[3], vs0[3], re2[3], te1[3], re2e1, t, u, v;
#ifdef PERIODIC
  double vsptemp[3];
#endif

  for(k = 0; k < 3; k++)
    {
#ifdef PERIODIC
      vsptemp[0] = position_gas[k] - position_star[k];
      vsptemp[1] = vsptemp[0] + header.BoxSize;
      vsptemp[2] = vsptemp[0] - header.BoxSize;
      
      if(fabs(vsptemp[0]) < fabs(vsptemp[1]))
        {
          if(fabs(vsptemp[0]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[0];
          else
            vsp[k] = vsptemp[2];
        }
      else
        {
          if(fabs(vsptemp[1]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[1];
          else
            vsp[k] = vsptemp[2];
        }
#else
      vsp[k] = position_gas[k] - position_star[k];
#endif
    }
    
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      for(k = 0; k < 3; k++)
        {
          v01[k] = surface[i].vertex[1][k] - surface[i].vertex[0][k];
          v02[k] = surface[i].vertex[2][k] - surface[i].vertex[0][k];
          vs0[k] = position_star[k] - surface[i].vertex[0][k];
        }
        
      osaka_cross_product_snia(vsp, v02, re2);
      osaka_cross_product_snia(vs0, v01, te1);
      re2e1 = osaka_inner_product_snia(re2, v01);
      
      t = osaka_inner_product_snia(te1, v02) / re2e1;
      u = osaka_inner_product_snia(re2, vs0) / re2e1;
      v = osaka_inner_product_snia(te1, vsp) / re2e1;
      
      if(t < 0.0)
        continue;
        
      if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
        {
          surface[i].nparticle++;
          
          for(j = 0; j < 4; j++)
            {
              for(k = 0; k < 3; k++)
                {
                  v01[k] = surface[surface[i].children[j]].vertex[1][k] - surface[surface[i].children[j]].vertex[0][k];
                  v02[k] = surface[surface[i].children[j]].vertex[2][k] - surface[surface[i].children[j]].vertex[0][k];
                  vs0[k] = position_star[k] - surface[surface[i].children[j]].vertex[0][k];
                }
                
              osaka_cross_product_snia(vsp, v02, re2);
              osaka_cross_product_snia(vs0, v01, te1);
              re2e1 = osaka_inner_product_snia(re2, v01);
              
              t = osaka_inner_product_snia(te1, v02) / re2e1;
              u = osaka_inner_product_snia(re2, vs0) / re2e1;
              v = osaka_inner_product_snia(te1, vsp) / re2e1;
              
              if(t < 0.0)
                continue;
                
              if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
                {
                  surface[surface[i].children[j]].nparticle++;
                  break;
                }
            }
        }
    }
}

double osaka_inner_product_snia(double aa[], double bb[])
{
  return aa[0] * bb[0] + aa[1] * bb[1] + aa[2] * bb[2];
}

void osaka_cross_product_snia(double aa[], double bb[], double cc[])
{
  cc[0] = aa[1] * bb[2] - aa[2] * bb[1];
  cc[1] = aa[2] * bb[0] - aa[0] * bb[2];
  cc[2] = aa[0] * bb[1] - aa[1] * bb[0];
}

void osaka_assign_surface_snia(struct Surface surface_out[], struct Surface surface_in[])
{
  int i;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    surface_out[i] = surface_in[i];
}

void osaka_update_surface_nparticle_snia(struct Surface surface_out[], struct Surface surface_in[])
{
  int i;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    surface_out[i].nparticle += surface_in[i].nparticle;
}

void osaka_make_geodesic_dome_snia(struct Surface surface[])
{
  int i, k;
  double norm;
  struct Point point[N_ICOSAHEDRON_VERTEX];
  
  osaka_set_icosahedron_vertex_snia(point);
  osaka_set_icosahedron_surface_snia(surface, point);
  osaka_surface_refinement_snia(surface);
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    {
      for(k = 0; k < 3; k++)
        {
          norm = surface[i].vertex[k][0] * surface[i].vertex[k][0] + surface[i].vertex[k][1] * surface[i].vertex[k][1] + surface[i].vertex[k][2] * surface[i].vertex[k][2];
          norm = sqrt(norm);
          surface[i].vertex[k][0] /= norm;
          surface[i].vertex[k][1] /= norm;
          surface[i].vertex[k][2] /= norm;
        }
        
      for(k = 0, norm = 0.0; k < 3; k++)
        {
          surface[i].vector[k] = (surface[i].vertex[0][k] + surface[i].vertex[1][k] + surface[i].vertex[2][k]) / 3.0;
          norm += surface[i].vector[k] * surface[i].vector[k];
        }
        
      norm = sqrt(norm);
      surface[i].vector[0] /= norm;
      surface[i].vector[1] /= norm;
      surface[i].vector[2] /= norm;
      surface[i].nparticle = 0;
      
      /*for(k = 0; k < 3; k++)
        {
          surface[i].vertex[k][0] *= header.BoxSize;
          surface[i].vertex[k][1] *= header.BoxSize;
          surface[i].vertex[k][2] *= header.BoxSize;
        }*/
    }
}

void osaka_surface_refinement_snia(struct Surface surface[])
{
  int i, k, istart, iend;
  double middle_point[3][3];
  
  istart = 0;
  iend = N_ICOSAHEDRON_SURFACE;
  
  for(i = istart; i < iend; i++)
    {
      for(k = 0; k < 4; k++)
        {
          surface[surface[i].children[k]].id = surface[i].children[k];
          surface[surface[i].children[k]].parent = surface[i].id;
          surface[surface[i].children[k]].occupancy = 0.25* surface[i].occupancy;
          
          surface[surface[i].children[k]].children[0] = - 1;
          surface[surface[i].children[k]].children[1] = - 1;
          surface[surface[i].children[k]].children[2] = - 1;
          surface[surface[i].children[k]].children[3] = - 1;
        }
        
      surface[surface[i].children[0]].neighbour[0] = surface[surface[i].children[1]].id;
      surface[surface[i].children[0]].neighbour[1] = surface[surface[i].children[2]].id;
      surface[surface[i].children[0]].neighbour[2] = surface[surface[i].children[3]].id;
      
      surface[surface[i].children[1]].neighbour[0] = surface[surface[i].children[2]].id;
      surface[surface[i].children[1]].neighbour[1] = surface[surface[i].children[3]].id;
      surface[surface[i].children[1]].neighbour[2] = surface[surface[i].children[0]].id;
      
      surface[surface[i].children[2]].neighbour[0] = surface[surface[i].children[3]].id;
      surface[surface[i].children[2]].neighbour[1] = surface[surface[i].children[0]].id;
      surface[surface[i].children[2]].neighbour[2] = surface[surface[i].children[1]].id;
      
      surface[surface[i].children[3]].neighbour[0] = surface[surface[i].children[0]].id;
      surface[surface[i].children[3]].neighbour[1] = surface[surface[i].children[1]].id;
      surface[surface[i].children[3]].neighbour[2] = surface[surface[i].children[2]].id;
      
      for(k = 0; k < 3; k++)
        {
          middle_point[0][k] = 0.5 * (surface[i].vertex[0][k] + surface[i].vertex[1][k]);
          middle_point[1][k] = 0.5 * (surface[i].vertex[1][k] + surface[i].vertex[2][k]);
          middle_point[2][k] = 0.5 * (surface[i].vertex[2][k] + surface[i].vertex[0][k]);
          
          surface[surface[i].children[0]].vertex[0][k] = surface[i].vertex[0][k];
          surface[surface[i].children[0]].vertex[1][k] = middle_point[0][k];
          surface[surface[i].children[0]].vertex[2][k] = middle_point[2][k];
          
          surface[surface[i].children[1]].vertex[0][k] = surface[i].vertex[1][k];
          surface[surface[i].children[1]].vertex[1][k] = middle_point[1][k];
          surface[surface[i].children[1]].vertex[2][k] = middle_point[0][k];
          
          surface[surface[i].children[2]].vertex[0][k] = surface[i].vertex[2][k];
          surface[surface[i].children[2]].vertex[1][k] = middle_point[2][k];
          surface[surface[i].children[2]].vertex[2][k] = middle_point[1][k];
          
          surface[surface[i].children[3]].vertex[0][k] = middle_point[0][k];
          surface[surface[i].children[3]].vertex[1][k] = middle_point[1][k];
          surface[surface[i].children[3]].vertex[2][k] = middle_point[2][k];
        }
    }
}

void osaka_set_icosahedron_vertex_snia(struct Point point[])
{
  int i , k;
  double golden_ratio, norm;
  
  golden_ratio = 0.5 * (1.0 + sqrt(5));
  
  point[0].x[0] = 1.0;
  point[0].x[1] = golden_ratio;
  point[0].x[2] = 0.0;
  
  point[1].x[0] = - 1.0;
  point[1].x[1] = golden_ratio;
  point[1].x[2] = 0.0;
  
  point[2].x[0] = 1.0;
  point[2].x[1] = - golden_ratio;
  point[2].x[2] = 0.0;
  
  point[3].x[0] = - 1.0;
  point[3].x[1] = - golden_ratio;
  point[3].x[2] = 0.0;
  
  point[4].x[0] = 0.0;
  point[4].x[1] = 1.0;
  point[4].x[2] = golden_ratio;
  
  point[5].x[0] = 0.0;
  point[5].x[1] = - 1.0;
  point[5].x[2] = golden_ratio;
  
  point[6].x[0] = 0.0;
  point[6].x[1] = 1.0;
  point[6].x[2] = - golden_ratio;
  
  point[7].x[0] = 0.0;
  point[7].x[1] = - 1.0;
  point[7].x[2] = - golden_ratio;
  
  point[8].x[0] = golden_ratio;
  point[8].x[1] = 0.0;
  point[8].x[2] = 1.0;
  
  point[9].x[0] = golden_ratio;
  point[9].x[1] = 0.0;
  point[9].x[2] = - 1.0;
  
  point[10].x[0] = - golden_ratio;
  point[10].x[1] = 0.0;
  point[10].x[2] = 1.0;
  
  point[11].x[0] = - golden_ratio;
  point[11].x[1] = 0.0;
  point[11].x[2] = - 1.0;
  
  norm = sqrt(1.0 + golden_ratio * golden_ratio);
  
  for(i = 0; i < N_ICOSAHEDRON_VERTEX; i++)
    for(k = 0; k < 3; k++)
      point[i].x[k] /= norm;
}

void osaka_set_icosahedron_surface_snia(struct Surface surface[], struct Point point[])
{
  int i, k, children_id_offset;
  double occupancy;
  
  occupancy = 1.0 / (double) N_ICOSAHEDRON_SURFACE;
  
  for(i = 0, children_id_offset = N_ICOSAHEDRON_SURFACE; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      surface[i].parent = - 1;
      surface[i].id = i;
      surface[i].children[0] = children_id_offset + 0;
      surface[i].children[1] = children_id_offset + 1;
      surface[i].children[2] = children_id_offset + 2;
      surface[i].children[3] = children_id_offset + 3;
      children_id_offset += NDIVISION;
      surface[i].occupancy = occupancy;
    }
    
  for(k = 0; k < 3; k++)
    {
      surface[0].vertex[0][k] = point[0].x[k];
      surface[0].vertex[1][k] = point[1].x[k];
      surface[0].vertex[2][k] = point[4].x[k];
      surface[0].vector[k] = (surface[0].vertex[0][k] + surface[0].vertex[1][k] + surface[0].vertex[2][k]) / 3.0;
      
      surface[1].vertex[0][k] = point[1].x[k];
      surface[1].vertex[1][k] = point[4].x[k];
      surface[1].vertex[2][k] = point[10].x[k];
      surface[1].vector[k] = (surface[1].vertex[0][k] + surface[1].vertex[1][k] + surface[1].vertex[2][k]) / 3.0;
      
      surface[2].vertex[0][k] = point[4].x[k];
      surface[2].vertex[1][k] = point[5].x[k];
      surface[2].vertex[2][k] = point[10].x[k];
      surface[2].vector[k] = (surface[2].vertex[0][k] + surface[2].vertex[1][k] + surface[2].vertex[2][k]) / 3.0;
      
      surface[3].vertex[0][k] = point[4].x[k];
      surface[3].vertex[1][k] = point[5].x[k];
      surface[3].vertex[2][k] = point[8].x[k];
      surface[3].vector[k] = (surface[3].vertex[0][k] + surface[3].vertex[1][k] + surface[3].vertex[2][k]) / 3.0;
      
      surface[4].vertex[0][k] = point[0].x[k];
      surface[4].vertex[1][k] = point[4].x[k];
      surface[4].vertex[2][k] = point[8].x[k];
      surface[4].vector[k] = (surface[4].vertex[0][k] + surface[4].vertex[1][k] + surface[4].vertex[2][k]) / 3.0;
      
      surface[5].vertex[0][k] = point[0].x[k];
      surface[5].vertex[1][k] = point[1].x[k];
      surface[5].vertex[2][k] = point[6].x[k];
      surface[5].vector[k] = (surface[5].vertex[0][k] + surface[5].vertex[1][k] + surface[5].vertex[2][k]) / 3.0;
      
      surface[6].vertex[0][k] = point[1].x[k];
      surface[6].vertex[1][k] = point[6].x[k];
      surface[6].vertex[2][k] = point[11].x[k];
      surface[6].vector[k] = (surface[6].vertex[0][k] + surface[6].vertex[1][k] + surface[6].vertex[2][k]) / 3.0;
      
      surface[7].vertex[0][k] = point[1].x[k];
      surface[7].vertex[1][k] = point[10].x[k];
      surface[7].vertex[2][k] = point[11].x[k];
      surface[7].vector[k] = (surface[7].vertex[0][k] + surface[7].vertex[1][k] + surface[7].vertex[2][k]) / 3.0;
      
      surface[8].vertex[0][k] = point[3].x[k];
      surface[8].vertex[1][k] = point[10].x[k];
      surface[8].vertex[2][k] = point[11].x[k];
      surface[8].vector[k] = (surface[8].vertex[0][k] + surface[8].vertex[1][k] + surface[8].vertex[2][k]) / 3.0;
      
      surface[9].vertex[0][k] = point[3].x[k];
      surface[9].vertex[1][k] = point[5].x[k];
      surface[9].vertex[2][k] = point[10].x[k];
      surface[9].vector[k] = (surface[9].vertex[0][k] + surface[9].vertex[1][k] + surface[9].vertex[2][k]) / 3.0;
      
      surface[10].vertex[0][k] = point[2].x[k];
      surface[10].vertex[1][k] = point[3].x[k];
      surface[10].vertex[2][k] = point[5].x[k];
      surface[10].vector[k] = (surface[10].vertex[0][k] + surface[10].vertex[1][k] + surface[10].vertex[2][k]) / 3.0;
      
      surface[11].vertex[0][k] = point[2].x[k];
      surface[11].vertex[1][k] = point[5].x[k];
      surface[11].vertex[2][k] = point[8].x[k];
      surface[11].vector[k] = (surface[11].vertex[0][k] + surface[11].vertex[1][k] + surface[11].vertex[2][k]) / 3.0;
      
      surface[12].vertex[0][k] = point[2].x[k];
      surface[12].vertex[1][k] = point[8].x[k];
      surface[12].vertex[2][k] = point[9].x[k];
      surface[12].vector[k] = (surface[12].vertex[0][k] + surface[12].vertex[1][k] + surface[12].vertex[2][k]) / 3.0;
      
      surface[13].vertex[0][k] = point[0].x[k];
      surface[13].vertex[1][k] = point[8].x[k];
      surface[13].vertex[2][k] = point[9].x[k];
      surface[13].vector[k] = (surface[13].vertex[0][k] + surface[13].vertex[1][k] + surface[13].vertex[2][k]) / 3.0;
      
      surface[14].vertex[0][k] = point[0].x[k];
      surface[14].vertex[1][k] = point[6].x[k];
      surface[14].vertex[2][k] = point[9].x[k];
      surface[14].vector[k] = (surface[14].vertex[0][k] + surface[14].vertex[1][k] + surface[14].vertex[2][k]) / 3.0;
      
      surface[15].vertex[0][k] = point[6].x[k];
      surface[15].vertex[1][k] = point[7].x[k];
      surface[15].vertex[2][k] = point[11].x[k];
      surface[15].vector[k] = (surface[15].vertex[0][k] + surface[15].vertex[1][k] + surface[15].vertex[2][k]) / 3.0;
      
      surface[16].vertex[0][k] = point[3].x[k];
      surface[16].vertex[1][k] = point[7].x[k];
      surface[16].vertex[2][k] = point[11].x[k];
      surface[16].vector[k] = (surface[16].vertex[0][k] + surface[16].vertex[1][k] + surface[16].vertex[2][k]) / 3.0;
      
      surface[17].vertex[0][k] = point[2].x[k];
      surface[17].vertex[1][k] = point[3].x[k];
      surface[17].vertex[2][k] = point[7].x[k];
      surface[17].vector[k] = (surface[17].vertex[0][k] + surface[17].vertex[1][k] + surface[17].vertex[2][k]) / 3.0;
      
      surface[18].vertex[0][k] = point[2].x[k];
      surface[18].vertex[1][k] = point[7].x[k];
      surface[18].vertex[2][k] = point[9].x[k];
      surface[18].vector[k] = (surface[18].vertex[0][k] + surface[18].vertex[1][k] + surface[18].vertex[2][k]) / 3.0;
      
      surface[19].vertex[0][k] = point[6].x[k];
      surface[19].vertex[1][k] = point[7].x[k];
      surface[19].vertex[2][k] = point[9].x[k];
      surface[19].vector[k] = (surface[19].vertex[0][k] + surface[19].vertex[1][k] + surface[19].vertex[2][k]) / 3.0;
    }
    
  surface[0].neighbour[0] = 1;
  surface[0].neighbour[1] = 4;
  surface[0].neighbour[2] = 5;
  
  surface[1].neighbour[0] = 0;
  surface[1].neighbour[1] = 2;
  surface[1].neighbour[2] = 7;
  
  surface[2].neighbour[0] = 1;
  surface[2].neighbour[1] = 3;
  surface[2].neighbour[2] = 9;
  
  surface[3].neighbour[0] = 2;
  surface[3].neighbour[1] = 4;
  surface[3].neighbour[2] = 11;
  
  surface[4].neighbour[0] = 0;
  surface[4].neighbour[1] = 3;
  surface[4].neighbour[2] = 13;
  
  surface[5].neighbour[0] = 0;
  surface[5].neighbour[1] = 14;
  surface[5].neighbour[2] = 6;
  
  surface[6].neighbour[0] = 5;
  surface[6].neighbour[1] = 7;
  surface[6].neighbour[2] = 15;
  
  surface[7].neighbour[0] = 1;
  surface[7].neighbour[1] = 6;
  surface[7].neighbour[2] = 8;
  
  surface[8].neighbour[0] = 7;
  surface[8].neighbour[1] = 9;
  surface[8].neighbour[2] = 16;
  
  surface[9].neighbour[0] = 2;
  surface[9].neighbour[1] = 8;
  surface[9].neighbour[2] = 10;
  
  surface[10].neighbour[0] = 9;
  surface[10].neighbour[1] = 11;
  surface[10].neighbour[2] = 17;
  
  surface[11].neighbour[0] = 3;
  surface[11].neighbour[1] = 10;
  surface[11].neighbour[2] = 12;
  
  surface[12].neighbour[0] = 11;
  surface[12].neighbour[1] = 13;
  surface[12].neighbour[2] = 18;
  
  surface[13].neighbour[0] = 4;
  surface[13].neighbour[1] = 12;
  surface[13].neighbour[2] = 14;
  
  surface[14].neighbour[0] = 5;
  surface[14].neighbour[1] = 13;
  surface[14].neighbour[2] = 19;
  
  surface[15].neighbour[0] = 6;
  surface[15].neighbour[1] = 16;
  surface[15].neighbour[2] = 19;
  
  surface[16].neighbour[0] = 8;
  surface[16].neighbour[1] = 15;
  surface[16].neighbour[2] = 17;
  
  surface[17].neighbour[0] = 10;
  surface[17].neighbour[1] = 16;
  surface[17].neighbour[2] = 18;
  
  surface[18].neighbour[0] = 12;
  surface[18].neighbour[1] = 17;
  surface[18].neighbour[2] = 19;
  
  surface[19].neighbour[0] = 14;
  surface[19].neighbour[1] = 15;
  surface[19].neighbour[2] = 18;
}
#endif

#endif
#endif
