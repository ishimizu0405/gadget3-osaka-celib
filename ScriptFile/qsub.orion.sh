#!/bin/bash
#PBS -N name
#PBS -l nodes=1:xeon:ppn=40
##PBS -l nodes=1:ppn=64
#PBS -q default
#PBS -j oe

cd $PBS_O_WORKDIR
NPROCS=`wc -l <$PBS_NODEFILE`

##start from scratch
mpirun -x OMPI_MCA_mpi_leave_pinned=0 -np 40 -machinefile $PBS_NODEFILE ./a.out > log
