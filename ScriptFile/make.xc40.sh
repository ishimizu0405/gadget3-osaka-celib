#!/bin/bash
#SBATCH -J Make
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -p S

COMPILER=intel
#COMPILER=gnu

source /opt/modules/default/init/bash

if [ "$COMPILER" = "intel" ]; then
module unload PrgEnv-cray PrgEnv-gnu
module load PrgEnv-intel
module list
INSTALL_DIR=${HOME}/local/intel
elif [ "$COMPILER" = "gnu" ]; then
module unload PrgEnv-cray PrgEnv-intel
module load PrgEnv-gnu
module list
INSTALL_DIR=${HOME}/local/gnu
fi

export LD_LIBRARY_PATH=${INSTALL_DIR}/lib:${INSTALL_DIR}/gracklev3/lib:$LD_LIBRARY_PATH

make clean
make -j > log
