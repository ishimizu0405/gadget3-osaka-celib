#PBS -l nodes=2
#PBS -l walltime=12:00:00
#PBS -N name
##PBS -q large-b
#PBS -q bulk-b
##PBS -q debug
#PBS -j oe

COMPILER=intel
#COMPILER=gnu

source /opt/modules/default/init/bash

if [ "$COMPILER" = "intel" ]; then
module unload PrgEnv-cray PrgEnv-gnu cray-fftw
module load PrgEnv-intel
module load fftw gsl/2.4_intel-18.0 cray-hdf5-parallel
module list
INSTALL_HOMEDIR=/home/shimzuik/usr/local/intel
INSTALL_WORKDIR=/work/shimzuik/local/intel
elif [ "$COMPILER" = "gnu" ]; then
module unload PrgEnv-cray PrgEnv-intel cray-fftw
module load PrgEnv-gnu
module load fftw gsl/2.4_gnu-7.3 cray-hdf5-parallel
module list
INSTALL_HOMEDIR=/home/shimzuik/usr/local/gnu
INSTALL_WORKDIR=/work/shimzuik/local/gnu
fi

export LD_LIBRARY_PATH=$INSTALL_WORKDIR/gracklev3:$LD_LIBRARY_PATH

cd ${PBS_O_WORKDIR}
aprun -n 80 ./P-Gadget3_osaka inputNakamura.param > log
