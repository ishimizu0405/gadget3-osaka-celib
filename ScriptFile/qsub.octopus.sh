#!/bin/sh
#PBS -q OCTOPUS
#PBS -N LightCone
#PBS -l elapstim_req=4:00:00,cpunum_job=24,memsz_job=190GB
#PBS -b 2
#PBS --group=hp190050
#PBS -l stacksz_prc=10240KB
##PBS -T intmpi
##PBS -v LD_LIBRARY_PATH=${HOME}/local/intel/lib:${HOME}/local/intel/gracklev3/lib:${LD_LIBRARY_PATH}
#PBS -T openmpi
#PBS -v LD_LIBRARY_PATH=${HOME}/local/gnu/lib:${HOME}/local/gnu/gracklev3/lib:${LD_LIBRARY_PATH}
#PBS -v PATH=/usr/mpi/gcc/openmpi/bin:${PATH}


cd $PBS_O_WORKDIR
mpirun ${NQSII_MPIOPTS} -np 48 ./a.out > log
