#!/bin/sh
#PBS -N name
#PBS -l nodes=1:ppn=64
#PBS -q default
#PBS -j oe

cd $PBS_O_WORKDIR
mpirun -np 64 ./a.out > log
