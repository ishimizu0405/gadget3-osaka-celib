#!/bin/sh
#PBS -q OCTOPUS
#PBS -N make
#PBS -l elapstim_req=0:05:00,cpunum_job=1,memsz_job=128GB
#PBS -b 1
#PBS --group=hp190050
#PBS -l stacksz_prc=10240KB
##PBS -T intmpi
##PBS -v LD_LIBRARY_PATH=${HOME}/local/intel/lib:${HOME}/local/intel/gracklev3/lib:${LD_LIBRARY_PATH}
#PBS -T openmpi
#PBS -v LD_LIBRARY_PATH=${HOME}/local/gnu/lib:${HOME}/local/gnu/gracklev3/lib:${LD_LIBRARY_PATH}
#PBS -v PATH=/usr/mpi/gcc/openmpi/bin:${PATH}

cd $PBS_O_WORKDIR
make clean
make -j > log
