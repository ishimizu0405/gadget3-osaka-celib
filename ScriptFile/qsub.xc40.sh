#!/bin/bash
#SBATCH -J name
#SBATCH -N 32
#SBATCH -n 1024
#SBATCH -c 1
#SBATCH -p L
##SBATCH -p M
##SBATCH -p S

COMPILER=intel
#COMPILER=gnu

source /opt/modules/default/init/bash

if [ "$COMPILER" = "intel" ]; then
module unload PrgEnv-cray PrgEnv-gnu
module load PrgEnv-intel
module list
INSTALL_DIR=${HOME}/local/intel
elif [ "$COMPILER" = "gnu" ]; then
module unload PrgEnv-cray PrgEnv-intel
module load PrgEnv-gnu
module list
INSTALL_DIR=${HOME}/local/gnu
fi

export LD_LIBRARY_PATH=${INSTALL_DIR}/lib:${INSTALL_DIR}/gracklev3/lib:$LD_LIBRARY_PATH

srun ./P-Gadget3_osaka input.param > log
