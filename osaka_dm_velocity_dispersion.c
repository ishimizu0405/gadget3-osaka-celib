#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "allvars.h"
#include "proto.h"

#ifdef OSAKA_VELOCITY_COOLING_STOP
static struct data_in
{
  double Pos[3];
  double Vel[3];
  double Hsml;
  int NodeList[NODELISTLENGTH];
}*DataIn, *DataGet;

static struct data_out
{
  double Vel[3];
  double Vel2[3];
  double Rho;
  double DhsmlDensity;
  double Ngb;
}*DataResult, *DataOut;

static struct velocity_data
{
  double Vel[3];
  double Vel2[3];
}*VelData;

void osaka_dm_velocity_dispersion(void)
{
  //set_gas_hsml_guess();
  calc_dm_velocity_dispersion();
}


void calc_dm_velocity_dispersion(void)
{
  MyFloat *Left, *Right;
  int i, j, ndone, ndone_flag, npleft, dummy, iter = 0;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  long long ntot;
  double dmax1, dmax2, fac;
  double desnumngb, vsigma2[3];
    
#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif

  if(ThisTask == 0)
    {
      printf("Star DM velocity dispersion calculation...\n");
      fflush(stdout);
    }
    
  Left = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  Right = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  VelData = (struct velocity_data *) mymalloc(NumPart * sizeof(struct velocity_data));
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 0)
        Left[i] = Right[i] = 0;
      else
        Left[i] = Right[i] = -1;
    }
    
  // allocate buffers to arrange communication //
  Ngblist = (int *) mymalloc(NumPart * sizeof(int));
  
  All.BunchSize = (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                    sizeof(struct data_in) + sizeof(struct data_out) +
                      sizemax(sizeof(struct data_in), sizeof(struct data_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
  
  desnumngb = All.DesNumNgb;
    
  // we will repeat the whole thing for those particles where we didn't find enough neighbours //
  do
    {
      do
        {
          for(j = 0; j < NTask; j++)
            {
              Send_count[j] = 0;
              Exportflag[j] = -1;
            }
            
          // do local particles and prepare export list //
          for(i = FirstActiveParticle, nexport = 0; i >= 0; i = NextActiveParticle[i])
            {
              if(P[i].Type == 0)
                {
                  P[i].Hsml = SphP[i].Hsml;
                  
                  if(velocity_dispersion_evaluate(i, 0, &nexport, Send_count) < 0)
                    break;
                }
            }
            
#ifdef MYSORT
          mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
          qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
          MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
          
          for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
            {
              Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
              nimport += Recv_count[j];
              
              if(j > 0)
                {
                  Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
                  Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
                }
            }
            
          DataGet = (struct data_in *) mymalloc(nimport * sizeof(struct data_in));
          DataIn = (struct data_in *) mymalloc(nexport * sizeof(struct data_in));
          
          // prepare particle data for export //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              DataIn[j].Pos[0] = P[place].Pos[0];
              DataIn[j].Pos[1] = P[place].Pos[1];
              DataIn[j].Pos[2] = P[place].Pos[2];
              DataIn[j].Vel[0] = SphP[place].VelPred[0];
              DataIn[j].Vel[1] = SphP[place].VelPred[1];
              DataIn[j].Vel[2] = SphP[place].VelPred[2];
              DataIn[j].Hsml   = P[place].Hsml;
              
              memcpy(DataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
            }
            
          // exchange particle data //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // get the particles //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&DataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct data_in), 
                        MPI_BYTE,recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&DataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct data_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&DataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct data_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, &DataGet[Recv_offset[recvTask]], 
                          Recv_count[recvTask] * sizeof(struct data_in), MPI_BYTE, recvTask, 
                            TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          myfree(DataIn);
          
          DataResult = (struct data_out *) mymalloc(nimport * sizeof(struct data_out));
          DataOut = (struct data_out *) mymalloc(nexport * sizeof(struct data_out));
          
          // now do the particles that were sent to us //
          for(j = 0; j < nimport; j++)
            velocity_dispersion_evaluate(j, 1, &dummy, &dummy);
            
          if(i < 0)
            ndone_flag = 1;
          else
            ndone_flag = 0;
            
          MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
          
          // get the result //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // send the results //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&DataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct data_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&DataOut[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct data_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&DataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct data_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, &DataOut[Send_offset[recvTask]],
                          Send_count[recvTask] * sizeof(struct data_out), MPI_BYTE, recvTask, 
                            TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          // add the result to the local particles //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              P[place].NgbNum += DataOut[j].Ngb;
              VelData[place].Vel[0] += DataOut[j].Vel[0];
              VelData[place].Vel[1] += DataOut[j].Vel[1];
              VelData[place].Vel[2] += DataOut[j].Vel[2];
              VelData[place].Vel2[0] += DataOut[j].Vel2[0];
              VelData[place].Vel2[1] += DataOut[j].Vel2[1];
              VelData[place].Vel2[2] += DataOut[j].Vel2[2];
              P[place].Density += DataOut[j].Rho;
              P[place].DhsmlDensityFactor += DataOut[j].DhsmlDensity;
            }
            
          myfree(DataOut);
          myfree(DataResult);
          myfree(DataGet);
        }
      while(ndone < NTask);
      
      // The following is the main difference from hydro_force() because now numngb_inbox has to be //
      // reduced to a sphere, which ngbnum returned from ngb_treefind_pairs() does not need doing.  //
      for(i = FirstActiveParticle, npleft = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(P[i].Type == 0)
            {
              if(P[i].NgbNum > 0.0)
                {
                  vsigma2[0] = VelData[i].Vel2[0] / P[i].NgbNum - VelData[i].Vel[0] * VelData[i].Vel[0] / P[i].NgbNum / P[i].NgbNum;
                  vsigma2[1] = VelData[i].Vel2[1] / P[i].NgbNum - VelData[i].Vel[1] * VelData[i].Vel[1] / P[i].NgbNum / P[i].NgbNum;
                  vsigma2[2] = VelData[i].Vel2[2] / P[i].NgbNum - VelData[i].Vel[2] * VelData[i].Vel[2] / P[i].NgbNum / P[i].NgbNum;
                  vsigma2[0] = DMAX(MIN_REAL_NUMBER, vsigma2[0]);
                  vsigma2[1] = DMAX(MIN_REAL_NUMBER, vsigma2[1]);
                  vsigma2[2] = DMAX(MIN_REAL_NUMBER, vsigma2[2]);
                  SphP[i].VelDisp = sqrt((vsigma2[0] + vsigma2[1] + vsigma2[2]) / 3.0);
                }
                
              if(P[i].Density > 0.0)
                {
                  P[i].DhsmlDensityFactor *= P[i].Hsml / (NUMDIMS * P[i].Density);
                  
                  // note: this would be -1 if only a single particle at zero lag is found //
                  if(P[i].DhsmlDensityFactor > - 0.9)
                    P[i].DhsmlDensityFactor = 1.0 / (1.0 + P[i].DhsmlDensityFactor);
                  else
                    P[i].DhsmlDensityFactor = 1.0;
                }
                
              // now check whether we had enough neighbours //
              /*if(P[i].NgbNum < (desnumngb - All.MaxNumNgbDeviation) || P[i].NgbNum > (desnumngb + All.MaxNumNgbDeviation))
                {
                  // need to redo this particle //
                  npleft++;
                  
                  if(Left[i] > 0 && Right[i] > 0)
                    if((Right[i] - Left[i]) < 1.0e-3 * Left[i])
                      {
                        // this one should be ok //
                        npleft--;
                        Left[i] = -1;// Mark as inactive //
                        continue;
                      }
                      
                  if(P[i].NgbNum < (desnumngb - All.MaxNumNgbDeviation))
                    Left[i] = DMAX(P[i].Hsml, Left[i]);
                  else
                    {
                      if(Right[i] != 0)
                        {
                          if(P[i].Hsml < Right[i])
                            Right[i] = P[i].Hsml;
                        }
                      else
                        Right[i] = P[i].Hsml;
                    }
                    
                  if(iter >= MAXITER - 10)
                    {
                      printf("i = %d task = %d ID = %d Hsml = %g Left = %g Right = %g Ngbs = %d Right - Left = %g\n", i, ThisTask, (int) P[i].ID, P[i].Hsml, Left[i], Right[i], (int) P[i].NgbNum, Right[i] - Left[i]);
                      printf("pos = (%g |%g |%g)\n", P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
                      fflush(stdout);
                    }
                    
                  if(Right[i] > 0 && Left[i] > 0)
                    P[i].Hsml = pow(0.5 * (pow(Left[i], 3) + pow(Right[i], 3)), 1.0 / 3);
                  else
                    {
                      if(Right[i] == 0 && Left[i] == 0)
                        endrun(8188);// can't occur //
                        
                      if(Right[i] == 0 && Left[i] > 0)
                        {
                          if(fabs(P[i].NgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - ((double) P[i].NgbNum - desnumngb) / ((double) NUMDIMS * (double) P[i].NgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac < 1.26)
                                P[i].Hsml *= fac;
                              else
                                P[i].Hsml *= 1.26;
                            }
                          else
                            P[i].Hsml *= 1.26;
                        }
                      
                      if(Right[i] > 0 && Left[i] == 0)
                        {
                          if(fabs(P[i].NgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - ((double) P[i].NgbNum - desnumngb) / ((double) NUMDIMS * (double) P[i].NgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac > 1.0 / 1.26)
                                  P[i].Hsml *= fac;
                              else
                                  P[i].Hsml /= 1.26;
                            }
                          else
                            P[i].Hsml /= 1.26;
                        }
                    }
                }
              else
                Left[i] = -1;// Mark as inactive //*/
                
              // now check whether we had enough neighbours //
              if(P[i].NgbNum < (P[i].NgbNum - All.MaxNumNgbDeviation) || P[i].NgbNum > (P[i].NgbNum + All.MaxNumNgbDeviation))
                {
                  // need to redo this particle //
                  npleft++;
                  
                  if(Left[i] > 0 && Right[i] > 0)
                    if((Right[i] - Left[i]) < 1.0e-3 * Left[i])
                      {
                        // this one should be ok //
                        npleft--;
                        Left[i] = -1;// Mark as inactive //
                        continue;
                      }
                      
                  if(P[i].NgbNum < (P[i].NgbNum - All.MaxNumNgbDeviation))
                    Left[i] = DMAX(P[i].Hsml, Left[i]);
                  else
                    {
                      if(Right[i] != 0)
                        {
                          if(P[i].Hsml < Right[i])
                            Right[i] = P[i].Hsml;
                        }
                      else
                        Right[i] = P[i].Hsml;
                    }
                    
                  if(iter >= MAXITER - 10)
                    {
                      printf("i = %d task = %d ID = %d Hsml = %g Left = %g Right = %g Ngbs = %d Right - Left = %g\n", i, ThisTask, (int) P[i].ID, P[i].Hsml, Left[i], Right[i], (int) P[i].NgbNum, Right[i] - Left[i]);
                      printf("pos = (%g |%g |%g)\n", P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
                      fflush(stdout);
                    }
                    
                  if(Right[i] > 0 && Left[i] > 0)
                    P[i].Hsml = pow(0.5 * (pow(Left[i], 3) + pow(Right[i], 3)), 1.0 / 3);
                  else
                    {
                      if(Right[i] == 0 && Left[i] == 0)
                        endrun(8188);// can't occur //
                        
                      if(Right[i] == 0 && Left[i] > 0)
                        P[i].Hsml *= 2.0;
                        
                      if(Right[i] > 0 && Left[i] == 0)
                        P[i].Hsml *= 0.8;
                    }
                }
              else
                Left[i] = -1;// Mark as inactive ///**/
            }
        }
        
      sumup_large_ints(1, &npleft, &ntot);
      
      if(ntot > 0)
        {
          iter++;
          
          if(iter > MAXITER)
            {
              printf("failed to converge in neighbour iteration in calc_dm_velocity_dispersion()\n");
              fflush(stdout);
              endrun(1155);
            }
        }
    }
  while(ntot > 0);
  
  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  myfree(VelData);
  myfree(Right);
  myfree(Left);
}

int velocity_dispersion_evaluate(int target, int mode, int *nexport, int *nsend_local)
{
  int j, n;
  int startnode, numngb, numngb_inbox, listindex = 0;
  int dt_step;
  double h, h2, hinv, hinv3, hinv4;
  double wk, dwk;
  double dx, dy, dz, r, r2, u, mass_j, rho, dhsmlrho;
  MyLongDouble weighted_numngb;
  double pos[3], vel[3];
  double hubble_a, ainv, a3inv;
#ifdef PMGRID
  double dt_gravkick_pm = 0;
#endif
  double dt_gravkick;
  double vj[3], dv[3], v[3], v2[3];
  
  if(All.ComovingIntegrationOn)
    {
      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time) + (1.0 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + All.OmegaLambda;
      hubble_a = All.Hubble * sqrt(hubble_a);
      ainv = 1.0 / All.Time;
      a3inv = ainv * ainv * ainv;
    }
  else
    hubble_a = ainv = a3inv = 1.0;
    
#ifdef PMGRID
  if(All.ComovingIntegrationOn)
    dt_gravkick_pm = get_gravkick_factor(All.PM_Ti_begstep, All.Ti_Current) - get_gravkick_factor(All.PM_Ti_begstep, (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2);
  else
    dt_gravkick_pm = (All.Ti_Current - (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2) * All.Timebase_interval;
#endif

  weighted_numngb = 0.0;
  v[0] = v[1] = v[2] = v2[0] = v2[1] = v2[2] = rho = 0.0;
  
  if(mode == 0)
    {
      pos[0] = P[target].Pos[0];
      pos[1] = P[target].Pos[1];
      pos[2] = P[target].Pos[2];
      vel[0] = SphP[target].VelPred[0];
      vel[1] = SphP[target].VelPred[1];
      vel[2] = SphP[target].VelPred[2];
      h = P[target].Hsml;
    }
  else
    {
      pos[0] = DataGet[target].Pos[0];
      pos[1] = DataGet[target].Pos[1];
      pos[2] = DataGet[target].Pos[2];
      vel[0] = DataGet[target].Vel[0];
      vel[1] = DataGet[target].Vel[1];
      vel[2] = DataGet[target].Vel[2];
      h = DataGet[target].Hsml;
    }
    
  h2 = h * h;
  hinv = 1.0 / h;
#ifndef  TWODIMS
  hinv3 = hinv * hinv * hinv;
#else
  hinv3 = hinv * hinv / boxSize_Z;
#endif
  hinv4 = hinv3 * hinv;
  
  if(mode == 0)
    {
      startnode = All.MaxPart;// root node //
    }
  else
    {
      startnode = DataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  numngb = 0;
  
  while(startnode >= 0)
    {
      while(startnode >= 0)
        {
          numngb_inbox = ngb_treefind_nonbaryon(pos, h, target, &startnode, mode, nexport, nsend_local);
          
          if(numngb_inbox < 0)
            return -1;
            
          for(n = 0; n < numngb_inbox; n++)
            {
              j = Ngblist[n];
              dx = pos[0] - P[j].Pos[0];
              dy = pos[1] - P[j].Pos[1];
              dz = pos[2] - P[j].Pos[2];
              //  now find the closest image in the given box size  //
#ifdef PERIODIC
              if(dx > boxHalf_X)
                dx -= boxSize_X;
              if(dx < -boxHalf_X)
                dx += boxSize_X;
              if(dy > boxHalf_Y)
                dy -= boxSize_Y;
              if(dy < -boxHalf_Y)
                dy += boxSize_Y;
              if(dz > boxHalf_Z)
                dz -= boxSize_Z;
              if(dz < -boxHalf_Z)
                dz += boxSize_Z;
#endif
              r2 = dx * dx + dy * dy + dz * dz;
              
              if(r2 < h2)
                {
                  numngb++;
                  r = sqrt(r2);
                  u = r * hinv;
                  
#if !defined(QUINTIC_KERNEL) && !defined(WENDLAND_C4_KERNEL)
                  if(u < 0.5)
                    {
                      wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1.0) * u * u);
                      dwk = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
                    }
                  else
                    {
                      wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
                      dwk = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
                    }
#elif QUINTIC_KERNEL
                  if(0.0 <= u && u < ONETHIRD)
                    {
                      wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) + 15.0 * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u);
                      dwk = - 5.0 * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) + 30.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) - 75.0 * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u);
                    }
                  else if(ONETHIRD <= u && u < TWOTHIRD)
                    {
                      wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u);
                      dwk = - 5.0 * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) + 30.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u);
                    }
                  else
                    {
                      wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u);
                      dwk = - 5.0 * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u);
                    }
                    
                  wk *= KERNEL_NORM * hinv3;
                  dwk *= KERNEL_NORM * hinv4;
#elif WENDLAND_C4_KERNEL
                  wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 + 6.0 * u + 35.0 / 3 * u * u);
                  dwk = - (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (280.0 / 3.0 * u * u + 56.0 / 3.0 * u);
                  wk *= KERNEL_NORM * hinv3;
                  dwk *= KERNEL_NORM * hinv4;
#endif

                  mass_j = P[j].Mass;
                  rho += mass_j * wk;
                  dhsmlrho += FLT(- mass_j * ((double) NUMDIMS * hinv * wk + u * dwk));
                  
#ifdef WAKEUP
                  dt_step = P[j].dt_step;
#else
                  dt_step = P[j].TimeBin ? (1 << P[j].TimeBin) : 0;
#endif

                  if(All.ComovingIntegrationOn)
                    {
                      if(dt_step > 0)
                        dt_gravkick = get_gravkick_factor(P[j].Ti_begstep, All.Ti_Current) - get_gravkick_factor(P[j].Ti_begstep, P[j].Ti_begstep + dt_step / 2);
                      else
                        dt_gravkick = (All.Ti_Current - (P[j].Ti_begstep + dt_step / 2)) * All.Timebase_interval;
                    }
                  else
                    dt_gravkick = (All.Ti_Current - (P[j].Ti_begstep + dt_step / 2)) * All.Timebase_interval;
                    
                  vj[0] = P[j].Vel[0] + P[j].g.GravAccel[0] * dt_gravkick;
                  vj[1] = P[j].Vel[1] + P[j].g.GravAccel[1] * dt_gravkick;
                  vj[2] = P[j].Vel[2] + P[j].g.GravAccel[2] * dt_gravkick;
#ifdef PMGRID
                  vj[0] += P[j].GravPM[0] * dt_gravkick_pm;
                  vj[1] += P[j].GravPM[1] * dt_gravkick_pm;
                  vj[2] += P[j].GravPM[2] * dt_gravkick_pm;
#endif
                  dv[0] = vj[0] - vel[0];
                  dv[1] = vj[1] - vel[1];
                  dv[2] = vj[2] - vel[2];
                  
                  if(All.ComovingIntegrationOn)
                    {
                      dv[0] *= ainv;
                      dv[1] *= ainv;
                      dv[2] *= ainv;
                      dv[0] += All.Time * dx * hubble_a;
                      dv[1] += All.Time * dy * hubble_a;
                      dv[2] += All.Time * dz * hubble_a;
                    }
                    
                  v[0] += dv[0];
                  v[1] += dv[1];
                  v[2] += dv[2];
                  v2[0] += dv[0] * dv[0];
                  v2[1] += dv[1] * dv[1];
                  v2[2] += dv[2] * dv[2];
                  
                  weighted_numngb += FLT(NORM_COEFF * wk / hinv3);
                }
            }
        }
        
      if(mode == 1)
        {
          listindex++;
          
          if(listindex < NODELISTLENGTH)
            {
              startnode = DataGet[target].NodeList[listindex];
              
              if(startnode >= 0)
                startnode = Nodes[startnode].u.d.nextnode;// open it //
            }
        }
    }
    
  if(mode == 0)
    {
      VelData[target].Vel[0] = v[0];
      VelData[target].Vel[1] = v[1];
      VelData[target].Vel[2] = v[2];
      VelData[target].Vel2[0] = v2[0];
      VelData[target].Vel2[1] = v2[1];
      VelData[target].Vel2[2] = v2[2];
      P[target].Density = rho;
      P[target].DhsmlDensityFactor = dhsmlrho;
      P[target].NgbNum = (MyLongDouble) numngb;
      //P[target].NgbNum = weighted_numngb;MyLongDouble
    }
  else
    {
      DataResult[target].Vel[0] = v[0];
      DataResult[target].Vel[1] = v[1];
      DataResult[target].Vel[2] = v[2];
      DataResult[target].Vel2[0] = v2[0];
      DataResult[target].Vel2[1] = v2[1];
      DataResult[target].Vel2[2] = v2[2];
      DataResult[target].Rho = rho;
      DataResult[target].DhsmlDensity = dhsmlrho;
      DataResult[target].Ngb = (MyLongDouble) numngb;
      //DataResult[target].Ngb = weighted_numngb;
    }
    
  return 0;
}

void set_gas_hsml_guess(void)
{
  int n, no;
  float hsml;
  
  for(n = FirstActiveParticle; n >= 0; n = NextActiveParticle[n])
    {
      no = Father[n];
      
      while(no >= 0)
        {
          if(Nodes[no].u.d.mass > All.DesNumNgb * P[n].Mass)
            break;
            
          no = Nodes[no].u.d.father;
        }
        
      hsml = P[n].Hsml;
      
      if(no >= 0)
        {
          if(Nodes[no].u.d.mass > All.DesNumNgb * P[n].Mass)
            hsml = pow(3.0 / (4.0 * M_PI) * All.DesNumNgb * P[n].Mass / Nodes[no].u.d.mass, 1.0 / 3.0) * Nodes[no].len;
        }
        
      if(hsml < P[n].Hsml)
        P[n].Hsml = hsml;
        
      if(P[n].Hsml < All.MinGasHsml)
        P[n].Hsml = All.MinGasHsml;
    }
}

int ngb_treefind_nonbaryon(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local)
{
  int numngb, no, p, task, nexport_save;
  struct NODE *current;
  double dx, dy, dz, dist;
#ifdef PERIODIC
  MyDouble xtmp;
#endif
  
  nexport_save = *nexport;
  numngb = 0;
  no = *startnode;
  
  while(no >= 0)
    {
      if(no < All.MaxPart)// single particle //
        {
          p = no;
          no = Nextnode[no];
          
          if(P[p].Type == 0 || P[p].Type == 4)
            continue;
            
          if(P[p].Ti_current != All.Ti_Current)
            drift_particle(p, All.Ti_Current);
            
          dist = hsml;
#ifdef PERIODIC
          dx = NGB_PERIODIC_LONG_X((double) P[p].Pos[0] - searchcenter[0]);
          
          if(dx > dist)
            continue;
            
          dy = NGB_PERIODIC_LONG_Y((double) P[p].Pos[1] - searchcenter[1]);
          
          if(dy > dist)
            continue;
            
          dz = NGB_PERIODIC_LONG_Z((double) P[p].Pos[2] - searchcenter[2]);
          
          if(dz > dist)
                continue;
#else
          dx = (double) P[p].Pos[0] - searchcenter[0];
          
          if(dx > dist)
            continue;
            
          dy = (double) P[p].Pos[1] - searchcenter[1];
          
          if(dy > dist)
            continue;
            
          dz = (double) P[p].Pos[2] - searchcenter[2];
          
          if(dz > dist)
            continue;
#endif
          if(dx * dx + dy * dy + dz * dz > dist * dist)
            continue;
            
          Ngblist[numngb++] = p;
        }
      else
        {
          if(no >= All.MaxPart + MaxNodes)// pseudo particle //
            {
              if(mode == 1)
                endrun(12312);
                
              if(target >= 0)// if no target is given, export will not occur //
                {
                  if(Exportflag[task = DomainTask[no - (All.MaxPart + MaxNodes)]] != target)
                    {
                      Exportflag[task] = target;
                      Exportnodecount[task] = NODELISTLENGTH;
                    }
                    
                  if(Exportnodecount[task] == NODELISTLENGTH)
                    {
                      if(*nexport >= All.BunchSize)
                        {
                          *nexport = nexport_save;
                          
                          if(nexport_save == 0)
                            endrun(13004);// in this case, the buffer is too small to process even a single particle //
                            
                          for(task = 0; task < NTask; task++)
                            nsend_local[task] = 0;
                            
                          for(no = 0; no < nexport_save; no++)
                            nsend_local[DataIndexTable[no].Task]++;
                            
                          return -1;
                        }
                        
                      Exportnodecount[task] = 0;
                      Exportindex[task] = *nexport;
                      DataIndexTable[*nexport].Task = task;
                      DataIndexTable[*nexport].Index = target;
                      DataIndexTable[*nexport].IndexGet = *nexport;
                      *nexport = *nexport + 1;
                      nsend_local[task]++;
                    }
                    
                  DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]++] =
                  DomainNodeIndex[no - (All.MaxPart + MaxNodes)];
                  
                  if(Exportnodecount[task] < NODELISTLENGTH)
                    DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]] = -1;
                }
                
              no = Nextnode[no - MaxNodes];
              continue;
            }
            
          current = &Nodes[no];
          
          if(mode == 1)
            {
              // we reached a top-level node again, which means that we are done with the branch //
              if(current->u.d.bitflags & (1 << BITFLAG_TOPLEVEL))
                {
                  *startnode = -1;
                  return numngb;
                }
            }
            
          if(current->Ti_current != All.Ti_Current)
            force_drift_node(no, All.Ti_Current);
            
          no = current->u.d.sibling;// in case the node can be discarded //
          
          dist = hsml + 0.5 * current->len;
#ifdef PERIODIC	  
          dx = NGB_PERIODIC_LONG_X((double) current->center[0] - searchcenter[0]);
          
          if(dx > dist)
            continue;
            
          dy = NGB_PERIODIC_LONG_Y((double) current->center[1] - searchcenter[1]);
          
          if(dy > dist)
            continue;
            
          dz = NGB_PERIODIC_LONG_Z((double) current->center[2] - searchcenter[2]);
          
          if(dz > dist)
            continue;
#else
          dx = (double) current->center[0] - searchcenter[0];
          
          if(dx > dist)
            continue;
            
          dy = (double) current->center[1] - searchcenter[1];
          
          if(dy > dist)
            continue;
            
          dz = (double) current->center[2] - searchcenter[2];
          
          if(dz > dist)
            continue;
#endif
          // now test against the minimal sphere enclosing everything //
          dist += FACT1 * (double) current->len;
          
          if(dx * dx + dy * dy + dz * dz > dist * dist)
            continue;
            
          no = current->u.d.nextnode;// ok, we need to open the node //
        }
    }
    
  *startnode = -1;
  
  return numngb;
}

#endif

