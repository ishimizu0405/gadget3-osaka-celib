//  \file allvars.h                                                                                      //
//  \brief declares global variables.                                                                    //
//                                                                                                       //
//  This file declares all global variables. Further variables should be added here, and declared as     //
//  'extern'. The actual existence of these variables is provided by the file 'allvars.c'. To produce    //
//  'allvars.c' from 'allvars.h', do the following:                                                      //
//                                                                                                       //
//     - Erase all #define statements                                                                    //
//     - add #include "allvars.h"                                                                        //
//     - delete all keywords 'extern'                                                                    //
//     - delete all struct definitions enclosed in {...}, e.g.                                           //
//        "extern struct global_data_all_processes {....} All;"                                          //
//        becomes "struct global_data_all_processes All;"                                                //
#ifndef ALLVARS_H
#define ALLVARS_H

#include <mpi.h>
#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_errno.h>

#ifdef OSAKA_COOLING
#include <grackle.h>
#endif

#include "tags.h"
#ifdef CHEMISTRY
#include "chemistry.h"
#endif
//#include "assert.h"

#define  GADGETVERSION   "3.0"	// code version string //
#define  GENERATIONS     2      // Number of star particles that may be created per gas particle //
#define  TIMEBINS        29
#define  TIMEBASE        (1<<TIMEBINS) // The simulated timespan is mapped onto the integer interval [0,TIMESPAN], where TIMESPAN needs to be a power of 2. Note that (1<<28) corresponds to 2^29 //

#ifndef  MULTIPLEDOMAINS
#define  MULTIPLEDOMAINS     1
#endif

#ifndef  TOPNODEFACTOR
#define  TOPNODEFACTOR       2.5
#endif

#define  NODELISTLENGTH      8

typedef unsigned long long peanokey;

#define  BITS_PER_DIMENSION 21	       // for Peano-Hilbert order. Note: Maximum is 10 to fit in 32-bit integer ! //
#define  PEANOCELLS         (((peanokey)1)<<(3*BITS_PER_DIMENSION))
#define  GAMMA              (5.0/3)    // adiabatic index of simulated gas //
#define  GAMMA_MINUS1       (GAMMA-1)
#define  HYDROGEN_MASSFRAC  0.76       // mass fraction of hydrogen, relevant only for radiative cooling //
#define  METAL_YIELD        0.02       // effective metal yield for star formation //
#define  MAX_REAL_NUMBER    1e37
#define  MIN_REAL_NUMBER    1e-37
#define  RNDTABLE           8192

// ... often used physical constants (cgs units) //
#define  GRAVITY                   6.672e-8
#define  SOLAR_MASS                1.989e33
#define  SOLAR_LUM                 3.826e33
#define  RAD_CONST                 7.565e-15
#define  AVOGADRO                  6.0222e23
#define  BOLTZMANN                 1.3806e-16
#define  GAS_CONST                 8.31425e7
#define  C                         2.9979e10
#define  PLANCK                    6.6262e-27
#define  CM_PER_MPC                3.085678e24
#define  PROTONMASS                1.6726e-24
#define  ELECTRONMASS              9.10953e-28
#define  THOMPSON                  6.65245e-25
#define  ELECTRONCHARGE            4.8032e-10
#define  HUBBLE                    3.2407789e-18 // in h/sec //
#define  LYMAN_ALPHA               1215.6e-8     // 1215.6 Angstroem //
#define  LYMAN_ALPHA_HeII          303.8e-8      // 303.8 Angstroem //
#define  OSCILLATOR_STRENGTH       0.41615
#define  OSCILLATOR_STRENGTH_HeII  0.41615

#define SOLAR_Z     0.0204 
#ifdef H2REGSF
//#define SIGMA_NORM  1.0e-4             //conversion from gadget units to Msun/pc^2
#define H2REG_EFF   0.01               //1.0e-2
#define DENSTHRESH  0.6                //in cm^{-3}
#define OTUVTHRESH  0.01               //OTUV threshold in percentage of DENSTHRESH (cm^-3)
#endif

#ifdef OSAKA
#define TIME_GYR          1.0e-9    // Converting time to Gyr             //
#define HUBBLETIME        9.7776e+9 // h^{-1}yr                           //
#define NSN_MINIMUM       1.5       // minimum number of sn (II, Ia) explosion (0-2) //
#define SOLAR_METALLICITY 0.0134    // solar metallicity Asplund et al. 2009 //
#ifdef QUINTIC_KERNEL
#define KERNEL_NORM       17.452071002104601080439772225411  // 3^9/359PI //
#define ONETHIRD          0.3333333333333333333333333333333
#define TWOTHIRD          0.6666666666666666666666666666666
#endif
#ifdef WENDLAND_C4_KERNEL
#define KERNEL_NORM       4.9238560519055119503498414293372   // 495/32PI //
#endif

#ifdef OSAKA_GEODESIC_DOME_WEIGHT
#define N_ICOSAHEDRON_VERTEX  12
#define N_ICOSAHEDRON_SURFACE 20
#define N_SURFACE_LV1         80
#define N_SURFACE_TOTAL_LV1   100
#define MAX_REFINEMENT_LV     3
#define NDIVISION             4

struct Point
{
  double x[3];
};

struct Surface
{
  int id, parent, children[4], neighbour[3];
  double vertex[3][3];
  double vector[3], occupancy;
  int nparticle;
};
#endif

#ifdef OSAKA_ESFB
#define NMETAL_BIN         31
#define NAGE_LYC           321
#define METAL_MIN          -4.0
#define METAL_MAX          -1.0
#define METAL_BIN          0.1
#define CASEB_COEFFICIENT  2.6e-13
extern double LyC_table[NMETAL_BIN][NAGE_LYC];
#endif

#ifdef OSAKA_CELIB
#define N_TIME_CELIB                  1000
#define TIME_MIN_CELIB                1.009593e6
#define DT_LOG_CELIB                  4.146128e-3
#define N_METALLCITY_BIN_CELIB        7
#define METALIICITY_MIN_POPIII_CELIB  1.0e-7
#define METALIICITY_MIN_CELIB         1.0e-6
#define METALLICITY_MAX_CELIB         0.1
#define METALLICITY_BIN_LOG           1.0
#define N_YIELD_TYPE_CELIB            3
#define N_YIELD_ELEMENT_CELIB         12 // ignore Europium //
//#define N_YIELD_ELEMENT_CELIB         13
#define TIME_MIN_SNII_CELIB           5.0e6
#define TIME_MAX_SNII_CELIB           2.2e7
#define TIME_MIN_SNIA_CELIB           4.0e7
#define TIME_MAX_SNIA_CELIB           1.4e10
#define TIME_MIN_AGB_CELIB            4.0e7
#define TIME_MAX_AGB_CELIB            1.4e10
#define N_MAX_FEEDBACK                256

struct CELIB_YIELD
{
  double stellar_age[N_TIME_CELIB];
  double snii_energy_cum[N_METALLCITY_BIN_CELIB][N_TIME_CELIB];
  double snia_energy_cum[N_METALLCITY_BIN_CELIB][N_TIME_CELIB];
  double snii_mass_released[N_METALLCITY_BIN_CELIB][N_TIME_CELIB];
  double snia_mass_released[N_METALLCITY_BIN_CELIB][N_TIME_CELIB];
  double agb_mass_released[N_METALLCITY_BIN_CELIB][N_TIME_CELIB];
  double snii_yield_cum[N_METALLCITY_BIN_CELIB][N_YIELD_ELEMENT_CELIB][N_TIME_CELIB];
  double snia_yield_cum[N_METALLCITY_BIN_CELIB][N_YIELD_ELEMENT_CELIB][N_TIME_CELIB];
  double agb_yield_cum[N_METALLCITY_BIN_CELIB][N_YIELD_ELEMENT_CELIB][N_TIME_CELIB];
  double snii_duration_time[N_METALLCITY_BIN_CELIB][2];
  double snia_duration_time[N_METALLCITY_BIN_CELIB][2];
  double agb_duration_time[N_METALLCITY_BIN_CELIB][2];
};

struct CELIB_YIELD_DIF
{
  double snii_energy_dif[N_METALLCITY_BIN_CELIB][N_TIME_CELIB];
  double snia_energy_dif[N_METALLCITY_BIN_CELIB][N_TIME_CELIB];
  double agb_yield_dif[N_METALLCITY_BIN_CELIB][N_TIME_CELIB];
};

extern struct CELIB_YIELD CELib_yield;
extern struct CELIB_YIELD_DIF CELib_yield_dif;
extern double snii_energy_table[N_METALLCITY_BIN_CELIB][N_MAX_FEEDBACK], snii_mass_released_table[N_METALLCITY_BIN_CELIB][N_MAX_FEEDBACK], snii_metal_yield_table[N_METALLCITY_BIN_CELIB][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK];
extern double snia_energy_table[N_METALLCITY_BIN_CELIB][N_MAX_FEEDBACK], snia_mass_released_table[N_METALLCITY_BIN_CELIB][N_MAX_FEEDBACK], snia_metal_yield_table[N_METALLCITY_BIN_CELIB][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK];
extern double agb_mass_released_table[N_METALLCITY_BIN_CELIB][N_MAX_FEEDBACK], agb_metal_yield_table[N_METALLCITY_BIN_CELIB][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK];
// 0:total, 1:Hydrogen, 2:Helium, 3:Carbon, 4:Nitrogen, 5:Oxygen, 6:Neon, 7:Magnesium, 8:Silicon, 9:Sulfur, 10:Calcium, 11:Iron, 12:Nickel, 13:Europium //
#endif
#endif

#ifdef NAVIERSTOKES
#define  LOG_LAMBDA  7.8   // logarithmic Coulomb factor //
#endif

#ifdef CHEMISTRY
#define  T_CMB0  2.728     // present-day CMB temperature //
#endif

#define  SEC_PER_GIGAYEAR   3.155e16
#define  SEC_PER_MEGAYEAR   3.155e13
#define  SEC_PER_YEAR       3.155e7

#ifndef FOF_PRIMARY_LINK_TYPES
#define FOF_PRIMARY_LINK_TYPES 2
#endif

#ifndef FOF_SECONDARY_LINK_TYPES
#define FOF_SECONDARY_LINK_TYPES 0
#endif

#ifndef ASMTH
// ASMTH gives the scale of the short-range/long-range force split in units of FFT-mesh cells //
#define ASMTH 1.25
#endif

#ifndef RCUT
// RCUT gives the maximum distance (in units of the scale used for the force split) out to which short-range forces are evaluated in the short-range tree walk. //
#define RCUT  4.5
#endif

#define COND_TIMESTEP_PARAMETER 0.25
#define VISC_TIMESTEP_PARAMETER 0.25
#define MAXLEN_OUTPUTLIST       10000  // maxmimum number of entries in output list //
#define DRIFT_TABLE_LENGTH      1000   // length of the lookup table used to hold the drift and kick factors //
#define MAXITER                 500
#define LINKLENGTH              0.2
#define GROUP_MIN_LEN           32
#define MINRESTFAC              0.05

#ifndef LONGIDS
typedef unsigned int MyIDType;
#else
typedef unsigned long long MyIDType;
#endif

#ifndef DOUBLEPRECISION     // default is single-precision //
typedef float  MyFloat;
typedef float  MyDouble;
#else
#if (DOUBLEPRECISION == 2)   // mixed precision //
typedef float   MyFloat;
typedef double  MyDouble;
#else                        // everything double-precision //
typedef double  MyFloat;
typedef double  MyDouble;
#endif
#endif

#ifdef OUTPUT_IN_DOUBLEPRECISION
typedef double MyOutputFloat;
#else
typedef float MyOutputFloat;
#endif

#ifdef INPUT_IN_DOUBLEPRECISION
typedef double MyInputFloat;
#else
typedef float MyInputFloat;
#endif

struct unbind_data
{
  int index;
};

#ifdef FIX_PATHSCALE_MPI_STATUS_IGNORE_BUG
extern MPI_Status mpistat;
#undef MPI_STATUS_IGNORE
#define MPI_STATUS_IGNORE &mpistat
#endif

#ifdef FLTROUNDOFFREDUCTION  
#define FLT(x) ((MyFloat)(x))
#ifdef SOFTDOUBLEDOUBLE      // this requires a C++ compilation //
#include "dd.h"
typedef dd MyLongDouble;
#else
typedef long double MyLongDouble;
#endif
#else  // not enabled //
#define FLT(x) (x)
typedef MyFloat MyLongDouble;
#endif  // end FLTROUNDOFFREDUCTION //

#define CPU_ALL            0
#define CPU_TREEWALK1      1
#define CPU_TREEWALK2      2
#define CPU_TREEWAIT1      3
#define CPU_TREEWAIT2      4
#define CPU_TREESEND       5
#define CPU_TREERECV       6
#define CPU_TREEMISC       7
#define CPU_TREEBUILD      8
#define CPU_TREEUPDATE     9
#define CPU_TREEHMAXUPDATE 10
#define CPU_DOMAIN         11
#define CPU_DENSCOMPUTE    12
#define CPU_DENSWAIT       13
#define CPU_DENSCOMM       14
#define CPU_DENSMISC       15
#define CPU_HYDCOMPUTE     16
#define CPU_HYDWAIT        17
#define CPU_HYDCOMM        18
#define CPU_HYDMISC        19
#define CPU_DRIFT          20
#define CPU_BLACKHOLE      21
#define CPU_TIMELINE       22
#define CPU_POTENTIAL      23
#define CPU_MESH           24
#define CPU_PEANO          25
#define CPU_COOLINGSFR     26
#define CPU_SNAPSHOT       27
#define CPU_FOF            28
#define CPU_BLACKHOLES     29
#define CPU_MISC           30
#define CPU_FEEDBACK       31
#define CPU_PARTS          32
#define CPU_STRING_LEN     120

#ifndef  TWODIMS
#define  NUMDIMS 3                                // For 3D-normalized kernel //
#define  KERNEL_COEFF_1  2.546479089470	          // Coefficients for SPH spline kernel and its derivative //
#define  KERNEL_COEFF_2  15.278874536822
#define  KERNEL_COEFF_3  45.836623610466
#define  KERNEL_COEFF_4  30.557749073644
#define  KERNEL_COEFF_5  5.092958178941
#define  KERNEL_COEFF_6  (-15.278874536822)
#define  NORM_COEFF      4.188790204786	           // Coefficient for kernel normalization. Note:  4.0/3 * PI = 4.188790204786 //
#else
#define  NUMDIMS 2                                 // For 2D-normalized kernel //
#define  KERNEL_COEFF_1  (5.0/7*2.546479089470)	   // Coefficients for SPH spline kernel and its derivative //
#define  KERNEL_COEFF_2  (5.0/7*15.278874536822)
#define  KERNEL_COEFF_3  (5.0/7*45.836623610466)
#define  KERNEL_COEFF_4  (5.0/7*30.557749073644)
#define  KERNEL_COEFF_5  (5.0/7*5.092958178941)
#define  KERNEL_COEFF_6  (5.0/7*(-15.278874536822))
#define  NORM_COEFF      M_PI                       // Coefficient for kernel normalization. //
#endif

#if defined (BLACK_HOLES)
#define PPP P
#else
#define PPP SphP
#endif

#define DMAX(a,b) (dmax1=(a),dmax2=(b),(dmax1>dmax2)?dmax1:dmax2)
#define DMIN(a,b) (dmin1=(a),dmin2=(b),(dmin1<dmin2)?dmin1:dmin2)
#define IMAX(a,b) (imax1=(a),imax2=(b),(imax1>imax2)?imax1:imax2)
#define IMIN(a,b) (imin1=(a),imin2=(b),(imin1<imin2)?imin1:imin2)

#ifdef PERIODIC
extern MyDouble boxSize, boxHalf;
#ifdef LONG_X
extern MyDouble boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
extern MyDouble boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
extern MyDouble boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif

#ifdef PERIODIC
#define NGB_PERIODIC_LONG_X(x) (xtmp=fabs(x),(xtmp>boxHalf_X)?(boxSize_X-xtmp):xtmp)
#define NGB_PERIODIC_LONG_Y(x) (xtmp=fabs(x),(xtmp>boxHalf_Y)?(boxSize_Y-xtmp):xtmp)
#define NGB_PERIODIC_LONG_Z(x) (xtmp=fabs(x),(xtmp>boxHalf_Z)?(boxSize_Z-xtmp):xtmp)
#else
#define NGB_PERIODIC_LONG_X(x) fabs(x)
#define NGB_PERIODIC_LONG_Y(x) fabs(x)
#define NGB_PERIODIC_LONG_Z(x) fabs(x)
#endif

#define FACT1 0.366025403785	// FACT1 = 0.5 * (sqrt(3)-1) //

///////////////////////////////////////////////////////////
//                Global variables                       //
///////////////////////////////////////////////////////////

extern int FirstActiveParticle;
extern int *NextActiveParticle;
extern int TimeBinCount[TIMEBINS];
extern int TimeBinCountSph[TIMEBINS];
extern int TimeBinActive[TIMEBINS];
extern int FirstInTimeBin[TIMEBINS];
extern int LastInTimeBin[TIMEBINS];
extern int *NextInTimeBin;
extern int *PrevInTimeBin;

#if defined(SFR) || defined(OSAKA_SFR)
extern double TimeBinSfr[TIMEBINS];
#endif

#ifdef BLACK_HOLES
extern double TimeBin_BH_mass[TIMEBINS];
extern double TimeBin_BH_dynamicalmass[TIMEBINS];
extern double TimeBin_BH_Mdot[TIMEBINS];
#endif

extern int ThisTask;                 // the number of the local processor  //
extern int NTask;                    // number of processors //
extern int PTask;                    // note: NTask = 2^PTask //
extern double CPUThisRun;            // Sums CPU time of current process //
extern int NumForceUpdate;           // number of active particles on local processor in current timestep  //
extern long long GlobNumForceUpdate;
extern int NumSphUpdate;	     // number of active SPH particles on local processor in current timestep  //
extern int MaxTopNodes;	             // Maximum number of nodes in the top-level tree used for domain decomposition //
extern int RestartFlag;	             // taken from command line used to start code. 0 is normal start-up from initial conditions, 1 is resuming a run from a set of restart files, while marks a restart from a snapshot file. //
extern int RestartSnapNum;
extern int *Exportflag;	             // Buffer used for flagging whether a particle needs to be exported to another process //
extern int *Exportnodecount;
extern int *Exportindex;
extern int *Send_offset, *Send_count, *Recv_count, *Recv_offset, *Sendcount_matrix;
extern size_t AllocatedBytes;
extern size_t FreeBytes;
extern double CPU_Step[CPU_PARTS];
extern char CPU_Symbol[CPU_PARTS];
extern char CPU_SymbolImbalance[CPU_PARTS];
extern char CPU_String[CPU_STRING_LEN + 1];
extern double WallclockTime;         // This holds the last wallclock time measurement for timings measurements //
extern int Flag_FullStep;	     // Flag used to signal that the current step involves all particles //
extern int TreeReconstructFlag;
extern int GlobFlag;
extern int NumPart;                  // number of particles on the LOCAL processor //
extern int N_gas;                    // number of gas particles on the LOCAL processor  //
extern long long Ntype[6];           // total number of particles of each type //
extern int NtypeLocal[6];            // local number of particles of each type //
extern gsl_rng *random_generator;    // the random number generator used //

#if defined(SFR) || defined(OSAKA_SFR)
extern int Stars_converted;                     // current number of star particles in gas particle block //
#endif

extern double TimeOfLastTreeConstruction;       // holds what it says //
extern int *Ngblist;                            // Buffer to hold indices of neighbours retrieved by the neighbour search routines //
extern double DomainCorner[3], DomainCenter[3], DomainLen, DomainFac;
extern int *DomainStartList, *DomainEndList;
extern double *DomainWork;
extern int *DomainCount;
extern int *DomainCountSph;
extern int *DomainTask;
extern int *DomainNodeIndex;
extern int *DomainList, DomainNumChanged;
extern peanokey *Key, *KeySorted;

extern struct topnode_data
{
  peanokey Size;
  peanokey StartKey;
  long long Count;
  MyFloat GravCost;
  int Daughter;
  int Pstart;
  int Blocks;
  int Leaf;
}*TopNodes;

extern int NTopnodes, NTopleaves;
extern double RndTable[RNDTABLE];

#ifdef SUBFIND
extern int GrNr;
extern int NumPartGroup;
#endif

#ifdef GROUPFINDER
extern int GroupOutput;         // Whether make group properties output file //
#endif

// variables for input/output , usually only used on process 0 //
extern char ParameterFile[100];	// file name of parameterfile used for starting the simulation //
extern FILE *FdInfo;            // file handle for info.txt log-file. //
extern FILE *FdEnergy;	        // file handle for energy.txt log-file. //
extern FILE *FdTimings;	        // file handle for timings.txt log-file. //
extern FILE *FdBalance;	        // file handle for balance.txt log-file. //
extern FILE *FdCPU;             // file handle for cpu.txt log-file. //

#if defined(SFR) || defined(OSAKA_SFR)
extern FILE *FdSfr;             // file handle for sfr.txt log-file. //
#endif

#ifdef RADTRANSFER
extern FILE *FdEddington;       // file handle for eddington.txt log-file. //
extern FILE *FdRadtransfer;     // file handle for radtransfer.txt log-file. //
#endif

#ifdef BLACK_HOLES
extern FILE *FdBlackHoles;	// file handle for blackholes.txt log-file. //
extern FILE *FdBlackHolesDetails;
#endif

#ifdef FORCETEST
extern FILE *FdForceTest;       // file handle for forcetest.txt log-file. //
#endif

#ifdef DARKENERGY
extern FILE *FdDE;              // file handle for darkenergy.txt log-file. //
#endif

// table for the cosmological drift factors //
extern double DriftTable[DRIFT_TABLE_LENGTH];

// table for the cosmological kick factor for gravitational forces //
extern double GravKickTable[DRIFT_TABLE_LENGTH];

// table for the cosmological kick factor for hydrodynmical forces //
extern double HydroKickTable[DRIFT_TABLE_LENGTH];
extern void *CommBuffer; // points to communication buffer, which is used at a few places //

// This structure contains data which is the SAME for all tasks (mostly code parameters read from the parameter file).  Holding this data in a structure is convenient for writing/reading the restart file, and it allows the introduction of new global variables in a simple way. The only thing to do is to introduce them into this structure. //
extern struct global_data_all_processes
{
  long long TotNumPart;	  //  total particle numbers (global value) //
  long long TotN_gas;     //  total gas particle number (global value) //
  
#ifdef BLACK_HOLES
  int TotBHs;
#endif

  int MaxPart;                    // This gives the maxmimum number of particles that can be stored on one processor. //
  int MaxPartSph;                 // This gives the maxmimum number of SPH particles that can be stored on one processor. //
  int ICFormat;	                  // selects different versions of IC file-format //
  int SnapFormat;                 // selects different versions of snapshot file-formats //
  int DoDynamicUpdate;
  int NumFilesPerSnapshot;        // number of files in multi-file snapshot dumps //
  int NumFilesWrittenInParallel;  // maximum number of files that may be written simultaneously when writing/reading restart-files, or when writing snapshot files //
  int BufferSize;                 // size of communication buffer in MB //
  int BunchSize;                  // number of particles fitting into the buffer in the parallel tree algorithm  //
  double PartAllocFactor;         // in order to maintain work-load balance, the particle load will usually NOT be balanced.  Each processor allocates memory for PartAllocFactor times the average number of particles to allow for that //
  double TreeAllocFactor;         // Each processor allocates a number of nodes which is TreeAllocFactor times the maximum(!) number of particles.  Note: A typical local tree for N particles needs usually about ~0.65*N nodes. //
  double TopNodeAllocFactor;      // Each processor allocates a number of nodes which is TreeAllocFactor times the maximum(!) number of particles.  Note: A typical local tree for N particles needs usually about ~0.65*N nodes. //
  
#ifdef SCALARFIELD
  double ScalarBeta;
  double ScalarScreeningLength;
#endif

  // some SPH parameters //
  int DesNumNgb;       // Desired number of SPH neighbours //
  
#ifdef SUBFIND
  int DesLinkNgb;
  double ErrTolThetaSubfind;
#endif

  double MaxNumNgbDeviation;         // Maximum allowed deviation neighbour number //
  
#ifdef START_WITH_EXTRA_NGBDEV
  double MaxNumNgbDeviationStart;    // Maximum allowed deviation neighbour number to start with//
#endif

  double ArtBulkViscConst; // Sets the parameter \f$\alpha\f$ of the artificial viscosity //
  double InitGasTemp;      // may be used to set the temperature in the IC's //
  double InitGasU;         // the same, but converted to thermal energy per unit mass //
  double MinGasTemp;       // may be used to set a floor for the gas temperature //
  double MinEgySpec;       // the minimum allowed temperature expressed as energy per unit mass //

#ifdef ARTIFICIAL_CONDUCTIVITY
  double ArtCondConstant;
#endif

  // some force counters  //
  long long TotNumOfForces;                  // counts total number of force computations  //
  long long NumForcesSinceLastDomainDecomp;  // count particle updates since last domain decomposition //
  
  // some variable for dynamic work-load adjustment based on CPU measurements //
  double Cadj_Cost;
  double Cadj_Cpu;
  
  // system of units  //
  double UnitTime_in_s;	           // factor to convert internal time unit to seconds/h //
  double UnitMass_in_g;	           // factor to convert internal mass unit to grams/h //
  double UnitVelocity_in_cm_per_s; // factor to convert intqernal velocity unit to cm/sec //
  double UnitLength_in_cm;         // factor to convert internal length unit to cm/h //
  double UnitPressure_in_cgs;      // factor to convert internal pressure unit to cgs units (little 'h' still around!) //
  double UnitDensity_in_cgs;       // factor to convert internal length unit to g/cm^3*h^2 //
  double UnitCoolingRate_in_cgs;   // factor to convert internal cooling rate to cgs units //
  double UnitEnergy_in_cgs;        // factor to convert internal energy to cgs units //
  double UnitTime_in_years;        // factor to convert internal time to megayears/h //
  double UnitTime_in_Megayears;	   // factor to convert internal time to megayears/h //
  double GravityConstantInternal;  // If set to zero in the parameterfile, the internal value of the gravitational constant is set to the Newtonian value based on the system of units specified. Otherwise the value provided is taken as internal gravity constant G. //
  double G;                        // Gravity-constant in internal units //
  
  // Cosmology //
  double Hubble;       // Hubble-constant in internal units //
  double Omega0;       // matter density in units of the critical density (at z=0) //
  double OmegaLambda;  // vaccum energy density relative to crictical density (at z=0) //
  double OmegaBaryon;  // baryon density in units of the critical density (at z=0) //
  double HubbleParam;  // little `h', i.e. Hubble constant in units of 100 km/s/Mpc.  Only needed to get absolute physical values for cooling physics //
  
  double BoxSize;      // Boxsize in case periodic boundary conditions are used //
  
  // Code options //
  int ComovingIntegrationOn;    // flags that comoving integration is enabled //
  int PeriodicBoundariesOn;     // flags that periodic boundaries are enabled //
  int ResubmitOn;               // flags that automatic resubmission of job to queue system is enabled //
  int TypeOfOpeningCriterion;   // determines tree cell-opening criterion: 0 for Barnes-Hut, 1 for relative criterion //
  int TypeOfTimestepCriterion;  // gives type of timestep criterion (only 0 supported right now - unlike gadget-1.1) //
  int OutputListOn;             // flags that output times are listed in a specified file //
  int CoolingOn;                // flags that cooling is enabled //
  int StarformationOn;          // flags that star formation is enabled //
  
  // parameters determining output frequency //
  int SnapshotFileCount;         // number of snapshot that is written next //
  double TimeBetSnapshot;        // simulation time interval between snapshot files //
  double TimeOfFirstSnapshot;    // simulation time of first snapshot files //
  double CpuTimeBetRestartFile;  // cpu-time between regularly generated restart files //
  double TimeLastRestartFile;    // cpu-time when last restart-file was written //
  double TimeBetStatistics;      // simulation time interval between computations of energy statistics //
  double TimeLastStatistics;     // simulation time when the energy statistics was computed the last time //
  int NumCurrentTiStep;	         // counts the number of system steps taken up to this point //
  
  // Current time of the simulation, global step, and end of simulation //
  double Time;        // current time of the simulation //
  double TimeBegin;   // time of initial conditions of the simulation //
  double TimeStep;    // difference between current times of previous and current timestep //
  double TimeMax;     // marks the point of time until the simulation is to be evolved //
  
  // variables for organizing discrete timeline //
  double Timebase_interval;     // factor to convert from floating point time interval to integer timeline //
  int Ti_Current;               // current time on integer timeline //
  int Ti_nextoutput;            // next output time on integer timeline //
  
#ifdef FLEXSTEPS
  int PresentMinStep;           // If FLEXSTEPS is used, particle timesteps are chosen as multiples of the present minimum timestep. //
  int PresentMaxStep;           // If FLEXSTEPS is used, this is the maximum timestep in timeline units, rounded down to the next power 2 division //
#endif

#ifdef PMGRID
  int PM_Ti_endstep, PM_Ti_begstep;
  double Asmth[2], Rcut[2];
  double Corner[2][3], UpperCorner[2][3], Xmintot[2][3], Xmaxtot[2][3];
  double TotalMeshSize[2];
#endif

#ifdef CHEMISTRY
  double Epsilon;
#endif

  int Ti_nextlineofsight;
#ifdef OUTPUTLINEOFSIGHT
  double TimeFirstLineOfSight;
#endif

  // variables that keep track of cumulative CPU consumption //
  double TimeLimitCPU;
  double CPU_Sum[CPU_PARTS];    // sums wallclock time/CPU consumption in whole run //
  
  // tree code opening criterion //
  double ErrTolTheta;       // BH tree opening angle //
  double ErrTolForceAcc;    // parameter for relative opening criterion in tree walk //
  
  // adjusts accuracy of time-integration //
  double ErrTolIntAccuracy;     // accuracy tolerance parameter \f$ \eta \f$ for timestep criterion. The timesteps is \f$ \Delta t = \sqrt{\frac{2 \eta eps}{a}} \f$ //
  double MinSizeTimestep;       // minimum allowed timestep. Normally, the simulation terminates if the timestep determined by the timestep criteria falls below this limit. //
  double MaxSizeTimestep;       // maximum allowed timestep //
  double MaxRMSDisplacementFac;	// this determines a global timestep criterion for cosmological simulations in comoving coordinates.  To this end, the code computes the rms velocity of all particles, and limits the timestep such that the rms displacement is a fraction of the mean particle separation (determined from the particle mass and the cosmological parameters). This parameter specifies this fraction. //
  double CourantFac;            // SPH-Courant factor //
  
  // frequency of tree reconstruction/domain decomposition //
  double TreeDomainUpdateFrequency;	// controls frequency of domain decompositions  //
  
  // gravitational and hydrodynamical softening lengths (given in terms of an `equivalent' Plummer softening length) five groups of particles are supported 0=gas,1=halo,2=disk,3=bulge,4=stars //
  double MinGasHsmlFractional;	// minimum allowed SPH smoothing length in units of SPH gravitational softening length //
  double MinGasHsml;	        // minimum allowed SPH smoothing length //
  double SofteningGas;		// for type 0 //
  double SofteningHalo;		// for type 1 //
  double SofteningDisk;		// for type 2 //
  double SofteningBulge;        // for type 3 //
  double SofteningStars;        // for type 4 //
  double SofteningBndry;        // for type 5 //
  double SofteningGasMaxPhys;	// for type 0 //
  double SofteningHaloMaxPhys;	// for type 1 //
  double SofteningDiskMaxPhys;	// for type 2 //
  double SofteningBulgeMaxPhys;	// for type 3 //
  double SofteningStarsMaxPhys;	// for type 4 //
  double SofteningBndryMaxPhys;	// for type 5 //
  double SofteningTable[6];	// current (comoving) gravitational softening lengths for each particle type //
  double ForceSoftening[6];	// the same, but multiplied by a factor 2.8 - at that scale the force is Newtonian //
  
  // If particle masses are all equal for one type, the corresponding entry in MassTable is set to this value, allowing the size of the snapshot files to be reduced //
  double MassTable[6];
  
  // some filenames //
  char InitCondFile[100],
    OutputDir[100],
    SnapshotFileBase[100],
    EnergyFile[100],
    CpuFile[100],
    InfoFile[100], TimingsFile[100], RestartFile[100], ResubmitCommand[100], OutputListFilename[100];
    
  // table with desired output times //
  double OutputListTimes[MAXLEN_OUTPUTLIST];
  int OutputListLength;	 // number of times stored in table of desired output times //

#if defined(ADAPTIVE_GRAVSOFT_FORGAS) && !defined(ADAPTIVE_GRAVSOFT_FORGAS_HSML)
  double ReferenceGasMass;
#endif

#ifdef OSAKA
#ifdef OSAKA_COOLING
  int GrackleChemistry;
  int MetalCooling;
  int UVBackground;
  int H2onDust;
  char GrackleUVBackgroundData[256];
  int self_shielding_method;
  int H2_self_shielding;
  double UVbackground_redshift_on;
  double UVbackground_redshift_off;
  double UVbackground_redshift_fullon;
  double UVbackground_redshift_drop;
  double UVbackground_redshift_in;
  double SolarMetalFractionByMass;
#endif

#ifdef OSAKA_VELOCITY_COOLING_STOP
  int DesNumNgbDM;
  double CoolingStopVelocity;
  double CoolingStopAlpha;
#endif

#ifdef OSAKA_METAL_FLOOR
  double MetallicityFloor;
#endif

#ifdef OSAKA_SFR
  double CritOverDensity;
  double CritPhysDensity;
  double OverDensThresh;
  double PhysDensThresh;
  int Nspawn;
  double SFEfficiency;
  double InitialGasMass;
  double TemperatureThresh;
  MyIDType MaxID;
#endif

#ifdef OSAKA_CELIB
  double InitAbundance_Hydrogen;
  double InitAbundance_Helium;
  double InitAbundance_Carbon;
  double InitAbundance_Nitrogen;
  double InitAbundance_Oxygen;
  double InitAbundance_Neon;
  double InitAbundance_Magnesium;
  double InitAbundance_Silicon;
  double InitAbundance_Sulfur;
  double InitAbundance_Calcium;
  double InitAbundance_Iron;
  double InitAbundance_Nickel;
#endif

#ifdef OSAKA_ESFB
  int DesNumNgbESFB;
  int ESFBEventNumber;
  double ESFBEnergy;
  double ESFBEfficiency;
  double TemperatureHIIBubble;
  char LyCTableDir[256];
#endif

#ifdef OSAKA_SNII
  int DesNumNgbSNII;
  int SNIIEventNumber;
  double SNIIKineticFBEfficiency;
  double SNIIThermalFBEfficiency;
  double Energy_per_SNII;
#if !defined(CHEVALIER1974) || !defined(SNII_ENERGY_CONSERVATION)
  double STCoolingFactorSNII;
#endif
#ifdef OSAKA_STRONG_THERMAL_FEEDBACK
  double TemperatureSNIIFeedback;
#endif
#ifdef OSAKA_CONSTANT_WIND
  double WindVelocitySNII;
  double EtaSNII;
#endif
#ifdef DENSITY_WEIGHTED_VALUE
  double SNIIDensityWeightFactor;
#endif
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  int MinNumNgbSNII;
  int MaxNumNgbSNII;
#endif
#endif

#ifdef OSAKA_SNIA
  int DesNumNgbSNIa;
  int SNIaEventNumber;
  double SNIaKineticFBEfficiency;
  double SNIaThermalFBEfficiency;
  double Energy_per_SNIa;
#if !defined(CHEVALIER1974) || !defined(SNII_ENERGY_CONSERVATION)
  double STCoolingFactorSNIa;
#endif
#ifdef OSAKA_STRONG_THERMAL_FEEDBACK
  double TemperatureSNIaFeedback;
#endif
#ifdef OSAKA_CONSTANT_WIND
  double WindVelocitySNIa;
  double EtaSNIa;
#endif
#ifdef DENSITY_WEIGHTED_VALUE
  double SNIaDensityWeightFactor;
#endif
#ifdef OSAKA_MINMAX_NEIGHBOUR_NUMBER
  int MinNumNgbSNIa;
  int MaxNumNgbSNIa;
#endif
#endif

#ifdef OSAKA_AGB
  int DesNumNgbAGB;
  int AGBEventNumber;
  double AGBFBVelocity;
#endif

#ifdef OSAKA_METAL_FLOOR
  double SolarMetallicity;
  double MetallicityFloorCoefficient;
#endif
#endif

#ifdef SFR		// star formation and feedback sector //
#ifdef H2REGSF
  double OTUVThresh;
  double SIGMA_NORM;
  double GInternal;
  double MetalFloor;
#endif
  double CritOverDensity;
  double CritPhysDensity;
  double OverDensThresh;
  double PhysDensThresh;
  double EgySpecSN;
  double FactorSN;
  double EgySpecCold;
  double FactorEVP;
  double FeedbackEnergy;
  double TempSupernova;
  double TempClouds;
  double MaxSfrTimescale;
  double WindEfficiency;
  double WindEnergyFraction;
  double WindFreeTravelLength;
  double WindFreeTravelDensFac;
  double FactorForSofterEQS;
#ifdef RESCALEPHYSDENSTHRESH
  double ReScalePhysDensThresh;
#endif
  //#if defined(SCHAYE_PRESSURE_MULTI) || defined(H2REGSF)
#ifdef SCHAYE_PRESSURE_MULTI
  double SigmaThresh;
  double GammaPoly;
#endif
#if defined(VWINDS) && defined(GROUPFINDER)
  double totsfrrate;
  double prev_totsfrrate;
  double init_totsfrrate;
  double total_sum_mass_stars;
  double prev_total_sum_mass_stars;
  double init_total_sum_mass_stars;
#endif
#endif

#ifdef DARKENERGY
  double DarkEnergyParam;	// fixed w for equation of state //
#ifdef TIMEDEPDE
  char DarkEnergyFile[100];	// tabelized w for equation of state //
#ifdef TIMEDEPGRAV
  double Gini;
#endif
#endif
#endif

#ifdef RESCALEVINI
  double VelIniScale;		// Scale the initial velocities by this amount //
#endif

#ifdef TIME_DEP_ART_VISC
  double ViscSource0;		// Given sourceterm in viscosity evolution //
  double DecayLength;		// Number of h for the viscosity decay //
  double ViscSource;		// Reduced sourceterm in viscosity evolution //
  double DecayTime;		// Calculated decaytimescale //
  double AlphaMin;		// Minimum of allowed viscosity parameter //
#endif

#ifdef CONDUCTION
  double ConductionCoeff;         // Thermal Conductivity //
#ifdef CONDUCTION_SATURATION
  double ElectronFreePathFactor;  // Factor to get electron mean free path //
#endif
#endif

#ifdef MAGNETIC
#ifdef BINISET
  double BiniX, BiniY, BiniZ;	// Initial values for B //
#endif
#ifdef BSMOOTH
  int BSmoothInt;
  double BSmoothFrac;
  int MainTimestepCounts;
#endif
#ifdef MAGNETIC_DISSIPATION
  double ArtMagDispConst;	// Sets the parameter \f$\alpha\f$ of the artificial magnetic disipation //
#ifdef TIME_DEP_MAGN_DISP
  double ArtMagDispMin;
  double ArtMagDispSource;
  double ArtMagDispTime;
#endif
#endif
#ifdef DIVBCLEANING_DEDNER
  double DivBcleanParabolicSigma;
  double DivBcleanHyperbolicSigma;
#endif
#ifdef MAGNETIC_DIFFUSION
  double MagneticEta;
#endif
#endif

#ifdef BLACK_HOLES
  double TimeNextBlackHoleCheck;
  double TimeBetBlackHoleSearch;
  double BlackHoleAccretionFactor; // Fraction of BH bondi accretion rate //
  double BlackHoleFeedbackFactor;  // Fraction of the black luminosity feed into thermal feedback //
  double SeedBlackHoleMass;        // Seed black hole mass //
  double MinFoFMassForNewSeed;     // Halo mass required before new seed is put in //
  double BlackHoleNgbFactor;       // Factor by which the normal SPH neighbour should be increased/decreased //
  double BlackHoleActiveTime;
  double BlackHoleEddingtonFactor; //! Factor above Eddington //
#ifdef FOF
  double massDMpart;
#endif
#ifdef MODIFIEDBONDI
  double BlackHoleRefDensity;
  double BlackHoleRefSoundspeed;
#endif
#endif

#ifdef COSMIC_RAYS
  double CR_Alpha;		// Cosmic ray spectral index [2..3] //
  double CR_SNEff;		// SN injection efficiency [0..1] //
  double CR_SNAlpha;		// SN injection spectral index [2..3] //
  int bDebugFlag;		// enables debug outputs after triggered //
#if defined(CR_DIFFUSION) || defined (CR_DIFFUSION_GREEN)
  double CR_DiffusionCoeff;          // (temporary) fixed value for CR diffusivity //
  double CR_DiffusionDensScaling;    // grade of density dependence of diffusivity //
  double CR_DiffusionDensZero;       // Reference point density for diffusivity //
  double CR_DiffusionEntropyScaling; // grade of specific energy dependence of diffusivity //
  double CR_DiffusionEntropyZero;    // Reference Entropic function for diffusivity //
  double CR_DiffusionTimeScale;	     // Parameter for Diffusion Timestep Criterion //
  double TimeOfLastDiffusion;
#endif				     // CR_DIFFUSION //
#if defined(CR_SHOCK)
#if (CR_SHOCK == 1)
  double CR_ShockAlpha;		// spectral index to be used in shock injection //
#else
  double CR_ShockCutoff;	// Cutoff factor x_inj for CR accel //
#endif
  double CR_ShockEfficiency;	// energy fraction of shock energy fed into CR //
#endif				// CR_SHOCK //

#ifdef FIX_QINJ
  double Shock_Fix_Qinj;	// inject only CRps with threshold cutoff Shock_Fix_Qinj //
#endif
#endif                          // COSMIC_RAYS //

#ifdef MACHNUM
  double Shock_Length;            // length scale on which the shock is smoothed out //
  double Shock_DeltaDecayTimeMax; // maximum time interval (Dloga) for which the Mach number is kept at its maximum //
#endif

#ifdef REIONIZATION
  int not_yet_reionized;          // flag that makes sure that there is only one reionization //
#endif

#ifdef BUBBLES
  double BubbleDistance;
  double BubbleRadius;
  double BubbleTimeInterval;
  double BubbleEnergy;
  double TimeOfNextBubble;
  double FirstBubbleRedshift;
#ifdef FOF
  int BiggestGroupLen;
  float BiggestGroupCM[3];
  double BiggestGroupMass;
#endif
#endif

#if defined(MULTI_BUBBLES) && defined(FOF)
#ifndef BLACK_HOLES
  double MinFoFMassForNewSeed; // Halo mass required before new seed is put in //
  double massDMpart;
#endif
  double BubbleDistance;
  double BubbleRadius;
  double BubbleTimeInterval;
  double BubbleEnergy;
  double TimeOfNextBubble;
  double ClusterMass200;
  double FirstBubbleRedshift;
#endif

#ifdef NAVIERSTOKES
  double NavierStokes_ShearViscosity;
  double FractionSpitzerViscosity;
  double ShearViscosityTemperature;
#endif
#ifdef NAVIERSTOKES_BULK
  double NavierStokes_BulkViscosity;
#endif
#ifdef VISCOSITY_SATURATION
  double IonMeanFreePath;
#endif

#ifdef RADTRANSFER
  MyFloat kappaMean;
  MyFloat JMean;
  MyFloat RadIMean;
  MyDouble Residue;
  float TotVol;
#endif
}All;

//! This structure holds all the information that is stored for each particle of the simulation. //
extern struct particle_data
{
  MyDouble Pos[3];   // particle position at its current time //
  MyDouble Vel[3];   // particle velocity at its current time //
  MyDouble Mass;     // particle mass //
  MyIDType ID;
  
#ifdef OSAKA
  int FBflagESFB;
  int FBflagSNII;
  int FBflagSNIa;
  int FBflagAGB;
  double Metallicity;
  double MetallicitySmoothed;
  double InitialStarMass;
  double FormationTime;
  double Hsml;
  double Density;
  double DhsmlDensityFactor;
  double Pressure;
  double ESFBNgbNum;
  double SNIINgbNum;
  double SNIaNgbNum;
  double AGBNgbNum;
  double ESFBNgbMass;
  double SNIINgbMass;
  double SNIaNgbMass;
  double AGBNgbMass;
  double RHII;
  double Rshock;
  double TimeSNII;
  double TimeSNIa;
  double ESFBDepositedTime;
  double SNIIExplosionTime;
  double SNIaExplosionTime;
  double AGBExplosionTime;
  double SNIIStartTime;
  double SNIIEndTime;
  double SNIaStartTime;
  double SNIaEndTime;
  double AGBStartTime;
  double AGBEndTime;
#ifdef OSAKA_CELIB
  double Metals[N_YIELD_ELEMENT_CELIB];
#ifdef OSAKA_METAL_ELEMENT_TRACE_SNII
  double Metals_SNII[N_YIELD_ELEMENT_CELIB];
#endif
#ifdef OSAKA_METAL_ELEMENT_TRACE_SNIA
  double Metals_SNIa[N_YIELD_ELEMENT_CELIB];
#endif
#ifdef OSAKA_METAL_ELEMENT_TRACE_AGB
  double Metals_AGB[N_YIELD_ELEMENT_CELIB];
#endif
#ifdef OSAKA_METALSMOOTHING
  double MetalsSmoothed[N_YIELD_ELEMENT_CELIB];
#endif
#endif
#ifdef OSAKA_VELOCITY_COOLING_STOP
  double DMNgbNum;
#endif
#endif

  union
  {
    MyFloat       GravAccel[3];	       // particle acceleration due to gravity //
    MyLongDouble dGravAccel[3];
  } g;
  
#ifdef PMGRID
  MyFloat GravPM[3];                   // particle acceleration due to long-range PM gravity force //
#endif

#ifdef FORCETEST
  MyFloat GravAccelDirect[3];          // particle acceleration calculated by direct summation //
#endif

#if defined(EVALPOTENTIAL) || defined(COMPUTE_POTENTIAL_ENERGY) || defined(OUTPUTPOTENTIAL)
  union
  {
    MyFloat       Potential;           // gravitational potential //
    MyLongDouble dPotential;
  } p;
#endif

#if defined(DISTORTIONTENSOR) || defined(OUTPUT_TIDALTENSOR)
  union
  {
    MyFloat       tidal_tensor[6];      // tidal tensor (=second derivatives of grav. potential)// 
    MyLongDouble dtidal_tensor[6];
  } tite;
#endif

  MyFloat OldAcc;                       // magnitude of old gravitational force. Used in relative opening criterion //
#ifdef DISTORTIONTENSOR
   MyDouble distortion_tensor[9];       // Distortion tensor for that particle//
   MyDouble distortion_tensor_vel[9];   // Distortion tensor 'velocity' for that particle//   
#endif

#if defined(EVALPOTENTIAL) && defined(PMGRID)
  MyFloat PM_Potential;
#endif

#ifdef STELLARAGE
  MyFloat StellarAge;		// formation time of star particle //
#endif

#ifdef METALS
  MyFloat Metallicity;		// metallicity of gas or star particle //
#ifdef METALSMOOTHING
  MyFloat MetallicitySmoothed;
#endif
#endif

#if defined (BLACK_HOLES)
  MyFloat Hsml;
  
  union
  {
    MyFloat       NumNgb;
    MyLongDouble dNumNgb;
  } n;
#endif

#ifdef BLACK_HOLES
  int SwallowID;
  MyFloat BH_Mass;
  MyFloat BH_Mdot;
  
  union
  {
    MyFloat BH_Density;
    MyLongDouble dBH_Density;
  } b1;
  
  union
  {
    MyFloat BH_Entropy;
    MyLongDouble dBH_Entropy;
  } b2;
  
  union
  {
    MyFloat BH_SurroundingGasVel[3];
    MyLongDouble dBH_SurroundingGasVel[3];
  } b3;
  
  union
  {
    MyFloat BH_accreted_Mass;
    MyLongDouble dBH_accreted_Mass;
  } b4;
  
  union
  {
    MyFloat BH_accreted_BHMass;
    MyLongDouble dBH_accreted_BHMass;
  } b5;
  
  union
  {
    MyFloat BH_accreted_momentum[3];
    MyLongDouble dBH_accreted_momentum[3];
  } b6;
  
#ifdef REPOSITION_ON_POTMIN
  MyFloat BH_MinPotPos[3];
  MyFloat BH_MinPot;
#endif
#ifdef BH_KINETICFEEDBACK
  MyFloat ActiveTime;
  MyFloat ActiveEnergy;
#endif
#endif

#ifdef SUBFIND
  int GrNr;
  int SubNr;
  int DM_NumNgb;
  int targettask, origintask, submark;
  MyFloat DM_Hsml;
  
  union
  {
    MyFloat DM_Density;	
    MyFloat DM_Potential;
  } u;
  
  union
  {
    MyFloat DM_VelDisp;
    MyFloat DM_BindingEnergy;
  } v;
#endif

#ifdef GROUPFINDER
  int GroupIn;                         // Whether this particle is considered as Group or not //
  MyFloat gHsml;                       // current smoothing length //
  MyIDType GroupID;
  MyFloat GroupSfr;
  MyFloat PartSfr;
  MyFloat GroupSize;                    // maximum extend of the group from the densiest particle //
  MyFloat GroupMassGas;
  MyFloat GroupMassStar;
  
  union
  {
    MyFloat       Density;             // current baryonic mass density of particle //
    MyLongDouble dDensity;
  } gd;
  
  union
  {
    MyFloat       NumNgb;
    MyLongDouble dNumNgb;
  } gn;
  
  union
  {
    MyFloat       DhsmlDensityFactor;   // correction factor needed in entropy formulation of SPH //
    MyLongDouble dDhsmlDensityFactor;
  } gh;
#endif

#if defined(ORDER_SNAPSHOTS_BY_ID) && !defined(SUBFIND)
  int     GrNr;
  int     SubNr;
#endif

  float GravCost;		// weight factor used for balancing the work-load //
  
  int Ti_begstep;		// marks start of current timestep of particle on integer timeline //
  int Ti_current;		// current time of the particle //
  
  short int Type;		// flags particle type.  0=gas, 1=halo, 2=disk, 3=bulge, 4=stars, 5=bndry //
  short int TimeBin;
  
#ifdef WAKEUP
  int dt_step;
#endif
}*P, *DomainPartBuf;
// holds particle data on local processor //
// buffer for particle data used in domain decomposition //

// the following struture holds data that is stored for each SPH particle in addition to the collisionless variables. //
extern struct sph_particle_data
{
  MyDouble Entropy;		// current value of entropy (actually entropic function) of particle //
#ifdef DENSITY_INDEPENDENT_SPH
  MyFloat EntropyPred;
#endif
  MyFloat  Pressure;		// current pressure //
  MyFloat  VelPred[3];		// predicted SPH particle velocity at the current time //
#ifdef ALTERNATIVE_VISCOUS_TIMESTEP
  MyFloat MinViscousDt;
#else
  MyFloat MaxSignalVel;           // maximum signal velocity //
#endif

#ifdef DT_COOL
  MyFloat t_cool;
#endif

#ifdef DENSITY_INDEPENDENT_SPH
  MyDouble EgyWtDensity;          // 'effective' rho to use in hydro equations //
  MyDouble DhsmlEgyDensityFactor; // predicted entropy variable //
  MyDouble EntVarPred;            // correction factor for density-independent entropy formulation //
#endif

  union
  {
    MyFloat       Density;		// current baryonic mass density of particle //
    MyLongDouble dDensity;
  } d;
  
  union
  {
    MyFloat       DtEntropy;		// rate of change of entropy //
    MyLongDouble dDtEntropy;
  } e;
  
  union
  {
    MyFloat       HydroAccel[3];	// acceleration due to hydrodynamical force //
    MyLongDouble dHydroAccel[3];
  } a;
  
  union
  {
    MyFloat       DhsmlDensityFactor;	// correction factor needed in entropy formulation of SPH //
    MyLongDouble dDhsmlDensityFactor;
  } h;
  
  union
  {
    MyFloat       DivVel;		// local velocity divergence //
    MyLongDouble dDivVel;
  } v;
  
#ifndef NAVIERSTOKES
  union
  {
    MyFloat CurlVel;     	        // local velocity curl //
    MyFloat       Rot[3];		// local velocity curl //
    MyLongDouble dRot[3];
  } r;
#else
  union
  {
    MyFloat DV[3][3];
    struct
    {
      MyFloat DivVel;
      MyFloat CurlVel;
      MyFloat StressDiag[3];
      MyFloat StressOffDiag[3];
#ifdef NAVIERSTOKES_BULK
      MyFloat StressBulk;
#endif
    } s;
  } u;
#endif

#if !defined(BLACK_HOLES)
  MyFloat Hsml;	 // current smoothing length //
  MyFloat Left;	 // lower bound in iterative smoothing length search //
  MyFloat Right; // upper bound in iterative smoothing length search //
  
  union
  {
    MyFloat       NumNgb;
    MyLongDouble dNumNgb;
  } n;
#endif

#if defined(BH_THERMALFEEDBACK) || defined(BH_KINETICFEEDBACK)
  union
  {
    MyFloat       Injected_BH_Energy;
    MyLongDouble dInjected_BH_Energy;
  } i;
#endif

#ifdef COOLING
  MyFloat Ne;  // electron fraction, expressed as local electron number density normalized to the hydrogen number density. Gives indirectly ionization state and mean molecular weight. //
#endif

#if defined(SFR) || defined(OSAKA_SFR)
  MyFloat Sfr;
#endif

#ifdef WINDS
  MyFloat DelayTime;		// remaining maximum decoupling time of wind particle //
#ifdef OUT_WINDS_INFO
  MyFloat Wind_FormTime[2];     // 0 is for wind start time and 1 is for wind end time. < If ongoing wind, Wind_time[0] = Wind_time[1]. < Initially Wind_time[0] = Wind_time[1] = -1 //
#endif
#endif

#ifdef OSAKA
  int nStarSpawn;
  int flagCoolingSNII;
  int flagCoolingSNIa;
  double Rshock;
  double WindVelocity;
  double timeSNII;
  double timeSNIa;
  double timeNoCoolingSNII;
  double timeNoCoolingSNIa;
  double KineticEnergySNII;
  double ThermalEnergySNII;
  double SNIIEnergyStorage;
  double KineticEnergySNIa;
  double ThermalEnergySNIa;
  double SNIaEnergyStorage;
  double ThermalEnergyESFB;
  double StarSNIIDensity;
  double StarSNIaDensity;
  MyIDType IDStar;
  double PosStar[3];
  double MassStar;
  double VelDisp;
  double DMDensity;
#if defined(OSAKA_MECHANICAL_FB) || defined(OSAKA_GEODESIC_DOME_WEIGHT)
  double MomentumSNII[3];
  double MomentumSNIa[3];
#endif
#endif

#ifdef MAGNETIC
  MyFloat B[3], BPred[3];
  MyFloat DtB[3];
#if defined(TRACEDIVB) || defined(TIME_DEP_MAGN_DISP)
  MyFloat divB;
#endif
#if defined(BSMOOTH) || defined(BFROMROTA)
  MyFloat BSmooth[3];
  MyFloat DensityNorm;
#endif
#ifdef TIME_DEP_MAGN_DISP
  MyFloat Balpha, DtBalpha;
#endif
#ifdef DIVBCLEANING_DEDNER
  MyFloat Phi, PhiPred, DtPhi;
#ifdef SMOOTH_PHI
  MyFloat SmoothPhi;
#endif
#endif
#if defined(MAGNETIC_DIFFUSION) || defined(ROT_IN_MAG_DIS)
  MyFloat RotB[3];
#ifdef SMOOTH_ROTB
  MyFloat SmoothedRotB[3];
#endif
#endif
#endif

#ifdef TIME_DEP_ART_VISC
  MyFloat alpha, Dtalpha;
#endif

#ifdef NS_TIMESTEP
  MyFloat ViscEntropyChange;
#endif

#ifdef CONDUCTION
  MyFloat CondEnergyChange;
  MyFloat SmoothedEntr;
#ifdef CONDUCTION_SATURATION
  MyFloat GradEntr[3];
#endif
#ifdef OUTPUTCOOLRATE
  MyFloat CondRate;
#endif
#endif

#ifdef H2REGSF
  MyFloat GradRho[3];
  MyFloat fH2;
  MyFloat HIgrad;
  MyFloat HIrho;
  MyFloat nh0;
  MyFloat XHI;
  MyFloat Sigma;
#endif

#ifdef MHM
  MyFloat FeedbackEnergy;
#endif

#ifdef COSMIC_RAYS
  MyFloat CR_C0;      // Cosmic ray amplitude adiabatic invariable //
  MyFloat CR_q0;      // Cosmic ray cutoff adiabatic invariable //
  MyFloat CR_E0;      // Specific Energy at Rho0 //
  MyFloat CR_n0;      // baryon fraction in cosmic rays //
  MyFloat CR_DeltaE;  // Specific Energy growth during timestep //
  MyFloat CR_DeltaN;  // baryon fraction growth during timestep //
#ifdef MACHNUM
  MyFloat CR_Gamma0;
#endif

#ifdef CR_OUTPUT_INJECTION
  MyFloat CR_Specific_SupernovaHeatingRate;
#endif

#ifdef CR_DIFFUSION
  MyFloat CR_SmoothE0;        // SPH-smoothed interpolant of diffusion term //
  MyFloat CR_Smoothn0;        // SPH-smoothed interpolant for diffusion term //
#endif                        // CR_DIFFUSION //
#ifdef CR_DIFFUSION_GREEN
  MyFloat CR_Kappa;
  MyFloat CR_Kappa_egy;
  MyFloat CR_WeightSum;
  MyFloat CR_WeightSum_egy;
#endif
#endif                        // COSMIC_RAYS //

#ifdef MACHNUM
  MyFloat Shock_MachNumber;	    // Mach number //
  MyFloat Shock_DecayTime;	    // Shock decay time //
#ifdef COSMIC_RAYS
  MyFloat Shock_DensityJump;	    // Density jump at the shock //
  MyFloat Shock_EnergyJump;	    // Energy jump at the shock //
  MyFloat PreShock_PhysicalDensity; // Specific energy in the preshock regime //
  MyFloat PreShock_PhysicalEnergy;  // Density in the preshock regime //
  MyFloat PreShock_XCR;		    // XCR = PCR / Pth in the preshock regime //
#endif
#ifdef MACHSTATISTIC
  MyFloat Shock_DtEnergy;           // Change of thermal specific energy at Shocks //
#endif
#endif                              // Mach number estimate //

#if defined(OSAKA_COOLING) && !defined(CHEMISTRY)
  // GrackleChemistry = 1 //
  MyFloat HI;
  MyFloat HII;
  MyFloat HeI;
  MyFloat HeII;
  MyFloat HeIII;
  // GrackleChemistry = 2 //
  MyFloat HM;
  MyFloat H2I;
  MyFloat H2II;
  // GrackleChemistry = 3 //
  MyFloat DI;
  MyFloat DII;
  MyFloat HDI;
#elif CHEMISTRY
  MyFloat elec;
  MyFloat HI;
  MyFloat HII;
  MyFloat HeI;
  MyFloat HeII;
  MyFloat HeIII;
  MyFloat H2I;
  MyFloat H2II;
  MyFloat HM;
  MyFloat Gamma;
  MyFloat t_elec, t_cool;
#endif

#ifdef RADTRANSFER
  MyFloat ET[6];                // eddington tensor - symmetric -> only 6 elements needed //
  MyFloat RadJ;                 // mean intensity //
  MyFloat P1, D1;               // coefficients in GAuss-Seidel method //
  MyFloat Je;                   // emissivity //
  MyFloat nH;
  MyFloat x;
  MyFloat nHI;                  // HI number density //
  MyFloat nHII;                 // HII number density //
  MyFloat n_elec;               // electron number density //
#endif

#ifdef WAKEUP
  short int wakeup;
#endif
}*SphP, *DomainSphBuf;
// holds SPH particle data on local processor //
// buffer for SPH particle data in domain decomposition //

extern peanokey *DomainKeyBuf;

// global state of system //
extern struct state_of_system
{
  double Mass,
    EnergyKin,
    EnergyPot,
    EnergyInt,
    EnergyTot,
    Momentum[4],
    AngMomentum[4],
    CenterOfMass[4],
    MassComp[6],
    EnergyKinComp[6],
    EnergyPotComp[6],
    EnergyIntComp[6], EnergyTotComp[6], MomentumComp[6][4], AngMomentumComp[6][4], CenterOfMassComp[6][4];
}SysState, SysStateAtStart, SysStateAtEnd;

// Various structures for communication during the gravity computation. //
extern struct data_index
{
  int Task;
  int Index;
  int IndexGet;
}*DataIndexTable; // the particles to be exported are grouped by task-number. This table allows the results to be disentangled again and to be assigned to the correct particle //

extern struct data_nodelist
{
  int NodeList[NODELISTLENGTH];
}*DataNodeList;

extern struct gravdata_in
{
  MyFloat Pos[3];
#ifdef UNEQUALSOFTENINGS
  int Type;
#ifdef ADAPTIVE_GRAVSOFT_FORGAS
  MyFloat Soft;
#endif
#endif
  MyFloat OldAcc;
  int NodeList[NODELISTLENGTH];
}*GravDataIn, *GravDataGet;
// holds particle data to be exported to other processors //
// holds particle data imported from other processors //

extern struct gravdata_out
{
  MyLongDouble Acc[3];
#ifdef EVALPOTENTIAL
  MyLongDouble Potential;
#endif
#if defined(DISTORTIONTENSOR) || defined(OUTPUT_TIDALTENSOR)
  MyLongDouble tidal_tensor[6];
#endif
  int Ninteractions;
}*GravDataResult, *GravDataOut;
// holds the partial results computed for imported particles. Note: We use GravDataResult = GravDataGet, such that the result replaces the imported data //
// holds partial results received from other processors. This will overwrite the GravDataIn array //

extern struct potdata_out
{
  MyLongDouble Potential;
}*PotDataResult, *PotDataOut;
// holds the partial results computed for imported particles. Note: We use GravDataResult = GravDataGet, such that the result replaces the imported data //
// holds partial results received from other processors. This will overwrite the GravDataIn array //

// Header for the standard file format. //
extern struct io_header
{
  int npart[6];			      // number of particles of each type in this file //
  double mass[6];		      // mass of particles of each type. If 0, then the masses are explicitly stored in the mass-block of the snapshot file, otherwise they are omitted //
  double time;			      // time of snapshot file //
  double redshift;		      // redshift of snapshot file //
  int flag_sfr;			      // flags whether the simulation was including star formation //
  int flag_feedback;		      // flags whether feedback was included (obsolete) //
  unsigned int npartTotal[6];	      // total number of particles of each type in this snapshot. This can be different from npart if one is dealing with a multi-file snapshot. //
  int flag_cooling;		      // flags whether cooling was included  //
  int num_files;		      // number of files in multi-file snapshot //
  double BoxSize;		      // box-size of simulation in case periodic boundaries were used //
  double Omega0;		      // matter density in units of critical density //
  double OmegaLambda;		      // cosmological constant parameter //
  double HubbleParam;		      // Hubble parameter in units of 100 km/sec/Mpc //
  int flag_stellarage;		      // flags whether the file contains formation times of star particles //
  int flag_metals;		      // flags whether the file contains metallicity values for gas and star particles //
  unsigned int npartTotalHighWord[6]; // High word of the total number of particles of each type //
  int flag_entropy_instead_u;	      // flags that IC-file contains entropy instead of u //
  int flag_doubleprecision;	      // flags that snapshot contains double-precision instead of single precision //
  int flag_potential;
  int flag_fH2;
  char fill[48];		      // fills to 256 Bytes //
}header; // holds header for snapshot files //

enum iofields
{ IO_POS,
  IO_VEL,
  IO_ID,
  IO_MASS,
  IO_U,
  IO_TEMPERATURE,
  IO_RHO,
  IO_PRESSURE,
  IO_NE,
  IO_HSML,
  IO_SFR,
  IO_AGE,
  IO_Z,
  IO_Z_SMOOTH,
  IO_BHMASS,
  IO_BHMDOT,
  IO_POT,
  IO_ACCEL,
  IO_CR_C0,
  IO_CR_Q0,
  IO_CR_P0,
  IO_CR_E0,
  IO_CR_n0,
  IO_CR_ThermalizationTime,
  IO_CR_DissipationTime,
  IO_ELECT,
  IO_HI,
  IO_HII,
  IO_HeI,
  IO_HeII,
  IO_HeIII,
  IO_H2I,
  IO_H2II,
  IO_HM,
  IO_DI,
  IO_DII,
  IO_HDI,
  IO_DTENTR,
  IO_STRESSDIAG,
  IO_STRESSOFFDIAG,
  IO_STRESSBULK,
  IO_SHEARCOEFF,
  IO_TSTP,
  IO_BFLD,
  IO_BSMTH,
  IO_DBDT,
  IO_DIVB,
  IO_ABVC,
  IO_AMDC,
  IO_PHI,
  IO_ROTB,
  IO_SROTB,
  IO_COOLRATE,
  IO_CONDRATE,
  IO_DENN,
  IO_MACH,
  IO_DTENERGY,
  IO_PRESHOCK_DENSITY,
  IO_PRESHOCK_ENERGY,
  IO_PRESHOCK_XCR,
  IO_DENSITY_JUMP,
  IO_ENERGY_JUMP,
  IO_CRINJECT,
  IO_TIDALTENSOR,
  IO_DISTORTIONTENSOR,
  IO_ENERGY_WEIGHTED_RHO,
  IO_CELIB_HYDROGEN,
  IO_CELIB_HELIUM,
  IO_CELIB_CARBON,
  IO_CELIB_NITROGEN,
  IO_CELIB_OXYGEN,
  IO_CELIB_NEON,
  IO_CELIB_MAGNESIUM,
  IO_CELIB_SILICON,
  IO_CELIB_SULFUR,
  IO_CELIB_CALCIUM,
  IO_CELIB_IRON,
  IO_CELIB_NICKEL,
  IO_CELIB_HYDROGEN_SMOOTHED,
  IO_CELIB_HELIUM_SMOOTHED,
  IO_CELIB_CARBON_SMOOTHED,
  IO_CELIB_NITROGEN_SMOOTHED,
  IO_CELIB_OXYGEN_SMOOTHED,
  IO_CELIB_NEON_SMOOTHED,
  IO_CELIB_MAGNESIUM_SMOOTHED,
  IO_CELIB_SILICON_SMOOTHED,
  IO_CELIB_SULFUR_SMOOTHED,
  IO_CELIB_CALCIUM_SMOOTHED,
  IO_CELIB_IRON_SMOOTHED,
  IO_CELIB_NICKEL_SMOOTHED,
  IO_CELIB_SNII_HYDROGEN,
  IO_CELIB_SNII_HELIUM,
  IO_CELIB_SNII_CARBON,
  IO_CELIB_SNII_NITROGEN,
  IO_CELIB_SNII_OXYGEN,
  IO_CELIB_SNII_NEON,
  IO_CELIB_SNII_MAGNESIUM,
  IO_CELIB_SNII_SILICON,
  IO_CELIB_SNII_SULFUR,
  IO_CELIB_SNII_CALCIUM,
  IO_CELIB_SNII_IRON,
  IO_CELIB_SNII_NICKEL,
  IO_CELIB_SNIA_HYDROGEN,
  IO_CELIB_SNIA_HELIUM,
  IO_CELIB_SNIA_CARBON,
  IO_CELIB_SNIA_NITROGEN,
  IO_CELIB_SNIA_OXYGEN,
  IO_CELIB_SNIA_NEON,
  IO_CELIB_SNIA_MAGNESIUM,
  IO_CELIB_SNIA_SILICON,
  IO_CELIB_SNIA_SULFUR,
  IO_CELIB_SNIA_CALCIUM,
  IO_CELIB_SNIA_IRON,
  IO_CELIB_SNIA_NICKEL,
  IO_CELIB_AGB_HYDROGEN,
  IO_CELIB_AGB_HELIUM,
  IO_CELIB_AGB_CARBON,
  IO_CELIB_AGB_NITROGEN,
  IO_CELIB_AGB_OXYGEN,
  IO_CELIB_AGB_NEON,
  IO_CELIB_AGB_MAGNESIUM,
  IO_CELIB_AGB_SILICON,
  IO_CELIB_AGB_SULFUR,
  IO_CELIB_AGB_CALCIUM,
  IO_CELIB_AGB_IRON,
  IO_CELIB_AGB_NICKEL,
  IO_METALLICITY,
  IO_METALLICITYSMOOTHED,
  IO_STELLAR_FORMATION_TIME,
  IO_NSTARSPAWN,
  IO_INITIAL_STAR_MASS,
  IO_STAR_ESFBNGBNUM,
  IO_STAR_SNIINGBNUM,
  IO_STAR_SNIANGBNUM,
  IO_STAR_AGBNGBNUM,
  IO_STAR_ESFB_FEEDBACK_FLAG,
  IO_STAR_ESFB_DEPOSITED_TIME,
  IO_STAR_SNII_FEEDBACK_FLAG,
  IO_STAR_SNII_EXPLOSION_TIME,
  IO_STAR_SNII_START_TIME,
  IO_STAR_SNII_END_TIME,
  IO_STAR_SNII_TIME,
  IO_GAS_SNII_COOLING_FLAG,
  IO_GAS_SNII_ENDTIME,
  IO_GAS_SNII_NOCOOLING_TIME,
  IO_GAS_SNII_ENERGY_STORAGE,
  IO_STAR_SNIA_FEEDBACK_FLAG,
  IO_STAR_SNIA_EXPLOSION_TIME,
  IO_STAR_SNIA_START_TIME,
  IO_STAR_SNIA_END_TIME,
  IO_STAR_SNIA_TIME,
  IO_GAS_SNIA_COOLING_FLAG,
  IO_GAS_SNIA_ENDTIME,
  IO_GAS_SNIA_NOCOOLING_TIME,
  IO_GAS_SNIA_ENERGY_STORAGE,
  IO_RSHOCK,
  IO_RHII,
  IO_WIND_VELOCITY,
  IO_STAR_AGB_FEEDBACK_FLAG,
  IO_STAR_AGB_EXPLOSION_TIME,
  IO_STAR_AGB_START_TIME,
  IO_STAR_AGB_END_TIME,

  IO_LASTENTRY			// This should be kept - it signals the end of the list //
};

//                    //
// Variables for Tree //
// ------------------ //
extern struct NODE
{
  MyFloat len;			// sidelength of treenode //
  MyFloat center[3];		// geometrical center of node //

#ifdef RADTRANSFER
  MyFloat stellar_mass;         // mass in stars in the node//
  MyFloat stellar_s[3];         // enter of mass for the stars in the node//
#endif

#ifdef ADAPTIVE_GRAVSOFT_FORGAS
  MyFloat maxsoft;		// hold the maximum gravitational softening of particles in the node if the ADAPTIVE_GRAVSOFT_FORGAS option is selected //
#endif
  union
  {
    int suns[8];		// temporary pointers to daughter nodes //
    struct
    {
      MyFloat s[3];		// center of mass of node //
      MyFloat mass;		// mass of node //
      unsigned int bitflags;	// flags certain node properties //
      int sibling;		// this gives the next node in the walk in case the current node can be used //
      int nextnode;		// this gives the next node in case the current node needs to be opened //
      int father;		// this gives the parent node of each node (or -1 if we have the root node) //
    }
    d;
  }
  u;
#ifdef SCALARFIELD
  MyFloat s_dm[3];
  MyFloat mass_dm;
#endif
  int Ti_current;
}*Nodes_base, *Nodes;
// points to the actual memory allocted for the nodes //
// this is a pointer used to access the nodes which is shifted such that Nodes[All.MaxPart] gives the first allocated node //

extern struct extNODE
{
  MyLongDouble dp[3];
#ifdef SCALARFIELD
  MyLongDouble dp_dm[3];
  MyFloat vs_dm[3];
#endif
#ifdef FLTROUNDOFFREDUCTION
  MyFloat s_base[3];
  MyFloat len_base;
#ifdef SCALARFIELD
  MyFloat s_dm_base[3];
#endif
#endif
  MyFloat vs[3];
  MyFloat vmax;
  MyFloat divVmax;
  MyFloat hmax;			// maximum SPH smoothing length in node. Only used for gas particles //
  int Ti_lastkicked;
  int Flag;
}*Extnodes, *Extnodes_base;

extern int MaxNodes;      // maximum allowed number of internal nodes //
extern int Numnodestree;  // number of (internal) nodes in each tree //
extern int *Nextnode;     // gives next node in tree walk  (nodes array) //
extern int *Father;       // gives parent node in tree (Prenodes array) //

#ifdef STATICNFW
extern double Rs, R200;
extern double Dc;
extern double RhoCrit, V200;
extern double fac;
#endif

#ifdef CHEMISTRY
// ----- chemistry part ------- //
#define H_number_fraction 0.76
#define He_number_fraction 0.06
// ----- Tables ------- //
extern double T[N_T], J0_nu[N_nu], J_nu[N_nu], nu[N_nu];
extern double k1a[N_T], k2a[N_T], k3a[N_T], k4a[N_T], k5a[N_T], k6a[N_T], k7a[N_T], k8a[N_T], k9a[N_T], k10a[N_T], k11a[N_T];
extern double k12a[N_T], k13a[N_T], k14a[N_T], k15a[N_T], k16a[N_T], k17a[N_T], k18a[N_T], k19a[N_T], k20a[N_T], k21a[N_T];
extern double ciHIa[N_T], ciHeIa[N_T], ciHeIIa[N_T], ciHeISa[N_T], reHIIa[N_T], brema[N_T];
extern double ceHIa[N_T], ceHeIa[N_T], ceHeIIa[N_T], reHeII1a[N_T], reHeII2a[N_T], reHeIIIa[N_T];
// cross-sections //
#ifdef RADIATION
extern double sigma24[N_nu], sigma25[N_nu], sigma26[N_nu], sigma27[N_nu], sigma28[N_nu], sigma29[N_nu], sigma30[N_nu], sigma31[N_nu];
#endif
#endif

//structures for eddington tensor//
#ifdef RADTRANSFER
struct eddingtondata_in
{
  int NodeList[NODELISTLENGTH];
  MyDouble Pos[3];
  MyFloat Hsml;
  MyFloat ET[6];
  MyFloat nHI;
}*EddingtonDataIn, *EddingtonDataGet;

struct eddingtondata_out
{
  MyFloat ET[6];
  MyFloat P1, D1;
}*EddingtonDataResult, *EddingtonDataOut;
#endif

#endif // ALLVARS_H //
