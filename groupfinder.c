#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#include "allvars.h"
#include "proto.h"

#ifdef GROUPFINDER
#include "groupfinder.h"

/* Data structures for the ghost particles */
static struct ghostdata_in
{
  MyFloat Pos[3];
  MyFloat HeadPos[3];
  MyFloat Hsml;
  MyFloat Density;
  MyFloat HeadDensity;
  MyFloat HeadHsml;
  MyFloat dist;
  int Task;
  int HeadLen;
  int NodeList[NODELISTLENGTH];
}
  *GhostDataIn, *GhostDataGet;

static struct ghostdata_out
{
  int Flag;
  int NewTask;
  int NewHead;
  MyFloat HeadPos[3];
}
  *GhostDataResult, *GhostDataOut;

/* for linking subgroups or setting group ID */
static struct grouplinker
{
  int task, head;
  int flag;
  MyFloat HeadPos[3];  
}
  *CommHead;

static struct globallinker
{
  int task, head;
  int task_local, head_local;
  int ReLink;
  MyFloat HeadPos[3];
}
  *GlobalLinker, *GlobalLinkerSend, *GlobalLinkerAll;

int *Index, *GhostIndex;
int *Head, *Next, *Tail, *Len; 
int *Group2Head;
int *CommIndex;

/* Data structures for the group finder */
static struct groupdata
{  
  MyIDType ID;
  int Task;
  int Head;
  int Task_Local;
  int Head_Local;
  int Len;
  MyFloat HeadPos[3], COM[3];
  MyFloat MassGas, MassStar;
  MyFloat MetalGas, MetalStar;
  MyFloat GroupSfr;
  MyFloat Size;
}
  *GroupData, *GroupDataIn, *GroupDataGet, *GroupDataOut;

double periodic(double );
double periodic_wrap(double );


/*
 * This is Main Routine to find subgroup and calculate the group properties
 * It is base on Star Groupfinder but  significantly modified.
 */
void groupfinder_properties(void)
{
  MyFloat *r2list, *r2list_sum;
  int *listofdifferent, *Ngblist_sum;
  int i, j, k;
  int ind, ind1, ind2;
  int totmember, totmember_all;
  int n, startnode, numngb_inbox, numngb, listindex, noffset;
  int ngrp, sendTask, recvTask, place, nexport, nimport, dummy;
  int head = 0, head_attach, head_s, head_p;
  int head1, head2, com_hsml, ss;
  int ndiff, oldhead;
  int grimport, grexport_send, grexport, grexport_max, *grexport_arr;
  int NumPart_Group, Ngroups, Ngroups_local;
  int flag, task;
  MyIDType id, id_count, idcount_array[NTask], id_offset;
  MyFloat DenFactor;
  MyFloat dist, dist_p, dist_s, dist_h, h2;
  MyFloat dens_p, dens_s;
  MyFloat HeadPos[3], rr;
  MyFloat dx, dy, dz, r2;
  MyFloat a3inv, hubble_a, time_hubble_a;


  struct data_index *GroupIndexTable;

  if(All.Time < FirstGroupingTime) 
    {
      if(ThisTask == 0)
	{
	  printf ("It is too early to start OTF grouping.\n");
	  printf ("Current Time : %g, First Grouping Time : %g\n",All.Time, FirstGroupingTime);
	}
      return;
    }

  if(ThisTask == 0) printf ("Start GroupFinder @ Time: %g  >>>>>\n",All.Time);

  if(All.ComovingIntegrationOn)
    {
      /* Factors for comoving integration of hydro */
      a3inv = 1 / (All.Time * All.Time * All.Time);
      hubble_a = hubble_function(All.Time);
      time_hubble_a = All.Time * hubble_a;
    }
  else
    {
      a3inv = time_hubble_a = hubble_a = 1;
    }

  /* Need to initialize particle data*/
  for(i = 0; i < NumPart; i++)
    {
      if(P[i].Type == 0 || P[i].Type == 4)
	P[i].GroupIn = 1;
      else 
	P[i].GroupIn = 0;

      P[i].GroupID       = -1;
      P[i].gd.Density    = 0.0;
      P[i].GroupMassGas  = 0.0;
      P[i].GroupMassStar = 0.0;
      P[i].GroupSfr      = 0.0;
      P[i].GroupSize     = 0.0;
      P[i].PartSfr       = 0.0;
      P[i].gHsml         = 0.0;
      if(P[i].Type == 0)
        {
          P[i].gHsml = SphP[i].Hsml;
          P[i].PartSfr = SphP[i].Sfr;
        }
    }

  /** Compute the baryon (gas + star) density **/
  set_hsml_guess();
  //printf("hsml_guess GO task %d\n", ThisTask);
  baryondensity();
  //printf("baryondensity GO task %d\n", ThisTask);

  /* eliminate low density gas particle from group member */
  if(PhysicalSFDensity)
    DenFactor = a3inv;
  else
    DenFactor = 1.0;

  NumPart_Group = 0;
  for(i = 0; i < NumPart; i++)
    {
      if((P[i].Type == 0) && ((P[i].gd.Density * DenFactor) <= (All.PhysDensThresh * MinimumGasDenCut)))
	P[i].GroupIn = 0;

      if(P[i].GroupIn == 1) 
	NumPart_Group++;
    }
  MPI_Barrier(MPI_COMM_WORLD);

  totmember = NumPart_Group;
  MPI_Allreduce(&totmember, &totmember_all, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  if(ThisTask == 0) printf("Total possible member of the groupfinder is %d\n",totmember_all);
  if(totmember_all < DesMinSize) 
    {
      if(ThisTask == 0) printf ("<<<<< Finish GroupFinder.\n");
      return;
    }

  /*********************************************/
  /**          Start Group Finder             **/

#ifdef USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif

  /* Allocate buffers to arrange communication for ghost particles*/
  All.BunchSize =
    (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                                             sizeof(struct ghostdata_in) + sizeof(struct ghostdata_out) +
                                             sizemax(sizeof(struct ghostdata_in), 
                                                     sizeof(struct ghostdata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));

  Ngblist = (int *) mymalloc(NumPart * sizeof(int));

  for(j=0 ; j< NTask; j++)
    {
      Send_count[j] = 0;
      Exportflag[j] = -1;
    }

  /** Start local group finder first **/
  /* For group sorting */
  Head = (int *) mymalloc(NumPart * sizeof(int));
  Next = (int *) mymalloc(NumPart * sizeof(int));
  Tail = (int *) mymalloc(NumPart * sizeof(int));
  Len  = (int *) mymalloc(NumPart * sizeof(int));
  CommHead = (struct grouplinker *) mymalloc(NumPart * sizeof(struct grouplinker));
  CommIndex = (int *) mymalloc(NumPart * sizeof(int));

  for(i = 0; i < NumPart; i++)	/* initially, there are no groups */
    {
      CommIndex[i] = -1;
      CommHead[i].flag = 0;
      CommHead[i].task = ThisTask;
      CommHead[i].head = -1;
      Head[i] = Next[i] = Tail[i] = -1;
      Len[i]  = 0;
      for(k=0; k<3; k++)
	CommHead[i].HeadPos[k] = -9999999;
    }

  /* set index for all member-to-be particles */
  Index = (int *) mymalloc(NumPart_Group * sizeof(int));
  for(i = 0, j = 0; i < NumPart; i++)
    {
      if(P[i].GroupIn != 0)
        {
          Index[j] = i;
          j++;
	  if(j > NumPart_Group)
	    {
	      printf("error on assign Index in find_subgroups\n");
	      endrun(9911);
	    }
        }
    }

  MPI_Barrier(MPI_COMM_WORLD);
  /* sort by density */
  qsort(Index, NumPart_Group, sizeof(int), compare_dens_index);

  listofdifferent = (int *) mymalloc(sizeof(int) * MAX_NGB);
  r2list = (MyFloat *) mymalloc(sizeof(MyFloat) * MAX_NGB);

  nexport = 0;
  for(i = NumPart_Group - 1 ; i >= 0; i--)
    {

      /* Here we only perform ngb search for local particles and select particles to be exported*/      
      startnode = All.MaxPart;
      numngb_inbox = ngb_treefind_baryon(&P[Index[i]].Pos[0], P[Index[i]].gHsml, Index[i], &startnode, 0, &nexport, Send_count);

      h2 = P[Index[i]].gHsml * P[Index[i]].gHsml;
      numngb = 0;

      for(n = 0; n < numngb_inbox; n++)
	{
	  j = Ngblist[n];

          dx = periodic(P[Index[i]].Pos[0] - P[j].Pos[0]);
          dy = periodic(P[Index[i]].Pos[1] - P[j].Pos[1]);
          dz = periodic(P[Index[i]].Pos[2] - P[j].Pos[2]);

	  r2 = dx * dx + dy * dy + dz * dz;

	  if(r2 <= h2 && P[j].GroupIn != 0)
	    {
	      numngb++;
	      r2list[n] = r2;
	    }
	  else
	    {
	      Ngblist[n] = Ngblist[numngb_inbox - 1];
	      numngb_inbox--;
	      n--;
	    }
	}

      sort2_flt_int(numngb, r2list - 1, Ngblist - 1);

      /* ok, let's first see how many different groups there are */

      for(k = 0, ndiff = 0; k < numngb; k++)
	{
	  ind = Ngblist[k];

	  if(ind != Index[i])
	    {
	      if(P[ind].gd.Density > P[Index[i]].gd.Density)
		{
		  if(Head[ind] >= 0)	/* neighbor is attached to a group */
		    {
		      for(j = 0; j < ndiff; j++)
			if(listofdifferent[j] == Head[ind])
			  break;

		      if(j >= ndiff)	/* a new group has been found */
			listofdifferent[ndiff++] = Head[ind];
		    }
		  else
		    {
		      printf("\nthis may not occur.\n");
		    }
		}
	    }
	}


      if(ndiff == 0)		/* this appears to be a lonely maximum -> new group */
	{
	  Head[Index[i]] = Tail[Index[i]] = Index[i];
	  Len[Index[i]] = 1;
	  Next[Index[i]] = -1;	  
	}

      if(ndiff == 1)		/* the particle is attached to exactly one group */
	{
	  head = listofdifferent[0];
	  
	  Head[Index[i]]   = head;
	  Next[Tail[head]]   = Index[i];
	  Tail[head]   = Index[i];
	  Len[head]++;
	  Next[Index[i]] = -1;
	}


      if(ndiff > 1)		/* the particle merges (at least) two groups together */
	{	  
	  for(j = 0; j < (ndiff - 1); j++)
            {

	      head_p = listofdifferent[j];
	      head_s = listofdifferent[j + 1];

	      dist_p = dist_s = 0.0;
	      dist_h = 0.0;
	      for(k = 0 ; k < 3; k++)
		{
		  dist_p += periodic(P[Index[i]].Pos[k] - P[head_p].Pos[k])
		    *periodic(P[Index[i]].Pos[k] - P[head_p].Pos[k]);
		  dist_s += periodic(P[Index[i]].Pos[k] - P[head_s].Pos[k])
		    *periodic(P[Index[i]].Pos[k] - P[head_s].Pos[k]);
		  dist_h += periodic(P[head_p].Pos[k] - P[head_s].Pos[k])
		    *periodic(P[head_p].Pos[k] - P[head_s].Pos[k]);
		}
	      dist_p = sqrt(dist_p);
	      dist_s = sqrt(dist_s);
	      dist_h = sqrt(dist_h);

	      if(dist_p > dist_s)	/* p group is longer */
		{
		  head = head_p;
		  head_attach = head_s;
		}
	      else
		{
		  head = head_s;
		  head_attach = head_p;
		}
	      
	      /* If two group's center distance is smaller than their combined hsml merger to denser group */
	      if(dist_h < MergerLengthFactor*(P[head_p].gHsml + P[head_s].gHsml))
		{ 
		  if(P[head_p].gd.Density > P[head_s].gd.Density)     /* p group is longer */
		    {
		      head1 = head_p;
		      head2 = head_s;
		    }
		  else
		    {
		      head1 = head_s;
		      head2 = head_p;
		    }

		  Next[Tail[head1]]   = head2;
		  Tail[head1]   = Tail[head2];
		  Len[head1] += Len[head2];

		  ss = head2;
		  do
		    {
		      Head[ss] = head1;
		    }
		  while((ss = Next[ss]) >= 0);
		  head = head1;
		}

	      listofdifferent[j + 1] = head;
	    }
	  Head[Index[i]]     = head;
	  Next[Tail[head]]   = Index[i];
	  Tail[head]         = Index[i];
	  Len[head]++;
	  Next[Index[i]]   = -1;
	}
    }

  /** end of local group finder **/
  /*******************************/
  MPI_Barrier(MPI_COMM_WORLD);
  if(ThisTask == 0) printf ("End of local group search\n");

  /******************************/
  /**   start global search    **/

#ifdef MYSORT
  mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
  qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif

  MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
  for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
    {
      Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
      nimport += Recv_count[j];

      if(j > 0)
	{
	  Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
	  Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
	}
    }
  GhostDataGet = (struct ghostdata_in *) mymalloc(nimport * sizeof(struct ghostdata_in));
  GhostDataIn = (struct ghostdata_in *) mymalloc(nexport * sizeof(struct ghostdata_in));

  /* prepare particle data for export */
  /* We need to send the group info **/ 
  for(j = 0; j < nexport; j++)
    {
      place = DataIndexTable[j].Index;
      for(k = 0 ; k < 3; k++)
	GhostDataIn[j].Pos[k] = P[place].Pos[k];
      GhostDataIn[j].Density = P[place].gd.Density;
      GhostDataIn[j].Hsml = P[place].gHsml;
      GhostDataIn[j].HeadLen = Len[Head[place]];

      dist = 0.0;
      for(k = 0 ; k < 3; k++)
	{
	  GhostDataIn[j].HeadPos[k] = P[Head[place]].Pos[k];
	  dist += periodic(P[place].Pos[k] - P[Head[place]].Pos[k])*periodic(P[place].Pos[k] - P[Head[place]].Pos[k]);
	}
      GhostDataIn[j].HeadDensity = P[Head[place]].gd.Density;
      GhostDataIn[j].HeadHsml = P[Head[place]].gHsml;
      GhostDataIn[j].dist = sqrt(dist);

      if(place == Head[place]) /* If this is the head particle, make sure dist = 0 */
	GhostDataIn[j].dist = 0.0;

      memcpy(GhostDataIn[j].NodeList,
	     DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
    }

  /* exchange particle data */
  for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
    {
      sendTask = ThisTask;
      recvTask = ThisTask ^ ngrp;

      if(recvTask < NTask)
	{
	  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
	    {
	      /* get the particles */
#ifdef USE_ISEND_IRECV
	      MPI_Isend(&GhostDataIn[Send_offset[recvTask]],
			Send_count[recvTask] * sizeof(struct ghostdata_in), MPI_BYTE,
			recvTask, TAG_GHOST_A, MPI_COMM_WORLD, &mpireq[0]);
	      MPI_Irecv(&GhostDataGet[Recv_offset[recvTask]],
			Recv_count[recvTask] * sizeof(struct ghostdata_in), MPI_BYTE,
			recvTask, TAG_GHOST_A, MPI_COMM_WORLD, &mpireq[1]);
	      MPI_Waitall(2,mpireq,stat);
#else
	      MPI_Sendrecv(&GhostDataIn[Send_offset[recvTask]],
			   Send_count[recvTask] * sizeof(struct ghostdata_in), MPI_BYTE,
			   recvTask, TAG_GHOST_A,
			   &GhostDataGet[Recv_offset[recvTask]],
			   Recv_count[recvTask] * sizeof(struct ghostdata_in), MPI_BYTE,
			   recvTask, TAG_GHOST_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
	    }
	}
    }
  MPI_Barrier(MPI_COMM_WORLD);
  myfree(GhostDataIn);

  /******************************************/
  /****   group  search for global nodes ****/
  GhostDataOut = (struct ghostdata_out *) mymalloc(nexport * sizeof(struct ghostdata_out));
  GhostDataResult = (struct ghostdata_out *) mymalloc(nimport * sizeof(struct ghostdata_out));
  GhostIndex = (int *) mymalloc(sizeof(int) * nimport);

  Ngblist_sum = (int *) mymalloc(NumPart * sizeof(int));
  r2list_sum = (MyFloat *) mymalloc(sizeof(MyFloat) * MAX_NGB);

  /* initialize GhostIndex and GhostDataResult */
  for(i = 0; i < nimport; i++)
    {
      GhostIndex[i] = i;
      GhostDataResult[i].Flag = 0;  /* Flag: 0-stay; 1-shift; 2-merger */
      GhostDataResult[i].NewTask = -1;
      GhostDataResult[i].NewHead = -1;      
    } 

  /* sort by density */
  qsort(GhostIndex, nimport, sizeof(int), compare_ghostdens_index);

  for(i = 0; i < nimport; i++)
    {
      startnode = GhostDataGet[GhostIndex[i]].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;        /* open it */
      
      numngb = 0;
      listindex = 0;
      while(startnode >= 0)
	{
	  while(startnode >= 0)
	    {
	      numngb_inbox = ngb_treefind_baryon(&GhostDataGet[GhostIndex[i]].Pos[0], GhostDataGet[GhostIndex[i]].Hsml, GhostIndex[i], &startnode, 1, &dummy, &dummy);
	      h2 = GhostDataGet[GhostIndex[i]].Hsml * GhostDataGet[GhostIndex[i]].Hsml;

	      noffset = numngb;
	      for(n = 0; n < numngb_inbox; n++)
		{
		  j = Ngblist[n];
		  
		  dx = periodic(GhostDataGet[GhostIndex[i]].Pos[0] - P[j].Pos[0]);
		  dy = periodic(GhostDataGet[GhostIndex[i]].Pos[1] - P[j].Pos[1]);
		  dz = periodic(GhostDataGet[GhostIndex[i]].Pos[2] - P[j].Pos[2]);
		  
		  r2 = dx * dx + dy * dy + dz * dz;
      
		  if(r2 <= h2 && P[j].GroupIn != 0)
		    {
		      numngb++;
		      r2list[n] = r2;
		    }
		  else
		    {
		      Ngblist[n] = Ngblist[numngb_inbox - 1];
		      numngb_inbox--;
		      n--;
		    }
		}	      

	      listindex++;
	      if(listindex < NODELISTLENGTH)
		{
		  startnode = GhostDataGet[GhostIndex[i]].NodeList[listindex];
		  if(startnode >= 0)
		    startnode = Nodes[startnode].u.d.nextnode;      /* open it */
		}

	      for(n = 0; n < numngb_inbox; n++)
		{
		  r2list_sum[n+noffset] = r2list[n];
		  Ngblist_sum[n+noffset] = Ngblist[n];
		}
	    }
	} 
      /* end of while loop for single ngb search */
      sort2_flt_int(numngb, r2list_sum - 1, Ngblist_sum - 1);

      /* global group search start **/  
      /* ok, let's first see how many different groups there are */
      for(k = 0, ndiff = 0; k < numngb; k++)
	{
	  ind = Ngblist_sum[k];
	  if(P[ind].gd.Density > GhostDataGet[GhostIndex[i]].Density)
	    {
	      if(Head[ind] >= 0)    /* neighbor is attached to a group */
		{
		  for(j = 0; j < ndiff; j++)
		    if(listofdifferent[j] == Head[ind])
		      break;
	      
		  if(j >= ndiff)    /* a new group has been found */
		    listofdifferent[ndiff++] = Head[ind];
		}
	      else
		{
		  printf("\nthis may not occur.\n");
		}
	    }
	}

      if(ndiff == 0)
        GhostDataResult[GhostIndex[i]].Flag = 0;

      if(ndiff == 1)		/* the particle is attached to exactly one group */
	{
	  /* Note: All eligible particles are in a group */
	  head = listofdifferent[0];
	  dist = dist_h = 0.0;
	  for(k = 0 ; k < 3; k++)
	    {
	      dist += periodic(GhostDataGet[GhostIndex[i]].Pos[k] - P[head].Pos[k])
		*periodic(GhostDataGet[GhostIndex[i]].Pos[k] - P[head].Pos[k]);
	      dist_h += periodic(GhostDataGet[GhostIndex[i]].HeadPos[k] - P[head].Pos[k])
		*periodic(GhostDataGet[GhostIndex[i]].HeadPos[k] - P[head].Pos[k]);
	    }
	  dist = sqrt(dist);
	  dist_h = sqrt(dist_h);

	  if(GhostDataGet[GhostIndex[i]].HeadLen == 1)
	    { /* Remote head is larger */
	      if(GhostDataGet[GhostIndex[i]].Density < P[head].gd.Density)
		{
		  GhostDataResult[GhostIndex[i]].Flag = 2;
		  GhostDataResult[GhostIndex[i]].NewTask = ThisTask;
		  GhostDataResult[GhostIndex[i]].NewHead = head;
		  for(k = 0 ; k < 3; k++)
		    GhostDataResult[GhostIndex[i]].HeadPos[k] = P[head].Pos[k]; 
		}
	    }
	  else
	    {
	      if(GhostDataGet[GhostIndex[i]].dist > dist) /* original group is longer */ 
		{		  
		  GhostDataResult[GhostIndex[i]].Flag = 0; 
		}
	      else
		{
		  if(dist_h > (MergerLengthFactor*(GhostDataGet[GhostIndex[i]].Hsml + P[head].gHsml))
		     && GhostDataGet[GhostIndex[i]].dist > 0)
		    { 
		      /* move the current particle to new group */
		      GhostDataResult[GhostIndex[i]].Flag = 1;
		      GhostDataResult[GhostIndex[i]].NewTask = ThisTask;
		      GhostDataResult[GhostIndex[i]].NewHead = head;
		      for(k = 0 ; k < 3; k++)
			GhostDataResult[GhostIndex[i]].HeadPos[k] = P[head].Pos[k];
		    }
		  else
		    { 
		      /* If two group's center distance is smaller than their combined hsml merger to denser group */
		      if(P[head].gd.Density > GhostDataGet[GhostIndex[i]].HeadDensity)
			{
			  GhostDataResult[GhostIndex[i]].Flag = 2;
			  GhostDataResult[GhostIndex[i]].NewTask = ThisTask;
			  GhostDataResult[GhostIndex[i]].NewHead = head;
			  for(k = 0 ; k < 3; k++)
			    GhostDataResult[GhostIndex[i]].HeadPos[k] = P[head].Pos[k];
			}
		    }
		}
	    }
	}
      
      if(ndiff > 1)		/* the particle merges (at least) two groups together */
	{
	  for(j = -1; j < (ndiff - 1); j++)
	    {
	      if(j == -1)
		head_p = -1; /* It is ghost group */
	      else 
		head_p = listofdifferent[j];
	      head_s = listofdifferent[j + 1];
	      
	      dist_p = dist_s = dist_h = 0.0;
	      if(head_p == -1)
		{
		  for(k = 0 ; k < 3; k++)
		    {
		      dist_s += periodic(GhostDataGet[GhostIndex[i]].Pos[k] - P[head_s].Pos[k])
			*periodic(GhostDataGet[GhostIndex[i]].Pos[k] - P[head_s].Pos[k]);
		      dist_h +=periodic(GhostDataGet[GhostIndex[i]].HeadPos[k] - P[head_s].Pos[k]) 
			*periodic(GhostDataGet[GhostIndex[i]].HeadPos[k] - P[head_s].Pos[k]);
		    }

		  dist_p = GhostDataGet[GhostIndex[i]].dist;
		  dist_s = sqrt(dist_s);
		  dist_h = sqrt(dist_h);
		  dens_p = GhostDataGet[GhostIndex[i]].HeadDensity;
		  dens_s = P[head_s].gd.Density;
		  com_hsml = GhostDataGet[GhostIndex[i]].Hsml + P[head_s].gHsml;
		}	      
	      else
		{
		  for(k = 0 ; k < 3; k++)
		    {
		      dist_p += periodic(GhostDataGet[GhostIndex[i]].Pos[k] - P[head_p].Pos[k])
			*periodic(GhostDataGet[GhostIndex[i]].Pos[k] - P[head_p].Pos[k]);
		      dist_s += periodic(GhostDataGet[GhostIndex[i]].Pos[k] - P[head_s].Pos[k])
			*periodic(GhostDataGet[GhostIndex[i]].Pos[k] - P[head_s].Pos[k]);
		      dist_h = periodic(P[head_p].Pos[k] - P[head_s].Pos[k])
			*periodic(P[head_p].Pos[k] - P[head_s].Pos[k]);
		    }

		  dist_p = sqrt(dist_p);
		  dist_s = sqrt(dist_s);
		  dist_h = sqrt(dist_h);
		  dens_p = P[head_p].gd.Density;
		  dens_s = P[head_s].gd.Density;
		  com_hsml = P[head_p].gHsml + P[head_s].gHsml;
		}
	      com_hsml *= MergerLengthFactor;

	      /* If the distance of two group's head is smaller than their combined hsml merger to denser group */
	      if(head_p == -1) 
		{
		  head = head_s; /* Since head_p is not in a given node */

		  listofdifferent[j + 1] = head;
		  if(GhostDataGet[GhostIndex[i]].HeadLen == 1)
		    { /* Remote head is larger */
		      if(GhostDataGet[GhostIndex[i]].Density < P[head].gd.Density)
			{
			  GhostDataResult[GhostIndex[i]].Flag = 2;
			  GhostDataResult[GhostIndex[i]].NewTask = ThisTask;
			  GhostDataResult[GhostIndex[i]].NewHead = head;
			  for(k = 0 ; k < 3; k++)
			    GhostDataResult[GhostIndex[i]].HeadPos[k] = P[head].Pos[k];
			}
		     }
		  else
		    {
		      if(dist_p < dist_s) /* p groups is smaller */
			{
			  if((dist_h > com_hsml) && (dist_p > 0))
			    {
			      GhostDataResult[GhostIndex[i]].Flag = 1;
			      GhostDataResult[GhostIndex[i]].NewTask = ThisTask;
			      GhostDataResult[GhostIndex[i]].NewHead = head;
			      for(k = 0 ; k < 3; k++)
				GhostDataResult[GhostIndex[i]].HeadPos[k] = P[head].Pos[k];
			    }
			  else
			    {
			      if(dens_p < dens_s) /* p group is smaller */
				{
				  GhostDataResult[GhostIndex[i]].Flag = 2;
				  GhostDataResult[GhostIndex[i]].NewTask = ThisTask;
				  GhostDataResult[GhostIndex[i]].NewHead = head;
				  for(k = 0 ; k < 3; k++)
				    GhostDataResult[GhostIndex[i]].HeadPos[k] = P[head].Pos[k];
				}
			      /* In case of dens_p > dens_s, the merger can be done by particle 
			       * in d2 group that comes to d1 node.
			       */
			    }
			}
		      else
			GhostDataResult[GhostIndex[i]].Flag = 0;
		    }
		}
	      else
		{
		  if(GhostDataResult[GhostIndex[i]].Flag == 0)
		    continue;
		  if(dist_p > dist_s)   /* p group is longer */
		    {
		      head = head_p;
		      head_attach = head_s;
		    }
		  else
		    {
		      head = head_s;
		      head_attach = head_p;
		    }
	      
		  if(dist_h < com_hsml)
		    {
		      if(dens_p > dens_s)     /* p group is bigger */
			{
			  head1 = head_p;
			  head2 = head_s;
			}
		      else
			{
			  head1 = head_s;
			  head2 = head_p;
			}
		      Next[Tail[head1]] = head2;
		      Tail[head1]  = Tail[head2];		      
		      Len[head1] += Len[head2];
		      
		      ss = head2;
		      do
			{
			  Head[ss] = head1;
			}
		      while((ss = Next[ss]) >= 0);
		      head = head1;
		    }
		  listofdifferent[j + 1] = head;
		  /* update old GhostDataResult */
		  for(k=0; k<=i; k++)
		    if(GhostDataResult[GhostIndex[k]].NewTask == ThisTask 
		       && GhostDataResult[GhostIndex[k]].NewHead == head2
		       && GhostDataResult[GhostIndex[k]].Flag != 0)
		      GhostDataResult[GhostIndex[k]].NewHead = head1;
		}
	    }
	}
    }
  myfree(r2list_sum);
  myfree(Ngblist_sum);
  myfree(GhostIndex);

  MPI_Barrier(MPI_COMM_WORLD);
  if(ThisTask == 0)
    printf ("End of remote group search now comminucation!\n");

  /** Export back the Ghost data to original Task **/
  for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
    {
      sendTask = ThisTask;
      recvTask = ThisTask ^ ngrp;
      if(recvTask < NTask)
	{
	  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
	    {
	      /* send the results */
#ifdef USE_ISEND_IRECV
	      MPI_Isend(&GhostDataResult[Recv_offset[recvTask]],
			Recv_count[recvTask] * sizeof(struct ghostdata_out), MPI_BYTE,
			recvTask, TAG_GHOST_B,MPI_COMM_WORLD, &mpireq[0]);
	      MPI_Irecv(&GhostDataOut[Send_offset[recvTask]],
			Send_count[recvTask] * sizeof(struct ghostdata_out), MPI_BYTE,
			recvTask, TAG_GHOST_B, MPI_COMM_WORLD, &mpireq[1]);
	      MPI_Waitall(2,mpireq,stat);
#else
	      MPI_Sendrecv(&GhostDataResult[Recv_offset[recvTask]],
			   Recv_count[recvTask] * sizeof(struct ghostdata_out),
			   MPI_BYTE, recvTask, TAG_GHOST_B,
			   &GhostDataOut[Send_offset[recvTask]],
			   Send_count[recvTask] * sizeof(struct ghostdata_out),
			   MPI_BYTE, recvTask, TAG_GHOST_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
	    }
	}
    }
  myfree(GhostDataResult);
  
  /** end of global search **/
  /**************************/

  MPI_Barrier(MPI_COMM_WORLD);
  if(ThisTask == 0) 
    printf ("End of remote group search and start resuffle (re-link)the data groups\n");

  /**************************************  
   * Now resuffle the data and make groups 
   * and add the result to the local particles 
   **************************************/
  for(j = 0; j < nexport; j++)
    {
      place = DataIndexTable[j].Index;

      if(CommHead[place].flag != 0)
	{
	  dist_p = dist_s = 0.0;
	  for(k=0; k<3; k++){
	    dist_p += (P[place].Pos[k]-GhostDataOut[j].HeadPos[k])*(P[place].Pos[k]-GhostDataOut[j].HeadPos[k]);
	    dist_s += (P[place].Pos[k]-CommHead[place].HeadPos[k])*(P[place].Pos[k]-CommHead[place].HeadPos[k]);
	  }
	  if(dist_p < dist_s) continue;
	}

      CommHead[place].flag = GhostDataOut[j].Flag;
      if(CommHead[place].flag != 0)
	{ 
	  CommHead[place].task = GhostDataOut[j].NewTask;
	  CommHead[place].head = GhostDataOut[j].NewHead;
	  for(k=0; k<3; k++)
	    CommHead[place].HeadPos[k] = GhostDataOut[j].HeadPos[k];
	}
    }
  /* clear buffers arrange to communication for ghost particle*/
  myfree(GhostDataOut);
  myfree(GhostDataGet);

  /*** Make a group list and set the queue **/
  for(i = 0, j = 0; i < NumPart; i++)
    {
      if(P[i].GroupIn != 0)
        {
          Index[j] = i;
          j++;
          if(j > NumPart_Group)
            {
              printf("error on assign Index in find_subgroups\n");
              endrun(9911);
            }
        }
    }
  qsort(Index, NumPart_Group, sizeof(int), compare_dens_index);

  /** modify the grouping to make new groups by external nodes **/
  for(i = 0, j = 0; i < NumPart_Group; i++)
    {
      if(CommHead[Index[i]].flag == 1)
	{
	  if(CommHead[Index[i]].task != ThisTask)
	    {
	      /* If entire original group merger to remote group, 
		the members follow group decision. */
	      ind1 = Head[Index[i]];
	      do
		{
		  ind2 = ind1;
		  ind1 = Next[ind1]; 
		}
	      while(ind1 != Index[i]);

	      if(Next[Index[i]] >= 0)
		{
		  Next[ind2] = Next[ind1];
		  Len[Head[Index[i]]]-- ;
		}
	      else
		{
		  Next[ind2] = -1;
		  Tail[Head[Index[i]]] = ind2;
		  Len[Head[Index[i]]]-- ;
		}
	      
	      /* Make a new group that attach to remote group */
	      CommIndex[j] = Index[i];
	      j++;
	      for(k=0; k<j; k++)  
		{
		  head = CommIndex[k];
		  if(CommHead[head].task == CommHead[Index[i]].task
		     && CommHead[head].head == CommHead[Index[i]].head)
		    break;
		}
	      if(head == Index[i])
		{
		  Head[Index[i]] = Tail[Index[i]] = Index[i];
		  Len[Index[i]] = 1;
		  Next[Index[i]] = -1;    
		}
	      else
		{
		  Head[Index[i]] = head;
		  Next[Tail[head]] = Index[i];
		  Tail[head] = Index[i];
		  Len[head]++;
		  Next[Index[i]] = -1;
		}
	    }
	}

      if(CommHead[Index[i]].flag == 2) 
	{
	  if(CommHead[Head[Index[i]]].task == ThisTask)
	    {
	      ss = Head[Index[i]];
	      do
		{
		  CommHead[ss].task = CommHead[Index[i]].task;
		  CommHead[ss].head = CommHead[Index[i]].head;
		  for(k=0; k<3; k++)
		    CommHead[ss].HeadPos[k] = CommHead[Index[i]].HeadPos[k];		    
		}
	      while((ss = Next[ss]) >= 0);
	    }
	}
    }

  /* Now count groups in a given node. 
   * Here, we count the group of the particle to connect one group in the remote group an one group.
   * We have not yet applied the part number criterion.
   */
  for(i = 0, j = 0; i < NumPart; i++)
    {
      if(P[i].GroupIn != 0)
        {
          Index[j] = i;
          j++;
          if(j > NumPart_Group)
            {
              printf("error on assign Index in find_subgroups\n");
              endrun(9911);
            }
        }
    }
  qsort(Index, NumPart_Group, sizeof(int), compare_head_index);
   
  for(i = 0, Ngroups = 0, oldhead = -1; i < NumPart_Group; i++)
    if(Head[Index[i]] != oldhead)
      {
	oldhead = Head[Index[i]];
	Ngroups ++;
      }

  /***************************/
  /* global group connection */
  /* Now re-check the link of new groups */
  Group2Head = (int *) mymalloc(Ngroups * sizeof(int));  
  GlobalLinker = (struct globallinker *) mymalloc(Ngroups * sizeof(struct globallinker));

  for(i = 0, j=0, grexport = 0, oldhead = -1; i < NumPart_Group; i++)
    if(Head[Index[i]] != oldhead)
      {
        oldhead = Head[Index[i]];
	Group2Head[j] = Head[Index[i]];
	j++;
	if(CommHead[Head[Index[i]]].flag != 0)
	  {
	    GlobalLinker[grexport].task = CommHead[Head[Index[i]]].task;
	    GlobalLinker[grexport].head = CommHead[Head[Index[i]]].head;
	    GlobalLinker[grexport].task_local = ThisTask;
	    GlobalLinker[grexport].head_local = Head[Index[i]];
	    for(k=0; k<3; k++)
	      GlobalLinker[grexport].HeadPos[k] = CommHead[Head[Index[i]]].HeadPos[k];
	    grexport ++;
	  }
      }
  MPI_Barrier(MPI_COMM_WORLD);

  /* Now re-check the link of new groups */
  /* Note: each node has differnet number of elements */
  MPI_Allreduce(&grexport, &grexport_max, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);

  GlobalLinkerAll = (struct globallinker *) mymalloc(grexport_max * NTask * sizeof(struct globallinker));
  GlobalLinkerSend = (struct globallinker *) mymalloc(grexport_max * sizeof(struct globallinker));
  grexport_arr = (int *) mymalloc(NTask * sizeof(int));

  for(i=0; i<grexport_max; i++)
    {
      if(i < grexport)
	{
	GlobalLinkerSend[i].task       = GlobalLinker[i].task;
	GlobalLinkerSend[i].head       = GlobalLinker[i].head;
	GlobalLinkerSend[i].task_local = GlobalLinker[i].task_local;
	GlobalLinkerSend[i].head_local = GlobalLinker[i].head_local;
	for(k=0; k<3; k++)
	  GlobalLinkerSend[i].HeadPos[k] = GlobalLinker[i].HeadPos[k];
	}
      else
	{
	  GlobalLinkerSend[i].task = GlobalLinkerSend[i].task_local = -1;
	  GlobalLinkerSend[i].head = GlobalLinkerSend[i].head_local = -1;
	}
    }
  MPI_Allgather(&grexport, 1, MPI_INT, grexport_arr, 1, MPI_INT, MPI_COMM_WORLD);
  MPI_Allgather(GlobalLinkerSend, grexport_max*sizeof(struct globallinker), MPI_BYTE, 
		GlobalLinkerAll, grexport_max*sizeof(struct globallinker), MPI_BYTE,
		MPI_COMM_WORLD);

  /* Update sequential connection group */
  for(i=0; i < grexport; i++)
    {
      flag = 0;
      task = GlobalLinker[i].task;
      head = GlobalLinker[i].head;
      for(k=0; k<3; k++)
	HeadPos[k] = GlobalLinker[i].HeadPos[k];
      do 
	{
	  for(j=0; j < grexport_max*NTask; j++)
	    {
	      /**
		 if((j%grexport_max) > grexport_arr[(j/grexport_max)])
		 continue;
	      **/
	      flag = 0;
	      if(GlobalLinkerAll[j].task_local == task
		 && GlobalLinkerAll[j].head_local == head)
		{
		  task = GlobalLinkerAll[j].task;
		  head = GlobalLinkerAll[j].head;
		  for(k=0; k<3; k++)
		    HeadPos[k] = GlobalLinkerAll[j].HeadPos[k];
		  flag = 1;
		  break;
		}
	    }
	} 
      while(flag == 1);
      GlobalLinker[i].task = task;
      GlobalLinker[i].head = head;
      for(k=0; k<3; k++)
	GlobalLinker[i].HeadPos[k] = HeadPos[k];
    }
  myfree(grexport_arr);
  myfree(GlobalLinkerSend);
  myfree(GlobalLinkerAll);

  MPI_Barrier(MPI_COMM_WORLD);

  if(ThisTask == 0) 
    printf ("Send re-linked groups to their main group\n");

  /* check the groups re-link to their own nodes */
  for(i=0, grexport_send=0; i < grexport; i++)
    {
      if(GlobalLinker[i].task == GlobalLinker[i].task_local)
	{
	  /* group head2 re-link to group head1 for a given node */
	  GlobalLinker[i].ReLink = 1;	  
	  head1 = GlobalLinker[i].head;
	  head2 = GlobalLinker[i].head_local;
	  
	  Next[Tail[head1]] = head2;
	  Tail[head1] = Tail[head2];
	  Len[head1] += Len[head2];
	  
	  ss = head2;
	  do
	    {
	      Head[ss] = head1;
	    }
	  while((ss = Next[ss]) >= 0);	  
	}
      else
	{
	  GlobalLinker[i].ReLink = 0;
	  grexport_send++;
	}
    }

  /* Send local companion group data to main group */
  for(j = 0; j < NTask; j++)
    Send_count[j] = 0;

  GroupDataIn = (struct groupdata *) mymalloc(grexport_send * sizeof(struct groupdata));
  GroupIndexTable = (struct data_index *) mymalloc(grexport_max * sizeof(struct data_index));
  for(i=0, j=0; i<grexport; i++)
    {
      if(!GlobalLinker[i].ReLink)
	{
	  Send_count[GlobalLinker[i].task]++;
	  GroupIndexTable[j].Task = GlobalLinker[i].task;
	  GroupIndexTable[j].Index = i;
	  GroupIndexTable[j].IndexGet = i;
	  j++;
	}
    }
  if(j != grexport_send)
    printf("Error for the miss-match for grexport_send=%d (Task=%d)\n",grexport_send,ThisTask);
  MPI_Barrier(MPI_COMM_WORLD);

#ifdef MYSORT
  mysort_dataindex(GroupIndexTable, grexport_send, sizeof(struct data_index), data_index_compare);
#else
  qsort(GroupIndexTable, grexport_send, sizeof(struct data_index), data_index_compare);
#endif

  for(i=0; i<grexport_send; i++)
    {
      place = GroupIndexTable[i].Index;
      GroupDataIn[i].ID = -1;
      GroupDataIn[i].Task = GlobalLinker[place].task;
      GroupDataIn[i].Task_Local = ThisTask;
      GroupDataIn[i].Head = GlobalLinker[place].head;
      GroupDataIn[i].Head_Local = GlobalLinker[place].head_local;

      for(k=0; k<3; k++)
	{
	  GroupDataIn[i].HeadPos[k] = GlobalLinker[place].HeadPos[k];
	  GroupDataIn[i].COM[k] = 0.0;
	}
      GroupDataIn[i].MassGas = GroupDataIn[i].MassStar = GroupDataIn[i].GroupSfr = 0.0;
      GroupDataIn[i].MetalGas = GroupDataIn[i].MetalStar = 0.0;
      GroupDataIn[i].Size = -1;

      ss=GlobalLinker[place].head_local;
      GroupDataIn[i].Len = Len[ss];
      do
	{
	  if(P[ss].Type == 0)
	    {
	      GroupDataIn[i].MassGas  += P[ss].Mass;
	      GroupDataIn[i].GroupSfr += P[ss].PartSfr;
	      GroupDataIn[i].MetalGas += P[ss].Metallicity*P[ss].Mass;
	    }
	  if(P[ss].Type == 4)
	    {
	      GroupDataIn[i].MassStar  += P[ss].Mass;
	      GroupDataIn[i].MetalStar += P[ss].Metallicity*P[ss].Mass;
	    }

	  rr = 0;
	  for(k=0; k<3; k++)
	    rr += periodic(P[ss].Pos[k]-GlobalLinker[place].HeadPos[k])
	      * periodic(P[ss].Pos[k]-GlobalLinker[place].HeadPos[k]);
	  rr = sqrt(rr); 
	  if(rr > GroupDataIn[i].Size)
	    GroupDataIn[i].Size = rr;

	  for(k=0; k<3; k++)
	    GroupDataIn[i].COM[k] += P[ss].Mass*periodic(P[ss].Pos[k]-GroupDataIn[i].HeadPos[k]);
	}
      while((ss = Next[ss]) >= 0);
    }
  myfree(GroupIndexTable);

  MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
  for(j = 0, grimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
    {
      Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
      grimport += Recv_count[j];

      if(j > 0)
	{
	  Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
	  Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
	}
    }
  GroupDataGet = (struct groupdata *) mymalloc(grimport * sizeof(struct groupdata));

  /* exchange group data */
  for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
    {
      sendTask = ThisTask;
      recvTask = ThisTask ^ ngrp;

      if(recvTask < NTask)
	{
	  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
	    {
	      /* get the particles */
#ifdef USE_ISEND_IRECV
	      MPI_Isend(&GroupDataIn[Send_offset[recvTask]],
			Send_count[recvTask] * sizeof(struct groupdata), MPI_BYTE,
			recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
	      MPI_Irecv(&GroupDataGet[Recv_offset[recvTask]],
			Recv_count[recvTask] * sizeof(struct groupdata), MPI_BYTE,
			recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
	      MPI_Waitall(2,mpireq,stat);
#else
	      MPI_Sendrecv(&GroupDataIn[Send_offset[recvTask]],
			   Send_count[recvTask] * sizeof(struct groupdata), MPI_BYTE,
			   recvTask, TAG_DENS_A,
			   &GroupDataGet[Recv_offset[recvTask]],
			   Recv_count[recvTask] * sizeof(struct groupdata), MPI_BYTE,
			   recvTask, TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
	    }
	}
    }
  
  /* compute local group properties */
  MPI_Barrier(MPI_COMM_WORLD);
  if(ThisTask == 0)
    printf ("Compute Local Group Properties\n");
  
  GroupData = (struct groupdata *) mymalloc((Ngroups - grexport) * sizeof(struct groupdata));

  for(i=0, j=0; i<Ngroups; i++)
    {
      head = Group2Head[i];
      if(CommHead[head].flag != 0) continue;

      GroupData[j].Task = ThisTask;
      GroupData[j].ID = -1;
      GroupData[j].Head = GroupData[j].Head_Local = head;
      for(k=0; k<3; k++)
	{
	  GroupData[j].HeadPos[k] = P[head].Pos[k];
	  GroupData[j].COM[k] = 0.0;
        }
      GroupData[j].MassGas = GroupData[j].MassStar = GroupData[j].GroupSfr = 0.0;
      GroupData[j].MetalGas = GroupData[j].MetalStar = 0.0;
      GroupData[j].Size = -1;

      ss = head;
      GroupData[j].Len = Len[ss];
      do
	{
	  if(P[ss].Type == 0)
	    {
	      GroupData[j].MassGas  += P[ss].Mass;
	      GroupData[j].GroupSfr += P[ss].PartSfr;
	      GroupData[j].MetalGas += P[ss].Metallicity*P[ss].Mass;
	    }
	  if(P[ss].Type == 4)
	    {
	      GroupData[j].MassStar  += P[ss].Mass;
	      GroupData[j].MetalStar += P[ss].Metallicity*P[ss].Mass;
	    }

	  rr = 0;
	  for(k=0; k<3; k++)
	    rr += periodic(P[ss].Pos[k]-GroupData[j].HeadPos[k])
	      *periodic(P[ss].Pos[k]-GroupData[j].HeadPos[k]);
	  rr = sqrt(rr); 
	  if(rr > GroupData[j].Size)
	    GroupData[j].Size = rr;

          for(k=0; k<3; k++)
            GroupData[j].COM[k] += P[ss].Mass*periodic(P[ss].Pos[k]-GroupData[j].HeadPos[k]);
	}
      while((ss = Next[ss]) >= 0);
      j++;
    }
  Ngroups_local = j;
  if(Ngroups_local != (Ngroups-grexport))
    printf("Warning ThisTask %d Ngroups_local=%d Ngroups-grexport=%d\n",ThisTask, Ngroups_local, Ngroups-grexport);

  MPI_Barrier(MPI_COMM_WORLD);

  /*************************/
  /* Add remoted group data*/
  for(i=0; i<grimport; i++)
    {
      if(GroupDataGet[i].Task != ThisTask)
	{
	  printf("error non-matching the group task %d %d %d %d %d %d\n",
		 ThisTask,i,GroupDataGet[i].Task,GroupDataGet[i].Task_Local,GroupDataGet[i].Head,GroupDataGet[i].Head_Local);
	  endrun(10000);
	}

      j = 0;
      while(GroupDataGet[i].Head != GroupData[j].Head_Local)
	{
	  if(j >= Ngroups_local){
	    printf("error non-matching the group task %d %d %d %d %d %d\n",
		   ThisTask,i,GroupDataGet[i].Task,GroupDataGet[i].Task_Local,GroupDataGet[i].Head,GroupDataGet[i].Head_Local);
	    for(k=0; k<Ngroups_local; k++)
	      printf("%d  %d  %d  %d\n",k,GroupData[k].Task,GroupData[k].Head,GroupData[k].Head_Local);
	    endrun(10001);
	  }
	  j++;
	}
      
      GroupData[j].Len       += GroupDataGet[i].Len;
      GroupData[j].MassGas   += GroupDataGet[i].MassGas; 
      GroupData[j].MetalGas  += GroupDataGet[i].MetalGas; 
      GroupData[j].GroupSfr  += GroupDataGet[i].GroupSfr; 
      GroupData[j].MassStar  += GroupDataGet[i].MassStar; 
      GroupData[j].MetalStar += GroupDataGet[i].MetalStar; 
      for(k=0; k<3; k++)
	GroupData[j].COM[k] += GroupDataGet[i].COM[k];

      if(GroupData[j].Size > GroupDataGet[i].Size)
	GroupData[j].Size = GroupDataGet[i].Size;
    }

  /* now remove small group and assigne the group ID*/ 
  /* and finalize the com computation */
  for(i=0, id=0; i<Ngroups_local; i++)
    {
      if(GroupData[i].Len >= DesMinSize)
	{
	  GroupData[i].ID = id;
	  for(k=0; k<3; k++)
	    {
	      GroupData[i].COM[k] = GroupData[i].COM[k] / 
		(GroupData[i].MassGas + GroupData[i].MassStar);
	      GroupData[i].COM[k] =  periodic_wrap(GroupData[i].COM[k] + GroupData[i].HeadPos[k]);
	    }
	  id++;
	}
    }

  /* make group id number global by offseting */
  id_count = id;
  MPI_Allgather(&id_count, sizeof(MyIDType), MPI_BYTE, idcount_array, sizeof(MyIDType), MPI_BYTE, MPI_COMM_WORLD);
  n = id_offset = 0;
  while(ThisTask > n)
    {
      id_offset += idcount_array[n];
      n++;
    }

  for(i=0; i<Ngroups_local; i++)
    if(GroupData[i].Len >= DesMinSize)
      GroupData[i].ID += id_offset;
      
  /* Now update remote group */
  for(i=0; i<grimport; i++)
    {
      j=0;
      while(GroupDataGet[i].Head != GroupData[j].Head_Local)
	{
	  if(j >= Ngroups_local) {
	    /* It should not happen because it is an identical search with above */
	    printf("error non-matching the group task\n");
	    endrun(10002);
	  }
	  j++;
	}

      GroupDataGet[i].ID = GroupData[j].ID;
      GroupDataGet[i].Len = GroupData[j].Len;
      GroupDataGet[i].MassGas = GroupData[j].MassGas; 
      GroupDataGet[i].MetalGas = GroupData[j].MetalGas; 
      GroupDataGet[i].GroupSfr = GroupData[j].GroupSfr; 
      GroupDataGet[i].MassStar = GroupData[j].MassStar; 
      GroupDataGet[i].MetalStar = GroupData[j].MetalStar; 
      GroupDataGet[i].Size = GroupData[j].Size;
      for(k=0; k<3; k++)
	GroupDataGet[i].COM[k] = GroupData[j].COM[k];
    }

  /** Get back to original node**/
  GroupDataOut = (struct groupdata *) mymalloc(grexport_send * sizeof(struct groupdata));

  for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
    {
      sendTask = ThisTask;
      recvTask = ThisTask ^ ngrp;
      if(recvTask < NTask)
	{
	  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
	    {
	      /* send the results */
#ifdef USE_ISEND_IRECV
	      MPI_Isend(&GroupDataGet[Recv_offset[recvTask]],
			Recv_count[recvTask] * sizeof(struct groupdata),
			MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
	      MPI_Irecv(&GroupDataOut[Send_offset[recvTask]],
			Send_count[recvTask] * sizeof(struct groupdata),
			MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
	      MPI_Waitall(2,mpireq,stat);
#else
	      MPI_Sendrecv(&GroupDataGet[Recv_offset[recvTask]],
			   Recv_count[recvTask] * sizeof(struct groupdata),
			   MPI_BYTE, recvTask, TAG_DENS_B,
			   &GroupDataOut[Send_offset[recvTask]],
			   Send_count[recvTask] * sizeof(struct groupdata),
			   MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
	    }
	}

    }
  MPI_Barrier(MPI_COMM_WORLD);
  if(ThisTask == 0)
    printf ("End of remote group computation\n");

  /** assign the Data to Part **/
  for(i=0; i<(Ngroups_local); i++)
    {
      if(GroupData[i].ID >= 0)
	{
	  ind = GroupData[i].Head_Local;
	  do
	    {
	      P[ind].GroupID       = GroupData[i].ID;	      
	      P[ind].GroupMassGas  = GroupData[i].MassGas;
	      P[ind].GroupMassStar = GroupData[i].MassStar;
	      P[ind].GroupSfr      = GroupData[i].GroupSfr;
	      P[ind].GroupSize     = GroupData[i].Size;
	    }
	  while((ind = Next[ind]) >= 0);
	}
    }

  for(i=0; i<grexport_send; i++)
    {
      if(GroupDataOut[i].ID >= 0)
        {
          ind = GroupDataOut[i].Head_Local;
          do
            {
	      P[ind].GroupID       = GroupDataOut[i].ID;	      
              P[ind].GroupMassGas  = GroupDataOut[i].MassGas;
              P[ind].GroupMassStar = GroupDataOut[i].MassStar;
              P[ind].GroupSfr      = GroupDataOut[i].GroupSfr;
              P[ind].GroupSize     = GroupDataOut[i].Size;
            }
          while((ind = Next[ind]) >= 0);
        }
    }

  /** IO **/
  MPI_Barrier(MPI_COMM_WORLD);
  if(GroupOutput)
    {
      if(ThisTask == 0)
	printf ("Write Properties data\n");
      char  dirbuf[200], grbuf[200];
      FILE *FdGrProp;

      if(ThisTask == 0)
	{
	  sprintf(dirbuf, "%s/Groups_%03d", All.OutputDir, (All.SnapshotFileCount-1));
	  mkdir(dirbuf, 02755);
	}
      MPI_Barrier(MPI_COMM_WORLD);
      
      sprintf(grbuf,"%s/Groups_%03d/GrProp_%03d.%d", All.OutputDir, (All.SnapshotFileCount-1), (All.SnapshotFileCount-1), ThisTask);
      FdGrProp = fopen(grbuf, "w");
      
      for(i=0; i<(Ngroups_local); i++)
	if(GroupData[i].Len >= DesMinSize)
	  fprintf(FdGrProp,"%5d  %5d  %13g  %13g  %13g  %13g  %13g  %13g  %13g  %13g  %13g  %13g  %13g  %13g\n", 
		  GroupData[i].ID, GroupData[i].Len, 
		  GroupData[i].HeadPos[0], GroupData[i].HeadPos[1], GroupData[i].HeadPos[2], 
		  GroupData[i].COM[0], GroupData[i].COM[1], GroupData[i].COM[2], 
		  GroupData[i].MassGas, GroupData[i].MassStar, GroupData[i].MetalGas, GroupData[i].MetalStar, 
		  GroupData[i].GroupSfr, GroupData[i].Size);
      fclose(FdGrProp);
    }

  /* Free the memory */
  myfree(GroupDataOut);
  myfree(GroupData);
  myfree(GroupDataGet);
  myfree(GroupDataIn);

  myfree(GlobalLinker);
  myfree(Group2Head);

  myfree(r2list);
  myfree(listofdifferent);

  myfree(Index);

  myfree(CommIndex);
  myfree(CommHead);

  myfree(Len);
  myfree(Tail);
  myfree(Next);
  myfree(Head);

  myfree(Ngblist);  
  myfree(DataNodeList);
  myfree(DataIndexTable);

  if(ThisTask == 0) printf ("<<<<< Finish GroupFinder.\n");
}

double periodic(double x)
{
#ifdef PERIODIC
  if(x > 0.5 * All.BoxSize)
    x -= All.BoxSize;

  if(x < -0.5 * All.BoxSize)
    x += All.BoxSize;
#endif

  return x;
}

double periodic_wrap(double x)
{
#ifdef PERIODIC
  while(x > All.BoxSize)
    x -= All.BoxSize;

  while(x < 0)
    x += All.BoxSize;
#endif

  return x;
}



/* This routine is the same as ngb_treefind_variable in ngb.c. 
 * But it searches gas and star particles. 
 */
int ngb_treefind_baryon(MyDouble searchcenter[3], MyFloat hsml, int target, int *startnode, int mode,
			  int *nexport, int *nsend_local)
{
  int numngb, no, p, task, nexport_save;
  struct NODE *current;
  MyDouble dx, dy, dz, dist;

#ifdef PERIODIC
  MyDouble xtmp;
#endif
  nexport_save = *nexport;

  numngb = 0;
  no = *startnode;

  while(no >= 0)
    {
      if(no < All.MaxPart)	/* single particle */
	{
	  p = no;
	  no = Nextnode[no];

	  if(P[p].GroupIn == 0) 
	    continue;

	  if(P[p].Ti_current != All.Ti_Current)
	    drift_particle(p, All.Ti_Current);

	  dist = hsml;
#ifdef PERIODIC
	  dx = NGB_PERIODIC_LONG_X(P[p].Pos[0] - searchcenter[0]);
	  if(dx > dist)
	    continue;
	  dy = NGB_PERIODIC_LONG_Y(P[p].Pos[1] - searchcenter[1]);
	  if(dy > dist)
	    continue;
	  dz = NGB_PERIODIC_LONG_Z(P[p].Pos[2] - searchcenter[2]);
	  if(dz > dist)
	    continue;
#else
	  dx = P[p].Pos[0] - searchcenter[0];
	  if(dx > dist)
	    continue;
	  dy = P[p].Pos[1] - searchcenter[1];
	  if(dy > dist)
	    continue;
	  dz = P[p].Pos[2] - searchcenter[2];
	  if(dz > dist)
	    continue;
#endif
	  if(dx * dx + dy * dy + dz * dz > dist * dist)
	    continue;

	  Ngblist[numngb++] = p;
	}
      else
	{
	  if(no >= All.MaxPart + MaxNodes)	/* pseudo particle */
	    {
	      if(mode == 1)
		endrun(12312);

	      if(target >= 0)	/* if no target is given, export will not occur */
		{
		  if(Exportflag[task = DomainTask[no - (All.MaxPart + MaxNodes)]] != target)
		    {
		      Exportflag[task] = target;
		      Exportnodecount[task] = NODELISTLENGTH;
		    }

		  if(Exportnodecount[task] == NODELISTLENGTH)
		    {
		      if(*nexport >= All.BunchSize)
			{
			  *nexport = nexport_save;
			  if(nexport_save == 0)
			    endrun(13004);	/* in this case, the buffer is too small to process even a single particle */
			  for(task = 0; task < NTask; task++)
			    nsend_local[task] = 0;
			  for(no = 0; no < nexport_save; no++)
			    nsend_local[DataIndexTable[no].Task]++;
			  return -1;
			}
		      Exportnodecount[task] = 0;
		      Exportindex[task] = *nexport;
		      DataIndexTable[*nexport].Task = task;
		      DataIndexTable[*nexport].Index = target;
		      DataIndexTable[*nexport].IndexGet = *nexport;
		      *nexport = *nexport + 1;
		      nsend_local[task]++;
		    }

		  DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]++] =
		    DomainNodeIndex[no - (All.MaxPart + MaxNodes)];

		  if(Exportnodecount[task] < NODELISTLENGTH)
		    DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]] = -1;
		}

	      no = Nextnode[no - MaxNodes];
	      continue;
	    }

	  current = &Nodes[no];

	  if(mode == 1)
	    {
	      if(current->u.d.bitflags & (1 << BITFLAG_TOPLEVEL))	/* we reached a top-level node again, which means that we are done with the branch */
		{
		  *startnode = -1;
		  return numngb;
		}
	    }

	  if(current->Ti_current != All.Ti_Current)
	    force_drift_node(no, All.Ti_Current);

	  no = current->u.d.sibling;	/* in case the node can be discarded */

	  dist = hsml + 0.5 * current->len;;
#ifdef PERIODIC	  
	  dx = NGB_PERIODIC_LONG_X(current->center[0] - searchcenter[0]);
	  if(dx > dist)
	    continue;
	  dy = NGB_PERIODIC_LONG_Y(current->center[1] - searchcenter[1]);
	  if(dy > dist)
	    continue;
	  dz = NGB_PERIODIC_LONG_Z(current->center[2] - searchcenter[2]);
	  if(dz > dist)
	    continue;
#else
	  dx = current->center[0] - searchcenter[0];
	  if(dx > dist)
	    continue;
	  dy = current->center[1] - searchcenter[1];
	  if(dy > dist)
	    continue;
	  dz = current->center[2] - searchcenter[2];
	  if(dz > dist)
	    continue;
#endif
	  /* now test against the minimal sphere enclosing everything */
	  dist += FACT1 * current->len;
	  if(dx * dx + dy * dy + dz * dz > dist * dist)
	    continue;

	  no = current->u.d.nextnode;	/* ok, we need to open the node */
	}
    }

  *startnode = -1;
  return numngb;
}


/*
 * qsorts in find_subgroups ()
 */

int compare_head_index(const void *a, const void *b)
{
  if(Head[*(int *) a] < Head[*(int *) b])
    return -1;

  if(Head[*(int *) a] > Head[*(int *) b])
    return +1;

  return 0;
}


int compare_dens_index(const void *a, const void *b)
{
  if(P[*(int *) a].gd.Density < P[*(int *) b].gd.Density)
    return -1;

  if(P[*(int *) a].gd.Density > P[*(int *) b].gd.Density)
    return +1;

  return 0;
}


int compare_ghostdens_index(const void *a, const void *b)
{
  if(GhostDataGet[*(int *) a].Density < GhostDataGet[*(int *) b].Density)
    return -1;

  if(GhostDataGet[*(int *) a].Density > GhostDataGet[*(int *) b].Density)
    return +1;

  return 0;
}


/*
 * sorting routines for StarGroup Finder based on NR
 */

#define NR_END 1
#define FREE_ARG char*

#define NRANSI
#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp;
#define SWAP2(a,b) temp2=(a);(a)=(b);(b)=temp2;
#define MSORT 7
#define NSTACK 50

void sort2_flt_int(unsigned long long n, float arr[], int brr[])
{
	unsigned long i,ir=n,j,k,l=1;
	int *istack,jstack=0;
	float a,temp;
	int   b,temp2;

	if(n < 2) return;
	istack = (int *) mymalloc((NSTACK+1) * sizeof(int));
	for (;;) {
		if (ir-l < MSORT) {
			for (j=l+1;j<=ir;j++) {
				a=arr[j];
				b=brr[j];
				for (i=j-1;i>=l;i--) {
					if (arr[i] <= a) break;
					arr[i+1]=arr[i];
					brr[i+1]=brr[i];
				}
				arr[i+1]=a;
				brr[i+1]=b;
			}
			if (!jstack) {
			  myfree(istack);
			  return;
			}
			ir=istack[jstack];
			l=istack[jstack-1];
			jstack -= 2;
		} else {
			k=(l+ir) >> 1;
			SWAP(arr[k],arr[l+1])
			SWAP2(brr[k],brr[l+1])
			if (arr[l] > arr[ir]) {
				SWAP(arr[l],arr[ir])
				SWAP2(brr[l],brr[ir])
			}
			if (arr[l+1] > arr[ir]) {
				SWAP(arr[l+1],arr[ir])
				SWAP2(brr[l+1],brr[ir])
			}
			if (arr[l] > arr[l+1]) {
			        SWAP(arr[l],arr[l+1])
				SWAP2(brr[l],brr[l+1])
			}
			i=l+1;
			j=ir;
			a=arr[l+1];
			b=brr[l+1];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				SWAP(arr[i],arr[j])
				SWAP2(brr[i],brr[j])
			}
			arr[l+1]=arr[j];
			arr[j]=a;
			brr[l+1]=brr[j];
			brr[j]=b;
			jstack += 2;
			if (jstack > NSTACK) {
			  fprintf(stderr,"Run-time error in sorting routine ...\n");
			  fprintf(stderr,"NSTACK too small in sort2\n");
			  fprintf(stderr,"...now exiting to system...\n");
			  exit(1);
			}
			if (ir-i+1 >= j-l) {
				istack[jstack]=ir;
				istack[jstack-1]=i;
				ir=j-1;
			} else {
				istack[jstack]=j-1;
				istack[jstack-1]=l;
				l=i;
			}
		}
	}
}

#undef MSORT
#undef NSTACK
#undef SWAP
#undef SWAP2
#undef NRANSI

#endif


