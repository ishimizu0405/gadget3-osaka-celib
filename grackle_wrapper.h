#ifndef __WRAPGRACKLE_H__
#define __WRAPGRACKLE_H__

#ifdef __cplusplus
extern "C" {
#endif 
  int wrap_init_cooling(double udensity, double ulength, double utime);
  int wrap_update_UVbackground_rates(double a_now);
  double wrap_get_alow_uvb(void);

  int wrap_do_cooling(double density, double *energy, double dtime,
		      double x_velocity, double y_velocity, double z_velocity,
		      double *HI, double *HII, double *HeI, double *HeII, double *HeIII,
		      double *HM, double *H2I, double *H2II, double *DI, double *DII, double *HDI,
		      double *ne, double Z, double a_now);
  int wrap_get_cooling_time(double density, double energy,
                            double x_velocity, double y_velocity, double z_velocity,
                            double HI, double HII, double HeI, double HeII, double HeIII,
                            double HM, double H2I, double H2II, double DI, double DII, double HDI,
                            double ne, double Z, double a_now, double *cooling_time);
#ifdef __cplusplus
}
#endif
    

#endif /*__WRAPGRACKLE_H__*/
