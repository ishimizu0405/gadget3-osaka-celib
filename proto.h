
#ifndef ALLVARS_H
#include "allvars.h"
#endif
#include "forcetree.h"
#include "cooling.h"
#ifdef COSMIC_RAYS
#include "cosmic_rays.h"
#endif
#ifdef LT_STELLAREVOLUTION
#include "lt.h"
#endif

#ifdef HAVE_HDF5
#include <hdf5.h>
void write_header_attributes_in_hdf5(hid_t handle);
void read_header_attributes_in_hdf5(char *fname);
void write_parameters_attributes_in_hdf5(hid_t handle);
void write_units_attributes_in_hdf5(hid_t handle);
void write_constants_attributes_in_hdf5(hid_t handle);
#endif

void fof_get_group_center(double *cm, int gr);
void fof_get_group_velocity(double *cmvel, int gr);
int fof_find_dmparticles_evaluate(int target, int mode, int *nexport, int *nsend_local);
void fof_compute_group_properties(int gr, int start, int len);

void parallel_sort(void *base, size_t nmemb, size_t size, int (*compar) (const void *, const void *));
int compare_IDs(const void *a, const void *b);
void test_id_uniqueness(void);

int io_compare_P_ID(const void *a, const void *b);
int io_compare_P_GrNr_SubNr(const void *a, const void *b);


void drift_particle(int i, int time1);
double get_pressure(int i, double dt_entr);
int ShouldWeDoDynamicUpdate(void);

void put_symbol(double t0, double t1, char c);
void write_cpu_log(void);
void *mymalloc_msg(size_t n, char *message);

int get_timestep_bin(int ti_step);

const char* svn_version(void);

void find_particles_and_save_them(int num);
void lineofsight_output(void);
void sum_over_processors_and_normalize(void);
void absorb_along_lines_of_sight(void);
void output_lines_of_sight(int num);
int find_next_lineofsighttime(int time0);
int find_next_gridoutputtime(int ti_curr);
void add_along_lines_of_sight(void);
double los_periodic(double x);
void do_the_kick(int i, int tstart, int tend, int tcurrent);


void x86_fix(void) ;

void *mymalloc(size_t n);
void myfree(void *p);
void *myrealloc(void *p, size_t n);
void mymalloc_init(void);

double get_shear_viscosity(int i);

void kinetic_feedback_mhm(void);
int kin_compare_key(const void *a, const void *b);
void kinetic_evaluate(int target, int mode);

void bubble(void);
void multi_bubbles(void);
void find_CM_of_biggest_group(void);
int compare_length_values(const void *a, const void *b);
double rho_dot(double z, void *params);
double bhgrowth(double z1, double z2);


void smoothed_evaluate(int target, int mode, int *nexport, int *nsend_local);
void smoothed_values(void);

int fof_find_dmparticles_evaluate(int target, int mode, int *nexport, int *nsend_local);

double INLINE_FUNC hubble_function(double a);
#ifdef DARKENERGY
double DarkEnergy_a(double);
double DarkEnergy_t(double);
#ifdef TIMEDEPDE
void fwa_init(void);
double INLINE_FUNC fwa(double);
double INLINE_FUNC get_wa(double);
#ifdef TIMEDEPGRAV
double INLINE_FUNC dHfak(double a);
double INLINE_FUNC dGfak(double a);
#endif
#ifdef EXTERNALHUBBLE
double INLINE_FUNC hubble_function_external(double a);
#endif
#endif

#endif

void blackhole_accretion(void);
int blackhole_evaluate(int target, int mode, int *nexport, int *nsend_local);
int blackhole_evaluate_swallow(int target, int mode, int *nexport, int *nsend_local);

int  blackhole_compare_key(const void *a, const void *b);




void fof_fof(int num);
void fof_import_ghosts(void);
void fof_course_binning(void);
void fof_find_groups(void);
void fof_check_cell(int p, int i, int j, int k);
void fof_find_minids(void);
int fof_link_accross(void);
void fof_exchange_id_lists(void);
int fof_grid_compare(const void *a, const void *b);
void fof_compile_catalogue(void);
void fof_save_groups(int num);
void fof_save_local_catalogue(int num);
double fof_periodic(double x);
double fof_periodic_wrap(double x);
void fof_find_nearest_dmparticle(void);
int fof_find_nearest_dmparticle_evaluate(int target, int mode, int *nexport, int *nsend_local);

int fof_compare_key(const void *a, const void *b);
void fof_link_special(void);
void fof_link_specialpair(int p, int s);
void fof_make_black_holes(void);


void write_file(char *fname, int readTask, int lastTask);

void distribute_file(int nfiles, int firstfile, int firsttask, int lasttask, int *filenr, int *master,
		     int *last);

int get_values_per_blockelement(enum iofields blocknr);

int get_datatype_in_block(enum iofields blocknr);
void get_dataset_name(enum iofields blocknr, char *buf);


int blockpresent(enum iofields blocknr);
void fill_write_buffer(enum iofields blocknr, int *pindex, int pc, int type);
void empty_read_buffer(enum iofields blocknr, int offset, int pc, int type);

int get_particles_in_block(enum iofields blocknr, int *typelist);

int get_bytes_per_blockelement(enum iofields blocknr, int mode);

void read_file(char *fname, int readTask, int lastTask);

void get_Tab_IO_Label(enum iofields blocknr, char *label);


void long_range_init_regionsize(void);

int find_files(char *fname);

int metals_compare_key(const void *a, const void *b);
void enrichment_evaluate(int target, int mode);

int hydro_evaluate(int target, int mode, int *nexport, int *nsend_local);

void pm_init_nonperiodic_allocate(void);

void  pm_init_nonperiodic_free(void);

double get_random_number(unsigned int id);
void set_random_numbers(void);

int grav_tree_compare_key(const void *a, const void *b);
int dens_compare_key(const void *a, const void *b);
int hydro_compare_key(const void *a, const void *b);

int data_index_compare(const void *a, const void *b);
int peano_compare_key(const void *a, const void *b);

void mysort_dataindex(void *b, size_t n, size_t s, int (*cmp) (const void *, const void *));
void mysort_domain(void *b, size_t n, size_t s, int (*cmp) (const void *, const void *));
void mysort_idlist(void *b, size_t n, size_t s, int (*cmp) (const void *, const void *));
void mysort_pmperiodic(void *b, size_t n, size_t s, int (*cmp) (const void *, const void *));
void mysort_pmnonperiodic(void *b, size_t n, size_t s, int (*cmp) (const void *, const void *));
void mysort_peano(void *b, size_t n, size_t s, int (*cmp) (const void *, const void *));


int density_evaluate(int target, int mode, int *nexport, int *nsend_local);
int density_isactive(int n);

void GetMachNumberCR( struct sph_particle_data *Particle );
void GetMachNumber( struct sph_particle_data* Particle );
void GetShock_DtEnergy( struct sph_particle_data* Particle );

#ifdef MAGNETIC
#ifdef BFROMROTA
void rot_a(void);
void rot_a_evaluate(int i, int mode);
#endif
#endif
size_t sizemax(size_t a, size_t b);


void reconstruct_timebins(void);

void init_peano_map(void);
peanokey peano_hilbert_key(int x, int y, int z, int bits);
peanokey peano_and_morton_key(int x, int y, int z, int bits, peanokey *morton);
peanokey morton_key(int x, int y, int z, int bits);

void catch_abort(int sig);
void catch_fatal(int sig);
void terminate_processes(void);
void enable_core_dumps_and_fpu_exceptions(void);
void write_pid_file(void);

void pm_init_periodic_allocate(void);

void pm_init_periodic_free(void);

void move_particles(int time1);


void find_next_sync_point_and_drift(void);
void find_dt_displacement_constraint(double hfac);

#ifdef WAKEUP
void process_wake_ups(void);
#endif

void set_units_sfr(void);

void gravity_forcetest(void);

void allocate_commbuffers(void);
void allocate_memory(void);
void begrun(void);
void check_omega(void);
void close_outputfiles(void);
void compute_accelerations(int mode);
void compute_global_quantities_of_system(void);
void compute_potential(void);
void construct_timetree(void);
void cooling_and_starformation(void);
void count_hot_phase(void);
void delete_node(int i);
void density(void);
void density_decouple(void);
void determine_interior(void);
int dissolvegas(void);
void do_box_wrapping(void);
void domain_Decomposition(void);
double drand48();
double enclosed_mass(double R);
void endrun(int);
void energy_statistics(void);
void ensure_neighbours(void);

void every_timestep_stuff(void);
void ewald_corr(double dx, double dy, double dz, double *fper);

void ewald_force(int ii, int jj, int kk, double x[3], double force[3]);
void ewald_force_ni(int iii, int jjj, int kkk, double x[3], double force[3]);

void ewald_init(void);
double ewald_psi(double x[3]);
double ewald_pot_corr(double dx, double dy, double dz);
int find_ancestor(int i);
int find_next_outputtime(int time);
void find_next_time(void);
int find_next_time_walk(int node);
void free_memory(void);
void advance_and_find_timesteps(void);
int get_timestep(int p, double *a, int flag);

void determine_PMinterior(void);

double get_starformation_rate(int i);
void gravity_tree(void);
void hydro_force(void);
void init(void);
#ifndef LT_STELLAREVOLUTION
void init_clouds(void);
void integrate_sfr(void);
#else
void init_clouds(int, double, double, double, double*, double*);
void integrate_sfr(double, double, double, double, double);
#endif
void insert_node(int i);
int mark_targets(void);
size_t my_fwrite(void *ptr, size_t size, size_t nmemb, FILE * stream);
size_t my_fread(void *ptr, size_t size, size_t nmemb, FILE * stream);
void open_outputfiles(void);
void write_outputfiles_header(void);
void peano_hilbert_order(void);
double pot_integrand(double xx);
void predict(double time);
void predict_collisionless_only(double time);
void predict_sph_particles(double time);
void prepare_decouple(void);
void read_ic(char *fname);
void read_ic_cluster(char *fname);
void read_ic_cluster_gas(char *fname);
void read_ic_cluster_wimp(char *fname);
int read_outputlist(char *fname);
void read_parameter_file(char *fname);
void rearrange_particle_sequence(void);
void reorder_gas(void);
void reorder_particles(void);
void restart(int mod);
void run(void);
void savepositions(int num, int mode);
void savepositions_ioformat1(int num);
double second(void);
void set_softenings(void);
void set_sph_kernel(void);
void set_units(void);
void setup_smoothinglengths(void);

void sumup_large_ints(int n, int *src, long long *res);
void sumup_longs(int n, long long *src, long long *res);

void statistics(void);
double timediff(double t0, double t1);
void veldisp(void);
void veldisp_ensure_neighbours(int mode);

void gravity_tree_shortrange(void);


double get_hydrokick_factor(int time0, int time1);
double get_gravkick_factor(int time0, int time1);
double drift_integ(double a, void *param);
double gravkick_integ(double a, void *param);
double hydrokick_integ(double a, void *param);
void init_drift_table(void);
double get_drift_factor(int time0, int time1);


double measure_time(void);


/* on some DEC Alphas, the correct prototype for pow() is missing,
   even when math.h is included ! */

double pow(double, double);


void long_range_init(void);
void long_range_force(void);
void pm_init_periodic(void);
void pmforce_periodic(void);
void pm_init_regionsize(void);
void pm_init_nonperiodic(void);
int pmforce_nonperiodic(int grnr);

int pmpotential_nonperiodic(int grnr);
void pmpotential_periodic(void);

void readjust_timebase(double TimeMax_old, double TimeMax_new);

double enclosed_mass(double R);
void pm_setup_nonperiodic_kernel(void);

#ifdef CHEMISTRY
double dmax(double, double);
double dmin(double, double);
#endif

#ifdef LT_STELLAREVOLUTION
void fsolver_error_handler(const char *, const char *, int, int);
int get_Yset(int);

void init_SN(void);
int evolve_SN(int);
double INLINE_FUNC get_cost_SE(int);
int INLINE_FUNC get_chemstep(int, int, double*, double);
int INLINE_FUNC get_current_chem_bin(double, int);
int compare_steps(const void*, const void*);

void read_metals(void);

void init_clouds_cm(double*, double*, double, double, int, double*);

int write_eff_model(int);
int read_eff_model(int);

void read_SnIa_yields(void);
void read_SnII_yields(void);
void read_AGB_yields(void);

void read_metalcool_table(void);

void initialize_star_lifetimes(void);
double INLINE_FUNC get_age(double);

void recalculate_stellarevolution_vars(void *, int);
void recalc_eff_model();

void calculate_effective_yields(double, double, void*);
double calculate_FactorSN(double, double, void*);

double get_imf_params(double);
double INLINE_FUNC normalize_imf(double, double, void*);

double *get_meanZ(void);
void write_metallicity_stat(void);
void get_metals_sumcheck(int mode);
double INLINE_FUNC get_metallicity(int, int);
double INLINE_FUNC get_metallicity_solarunits(MyFloat);

double INLINE_FUNC get_entropy(int);

int INLINE_FUNC getindex(double*, int, int, double*, int*);

int INLINE_FUNC perturb(double*, double*);

#endif

#ifdef RADTRANSFER
int eddington_treeevaluate(int target, int mode, int *nexport, int *nsend_local);
void eddington(void);
int radtransfer_evaluate(int target, int mode, int *nexport, int *nsend_local);
void radiative_transfer(void);
void mean(void);
double evaluate(int target);
void set_simple_inits(void);
void simple_output(void);
void update_nHI(void);
int ngb_treefind_stars(MyDouble searchcenter[3], MyFloat hsml, int target, int *startnode, int mode, 
		       int *nexport, int *nsend_local);
int star_density_evaluate(int target, int mode, int *nexport, int *nsend_local);
void star_density(void);
#endif

#ifdef HALO_MODEL
void   read_halomodel(char *fname);
#endif

#if defined(VWINDS) && defined(GROUPFINDER)
void init_group_sfr(int);
void compute_group_sfr(void);
#endif

#ifdef OSAKA
// osaka_cooling.c //
void osaka_grackle_initialization(void);
double DoCooling_grackle(int target, double uin, double dt, double anow);
double GetCoolingTime_grackle(int target, double uin, double anow);
double GetTemperature_grackle(int target, double uin, double anow);
double GetPressure_grackle(int target, double uin, double anow);
double GetGamma_grackle(int target, double uin, double anow);
int grackle_initialization_wrapper(double unit_density, double unit_length, double unit_time, double unit_velocity);
int docooling_grackle_wrapper(double metallicity, double dt, double anow, double density, double *u, double *ne, double *HI, double *HII, double *HeI, double *HeII, double *HeIII, double *HM, double *H2I, double *H2II, double *DI, double *DII, double *HDI);
int getcoolingtime_grackle_wrapper(double metallicity, double anow, double density, double *cooling_time, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI);
int gettemperature_grackle_wrapper(double metallicity, double anow, double density, double *temperature, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI);
int getpressure_grackle_wrapper(double metallicity, double anow, double density, double *pressure, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI);
int getgamma_grackle_wrapper(double metallicity, double anow, double density, double *gamma, double u, double ne, double HI, double HII, double HeI, double HeII, double HeIII, double HM, double H2I, double H2II, double DI, double DII, double HDI);

// osaka_sfr.c //
double virial_density(double *rho_z, double ascale);

// osaka_CELib_yield.c //
void read_yield_data(struct CELIB_YIELD *CELib_yield, struct CELIB_YIELD_DIF *CELib_yield_dif);
void calc_snii_yield(int target, double snii_energy_table[][N_MAX_FEEDBACK], double snii_mass_released_table[][N_MAX_FEEDBACK], double snii_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK], double *snii_energy, double *snii_mass_released, double snii_metal_yield[], int *nfb, double *next_snii_explosion_time);
void calc_snia_yield(int target, double snia_energy_table[][N_MAX_FEEDBACK], double snia_mass_released_table[][N_MAX_FEEDBACK], double snia_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK], double *snia_energy, double *snia_mass_released, double snia_metal_yield[], int *nfb, double *next_snia_explosion_time);
void calc_agb_yield(int target, double agb_mass_released_table[][N_MAX_FEEDBACK], double agb_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK], double *agb_mass_released, double agb_metal_yield[], int *nfb, double *next_agb_explosion_time);
void calc_snii_yield_table(struct CELIB_YIELD CELib_yield, double snii_energy_table[][N_MAX_FEEDBACK], double snii_mass_released_table[][N_MAX_FEEDBACK], double snii_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK]);
void calc_snia_yield_table(struct CELIB_YIELD CELib_yield, double snia_energy_table[][N_MAX_FEEDBACK], double snia_mass_released_table[][N_MAX_FEEDBACK], double snia_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK]);
void calc_agb_yield_table(struct CELIB_YIELD CELib_yield, double agb_mass_released_table[][N_MAX_FEEDBACK], double agb_metal_yield_table[][N_YIELD_ELEMENT_CELIB][N_MAX_FEEDBACK]);
void calc_yield_differential(struct CELIB_YIELD CELib_yield, struct CELIB_YIELD_DIF *CELib_yield_dif);
void calc_instantaneous_snii_feedback_time(int target, int id_particle, double metallicity, struct CELIB_YIELD CELib_yield, struct CELIB_YIELD_DIF CELib_yield_dif, double *snii_explosion_time);
void calc_instantaneous_snia_feedback_time(int target, int id_particle, double metallicity, struct CELIB_YIELD CELib_yield, struct CELIB_YIELD_DIF CELib_yield_dif, double *snia_explosion_time);
void calc_instantaneous_agb_feedback_time(int target, int id_particle, double metallicity, struct CELIB_YIELD CELib_yield, struct CELIB_YIELD_DIF CELib_yield_dif, double *agb_explosion_time);
void calc_instantaneous_feedback_time(int id_particle, double metallicity, struct CELIB_YIELD CELib_yield, struct CELIB_YIELD_DIF CELib_yield_dif, double *snii_explosion_time, double *snia_explosion_time, double *agb_explosion_time);
void calc_instantaneous_snii_yield(double metallicity, struct CELIB_YIELD CELib_yield, double *snii_energy, double *snii_mass_released, double snii_metal_yield[]);
void calc_instantaneous_snia_yield(double metallicity, struct CELIB_YIELD CELib_yield, double *snia_energy, double *snia_mass_released, double snia_metal_yield[]);
void calc_instantaneous_agb_yield(double metallicity, struct CELIB_YIELD CELib_yield, double *agb_mass_released, double agb_metal_yield[]);

// osaka_star_density.c //
void esfb_star_density(void);
void snii_star_density(void);
void snia_star_density(void);
void agb_star_density(void);
int star_density_evaluate(int target, int mode, int fbmode, int *nexport, int *nsend_local);
void set_esfb_star_hsml_guess(void);
void set_snii_star_hsml_guess(void);
void set_snia_star_hsml_guess(void);
void set_agb_star_hsml_guess(void);

// osaka_esfb_feedback.c //
void osaka_esfb_feedback(void);
void esfb_feedback(void);
int esfb_feedback_evaluate(int target, int mode, int loop, int *nexport, int *nsend_local);
int ngb_treefind_esfb(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local);
void read_LyC_table(void);
void calc_HII_bubble_radius(int target);
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
int esfb_feedback_evaluate_gdw(int target, int mode, int loop, int *nexport, int *nsend_local);
void osaka_physical_value_assignment_esfb(int target, double mass, double esfb_energy, double position_star[], double position_gas[], struct Surface surface[]);
void osaka_calc_surface_area_weight_esfb(struct Surface surface[]);
void osaka_set_current_star_surface_esfb(double position_star[], struct Surface surface[]);
void osaka_search_particle_inside_surface_esfb(double position_star[], double position_gas[], struct Surface surface[]);
double osaka_inner_product_esfb(double aa[], double bb[]);
void osaka_cross_product_esfb(double aa[], double bb[], double cc[]);
void osaka_assign_surface_esfb(struct Surface surface_out[], struct Surface surface_in[]);
void osaka_update_surface_nparticle_esfb(struct Surface surface_out[], struct Surface surface_in[]);
void osaka_make_geodesic_dome_esfb(struct Surface surface[]);
void osaka_surface_refinement_esfb(struct Surface surface[]);
void osaka_set_icosahedron_vertex_esfb(struct Point point[]);
void osaka_set_icosahedron_surface_esfb(struct Surface surface[], struct Point point[]);
#endif

// osaka_snii_feedback.c //
void osaka_snii_feedback(void);
void snii_feedback(void);
int snii_feedback_evaluate(int target, int mode, int loop, int *nexport, int *nsend_local);
void snii_feedback_kick(void);
void snii_feedback_hot_bubble(void);
void snii_constant_wind_kick(void);
void calc_snii_shock_radius(int target);
double fz_SNII(double metallicity);
int ngb_treefind_snii(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local);
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
int snii_feedback_evaluate_gdw(int target, int mode, int loop, int *nexport, int *nsend_local);
void osaka_physical_value_assignment_snii(int target, double r, double rshock, double ejecta_momentum, double terminal_momentum, double snii_mass_released, double metal_mass_element[], double sniik_energy, double sniith_energy, double position_star[], double position_gas[], struct Surface surface[]);
void osaka_calc_surface_area_weight_snii(struct Surface surface[]);
void osaka_set_current_star_surface_snii(double position_star[], struct Surface surface[]);
void osaka_search_particle_inside_surface_snii(double position_star[], double position_gas[], struct Surface surface[]);
double osaka_inner_product_snii(double aa[], double bb[]);
void osaka_cross_product_snii(double aa[], double bb[], double cc[]);
void osaka_assign_surface_snii(struct Surface surface_out[], struct Surface surface_in[]);
void osaka_update_surface_nparticle_snii(struct Surface surface_out[], struct Surface surface_in[]);
void osaka_make_geodesic_dome_snii(struct Surface surface[]);
void osaka_surface_refinement_snii(struct Surface surface[]);
void osaka_set_icosahedron_vertex_snii(struct Point point[]);
void osaka_set_icosahedron_surface_snii(struct Surface surface[], struct Point point[]);
#endif

// osaka_snia_feedback.c //
void osaka_snia_feedback(void);
void snia_feedback(void);
int snia_feedback_evaluate(int target, int mode, int loop, int *nexport, int *nsend_local);
void snia_feedback_kick(void);
void snia_feedback_hot_bubble(void);
void snia_constant_wind_kick(void);
void calc_snia_shock_radius(int target);
double fz_SNIa(double metallicity);
int ngb_treefind_snia(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local);
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
int snia_feedback_evaluate_gdw(int target, int mode, int loop, int *nexport, int *nsend_local);
void osaka_physical_value_assignment_snia(int target, double r, double rshock, double ejecta_momentum, double terminal_momentum, double snia_mass_released, double metal_mass_element[], double sniak_energy, double sniath_energy, double position_star[], double position_gas[], struct Surface surface[]);
void osaka_calc_surface_area_weight_snia(struct Surface surface[]);
void osaka_set_current_star_surface_snia(double position_star[], struct Surface surface[]);
void osaka_search_particle_inside_surface_snia(double position_star[], double position_gas[], struct Surface surface[]);
double osaka_inner_product_snia(double aa[], double bb[]);
void osaka_cross_product_snia(double aa[], double bb[], double cc[]);
void osaka_assign_surface_snia(struct Surface surface_out[], struct Surface surface_in[]);
void osaka_update_surface_nparticle_snia(struct Surface surface_out[], struct Surface surface_in[]);
void osaka_make_geodesic_dome_snia(struct Surface surface[]);
void osaka_surface_refinement_snia(struct Surface surface[]);
void osaka_set_icosahedron_vertex_snia(struct Point point[]);
void osaka_set_icosahedron_surface_snia(struct Surface surface[], struct Point point[]);
#endif

// osaka_agb_feedback.c //
void osaka_agb_feedback(void);
void agb_feedback(void);
int agb_feedback_evaluate(int target, int mode, int loop, int *nexport, int *nsend_local);
int ngb_treefind_agb(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local);
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
int agb_feedback_evaluate_gdw(int target, int mode, int loop, int *nexport, int *nsend_local);
void osaka_physical_value_assignment_agb(int target, double agb_mass_released, double metal_mass_element[], double position_star[], double position_gas[], struct Surface surface[]);
void osaka_calc_surface_area_weight_agb(struct Surface surface[]);
void osaka_set_current_star_surface_agb(double position_star[], struct Surface surface[]);
void osaka_search_particle_inside_surface_agb(double position_star[], double position_gas[], struct Surface surface[]);
double osaka_inner_product_agb(double aa[], double bb[]);
void osaka_cross_product_agb(double aa[], double bb[], double cc[]);
void osaka_assign_surface_agb(struct Surface surface_out[], struct Surface surface_in[]);
void osaka_update_surface_nparticle_agb(struct Surface surface_out[], struct Surface surface_in[]);
void osaka_make_geodesic_dome_agb(struct Surface surface[]);
void osaka_surface_refinement_agb(struct Surface surface[]);
void osaka_set_icosahedron_vertex_agb(struct Point point[]);
void osaka_set_icosahedron_surface_agb(struct Surface surface[], struct Point point[]);
#endif

// osaka_dm_velocity_dispersion.c //
void osaka_dm_velocity_dispersion(void);
void calc_dm_velocity_dispersion(void);
int velocity_dispersion_evaluate(int target, int mode, int *nexport, int *nsend_local);
void set_dm_hsml_guess(void);
int ngb_treefind_nonbaryon(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local);

// osaka_utilities.c //
double a2t(double scalefactor);
void init_genrand(unsigned long s);
void init_by_array(unsigned long init_key[], int key_length);
unsigned long genrand_int32(void);
long genrand_int31(void);
double genrand_real1(void);
double genrand_real2(void);
double genrand_real3(void);
double genrand_res53(void);
#endif

