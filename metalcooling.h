void     read_metalcool_table_SD(char *filename);
double   CoolingRate_from_SD(double LogT, double Z);
double   Ne_from_SD(double LogT, double Z);
double   Z_to_FeH(double Z);
double   Y_from_Z(double Z);
double   Mz_from_Z(double Z);
double   mu_from_Z(double Z, double ne);
