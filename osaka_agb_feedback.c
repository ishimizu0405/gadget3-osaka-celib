#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <mpi.h>
#include "allvars.h"
#include "proto.h"

#ifdef OSAKA
#ifdef OSAKA_AGB

static struct agbmaindata_in
{
  int NodeList[NODELISTLENGTH];
  double Pos[3];
  double Hsml;
  double AGBNgbNum;
  double AGBNgbMass;
  double Masswk;
  double Mass;
  double InitialStarMass;
  double MassReleased;
  double Metals[N_YIELD_ELEMENT_CELIB];
  double AGBExplosionTime;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
  MyIDType IDStar;
  int FBflagAGB;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*AGBDataIn, *AGBDataGet;

static struct agbmaindata_out
{
  double AGBNgbNum;
  double AGBNgbMass;
  double Masswk;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*AGBDataResult, *AGBDataOut;

static struct MASSWK
{
  double Masswk;
  double RminFeedbackInteraction;
  MyIDType PID_Rmin;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[100];
#endif
}*MassWk;

void osaka_agb_feedback(void)
{
  set_agb_star_hsml_guess();
  agb_star_density();
  
  agb_feedback();
}

void agb_feedback(void)
{
  int i, j, k, ndone, ndone_flag, dummy;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  int nfb, nstar_local, nstar_total;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  double agb_mass_released, next_agb_explosion_time, mass_fraction;
  static double agb_metal_yield[N_YIELD_ELEMENT_CELIB];
  double agb_mass_released_local, agb_mass_released_total;
#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  struct Surface surface[N_SURFACE_TOTAL_LV1];
#endif

  if(ThisTask == 0)
    {
      printf("Start agb feedback...\n");
      fflush(stdout);
    }
    
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  for(i = FirstActiveParticle, agb_mass_released_local = 0.0, nstar_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagAGB < All.AGBEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(All.AGBEventNumber == 1)
            {
              if(stellarage > P[i].AGBExplosionTime)
                calc_instantaneous_agb_yield(P[i].Metallicity, CELib_yield, &agb_mass_released, agb_metal_yield);
              else
                agb_mass_released = 0.0;
            }
          else
            if(stellarage > P[i].AGBExplosionTime)
              calc_agb_yield(i, agb_mass_released_table, agb_metal_yield_table, &agb_mass_released, agb_metal_yield, &nfb, &next_agb_explosion_time);
            else
              agb_mass_released = 0.0;
              
          agb_mass_released_local += agb_mass_released;
          
          if(agb_mass_released_local > 0.0)
            nstar_local++;
        }
    }
    
  MPI_Allreduce(&agb_mass_released_local, &agb_mass_released_total, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&nstar_local, &nstar_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(agb_mass_released_total > 0.0)
    {
      if(ThisTask == 0)
        {
          printf("Time = %e, agb_mass_released_total = %e, nstar_total = %d\n", All.Time, agb_mass_released_total, nstar_total);
          fflush(stdout);
        }
    }
  else
    {
      if(ThisTask == 0)
        {
          printf("This loop is no AGB feedback...\n");
          fflush(stdout);
        }
        
      return;
    }
    
  // First loop : calculation for only wk //
  if(ThisTask == 0)
    {
      printf("Prepare distributing agb energy and metals...\n");
      fflush(stdout);
    }
    
  Ngblist = (int *) mymalloc(NumPart * sizeof(int));
  
  All.BunchSize = (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                    sizeof(struct agbmaindata_in) + sizeof(struct agbmaindata_out) + sizemax(sizeof(struct agbmaindata_in),
                      sizeof(struct agbmaindata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
  
  MassWk = (struct MASSWK *) mymalloc(NumPart * sizeof(struct MASSWK));
  
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
  osaka_make_geodesic_dome_agb(surface);
#endif
  
  i = FirstActiveParticle;
  
  do
    {
      for(j = 0; j < NTask; j++)
        {
          Send_count[j] = 0;
          Exportflag[j] = -1;
        }
        
      // do local particles and prepare export list //
      for(nexport = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagAGB < All.AGBEventNumber && stellarage > P[i].AGBExplosionTime)
            {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
              if(agb_feedback_evaluate(i, 0, 1, &nexport, Send_count) < 0)
                break;
#else
              osaka_assign_surface_agb(MassWk[i].surface, surface);
              osaka_set_current_star_surface_agb(P[i].Pos, MassWk[i].surface);
              
              if(agb_feedback_evaluate_gdw(i, 0, 1, &nexport, Send_count) < 0)
                break;
#endif
            }
        }
        
#ifdef MYSORT
      mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
      qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
      
      MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
      
      for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
        {
          Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
          nimport += Recv_count[j];
          
          if(j > 0)
            {
              Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
              Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
            }
        }
        
      AGBDataGet = (struct agbmaindata_in *) mymalloc(nimport * sizeof(struct agbmaindata_in));
      AGBDataIn = (struct agbmaindata_in *) mymalloc(nexport * sizeof(struct agbmaindata_in));
      
      // prepare particle data for export //
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          AGBDataIn[j].Pos[0] = P[place].Pos[0];
          AGBDataIn[j].Pos[1] = P[place].Pos[1];
          AGBDataIn[j].Pos[2] = P[place].Pos[2];
          AGBDataIn[j].Mass = P[place].Mass;
          AGBDataIn[j].InitialStarMass = P[place].InitialStarMass;
          AGBDataIn[j].Hsml = P[place].Hsml;
          AGBDataIn[j].AGBExplosionTime = P[place].AGBExplosionTime;
          AGBDataIn[j].RminFeedbackInteraction = MassWk[place].RminFeedbackInteraction;
          AGBDataIn[j].IDStar = P[place].ID;
          AGBDataIn[j].FBflagAGB = P[place].FBflagAGB;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
          osaka_assign_surface_agb(AGBDataIn[j].surface, surface);
          osaka_set_current_star_surface_agb(P[place].Pos, AGBDataIn[j].surface);
#endif

          memcpy(AGBDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
        }
        
      // exchange particle data //
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // get the particles //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&AGBDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct agbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&AGBDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct agbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&AGBDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct agbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, &AGBDataGet[Recv_offset[recvTask]], 
                      Recv_count[recvTask] * sizeof(struct agbmaindata_in), MPI_BYTE, recvTask, 
                        TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      myfree(AGBDataIn);
      
      AGBDataResult = (struct agbmaindata_out *) mymalloc(nimport * sizeof(struct agbmaindata_out));
      AGBDataOut = (struct agbmaindata_out *) mymalloc(nexport * sizeof(struct agbmaindata_out));
      
      // now do the particles that were sent to us //
      for(j = 0; j < nimport; j++)
        {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          agb_feedback_evaluate(j, 1, 1, &dummy, &dummy);
#else
          agb_feedback_evaluate_gdw(j, 1, 1, &dummy, &dummy);
#endif
        }
        
      if(i < 0)
        ndone_flag = 1;
      else
        ndone_flag = 0;
        
      MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
      
      // get the result // 
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // send the results //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&AGBDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct agbmaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&AGBDataOut[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct agbmaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&AGBDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct agbmaindata_out), 
                    MPI_BYTE, recvTask, TAG_DENS_B, &AGBDataOut[Send_offset[recvTask]],
                      Send_count[recvTask] * sizeof(struct agbmaindata_out), MPI_BYTE, recvTask, 
                        TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      // add the result to the local particles // 
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          P[place].AGBNgbNum += AGBDataOut[j].AGBNgbNum;
          P[place].AGBNgbMass += AGBDataOut[j].AGBNgbMass;
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          MassWk[place].Masswk += AGBDataOut[j].Masswk;
#else
          osaka_update_surface_nparticle_agb(MassWk[place].surface, AGBDataOut[j].surface);
#endif

          if(MassWk[place].RminFeedbackInteraction > AGBDataOut[j].RminFeedbackInteraction)
            {
              MassWk[place].RminFeedbackInteraction = AGBDataOut[j].RminFeedbackInteraction;
              MassWk[place].PID_Rmin = AGBDataOut[j].PID_Rmin;
            }
        }
        
      myfree(AGBDataOut);
      myfree(AGBDataResult);
      myfree(AGBDataGet);
    }
  while(ndone < NTask);
  
  for(i = FirstActiveParticle, nstar_local = 0; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagAGB < All.AGBEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
           
          if(stellarage > P[i].AGBExplosionTime)
            {
              if(P[i].AGBNgbNum == 0.0)
                nstar_local++;
            }
        }
    }
    
  MPI_Allreduce(&nstar_local, &nstar_total, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(nstar_total > 0)
    {
      if(ThisTask == 0)
        {
          printf("*--- %d star particles can not find interaction gas particles. ---*\n", nstar_total);
          printf("*--- AGB metal assign nearest gas particle. ---*\n");
          fflush(stdout);
        }
    }
    
  // Second loop : calculation for real part of AGB feedback //
  if(ThisTask == 0)
    {
      printf("Distributing agb metals...\n");
      fflush(stdout);
    }
    
  i = FirstActiveParticle;
  
  do
    {
      for(j = 0; j < NTask; j++)
        {
          Send_count[j] = 0;
          Exportflag[j] = -1;
        }
        
      // do local particles and prepare export list //
      for(nexport = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagAGB < All.AGBEventNumber && stellarage > P[i].AGBExplosionTime)
            {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
              if(agb_feedback_evaluate(i, 0, 2, &nexport, Send_count) < 0)
                break;
#else
              osaka_calc_surface_area_weight_agb(MassWk[i].surface);
              
              if(agb_feedback_evaluate_gdw(i, 0, 2, &nexport, Send_count) < 0)
                break;
#endif
            }
        }
        
#ifdef MYSORT
      mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
      qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
      
      MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
      
      for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
        {
          Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
          nimport += Recv_count[j];
          
          if(j > 0)
            {
              Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
              Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
            }
        }
        
      AGBDataGet = (struct agbmaindata_in *) mymalloc(nimport * sizeof(struct agbmaindata_in));
      AGBDataIn = (struct agbmaindata_in *) mymalloc(nexport * sizeof(struct agbmaindata_in));
      
      // prepare particle data for export //
      for(j = 0; j < nexport; j++)
        {
          place = DataIndexTable[j].Index;
          AGBDataIn[j].Pos[0] = P[place].Pos[0];
          AGBDataIn[j].Pos[1] = P[place].Pos[1];
          AGBDataIn[j].Pos[2] = P[place].Pos[2];
          AGBDataIn[j].Mass = P[place].Mass;
          AGBDataIn[j].InitialStarMass = P[place].InitialStarMass;
          AGBDataIn[j].Hsml = P[place].Hsml;
          AGBDataIn[j].AGBNgbNum = P[place].AGBNgbNum;
          AGBDataIn[j].AGBNgbMass = P[place].AGBNgbMass;
          AGBDataIn[j].Masswk = MassWk[place].Masswk;
          AGBDataIn[j].AGBExplosionTime = P[place].AGBExplosionTime;
          AGBDataIn[j].RminFeedbackInteraction = MassWk[place].RminFeedbackInteraction;
          AGBDataIn[j].PID_Rmin = MassWk[place].PID_Rmin;
          AGBDataIn[j].IDStar = P[place].ID;
          AGBDataIn[j].FBflagAGB = P[place].FBflagAGB;
#ifdef OSAKA_GEODESIC_DOME_WEIGHT
          osaka_assign_surface_agb(AGBDataIn[j].surface, MassWk[place].surface);
#endif

#ifdef OSAKA_CELIB
          if(All.AGBEventNumber == 1)
            calc_instantaneous_agb_yield(P[place].Metallicity, CELib_yield, &agb_mass_released, agb_metal_yield);
          else
            calc_agb_yield(place, agb_mass_released_table, agb_metal_yield_table, &agb_mass_released, agb_metal_yield, &nfb, &next_agb_explosion_time);
            
          AGBDataIn[j].MassReleased = P[place].InitialStarMass * agb_mass_released;
          
          for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
            AGBDataIn[j].Metals[k] = P[place].InitialStarMass * agb_metal_yield[k];
#endif
          memcpy(AGBDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
        }
        
      // exchange particle data //
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
          sendTask = ThisTask;
          recvTask = ThisTask ^ ngrp;
          
          if(recvTask < NTask)
            {
              if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                  // get the particles //
#ifdef USE_ISEND_IRECV
                  MPI_Isend(&AGBDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct agbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                  MPI_Irecv(&AGBDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct agbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                  MPI_Waitall(2,mpireq,stat);
#else
                  MPI_Sendrecv(&AGBDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct agbmaindata_in), 
                    MPI_BYTE, recvTask, TAG_DENS_A, &AGBDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct agbmaindata_in), 
                      MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
      myfree(AGBDataIn);
      
      // now do the particles that were sent to us //
      for(j = 0; j < nimport; j++)
        {
#ifndef OSAKA_GEODESIC_DOME_WEIGHT
          agb_feedback_evaluate(j, 1, 2, &dummy, &dummy);
#else
          agb_feedback_evaluate_gdw(j, 1, 2, &dummy, &dummy);
#endif
        }
        
      if(i < 0)
        ndone_flag = 1;
      else
        ndone_flag = 0;
        
      MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
      
      myfree(AGBDataGet);
    }
  while(ndone < NTask);
  
  myfree(MassWk);
  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 4 && P[i].FBflagAGB < All.AGBEventNumber)
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(stellarage > P[i].AGBExplosionTime)
            {
#ifdef OSAKA_CELIB
              if(All.AGBEventNumber == 1)
                {
                  calc_instantaneous_agb_yield(P[i].Metallicity, CELib_yield, &agb_mass_released, agb_metal_yield);
                  P[i].FBflagAGB = All.AGBEventNumber;
                }
              else
                {
                  calc_agb_yield(i, agb_mass_released_table, agb_metal_yield_table, &agb_mass_released, agb_metal_yield, &nfb, &next_agb_explosion_time);
                  P[i].FBflagAGB += nfb;
                  P[i].AGBExplosionTime = next_agb_explosion_time;
                }
                
              mass_fraction = 1.0 - agb_mass_released * P[i].InitialStarMass / P[i].Mass;
              P[i].Mass -= P[i].InitialStarMass * agb_mass_released;
              
              for(j = 0; j < N_YIELD_ELEMENT_CELIB; j++)
                P[i].Metals[j] *= mass_fraction;
#endif
            }
        }
    }
}

int agb_feedback_evaluate(int target, int mode, int loop, int *nexport, int *nsend_local)
{
  int i, j, k, n, nfb, fbflagAGB;
  int startnode, numngb_inbox, listindex;
  double h, h2, hinv, hinv3, hinv4, dx, dy, dz, r, r2, u, mass_j, wk, dwk;
  double ascale, a3inv, hubble, time, factor_time;
  double pos[3], mass, initmass, ngb, ngbmass, masswk, rminfeedbackinteraction;
  double agb_explosion_time;
  double agb_mass_released, metal_mass_element[N_YIELD_ELEMENT_CELIB], next_agb_explosion_time;
  double theta, phi, norm, dir[3];
  static double agb_metal_yield[N_YIELD_ELEMENT_CELIB];
  MyIDType pid_rmin, IDStar;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (All.Time * All.Time * All.Time);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
      factor_time = 1.0;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      factor_time = All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  if(mode == 0)
    {
      pos[0] = P[target].Pos[0];
      pos[1] = P[target].Pos[1];
      pos[2] = P[target].Pos[2];
      h = P[target].Hsml;
      mass = P[target].Mass;
      initmass = P[target].InitialStarMass;
      agb_explosion_time = P[target].AGBExplosionTime;
      
      if(loop == 1)
        {
          masswk = ngb = ngbmass = 0.0;
          rminfeedbackinteraction = 1.0e30;
        }
        
      if(loop == 2)
        {
          masswk = MassWk[target].Masswk;
          ngb = P[target].AGBNgbNum;
          ngbmass = P[target].AGBNgbMass;
          rminfeedbackinteraction = MassWk[target].RminFeedbackInteraction;
          pid_rmin = MassWk[target].PID_Rmin;
          fbflagAGB = P[target].FBflagAGB;
          IDStar = P[target].ID;
          
#ifdef OSAKA_CELIB
          if(All.AGBEventNumber == 1)
            calc_instantaneous_agb_yield(P[target].Metallicity, CELib_yield, &agb_mass_released, agb_metal_yield);
          else
            calc_agb_yield(target, agb_mass_released_table, agb_metal_yield_table, &agb_mass_released, agb_metal_yield, &nfb, &next_agb_explosion_time);
            
          agb_mass_released = P[target].InitialStarMass * agb_mass_released;
          
          for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
            metal_mass_element[i] = P[target].InitialStarMass * agb_metal_yield[i];
#endif
        }
    }
  else
    {
      pos[0] = AGBDataGet[target].Pos[0];
      pos[1] = AGBDataGet[target].Pos[1];
      pos[2] = AGBDataGet[target].Pos[2];
      h = AGBDataGet[target].Hsml;
      mass = AGBDataGet[target].Mass;
      initmass = AGBDataGet[target].InitialStarMass;
      agb_explosion_time = AGBDataGet[target].AGBExplosionTime;
      
      if(loop == 1)
        {
          masswk = ngb = ngbmass = 0.0;
          rminfeedbackinteraction = AGBDataGet[target].RminFeedbackInteraction;
        }
        
      if(loop == 2)
        {
          masswk = AGBDataGet[target].Masswk;
          ngb = AGBDataGet[target].AGBNgbNum;
          ngbmass = AGBDataGet[target].AGBNgbMass;
          rminfeedbackinteraction = AGBDataGet[target].RminFeedbackInteraction;
          pid_rmin = AGBDataGet[target].PID_Rmin;
          fbflagAGB = AGBDataGet[target].FBflagAGB;
          IDStar = AGBDataGet[target].IDStar;
          
#ifdef OSAKA_CELIB
          agb_mass_released = AGBDataGet[target].MassReleased;
          
          for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
            metal_mass_element[i] = AGBDataGet[target].Metals[i];
#endif
        }
    }
    
  h2 = h * h;
  hinv = 1.0 / h;
#ifndef  TWODIMS
  hinv3 = hinv * hinv * hinv;
#else
  hinv3 = hinv * hinv / boxSize_Z;
#endif
  hinv4 = hinv3 * hinv;
  
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = AGBDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  if(loop == 2 && ngb == 0.0)
    {
      if(rminfeedbackinteraction < 0.0)
        return 0;
        
      while(startnode >= 0)
        {
          while(startnode >= 0)
            {
              numngb_inbox = ngb_treefind_agb(pos, h, target, &startnode, mode, nexport, nsend_local);
              
              if(numngb_inbox < 0)
                return -1;
                
              for(n = 0; n < numngb_inbox; n++)
                {
                  j = Ngblist[n];
                  
                  if(pid_rmin == P[j].ID)
                    {
                      dx = pos[0] - P[j].Pos[0];
                      dy = pos[1] - P[j].Pos[1];
                      dz = pos[2] - P[j].Pos[2];
                      
                      //  now find the closest image in the given box size  //
#ifdef PERIODIC
                      if(dx > boxHalf_X)
                        dx -= boxSize_X;
                      if(dx < -boxHalf_X)
                        dx += boxSize_X;
                      if(dy > boxHalf_Y)
                        dy -= boxSize_Y;
                      if(dy < -boxHalf_Y)
                        dy += boxSize_Y;
                      if(dz > boxHalf_Z)
                        dz -= boxSize_Z;
                      if(dz < -boxHalf_Z)
                        dz += boxSize_Z;
#endif
                      r2 = dx * dx + dy * dy + dz * dz;
                      r = sqrt(r2);
                      
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      SphP[j].MassStar = initmass;
                      P[j].AGBNgbMass = P[j].Mass;
                      
#ifdef OSAKA_CELIB
                      P[j].Mass += agb_mass_released;
                      
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals[k] += metal_mass_element[k];
                        
                      P[j].Metallicity = (P[j].Mass - P[j].Metals[0] - P[j].Metals[1]) / P[j].Mass;
                      
#ifdef OSAKA_METAL_ELEMENT_TRACE_AGB
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals_AGB[k] += metal_mass_element[k];
#endif
#endif

#ifdef OSAKA_AGB_OUTFLOW
#ifdef OSAKA_ISOTROPIC_WIND
                      theta = acos(2 * get_random_number(IDStar + P[j].ID + fbflagAGB + 3) - 1);
                      phi = 2 * M_PI * get_random_number(IDStar + P[j].ID + fbflagAGB + 4);
                      
                      dir[0] = sin(theta) * cos(phi);
                      dir[1] = sin(theta) * sin(phi);
                      dir[2] = cos(theta);
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagAGB + 5) < 0.5)
                        norm = -norm;
                        
#elif OSAKA_RADIAL_DIRECTION_WIND
                      dir[0] = P[j].Pos[0] - pos[0];
                      dir[1] = P[j].Pos[1] - pos[1];
                      dir[2] = P[j].Pos[2] - pos[2];
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
#else
                      dir[0] = P[j].g.GravAccel[1] * P[j].Vel[2] - P[j].g.GravAccel[2] * P[j].Vel[1];
                      dir[1] = P[j].g.GravAccel[2] * P[j].Vel[0] - P[j].g.GravAccel[0] * P[j].Vel[2];
                      dir[2] = P[j].g.GravAccel[0] * P[j].Vel[1] - P[j].g.GravAccel[1] * P[j].Vel[0];
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagAGB + 5) < 0.5)
                        norm = -norm;
#endif

                      if(norm != 0)
                        {
                          for(k = 0; k < 3; k++)
                            dir[k] /= norm;
                            
                          for(k = 0; k < 3; k++)
                            {
                              P[j].Vel[k] += All.AGBFBVelocity * ascale * dir[k];
                              SphP[j].VelPred[k] += All.AGBFBVelocity * ascale * dir[k];
                            }
                        }
#endif
                        
                      if(mode == 0)
                        MassWk[target].RminFeedbackInteraction = - 1.0;
                        
                      return 0;
                    }
                }// end of a for-loop for ngb_inbox
            }
            
          if(mode == 1)
            {
              listindex++;
              
              if(listindex < NODELISTLENGTH)
                {
                  startnode = AGBDataGet[target].NodeList[listindex];
                  
                  if(startnode >= 0)
                    startnode = Nodes[startnode].u.d.nextnode;// open it //
                }
            }
        }
    }
    
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = AGBDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  while(startnode >= 0)
    {
      while(startnode >= 0)
        {
          numngb_inbox = ngb_treefind_agb(pos, h, target, &startnode, mode, nexport, nsend_local);
          
          if(numngb_inbox < 0)
            return -1;
            
          for(n = 0; n < numngb_inbox; n++)
            {
              j = Ngblist[n];
              
              dx = pos[0] - P[j].Pos[0];
              dy = pos[1] - P[j].Pos[1];
              dz = pos[2] - P[j].Pos[2];
              
              //  now find the closest image in the given box size  //
#ifdef PERIODIC
              if(dx > boxHalf_X)
                dx -= boxSize_X;
              if(dx < -boxHalf_X)
                dx += boxSize_X;
              if(dy > boxHalf_Y)
                dy -= boxSize_Y;
              if(dy < -boxHalf_Y)
                dy += boxSize_Y;
              if(dz > boxHalf_Z)
                dz -= boxSize_Z;
              if(dz < -boxHalf_Z)
                dz += boxSize_Z;
#endif
              r2 = dx * dx + dy * dy + dz * dz;
              
              if(r2 < h2)
                {
                  r = sqrt(r2);
                  u = r * hinv;
                  
                  if(loop == 1)
                    if(rminfeedbackinteraction > r)
                      {
                        rminfeedbackinteraction = r;
                        pid_rmin = P[j].ID;
                      }
                      
#if !defined(QUINTIC_KERNEL) && !defined(WENDLAND_C4_KERNEL)
                  if(u < 0.5)
                    wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1.0) * u * u);
                  else
                    wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
#elif QUINTIC_KERNEL
                  if(0.0 <= u && u < ONETHIRD)
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) + 15.0 * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u);
                  else if(ONETHIRD <= u && u < TWOTHIRD)
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u);
                  else
                    wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u);
                    
                  wk *= KERNEL_NORM * hinv3;
#elif WENDLAND_C4_KERNEL
                  wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 + 6.0 * u + 35.0 / 3 * u * u);
                  wk *= KERNEL_NORM * hinv3;
#endif

                  mass_j = P[j].Mass;
                  
                  if(loop == 1)
                    {
                      masswk += FLT(mass_j * wk);
                      ngb += 1.0;
                      ngbmass += mass_j;
                    }
                    
                  if(loop == 2)
                    {
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      SphP[j].MassStar = initmass;
                      P[j].AGBNgbMass = ngbmass;
                      
#ifdef OSAKA_CELIB
                      P[j].Mass += FLT(agb_mass_released * mass_j * wk / masswk);
                      
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals[k] += FLT(metal_mass_element[k] * mass_j * wk / masswk);
                        
                      P[j].Metallicity = (P[j].Mass - P[j].Metals[0] - P[j].Metals[1]) / P[j].Mass;
                      
#ifdef OSAKA_METAL_ELEMENT_TRACE_AGB
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals_AGB[k] += FLT(metal_mass_element[k] * mass_j * wk / masswk);
#endif
#endif

#ifdef OSAKA_AGB_OUTFLOW
#ifdef OSAKA_ISOTROPIC_WIND
                      theta = acos(2 * get_random_number(IDStar + P[j].ID + fbflagAGB + 3) - 1);
                      phi = 2 * M_PI * get_random_number(IDStar + P[j].ID + fbflagAGB + 4);
                      
                      dir[0] = sin(theta) * cos(phi);
                      dir[1] = sin(theta) * sin(phi);
                      dir[2] = cos(theta);
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagAGB + 5) < 0.5)
                        norm = -norm;
                        
#elif OSAKA_RADIAL_DIRECTION_WIND
                      dir[0] = P[j].Pos[0] - pos[0];
                      dir[1] = P[j].Pos[1] - pos[1];
                      dir[2] = P[j].Pos[2] - pos[2];
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
#else
                      dir[0] = P[j].g.GravAccel[1] * P[j].Vel[2] - P[j].g.GravAccel[2] * P[j].Vel[1];
                      dir[1] = P[j].g.GravAccel[2] * P[j].Vel[0] - P[j].g.GravAccel[0] * P[j].Vel[2];
                      dir[2] = P[j].g.GravAccel[0] * P[j].Vel[1] - P[j].g.GravAccel[1] * P[j].Vel[0];
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagAGB + 5) < 0.5)
                        norm = -norm;
#endif

                      if(norm != 0)
                        {
                          for(k = 0; k < 3; k++)
                            dir[k] /= norm;
                            
                          for(k = 0; k < 3; k++)
                            {
                              P[j].Vel[k] += All.AGBFBVelocity * ascale * dir[k];
                              SphP[j].VelPred[k] += All.AGBFBVelocity * ascale * dir[k];
                            }
                        }
#endif
                    }
                }
            }// end of a for-loop for ngb_inbox
        }
        
      if(mode == 1)
        {
          listindex++;
          
          if(listindex < NODELISTLENGTH)
            {
              startnode = AGBDataGet[target].NodeList[listindex];
              
              if(startnode >= 0)
                startnode = Nodes[startnode].u.d.nextnode;// open it //
            }
        }
    }
    
  if(loop == 1)
    {
      if(mode == 0)
        {
          MassWk[target].Masswk = masswk;
          P[target].AGBNgbNum = ngb;
          P[target].AGBNgbMass = ngbmass;
          MassWk[target].RminFeedbackInteraction = rminfeedbackinteraction;
          MassWk[target].PID_Rmin = pid_rmin;
        }
      else
        {
          AGBDataResult[target].Masswk = masswk;
          AGBDataResult[target].AGBNgbNum = ngb;
          AGBDataResult[target].AGBNgbMass = ngbmass;
          AGBDataResult[target].RminFeedbackInteraction = rminfeedbackinteraction;
          AGBDataResult[target].PID_Rmin = pid_rmin;
        }
    }
    
  return 0;
}

int ngb_treefind_agb(double searchcenter[3], double hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local)
{
  int numngb, no, p, task, nexport_save;
  struct NODE *current;
  double dx, dy, dz, dist;
#ifdef PERIODIC
  MyDouble xtmp;
#endif
  
  nexport_save = *nexport;
  numngb = 0;
  no = *startnode;
  
  while(no >= 0)
    {
      if(no < All.MaxPart)// single particle //
        {
          p = no;
          no = Nextnode[no];
          
          if(P[p].Type != 0)
            continue;
            
          if(P[p].Ti_current != All.Ti_Current)
            drift_particle(p, All.Ti_Current);
            
          dist = hsml;
#ifdef PERIODIC
          dx = NGB_PERIODIC_LONG_X((double) P[p].Pos[0] - searchcenter[0]);
          
          if(dx > dist)
            continue;
            
          dy = NGB_PERIODIC_LONG_Y((double) P[p].Pos[1] - searchcenter[1]);
          
          if(dy > dist)
            continue;
            
          dz = NGB_PERIODIC_LONG_Z((double) P[p].Pos[2] - searchcenter[2]);
          
          if(dz > dist)
                continue;
#else
          dx = (double) P[p].Pos[0] - searchcenter[0];
          
          if(dx > dist)
            continue;
            
          dy = (double) P[p].Pos[1] - searchcenter[1];
          
          if(dy > dist)
            continue;
            
          dz = (double) P[p].Pos[2] - searchcenter[2];
          
          if(dz > dist)
            continue;
#endif
          if(dx * dx + dy * dy + dz * dz > dist * dist)
            continue;
            
          Ngblist[numngb++] = p;
        }
      else
        {
          if(no >= All.MaxPart + MaxNodes)// pseudo particle //
            {
              if(mode == 1)
                endrun(12312);
                
              if(target >= 0)// if no target is given, export will not occur //
                {
                  if(Exportflag[task = DomainTask[no - (All.MaxPart + MaxNodes)]] != target)
                    {
                      Exportflag[task] = target;
                      Exportnodecount[task] = NODELISTLENGTH;
                    }
                    
                  if(Exportnodecount[task] == NODELISTLENGTH)
                    {
                      if(*nexport >= All.BunchSize)
                        {
                          *nexport = nexport_save;
                          
                          if(nexport_save == 0)
                            endrun(13004);// in this case, the buffer is too small to process even a single particle //
                            
                          for(task = 0; task < NTask; task++)
                            nsend_local[task] = 0;
                            
                          for(no = 0; no < nexport_save; no++)
                            nsend_local[DataIndexTable[no].Task]++;
                            
                          return -1;
                        }
                        
                      Exportnodecount[task] = 0;
                      Exportindex[task] = *nexport;
                      DataIndexTable[*nexport].Task = task;
                      DataIndexTable[*nexport].Index = target;
                      DataIndexTable[*nexport].IndexGet = *nexport;
                      *nexport = *nexport + 1;
                      nsend_local[task]++;
                    }
                    
                  DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]++] =
                  DomainNodeIndex[no - (All.MaxPart + MaxNodes)];
                  
                  if(Exportnodecount[task] < NODELISTLENGTH)
                    DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]] = -1;
                }
                
              no = Nextnode[no - MaxNodes];
              continue;
            }
            
          current = &Nodes[no];
          
          if(mode == 1)
            {
              // we reached a top-level node again, which means that we are done with the branch //
              if(current->u.d.bitflags & (1 << BITFLAG_TOPLEVEL))
                {
                  *startnode = -1;
                  return numngb;
                }
            }
            
          if(current->Ti_current != All.Ti_Current)
            force_drift_node(no, All.Ti_Current);
            
          no = current->u.d.sibling;// in case the node can be discarded //
          
          dist = hsml + 0.5 * current->len;
#ifdef PERIODIC	  
          dx = NGB_PERIODIC_LONG_X((double) current->center[0] - searchcenter[0]);
          
          if(dx > dist)
            continue;
            
          dy = NGB_PERIODIC_LONG_Y((double) current->center[1] - searchcenter[1]);
          
          if(dy > dist)
            continue;
            
          dz = NGB_PERIODIC_LONG_Z((double) current->center[2] - searchcenter[2]);
          
          if(dz > dist)
            continue;
#else
          dx = (double) current->center[0] - searchcenter[0];
          
          if(dx > dist)
            continue;
            
          dy = (double) current->center[1] - searchcenter[1];
          
          if(dy > dist)
            continue;
            
          dz = (double) current->center[2] - searchcenter[2];
          
          if(dz > dist)
            continue;
#endif
          // now test against the minimal sphere enclosing everything //
          dist += FACT1 * (double) current->len;
          
          if(dx * dx + dy * dy + dz * dz > dist * dist)
            continue;
            
          no = current->u.d.nextnode;// ok, we need to open the node //
        }
    }
    
  *startnode = -1;
  
  return numngb;
}

#ifdef OSAKA_GEODESIC_DOME_WEIGHT
int agb_feedback_evaluate_gdw(int target, int mode, int loop, int *nexport, int *nsend_local)
{
  int i, j, k, n, nfb, fbflagAGB;
  int startnode, numngb_inbox, listindex;
  double h, h2, dx, dy, dz, r, r2, mass_j;
  double ascale, a3inv, hubble, time, factor_time;
  double pos[3], mass, initmass, ngb, ngbmass, rminfeedbackinteraction;
  double agb_explosion_time, dir[3], norm;
  double agb_mass_released, metal_mass_element[N_YIELD_ELEMENT_CELIB], next_agb_explosion_time;
  static double agb_metal_yield[N_YIELD_ELEMENT_CELIB];
  MyIDType pid_rmin, IDStar;
  struct Surface surface[N_SURFACE_TOTAL_LV1];
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (All.Time * All.Time * All.Time);
      time = a2t(All.Time);
      hubble = All.HubbleParam;
      factor_time = 1.0;
    }
  else
    {
      ascale = a3inv = hubble = 1.0;
      factor_time = All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  if(mode == 0)
    {
      osaka_assign_surface_agb(surface, MassWk[target].surface);
      pos[0] = P[target].Pos[0];
      pos[1] = P[target].Pos[1];
      pos[2] = P[target].Pos[2];
      h = P[target].Hsml;
      mass = P[target].Mass;
      initmass = P[target].InitialStarMass;
      agb_explosion_time = P[target].AGBExplosionTime;
      
      if(loop == 1)
        {
          ngb = ngbmass = 0.0;
          rminfeedbackinteraction = 1.0e30;
        }
        
      if(loop == 2)
        {
          ngb = P[target].AGBNgbNum;
          ngbmass = P[target].AGBNgbMass;
          rminfeedbackinteraction = MassWk[target].RminFeedbackInteraction;
          pid_rmin = MassWk[target].PID_Rmin;
          fbflagAGB = P[target].FBflagAGB;
          
#ifdef OSAKA_CELIB
          if(All.AGBEventNumber == 1)
            calc_instantaneous_agb_yield(P[target].Metallicity, CELib_yield, &agb_mass_released, agb_metal_yield);
          else
            calc_agb_yield(target, agb_mass_released_table, agb_metal_yield_table, &agb_mass_released, agb_metal_yield, &nfb, &next_agb_explosion_time);
            
          agb_mass_released = P[target].InitialStarMass * agb_mass_released;
          
          for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
            metal_mass_element[i] = P[target].InitialStarMass * agb_metal_yield[i];
#endif
        }
    }
  else
    {
      osaka_assign_surface_agb(surface, AGBDataGet[target].surface);
      pos[0] = AGBDataGet[target].Pos[0];
      pos[1] = AGBDataGet[target].Pos[1];
      pos[2] = AGBDataGet[target].Pos[2];
      h = AGBDataGet[target].Hsml;
      mass = AGBDataGet[target].Mass;
      initmass = AGBDataGet[target].InitialStarMass;
      agb_explosion_time = AGBDataGet[target].AGBExplosionTime;
      
      if(loop == 1)
        {
          ngb = ngbmass = 0.0;
          rminfeedbackinteraction = AGBDataGet[target].RminFeedbackInteraction;
        }
        
      if(loop == 2)
        {
          ngb = AGBDataGet[target].AGBNgbNum;
          ngbmass = AGBDataGet[target].AGBNgbMass;
          rminfeedbackinteraction = AGBDataGet[target].RminFeedbackInteraction;
          pid_rmin = AGBDataGet[target].PID_Rmin;
          fbflagAGB = AGBDataGet[target].FBflagAGB;
          
#ifdef OSAKA_CELIB
          agb_mass_released = AGBDataGet[target].MassReleased;
          
          for(i = 0; i < N_YIELD_ELEMENT_CELIB; i++)
            metal_mass_element[i] = AGBDataGet[target].Metals[i];
#endif
        }
    }
    
  h2 = h * h;
  
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = AGBDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  if(loop == 2 && ngb == 0.0)
    {
      if(rminfeedbackinteraction < 0.0)
        return 0;
        
      while(startnode >= 0)
        {
          while(startnode >= 0)
            {
              numngb_inbox = ngb_treefind_agb(pos, h, target, &startnode, mode, nexport, nsend_local);
              
              if(numngb_inbox < 0)
                return -1;
                
              for(n = 0; n < numngb_inbox; n++)
                {
                  j = Ngblist[n];
                  
                  if(pid_rmin == P[j].ID)
                    {
                      dx = pos[0] - P[j].Pos[0];
                      dy = pos[1] - P[j].Pos[1];
                      dz = pos[2] - P[j].Pos[2];
                      
                      //  now find the closest image in the given box size  //
#ifdef PERIODIC
                      if(dx > boxHalf_X)
                        dx -= boxSize_X;
                      if(dx < -boxHalf_X)
                        dx += boxSize_X;
                      if(dy > boxHalf_Y)
                        dy -= boxSize_Y;
                      if(dy < -boxHalf_Y)
                        dy += boxSize_Y;
                      if(dz > boxHalf_Z)
                        dz -= boxSize_Z;
                      if(dz < -boxHalf_Z)
                        dz += boxSize_Z;
#endif
                      r2 = dx * dx + dy * dy + dz * dz;
                      r = sqrt(r2);
                      
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      SphP[j].MassStar = initmass;
                      P[j].AGBNgbMass = P[j].Mass;
                      
#ifdef OSAKA_CELIB
                      P[j].Mass += agb_mass_released;
                      
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals[k] += metal_mass_element[k];
                        
                      P[j].Metallicity = (P[j].Mass - P[j].Metals[0] - P[j].Metals[1]) / P[j].Mass;
                      
#ifdef OSAKA_METAL_ELEMENT_TRACE_AGB
                      for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                        P[j].Metals_AGB[k] += metal_mass_element[k];
#endif
#endif

#ifdef OSAKA_AGB_OUTFLOW
#ifdef OSAKA_ISOTROPIC_WIND
                      theta = acos(2 * get_random_number(IDStar + P[j].ID + fbflagAGB + 3) - 1);
                      phi = 2 * M_PI * get_random_number(IDStar + P[j].ID + fbflagAGB + 4);
                      
                      dir[0] = sin(theta) * cos(phi);
                      dir[1] = sin(theta) * sin(phi);
                      dir[2] = cos(theta);
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagAGB + 5) < 0.5)
                        norm = -norm;
                        
#elif OSAKA_RADIAL_DIRECTION_WIND
                      dir[0] = P[j].Pos[0] - pos[0];
                      dir[1] = P[j].Pos[1] - pos[1];
                      dir[2] = P[j].Pos[2] - pos[2];
                      
#ifdef PERIODIC
                      if(dir[0] > boxHalf_X)
                        dir[0] -= boxSize_X;
                      if(dir[0] < -boxHalf_X)
                        dir[0] += boxSize_X;
                      if(dir[1] > boxHalf_Y)
                        dir[1] -= boxSize_Y;
                      if(dir[1] < -boxHalf_Y)
                        dir[1] += boxSize_Y;
                      if(dir[2] > boxHalf_Z)
                        dir[2] -= boxSize_Z;
                      if(dir[2] < -boxHalf_Z)
                        dir[2] += boxSize_Z;
#endif

                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
#else
                      dir[0] = P[j].g.GravAccel[1] * P[j].Vel[2] - P[j].g.GravAccel[2] * P[j].Vel[1];
                      dir[1] = P[j].g.GravAccel[2] * P[j].Vel[0] - P[j].g.GravAccel[0] * P[j].Vel[2];
                      dir[2] = P[j].g.GravAccel[0] * P[j].Vel[1] - P[j].g.GravAccel[1] * P[j].Vel[0];
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagAGB + 5) < 0.5)
                        norm = -norm;
#endif

                      if(norm != 0)
                        {
                          for(k = 0; k < 3; k++)
                            dir[k] /= norm;
                            
                          for(k = 0; k < 3; k++)
                            {
                              P[j].Vel[k] += All.AGBFBVelocity * ascale * dir[k];
                              SphP[j].VelPred[k] += All.AGBFBVelocity * ascale * dir[k];
                            }
                        }
#endif
                        
                      if(mode == 0)
                        MassWk[target].RminFeedbackInteraction = - 1.0;
                        
                      return 0;
                    }
                }// end of a for-loop for ngb_inbox
            }
            
          if(mode == 1)
            {
              listindex++;
              
              if(listindex < NODELISTLENGTH)
                {
                  startnode = AGBDataGet[target].NodeList[listindex];
                  
                  if(startnode >= 0)
                    startnode = Nodes[startnode].u.d.nextnode;// open it //
                }
            }
        }
    }
    
  if(mode == 0)
    {
      listindex = 0;
      startnode = All.MaxPart;// root node //
    }
  else
    {
      listindex = 0;
      startnode = AGBDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  while(startnode >= 0)
    {
      while(startnode >= 0)
        {
          numngb_inbox = ngb_treefind_agb(pos, h, target, &startnode, mode, nexport, nsend_local);
          
          if(numngb_inbox < 0)
            return -1;
            
          for(n = 0; n < numngb_inbox; n++)
            {
              j = Ngblist[n];
              
              dx = pos[0] - P[j].Pos[0];
              dy = pos[1] - P[j].Pos[1];
              dz = pos[2] - P[j].Pos[2];
              
              //  now find the closest image in the given box size  //
#ifdef PERIODIC
              if(dx > boxHalf_X)
                dx -= boxSize_X;
              if(dx < -boxHalf_X)
                dx += boxSize_X;
              if(dy > boxHalf_Y)
                dy -= boxSize_Y;
              if(dy < -boxHalf_Y)
                dy += boxSize_Y;
              if(dz > boxHalf_Z)
                dz -= boxSize_Z;
              if(dz < -boxHalf_Z)
                dz += boxSize_Z;
#endif
              r2 = dx * dx + dy * dy + dz * dz;
              
              if(r2 < h2)
                {
                  r = sqrt(r2);
                  
                  if(loop == 1)
                    if(rminfeedbackinteraction > r)
                      {
                        rminfeedbackinteraction = r;
                        pid_rmin = P[j].ID;
                      }
                      
                  mass_j = P[j].Mass;
                  
                  if(loop == 1)
                    {
                      osaka_search_particle_inside_surface_agb(pos, P[j].Pos, surface);
                      ngb += 1.0;
                      ngbmass += mass_j;
                    }
                    
                  if(loop == 2)
                    {
                      osaka_physical_value_assignment_agb(j, agb_mass_released, metal_mass_element, pos, P[j].Pos, surface);
                      
                      SphP[j].PosStar[0] = pos[0];
                      SphP[j].PosStar[1] = pos[1];
                      SphP[j].PosStar[2] = pos[2];
                      SphP[j].MassStar = initmass;
                      P[j].AGBNgbMass = ngbmass;
                      
#ifdef OSAKA_AGB_OUTFLOW
#ifdef OSAKA_ISOTROPIC_WIND
                      theta = acos(2 * get_random_number(IDStar + P[j].ID + fbflagAGB + 3) - 1);
                      phi = 2 * M_PI * get_random_number(IDStar + P[j].ID + fbflagAGB + 4);
                      
                      dir[0] = sin(theta) * cos(phi);
                      dir[1] = sin(theta) * sin(phi);
                      dir[2] = cos(theta);
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagAGB + 5) < 0.5)
                        norm = -norm;
                        
#elif OSAKA_RADIAL_DIRECTION_WIND
                      dir[0] = P[j].Pos[0] - pos[0];
                      dir[1] = P[j].Pos[1] - pos[1];
                      dir[2] = P[j].Pos[2] - pos[2];
                      
#ifdef PERIODIC
                      if(dir[0] > boxHalf_X)
                        dir[0] -= boxSize_X;
                      if(dir[0] < -boxHalf_X)
                        dir[0] += boxSize_X;
                      if(dir[1] > boxHalf_Y)
                        dir[1] -= boxSize_Y;
                      if(dir[1] < -boxHalf_Y)
                        dir[1] += boxSize_Y;
                      if(dir[2] > boxHalf_Z)
                        dir[2] -= boxSize_Z;
                      if(dir[2] < -boxHalf_Z)
                        dir[2] += boxSize_Z;
#endif
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
#else
                      dir[0] = P[j].g.GravAccel[1] * P[j].Vel[2] - P[j].g.GravAccel[2] * P[j].Vel[1];
                      dir[1] = P[j].g.GravAccel[2] * P[j].Vel[0] - P[j].g.GravAccel[0] * P[j].Vel[2];
                      dir[2] = P[j].g.GravAccel[0] * P[j].Vel[1] - P[j].g.GravAccel[1] * P[j].Vel[0];
                      
                      for(k = 0, norm = 0; k < 3; k++)
                        norm += dir[k] * dir[k];
                        
                      norm = sqrt(norm);
                      
                      if(get_random_number(IDStar + P[j].ID + fbflagAGB + 5) < 0.5)
                        norm = -norm;
#endif

                      if(norm != 0)
                        {
                          for(k = 0; k < 3; k++)
                            dir[k] /= norm;
                            
                          for(k = 0; k < 3; k++)
                            {
                              P[j].Vel[k] += All.AGBFBVelocity * ascale * dir[k];
                              SphP[j].VelPred[k] += All.AGBFBVelocity * ascale * dir[k];
                            }
                        }
#endif
                    }
                }
            }// end of a for-loop for ngb_inbox
        }
        
      if(mode == 1)
        {
          listindex++;
          
          if(listindex < NODELISTLENGTH)
            {
              startnode = AGBDataGet[target].NodeList[listindex];
              
              if(startnode >= 0)
                startnode = Nodes[startnode].u.d.nextnode;// open it //
            }
        }
    }
    
  if(loop == 1)
    {
      if(mode == 0)
        {
          osaka_assign_surface_agb(MassWk[target].surface, surface);
          P[target].AGBNgbNum = ngb;
          P[target].AGBNgbMass = ngbmass;
          MassWk[target].RminFeedbackInteraction = rminfeedbackinteraction;
          MassWk[target].PID_Rmin = pid_rmin;
        }
      else
        {
          osaka_assign_surface_agb(AGBDataResult[target].surface, surface);
          AGBDataResult[target].AGBNgbNum = ngb;
          AGBDataResult[target].AGBNgbMass = ngbmass;
          AGBDataResult[target].RminFeedbackInteraction = rminfeedbackinteraction;
          AGBDataResult[target].PID_Rmin = pid_rmin;
        }
    }
    
  return 0;
}

void osaka_physical_value_assignment_agb(int target, double agb_mass_released, double metal_mass_element[], double position_star[], double position_gas[], struct Surface surface[])
{
  int i, j, k;
  double v01[3], v02[3], vsp[3], vs0[3], re2[3], te1[3], re2e1, t, u, v;
#ifdef PERIODIC
  double vsptemp[3];
#endif

  for(k = 0; k < 3; k++)
    {
#ifdef PERIODIC
      vsptemp[0] = position_gas[k] - position_star[k];
      vsptemp[1] = vsptemp[0] + header.BoxSize;
      vsptemp[2] = vsptemp[0] - header.BoxSize;
      
      if(fabs(vsptemp[0]) < fabs(vsptemp[1]))
        {
          if(fabs(vsptemp[0]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[0];
          else
            vsp[k] = vsptemp[2];
        }
      else
        {
          if(fabs(vsptemp[1]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[1];
          else
            vsp[k] = vsptemp[2];
        }
#else
      vsp[k] = position_gas[k] - position_star[k];
#endif
    }
    
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle == 0)
        continue;
      if(surface[i].nparticle == 1)
        {
          for(k = 0; k < 3; k++)
            {
              v01[k] = surface[i].vertex[1][k] - surface[i].vertex[0][k];
              v02[k] = surface[i].vertex[2][k] - surface[i].vertex[0][k];
              vs0[k] = position_star[k] - surface[i].vertex[0][k];
            }
            
          osaka_cross_product_agb(vsp, v02, re2);
          osaka_cross_product_agb(vs0, v01, te1);
          re2e1 = osaka_inner_product_agb(re2, v01);
          
          t = osaka_inner_product_agb(te1, v02) / re2e1;
          u = osaka_inner_product_agb(re2, vs0) / re2e1;
          v = osaka_inner_product_agb(te1, vsp) / re2e1;
          
          if(t < 0.0)
            continue;
            
          if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
            {
#ifdef OSAKA_CELIB
              P[target].Mass += agb_mass_released * surface[i].occupancy;
              
              for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                P[target].Metals[k] += metal_mass_element[k] * surface[i].occupancy;
                
              P[target].Metallicity = (P[target].Mass - P[target].Metals[0] - P[target].Metals[1]) / P[target].Mass;
              
#ifdef OSAKA_METAL_ELEMENT_TRACE_AGB
              for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                P[target].Metals_AGB[k] += metal_mass_element[k] * surface[i].occupancy;
#endif
#endif

              return;
            }
        }
      else
        {
          for(j = 0; j < 4; j++)
            {
              if(surface[surface[i].children[j]].nparticle == 0)
                continue;
                
              for(k = 0; k < 3; k++)
                {
                  v01[k] = surface[surface[i].children[j]].vertex[1][k] - surface[surface[i].children[j]].vertex[0][k];
                  v02[k] = surface[surface[i].children[j]].vertex[2][k] - surface[surface[i].children[j]].vertex[0][k];
                  vs0[k] = position_star[k] - surface[surface[i].children[j]].vertex[0][k];
                }
                
              osaka_cross_product_agb(vsp, v02, re2);
              osaka_cross_product_agb(vs0, v01, te1);
              re2e1 = osaka_inner_product_agb(re2, v01);
              
              t = osaka_inner_product_agb(te1, v02) / re2e1;
              u = osaka_inner_product_agb(re2, vs0) / re2e1;
              v = osaka_inner_product_agb(te1, vsp) / re2e1;
              
              if(t < 0.0)
                continue;
                
              if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
                {
#ifdef OSAKA_CELIB
                  P[target].Mass += agb_mass_released * surface[surface[i].children[j]].occupancy;
                  
                  for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                    P[target].Metals[k] += metal_mass_element[k] * surface[surface[i].children[j]].occupancy;
                    
                  P[target].Metallicity = (P[target].Mass - P[target].Metals[0] - P[target].Metals[1]) / P[target].Mass;
                  
#ifdef OSAKA_METAL_ELEMENT_TRACE_AGB
                  for(k = 0; k < N_YIELD_ELEMENT_CELIB; k++)
                    P[target].Metals_AGB[k] += metal_mass_element[k] * surface[surface[i].children[j]].occupancy;
#endif
#endif

                  return;
                }
            }
        }
    }
}

void osaka_calc_surface_area_weight_agb(struct Surface surface[])
{
  int i, j, k, l;
  int nin, surface_id;
  double vectord[3], vectordc[3], division_factor, occupancyp, occupancyc, occupancyd, occupancydc, occupancy_leftp, occupancy_leftc, norm;
  
  occupancyp = 1.0 / (double) N_ICOSAHEDRON_SURFACE;
  occupancyc = 0.25 * occupancyp;
  occupancy_leftp = 0.0;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    if(surface[i].nparticle == 0)
        surface[i].occupancy = 0.0;
        
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle == 1)
        continue;
        
      if(surface[i].nparticle > 1)
        {
          vectordc[0] = vectordc[1] = vectordc[2] = 0.0;
          
          for(j = 0, nin = 0; j < 4; j++)
            {
              surface_id = surface[i].children[j];
              
              if(surface[surface_id].nparticle == 0)
                {
                  vectordc[0] += surface[surface_id].vector[0];
                  vectordc[1] += surface[surface_id].vector[1];
                  vectordc[2] += surface[surface_id].vector[2];
                  nin++;
                }
            }
            
          division_factor = 1.0 / (double) (NDIVISION - nin);
          occupancydc = occupancyc * (double) nin * division_factor;
          vectordc[0] *= division_factor;
          vectordc[1] *= division_factor;
          vectordc[2] *= division_factor;
          
          for(j = 0; j < 4; j++)
            {
              surface_id = surface[i].children[j];
              
              if(surface[surface_id].nparticle > 0)
                {
                  surface[surface_id].occupancy += occupancydc;
                  surface[surface_id].vector[0] += vectordc[0];
                  surface[surface_id].vector[1] += vectordc[1];
                  surface[surface_id].vector[2] += vectordc[2];
                }
            }
        }
      else if(surface[i].nparticle == 0)
        {
          for(j = 0, nin = 0; j < 3; j++)
            {
              surface_id = surface[i].neighbour[j];
              
              if(surface[surface_id].nparticle > 0)
                nin++;
            }
            
           if(nin > 0)
             {
               division_factor = 1.0 / (double) nin;
               occupancyd = occupancyp * division_factor;
               vectord[0] = surface[i].vector[0] * division_factor;
               vectord[1] = surface[i].vector[1] * division_factor;
               vectord[2] = surface[i].vector[2] * division_factor;
               
               for(j = 0; j < 3; j++)
                 {
                   surface_id = surface[i].neighbour[j];
                   
                   if(surface[surface_id].nparticle > 0)
                     {
                       surface[surface_id].occupancy += occupancyd;
                       surface[surface_id].vector[0] += vectord[0];
                       surface[surface_id].vector[1] += vectord[1];
                       surface[surface_id].vector[2] += vectord[2];
                       
                       if(surface[surface_id].nparticle > 1)
                         {
                           for(k = 0, nin = 0; k < 4; k++)
                             {
                               surface_id = surface[surface[i].neighbour[j]].children[k];
                               
                               if(surface[surface_id].nparticle > 0)
                                 nin++;
                             }
                             
                           division_factor = 1.0 / (double) nin;
                           occupancydc = occupancyd * division_factor;
                           vectordc[0] = vectord[0] * division_factor;
                           vectordc[1] = vectord[1] * division_factor;
                           vectordc[2] = vectord[2] * division_factor;
                           
                           for(k = 0; k < 4; k++)
                             {
                               surface_id = surface[surface[i].neighbour[j]].children[k];
                               
                               if(surface[surface_id].nparticle > 0)
                                 {
                                   surface[surface_id].occupancy += occupancydc;
                                   surface[surface_id].vector[0] += vectordc[0];
                                   surface[surface_id].vector[1] += vectordc[1];
                                   surface[surface_id].vector[2] += vectordc[2];
                                 }
                             }
                         }
                     }
                 }
             }
           else
             {
               for(j = 0, nin = 0; j < 3; j++)
                 for(k = 0; k < 3; k++)
                   {
                     surface_id = surface[surface[i].neighbour[j]].neighbour[k];
                     
                     if(surface[surface_id].nparticle > 0)
                       nin++;
                   }
                   
               if(nin == 0)
                 occupancy_leftp += occupancyp;
                 
               if(nin > 0)
                 {
                   division_factor = 1.0 / (double) nin;
                   occupancyd = occupancyp * division_factor;
                   vectord[0] = surface[i].vector[0] * division_factor;
                   vectord[1] = surface[i].vector[1] * division_factor;
                   vectord[2] = surface[i].vector[2] * division_factor;
                   
                   for(j = 0; j < 3; j++)
                     for(k = 0; k < 3; k++)
                       {
                         surface_id = surface[surface[i].neighbour[j]].neighbour[k];
                         
                         if(surface[surface_id].nparticle > 0)
                           {
                             surface[surface_id].occupancy += occupancyd;
                             surface[surface_id].vector[0] += vectord[0];
                             surface[surface_id].vector[1] += vectord[1];
                             surface[surface_id].vector[2] += vectord[2];
                             
                             if(surface[surface_id].nparticle > 1)
                               {
                                 for(l = 0, nin = 0; l < 4; l++)
                                   {
                                     surface_id = surface[surface[surface[i].neighbour[j]].neighbour[k]].children[l];
                                     
                                     if(surface[surface_id].nparticle > 0)
                                       nin++;
                                   }
                                   
                                 division_factor = 1.0 / (double) nin;
                                 occupancydc = occupancyd * division_factor;
                                 vectordc[0] = vectord[0] * division_factor;
                                 vectordc[1] = vectord[1] * division_factor;
                                 vectordc[2] = vectord[2] * division_factor;
                                 
                                 for(l = 0; l < 4; l++)
                                   {
                                     surface_id = surface[surface[surface[i].neighbour[j]].neighbour[k]].children[l];
                                     
                                     if(surface[surface_id].nparticle > 0)
                                       {
                                         surface[surface_id].occupancy += occupancydc;
                                         surface[surface_id].vector[0] += vectordc[0];
                                         surface[surface_id].vector[1] += vectordc[1];
                                         surface[surface_id].vector[2] += vectordc[2];
                                       }
                                   }
                               }
                           }
                       }
                 }
             }
        }
    }
    
  for(i = 0, nin = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    if(surface[i].nparticle > 0)
      nin++;
      
  occupancy_leftp /= (double) nin;
  
  for(i = 0, nin = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      if(surface[i].nparticle > 0)
        surface[i].occupancy += occupancy_leftp;
        
      if(surface[i].nparticle > 1)
        {
          for(j = 0, nin = 0; j < 4; j++)
            if(surface[surface[i].children[j]].nparticle > 0)
              nin++;
              
          occupancy_leftc = occupancy_leftp / (double) nin;
          
          for(j = 0, nin = 0; j < 4; j++)
            if(surface[surface[i].children[j]].nparticle > 0)
              surface[surface[i].children[j]].occupancy += occupancy_leftc;
        }
    }
    
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    if(surface[i].nparticle > 0)
      {
        norm = 0.0;
        norm += surface[i].vector[0] * surface[i].vector[0] + surface[i].vector[1] * surface[i].vector[1] + surface[i].vector[2] * surface[i].vector[2];
        norm = sqrt(norm);
        
        surface[i].vector[0] /= norm;
        surface[i].vector[1] /= norm;
        surface[i].vector[2] /= norm;
      }
    else
      {
        surface[i].vector[0] = 0.0;
        surface[i].vector[1] = 0.0;
        surface[i].vector[2] = 0.0;
      }
      
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    if(surface[i].nparticle > 1)
      for(j = 0; j < 4; j++)
        if(surface[surface[i].children[j]].nparticle > 1)
          surface[surface[i].children[j]].occupancy /= (double) surface[surface[i].children[j]].nparticle;
}

void osaka_set_current_star_surface_agb(double position_star[], struct Surface surface[])
{
  int i, j, k;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    for(j = 0; j < 3; j++)
      for(k = 0; k < 3; k++)
        surface[i].vertex[j][k] += position_star[k];
}

void osaka_search_particle_inside_surface_agb(double position_star[], double position_gas[], struct Surface surface[])
{
  int i, j, k;
  double v01[3], v02[3], vsp[3], vs0[3], re2[3], te1[3], re2e1, t, u, v;
#ifdef PERIODIC
  double vsptemp[3];
#endif

  for(k = 0; k < 3; k++)
    {
#ifdef PERIODIC
      vsptemp[0] = position_gas[k] - position_star[k];
      vsptemp[1] = vsptemp[0] + header.BoxSize;
      vsptemp[2] = vsptemp[0] - header.BoxSize;
      
      if(fabs(vsptemp[0]) < fabs(vsptemp[1]))
        {
          if(fabs(vsptemp[0]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[0];
          else
            vsp[k] = vsptemp[2];
        }
      else
        {
          if(fabs(vsptemp[1]) < fabs(vsptemp[2]))
            vsp[k] = vsptemp[1];
          else
            vsp[k] = vsptemp[2];
        }
#else
      vsp[k] = position_gas[k] - position_star[k];
#endif
    }
    
  for(i = 0; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      for(k = 0; k < 3; k++)
        {
          v01[k] = surface[i].vertex[1][k] - surface[i].vertex[0][k];
          v02[k] = surface[i].vertex[2][k] - surface[i].vertex[0][k];
          vs0[k] = position_star[k] - surface[i].vertex[0][k];
        }
        
      osaka_cross_product_agb(vsp, v02, re2);
      osaka_cross_product_agb(vs0, v01, te1);
      re2e1 = osaka_inner_product_agb(re2, v01);
      
      t = osaka_inner_product_agb(te1, v02) / re2e1;
      u = osaka_inner_product_agb(re2, vs0) / re2e1;
      v = osaka_inner_product_agb(te1, vsp) / re2e1;
      
      if(t < 0.0)
        continue;
        
      if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
        {
          surface[i].nparticle++;
          
          for(j = 0; j < 4; j++)
            {
              for(k = 0; k < 3; k++)
                {
                  v01[k] = surface[surface[i].children[j]].vertex[1][k] - surface[surface[i].children[j]].vertex[0][k];
                  v02[k] = surface[surface[i].children[j]].vertex[2][k] - surface[surface[i].children[j]].vertex[0][k];
                  vs0[k] = position_star[k] - surface[surface[i].children[j]].vertex[0][k];
                }
                
              osaka_cross_product_agb(vsp, v02, re2);
              osaka_cross_product_agb(vs0, v01, te1);
              re2e1 = osaka_inner_product_agb(re2, v01);
              
              t = osaka_inner_product_agb(te1, v02) / re2e1;
              u = osaka_inner_product_agb(re2, vs0) / re2e1;
              v = osaka_inner_product_agb(te1, vsp) / re2e1;
              
              if(t < 0.0)
                continue;
                
              if((0.0 <= u && u <= 1.0) && (0.0 <= v && v <= 1.0) && (0.0 <= u + v && u + v <= 1.0))
                {
                  surface[surface[i].children[j]].nparticle++;
                  break;
                }
            }
        }
    }
}

double osaka_inner_product_agb(double aa[], double bb[])
{
  return aa[0] * bb[0] + aa[1] * bb[1] + aa[2] * bb[2];
}

void osaka_cross_product_agb(double aa[], double bb[], double cc[])
{
  cc[0] = aa[1] * bb[2] - aa[2] * bb[1];
  cc[1] = aa[2] * bb[0] - aa[0] * bb[2];
  cc[2] = aa[0] * bb[1] - aa[1] * bb[0];
}

void osaka_assign_surface_agb(struct Surface surface_out[], struct Surface surface_in[])
{
  int i;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    surface_out[i] = surface_in[i];
}

void osaka_update_surface_nparticle_agb(struct Surface surface_out[], struct Surface surface_in[])
{
  int i;
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    surface_out[i].nparticle += surface_in[i].nparticle;
}

void osaka_make_geodesic_dome_agb(struct Surface surface[])
{
  int i, k;
  double norm;
  struct Point point[N_ICOSAHEDRON_VERTEX];
  
  osaka_set_icosahedron_vertex_agb(point);
  osaka_set_icosahedron_surface_agb(surface, point);
  osaka_surface_refinement_agb(surface);
  
  for(i = 0; i < N_SURFACE_TOTAL_LV1; i++)
    {
      for(k = 0; k < 3; k++)
        {
          norm = surface[i].vertex[k][0] * surface[i].vertex[k][0] + surface[i].vertex[k][1] * surface[i].vertex[k][1] + surface[i].vertex[k][2] * surface[i].vertex[k][2];
          norm = sqrt(norm);
          surface[i].vertex[k][0] /= norm;
          surface[i].vertex[k][1] /= norm;
          surface[i].vertex[k][2] /= norm;
        }
        
      for(k = 0, norm = 0.0; k < 3; k++)
        {
          surface[i].vector[k] = (surface[i].vertex[0][k] + surface[i].vertex[1][k] + surface[i].vertex[2][k]) / 3.0;
          norm += surface[i].vector[k] * surface[i].vector[k];
        }
        
      norm = sqrt(norm);
      surface[i].vector[0] /= norm;
      surface[i].vector[1] /= norm;
      surface[i].vector[2] /= norm;
      surface[i].nparticle = 0;
      
      /*for(k = 0; k < 3; k++)
        {
          surface[i].vertex[k][0] *= header.BoxSize;
          surface[i].vertex[k][1] *= header.BoxSize;
          surface[i].vertex[k][2] *= header.BoxSize;
        }*/
    }
}

void osaka_surface_refinement_agb(struct Surface surface[])
{
  int i, k, istart, iend;
  double middle_point[3][3];
  
  istart = 0;
  iend = N_ICOSAHEDRON_SURFACE;
  
  for(i = istart; i < iend; i++)
    {
      for(k = 0; k < 4; k++)
        {
          surface[surface[i].children[k]].id = surface[i].children[k];
          surface[surface[i].children[k]].parent = surface[i].id;
          surface[surface[i].children[k]].occupancy = 0.25* surface[i].occupancy;
          
          surface[surface[i].children[k]].children[0] = - 1;
          surface[surface[i].children[k]].children[1] = - 1;
          surface[surface[i].children[k]].children[2] = - 1;
          surface[surface[i].children[k]].children[3] = - 1;
        }
        
      surface[surface[i].children[0]].neighbour[0] = surface[surface[i].children[1]].id;
      surface[surface[i].children[0]].neighbour[1] = surface[surface[i].children[2]].id;
      surface[surface[i].children[0]].neighbour[2] = surface[surface[i].children[3]].id;
      
      surface[surface[i].children[1]].neighbour[0] = surface[surface[i].children[2]].id;
      surface[surface[i].children[1]].neighbour[1] = surface[surface[i].children[3]].id;
      surface[surface[i].children[1]].neighbour[2] = surface[surface[i].children[0]].id;
      
      surface[surface[i].children[2]].neighbour[0] = surface[surface[i].children[3]].id;
      surface[surface[i].children[2]].neighbour[1] = surface[surface[i].children[0]].id;
      surface[surface[i].children[2]].neighbour[2] = surface[surface[i].children[1]].id;
      
      surface[surface[i].children[3]].neighbour[0] = surface[surface[i].children[0]].id;
      surface[surface[i].children[3]].neighbour[1] = surface[surface[i].children[1]].id;
      surface[surface[i].children[3]].neighbour[2] = surface[surface[i].children[2]].id;
      
      for(k = 0; k < 3; k++)
        {
          middle_point[0][k] = 0.5 * (surface[i].vertex[0][k] + surface[i].vertex[1][k]);
          middle_point[1][k] = 0.5 * (surface[i].vertex[1][k] + surface[i].vertex[2][k]);
          middle_point[2][k] = 0.5 * (surface[i].vertex[2][k] + surface[i].vertex[0][k]);
          
          surface[surface[i].children[0]].vertex[0][k] = surface[i].vertex[0][k];
          surface[surface[i].children[0]].vertex[1][k] = middle_point[0][k];
          surface[surface[i].children[0]].vertex[2][k] = middle_point[2][k];
          
          surface[surface[i].children[1]].vertex[0][k] = surface[i].vertex[1][k];
          surface[surface[i].children[1]].vertex[1][k] = middle_point[1][k];
          surface[surface[i].children[1]].vertex[2][k] = middle_point[0][k];
          
          surface[surface[i].children[2]].vertex[0][k] = surface[i].vertex[2][k];
          surface[surface[i].children[2]].vertex[1][k] = middle_point[2][k];
          surface[surface[i].children[2]].vertex[2][k] = middle_point[1][k];
          
          surface[surface[i].children[3]].vertex[0][k] = middle_point[0][k];
          surface[surface[i].children[3]].vertex[1][k] = middle_point[1][k];
          surface[surface[i].children[3]].vertex[2][k] = middle_point[2][k];
        }
    }
}

void osaka_set_icosahedron_vertex_agb(struct Point point[])
{
  int i , k;
  double golden_ratio, norm;
  
  golden_ratio = 0.5 * (1.0 + sqrt(5));
  
  point[0].x[0] = 1.0;
  point[0].x[1] = golden_ratio;
  point[0].x[2] = 0.0;
  
  point[1].x[0] = - 1.0;
  point[1].x[1] = golden_ratio;
  point[1].x[2] = 0.0;
  
  point[2].x[0] = 1.0;
  point[2].x[1] = - golden_ratio;
  point[2].x[2] = 0.0;
  
  point[3].x[0] = - 1.0;
  point[3].x[1] = - golden_ratio;
  point[3].x[2] = 0.0;
  
  point[4].x[0] = 0.0;
  point[4].x[1] = 1.0;
  point[4].x[2] = golden_ratio;
  
  point[5].x[0] = 0.0;
  point[5].x[1] = - 1.0;
  point[5].x[2] = golden_ratio;
  
  point[6].x[0] = 0.0;
  point[6].x[1] = 1.0;
  point[6].x[2] = - golden_ratio;
  
  point[7].x[0] = 0.0;
  point[7].x[1] = - 1.0;
  point[7].x[2] = - golden_ratio;
  
  point[8].x[0] = golden_ratio;
  point[8].x[1] = 0.0;
  point[8].x[2] = 1.0;
  
  point[9].x[0] = golden_ratio;
  point[9].x[1] = 0.0;
  point[9].x[2] = - 1.0;
  
  point[10].x[0] = - golden_ratio;
  point[10].x[1] = 0.0;
  point[10].x[2] = 1.0;
  
  point[11].x[0] = - golden_ratio;
  point[11].x[1] = 0.0;
  point[11].x[2] = - 1.0;
  
  norm = sqrt(1.0 + golden_ratio * golden_ratio);
  
  for(i = 0; i < N_ICOSAHEDRON_VERTEX; i++)
    for(k = 0; k < 3; k++)
      point[i].x[k] /= norm;
}

void osaka_set_icosahedron_surface_agb(struct Surface surface[], struct Point point[])
{
  int i, k, children_id_offset;
  double occupancy;
  
  occupancy = 1.0 / (double) N_ICOSAHEDRON_SURFACE;
  
  for(i = 0, children_id_offset = N_ICOSAHEDRON_SURFACE; i < N_ICOSAHEDRON_SURFACE; i++)
    {
      surface[i].parent = - 1;
      surface[i].id = i;
      surface[i].children[0] = children_id_offset + 0;
      surface[i].children[1] = children_id_offset + 1;
      surface[i].children[2] = children_id_offset + 2;
      surface[i].children[3] = children_id_offset + 3;
      children_id_offset += NDIVISION;
      surface[i].occupancy = occupancy;
    }
    
  for(k = 0; k < 3; k++)
    {
      surface[0].vertex[0][k] = point[0].x[k];
      surface[0].vertex[1][k] = point[1].x[k];
      surface[0].vertex[2][k] = point[4].x[k];
      surface[0].vector[k] = (surface[0].vertex[0][k] + surface[0].vertex[1][k] + surface[0].vertex[2][k]) / 3.0;
      
      surface[1].vertex[0][k] = point[1].x[k];
      surface[1].vertex[1][k] = point[4].x[k];
      surface[1].vertex[2][k] = point[10].x[k];
      surface[1].vector[k] = (surface[1].vertex[0][k] + surface[1].vertex[1][k] + surface[1].vertex[2][k]) / 3.0;
      
      surface[2].vertex[0][k] = point[4].x[k];
      surface[2].vertex[1][k] = point[5].x[k];
      surface[2].vertex[2][k] = point[10].x[k];
      surface[2].vector[k] = (surface[2].vertex[0][k] + surface[2].vertex[1][k] + surface[2].vertex[2][k]) / 3.0;
      
      surface[3].vertex[0][k] = point[4].x[k];
      surface[3].vertex[1][k] = point[5].x[k];
      surface[3].vertex[2][k] = point[8].x[k];
      surface[3].vector[k] = (surface[3].vertex[0][k] + surface[3].vertex[1][k] + surface[3].vertex[2][k]) / 3.0;
      
      surface[4].vertex[0][k] = point[0].x[k];
      surface[4].vertex[1][k] = point[4].x[k];
      surface[4].vertex[2][k] = point[8].x[k];
      surface[4].vector[k] = (surface[4].vertex[0][k] + surface[4].vertex[1][k] + surface[4].vertex[2][k]) / 3.0;
      
      surface[5].vertex[0][k] = point[0].x[k];
      surface[5].vertex[1][k] = point[1].x[k];
      surface[5].vertex[2][k] = point[6].x[k];
      surface[5].vector[k] = (surface[5].vertex[0][k] + surface[5].vertex[1][k] + surface[5].vertex[2][k]) / 3.0;
      
      surface[6].vertex[0][k] = point[1].x[k];
      surface[6].vertex[1][k] = point[6].x[k];
      surface[6].vertex[2][k] = point[11].x[k];
      surface[6].vector[k] = (surface[6].vertex[0][k] + surface[6].vertex[1][k] + surface[6].vertex[2][k]) / 3.0;
      
      surface[7].vertex[0][k] = point[1].x[k];
      surface[7].vertex[1][k] = point[10].x[k];
      surface[7].vertex[2][k] = point[11].x[k];
      surface[7].vector[k] = (surface[7].vertex[0][k] + surface[7].vertex[1][k] + surface[7].vertex[2][k]) / 3.0;
      
      surface[8].vertex[0][k] = point[3].x[k];
      surface[8].vertex[1][k] = point[10].x[k];
      surface[8].vertex[2][k] = point[11].x[k];
      surface[8].vector[k] = (surface[8].vertex[0][k] + surface[8].vertex[1][k] + surface[8].vertex[2][k]) / 3.0;
      
      surface[9].vertex[0][k] = point[3].x[k];
      surface[9].vertex[1][k] = point[5].x[k];
      surface[9].vertex[2][k] = point[10].x[k];
      surface[9].vector[k] = (surface[9].vertex[0][k] + surface[9].vertex[1][k] + surface[9].vertex[2][k]) / 3.0;
      
      surface[10].vertex[0][k] = point[2].x[k];
      surface[10].vertex[1][k] = point[3].x[k];
      surface[10].vertex[2][k] = point[5].x[k];
      surface[10].vector[k] = (surface[10].vertex[0][k] + surface[10].vertex[1][k] + surface[10].vertex[2][k]) / 3.0;
      
      surface[11].vertex[0][k] = point[2].x[k];
      surface[11].vertex[1][k] = point[5].x[k];
      surface[11].vertex[2][k] = point[8].x[k];
      surface[11].vector[k] = (surface[11].vertex[0][k] + surface[11].vertex[1][k] + surface[11].vertex[2][k]) / 3.0;
      
      surface[12].vertex[0][k] = point[2].x[k];
      surface[12].vertex[1][k] = point[8].x[k];
      surface[12].vertex[2][k] = point[9].x[k];
      surface[12].vector[k] = (surface[12].vertex[0][k] + surface[12].vertex[1][k] + surface[12].vertex[2][k]) / 3.0;
      
      surface[13].vertex[0][k] = point[0].x[k];
      surface[13].vertex[1][k] = point[8].x[k];
      surface[13].vertex[2][k] = point[9].x[k];
      surface[13].vector[k] = (surface[13].vertex[0][k] + surface[13].vertex[1][k] + surface[13].vertex[2][k]) / 3.0;
      
      surface[14].vertex[0][k] = point[0].x[k];
      surface[14].vertex[1][k] = point[6].x[k];
      surface[14].vertex[2][k] = point[9].x[k];
      surface[14].vector[k] = (surface[14].vertex[0][k] + surface[14].vertex[1][k] + surface[14].vertex[2][k]) / 3.0;
      
      surface[15].vertex[0][k] = point[6].x[k];
      surface[15].vertex[1][k] = point[7].x[k];
      surface[15].vertex[2][k] = point[11].x[k];
      surface[15].vector[k] = (surface[15].vertex[0][k] + surface[15].vertex[1][k] + surface[15].vertex[2][k]) / 3.0;
      
      surface[16].vertex[0][k] = point[3].x[k];
      surface[16].vertex[1][k] = point[7].x[k];
      surface[16].vertex[2][k] = point[11].x[k];
      surface[16].vector[k] = (surface[16].vertex[0][k] + surface[16].vertex[1][k] + surface[16].vertex[2][k]) / 3.0;
      
      surface[17].vertex[0][k] = point[2].x[k];
      surface[17].vertex[1][k] = point[3].x[k];
      surface[17].vertex[2][k] = point[7].x[k];
      surface[17].vector[k] = (surface[17].vertex[0][k] + surface[17].vertex[1][k] + surface[17].vertex[2][k]) / 3.0;
      
      surface[18].vertex[0][k] = point[2].x[k];
      surface[18].vertex[1][k] = point[7].x[k];
      surface[18].vertex[2][k] = point[9].x[k];
      surface[18].vector[k] = (surface[18].vertex[0][k] + surface[18].vertex[1][k] + surface[18].vertex[2][k]) / 3.0;
      
      surface[19].vertex[0][k] = point[6].x[k];
      surface[19].vertex[1][k] = point[7].x[k];
      surface[19].vertex[2][k] = point[9].x[k];
      surface[19].vector[k] = (surface[19].vertex[0][k] + surface[19].vertex[1][k] + surface[19].vertex[2][k]) / 3.0;
    }
    
  surface[0].neighbour[0] = 1;
  surface[0].neighbour[1] = 4;
  surface[0].neighbour[2] = 5;
  
  surface[1].neighbour[0] = 0;
  surface[1].neighbour[1] = 2;
  surface[1].neighbour[2] = 7;
  
  surface[2].neighbour[0] = 1;
  surface[2].neighbour[1] = 3;
  surface[2].neighbour[2] = 9;
  
  surface[3].neighbour[0] = 2;
  surface[3].neighbour[1] = 4;
  surface[3].neighbour[2] = 11;
  
  surface[4].neighbour[0] = 0;
  surface[4].neighbour[1] = 3;
  surface[4].neighbour[2] = 13;
  
  surface[5].neighbour[0] = 0;
  surface[5].neighbour[1] = 14;
  surface[5].neighbour[2] = 6;
  
  surface[6].neighbour[0] = 5;
  surface[6].neighbour[1] = 7;
  surface[6].neighbour[2] = 15;
  
  surface[7].neighbour[0] = 1;
  surface[7].neighbour[1] = 6;
  surface[7].neighbour[2] = 8;
  
  surface[8].neighbour[0] = 7;
  surface[8].neighbour[1] = 9;
  surface[8].neighbour[2] = 16;
  
  surface[9].neighbour[0] = 2;
  surface[9].neighbour[1] = 8;
  surface[9].neighbour[2] = 10;
  
  surface[10].neighbour[0] = 9;
  surface[10].neighbour[1] = 11;
  surface[10].neighbour[2] = 17;
  
  surface[11].neighbour[0] = 3;
  surface[11].neighbour[1] = 10;
  surface[11].neighbour[2] = 12;
  
  surface[12].neighbour[0] = 11;
  surface[12].neighbour[1] = 13;
  surface[12].neighbour[2] = 18;
  
  surface[13].neighbour[0] = 4;
  surface[13].neighbour[1] = 12;
  surface[13].neighbour[2] = 14;
  
  surface[14].neighbour[0] = 5;
  surface[14].neighbour[1] = 13;
  surface[14].neighbour[2] = 19;
  
  surface[15].neighbour[0] = 6;
  surface[15].neighbour[1] = 16;
  surface[15].neighbour[2] = 19;
  
  surface[16].neighbour[0] = 8;
  surface[16].neighbour[1] = 15;
  surface[16].neighbour[2] = 17;
  
  surface[17].neighbour[0] = 10;
  surface[17].neighbour[1] = 16;
  surface[17].neighbour[2] = 18;
  
  surface[18].neighbour[0] = 12;
  surface[18].neighbour[1] = 17;
  surface[18].neighbour[2] = 19;
  
  surface[19].neighbour[0] = 14;
  surface[19].neighbour[1] = 15;
  surface[19].neighbour[2] = 18;
}
#endif

#endif
#endif
