#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"

#ifdef OSAKA

// Structure for communication during the density computation. Holds data that is sent to other processors //

static struct stardensdata_in
{
  MyDouble   Pos[3];
  MyFloat    Hsml;
  int NodeList[NODELISTLENGTH];
}*StarDensDataIn, *StarDensDataGet;

static struct stardensdata_out
{
  MyLongDouble Rho;
  MyLongDouble DhsmlDensity;
  MyLongDouble Ngb;
  MyLongDouble Pressure;
}*StarDensDataResult, *StarDensDataOut;

// Does almost the same calulcation as density.c, but including star particles and even wind particles. //

#ifdef OSAKA_ESFB
void esfb_star_density(void)
{
  MyFloat *Left, *Right;
  int i, j, ndone, ndone_flag, npleft, dummy, iter = 0;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  long long ntot;
  double dmax1, dmax2, fac;
  double desnumngb;
  double ascale, a3inv;
  double time, stellarage, factor_time;
    
#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif

  if(ThisTask == 0)
    {
      printf("Star density calculation...\n");
      fflush(stdout);
    }
    
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  Left = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  Right = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(All.ComovingIntegrationOn)
        stellarage = time - a2t(P[i].FormationTime);
      else
        stellarage = time - factor_time * P[i].FormationTime;
        
      if(P[i].Type == 4 && P[i].FBflagESFB < All.ESFBEventNumber && stellarage > P[i].ESFBDepositedTime)
        Left[i] = Right[i] = 0;
      else
        Left[i] = Right[i] = -1;
    }
    
  // allocate buffers to arrange communication //
  
  Ngblist = (int *) mymalloc(NumPart * sizeof(int));
  
  All.BunchSize = (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                    sizeof(struct stardensdata_in) + sizeof(struct stardensdata_out) +
                      sizemax(sizeof(struct stardensdata_in), sizeof(struct stardensdata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
  
  desnumngb = (double) All.DesNumNgbESFB;
    
  // we will repeat the whole thing for those particles where we didn't find enough neighbours //
  do
    {
      do
        {
          for(j = 0; j < NTask; j++)
            {
              Send_count[j] = 0;
              Exportflag[j] = -1;
            }
            
          // do local particles and prepare export list //
          for(i = FirstActiveParticle, nexport = 0; i >= 0; i = NextActiveParticle[i])
            {
              if(All.ComovingIntegrationOn)
                stellarage = time - a2t(P[i].FormationTime);
              else
                stellarage = time - factor_time * P[i].FormationTime;
                
              if(P[i].Type == 4 && P[i].FBflagESFB < All.ESFBEventNumber && Left[i] >= 0 && stellarage > P[i].ESFBDepositedTime)
                {
                  if(star_density_evaluate(i, 0, 0, &nexport, Send_count) < 0)
                    break;
                }
            }
            
#ifdef MYSORT
          mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
          qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
          MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
          
          for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
            {
              Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
              nimport += Recv_count[j];
              
              if(j > 0)
                {
                  Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
                  Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
                }
            }
            
          StarDensDataGet = (struct stardensdata_in *) mymalloc(nimport * sizeof(struct stardensdata_in));
          StarDensDataIn = (struct stardensdata_in *) mymalloc(nexport * sizeof(struct stardensdata_in));
          
          // prepare particle data for export //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              StarDensDataIn[j].Pos[0] = P[place].Pos[0];
              StarDensDataIn[j].Pos[1] = P[place].Pos[1];
              StarDensDataIn[j].Pos[2] = P[place].Pos[2];
              StarDensDataIn[j].Hsml   = P[place].Hsml;
              
              memcpy(StarDensDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
            }
            
          // exchange particle data //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // get the particles //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&StarDensDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE,recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&StarDensDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&StarDensDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, &StarDensDataGet[Recv_offset[recvTask]], 
                          Recv_count[recvTask] * sizeof(struct stardensdata_in), MPI_BYTE, recvTask, 
                            TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          myfree(StarDensDataIn);
          
          StarDensDataResult = (struct stardensdata_out *) mymalloc(nimport * sizeof(struct stardensdata_out));
          StarDensDataOut = (struct stardensdata_out *) mymalloc(nexport * sizeof(struct stardensdata_out));
          
          // now do the particles that were sent to us //
          for(j = 0; j < nimport; j++)
            star_density_evaluate(j, 1, 0, &dummy, &dummy);
            
          if(i < 0)
            ndone_flag = 1;
          else
            ndone_flag = 0;
            
          MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
          
          // get the result //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // send the results //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&StarDensDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&StarDensDataOut[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&StarDensDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, &StarDensDataOut[Send_offset[recvTask]],
                          Send_count[recvTask] * sizeof(struct stardensdata_out), MPI_BYTE, recvTask, 
                            TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          // add the result to the local particles //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              P[place].ESFBNgbNum += StarDensDataOut[j].Ngb;
              
              P[place].Density += StarDensDataOut[j].Rho;
              P[place].DhsmlDensityFactor += StarDensDataOut[j].DhsmlDensity;
              P[place].Pressure += StarDensDataOut[j].Pressure;
            }
            
          myfree(StarDensDataOut);
          myfree(StarDensDataResult);
          myfree(StarDensDataGet);
        }
      while(ndone < NTask);
      
      // The following is the main difference from hydro_force() because now numngb_inbox has to be //
      // reduced to a sphere, which ngbnum returned from ngb_treefind_pairs() does not need doing.  //
      for(i = FirstActiveParticle, npleft = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagESFB < All.ESFBEventNumber && Left[i] >= 0 && stellarage > P[i].ESFBDepositedTime)
            {
              if(P[i].Density > 0.0)
                {
                  P[i].DhsmlDensityFactor *= P[i].Hsml / (NUMDIMS * P[i].Density);
                  
                  // note: this would be -1 if only a single particle at zero lag is found //
                  if(P[i].DhsmlDensityFactor > - 0.9)
                    P[i].DhsmlDensityFactor = 1.0 / (1.0 + P[i].DhsmlDensityFactor);
                  else
                    P[i].DhsmlDensityFactor = 1.0;
                }
                
              // now check whether we had enough neighbours //
              if(P[i].ESFBNgbNum < (desnumngb - All.MaxNumNgbDeviation) || (P[i].ESFBNgbNum > (desnumngb + All.MaxNumNgbDeviation) && P[i].Hsml > (1.01 * All.MinGasHsml)))
                {
                  // need to redo this particle //
                  npleft++;
                  
                  if(Left[i] > 0 && Right[i] > 0)
                    if((Right[i] - Left[i]) < 1.0e-3 * Left[i])
                      {
                        // this one should be ok //
                        npleft--;
                        Left[i] = -1;// Mark as inactive //
                        continue;
                      }
                      
                  if(P[i].ESFBNgbNum < (desnumngb - All.MaxNumNgbDeviation))
                    Left[i] = DMAX(P[i].Hsml, Left[i]);
                  else
                    {
                      if(Right[i] != 0)
                        {
                          if(P[i].Hsml < Right[i])
                            Right[i] = P[i].Hsml;
                        }
                      else
                        Right[i] = P[i].Hsml;
                    }
                    
                  if(iter >= MAXITER - 10)
                    {
                      printf("i = %d task = %d ID = %d Hsml = %g Left = %g Right = %g Ngbs = %d Right - Left = %g\n", i, ThisTask, (int) P[i].ID, P[i].Hsml, Left[i], Right[i], (int) P[i].ESFBNgbNum, Right[i] - Left[i]);
                      printf("pos = (%g |%g |%g)\n", P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
                      fflush(stdout);
                    }
                    
                  if(Right[i] > 0 && Left[i] > 0)
                    P[i].Hsml = pow(0.5 * (pow(Left[i], 3) + pow(Right[i], 3)), 1.0 / 3);
                  else
                    {
                      if(Right[i] == 0 && Left[i] == 0)
                        endrun(8188);// can't occur //
                        
                      if(Right[i] == 0 && Left[i] > 0)
                        {
                          if(fabs(P[i].ESFBNgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - (P[i].ESFBNgbNum - desnumngb) / ((double) NUMDIMS * P[i].ESFBNgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac < 1.26)
                                P[i].Hsml *= fac;
                              else
                                P[i].Hsml *= 1.26;
                            }
                          else
                            P[i].Hsml *= 1.26;
                        }
                      
                      if(Right[i] > 0 && Left[i] == 0)
                        {
                          if(fabs(P[i].ESFBNgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - (P[i].ESFBNgbNum - desnumngb) / ((double) NUMDIMS * P[i].ESFBNgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac > 1.0 / 1.26)
                                  P[i].Hsml *= fac;
                              else
                                  P[i].Hsml /= 1.26;
                            }
                          else
                            P[i].Hsml /= 1.26;
                        }
                    }
                    
#ifdef OSAKA_SET_STAR_MINIMUM_HTML
                  if(P[i].Hsml < All.MinGasHsml)
		    P[i].Hsml = All.MinGasHsml;
#endif
                }
              else
                Left[i] = -1;// Mark as inactive //
            }
        }
        
      sumup_large_ints(1, &npleft, &ntot);
      
      if(ntot > 0)
        {
          iter++;
          
          if(iter > MAXITER)
            {
              printf("failed to converge in neighbour iteration in esfb_star_density()\n");
              fflush(stdout);
              endrun(1155);
            }
        }
    }
  while(ntot > 0);
  
  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  myfree(Right);
  myfree(Left);
}

void set_esfb_star_hsml_guess(void)
{
  int n, no;
  float hsml;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  for(n = FirstActiveParticle; n >= 0; n = NextActiveParticle[n])
    {
      if(All.ComovingIntegrationOn)
        stellarage = time - a2t(P[n].FormationTime);
      else
        stellarage = time - factor_time * P[n].FormationTime;
        
      if(P[n].Type == 4 && P[n].FBflagESFB < All.ESFBEventNumber && stellarage > P[n].ESFBDepositedTime)
        {
          no = Father[n];
          
          while(no >= 0)
            {
              if(Nodes[no].u.d.mass > (double) All.DesNumNgbESFB * P[n].Mass)
                break;
                
              no = Nodes[no].u.d.father;
            }
            
          hsml = P[n].Hsml;
          
          if(no >= 0)
            {
              if(Nodes[no].u.d.mass > (double) All.DesNumNgbESFB * P[n].Mass)
                hsml = pow(3.0 / (4.0 * M_PI) * (double) All.DesNumNgbESFB * P[n].Mass / Nodes[no].u.d.mass, 1.0 / 3.0) * Nodes[no].len;
            }
            
          if(hsml < P[n].Hsml)
            P[n].Hsml = hsml;
            
#ifdef OSAKA_SET_STAR_MINIMUM_HTML
          if(P[n].Hsml < All.MinGasHsml)
            P[n].Hsml = All.MinGasHsml;
#endif
        }
    }
}
#endif

#ifdef OSAKA_SNII
void snii_star_density(void)
{
  MyFloat *Left, *Right;
  int i, j, ndone, ndone_flag, npleft, dummy, iter = 0;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  long long ntot;
  double dmax1, dmax2, fac;
  double desnumngb;
  double ascale, a3inv;
  double time, stellarage, factor_time;
    
#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif

  if(ThisTask == 0)
    {
      printf("Star density calculation...\n");
      fflush(stdout);
    }
    
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  Left = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  Right = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(All.ComovingIntegrationOn)
        stellarage = time - a2t(P[i].FormationTime);
      else
        stellarage = time - factor_time * P[i].FormationTime;
        
      if(P[i].Type == 4 && P[i].FBflagSNII < All.SNIIEventNumber && stellarage > P[i].SNIIExplosionTime)
        Left[i] = Right[i] = 0;
      else
        Left[i] = Right[i] = -1;
    }
    
  // allocate buffers to arrange communication //
  
  Ngblist = (int *) mymalloc(NumPart * sizeof(int));
  
  All.BunchSize = (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                    sizeof(struct stardensdata_in) + sizeof(struct stardensdata_out) +
                      sizemax(sizeof(struct stardensdata_in), sizeof(struct stardensdata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
  
  desnumngb = (double) All.DesNumNgbSNII;
    
  // we will repeat the whole thing for those particles where we didn't find enough neighbours //
  do
    {
      do
        {
          for(j = 0; j < NTask; j++)
            {
              Send_count[j] = 0;
              Exportflag[j] = -1;
            }
            
          // do local particles and prepare export list //
          for(i = FirstActiveParticle, nexport = 0; i >= 0; i = NextActiveParticle[i])
            {
              if(All.ComovingIntegrationOn)
                stellarage = time - a2t(P[i].FormationTime);
              else
                stellarage = time - factor_time * P[i].FormationTime;
                
              if(P[i].Type == 4 && P[i].FBflagSNII < All.SNIIEventNumber && Left[i] >= 0 && stellarage > P[i].SNIIExplosionTime)
                {
                  if(star_density_evaluate(i, 0, 1, &nexport, Send_count) < 0)
                    break;
                }
            }
            
#ifdef MYSORT
          mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
          qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
          MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
          
          for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
            {
              Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
              nimport += Recv_count[j];
              
              if(j > 0)
                {
                  Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
                  Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
                }
            }
            
          StarDensDataGet = (struct stardensdata_in *) mymalloc(nimport * sizeof(struct stardensdata_in));
          StarDensDataIn = (struct stardensdata_in *) mymalloc(nexport * sizeof(struct stardensdata_in));
          
          // prepare particle data for export //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              StarDensDataIn[j].Pos[0] = P[place].Pos[0];
              StarDensDataIn[j].Pos[1] = P[place].Pos[1];
              StarDensDataIn[j].Pos[2] = P[place].Pos[2];
              StarDensDataIn[j].Hsml   = P[place].Hsml;
              
              memcpy(StarDensDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
            }
            
          // exchange particle data //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // get the particles //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&StarDensDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE,recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&StarDensDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&StarDensDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, &StarDensDataGet[Recv_offset[recvTask]], 
                          Recv_count[recvTask] * sizeof(struct stardensdata_in), MPI_BYTE, recvTask, 
                            TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          myfree(StarDensDataIn);
          
          StarDensDataResult = (struct stardensdata_out *) mymalloc(nimport * sizeof(struct stardensdata_out));
          StarDensDataOut = (struct stardensdata_out *) mymalloc(nexport * sizeof(struct stardensdata_out));
          
          // now do the particles that were sent to us //
          for(j = 0; j < nimport; j++)
            star_density_evaluate(j, 1, 1, &dummy, &dummy);
            
          if(i < 0)
            ndone_flag = 1;
          else
            ndone_flag = 0;
            
          MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
          
          // get the result //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // send the results //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&StarDensDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&StarDensDataOut[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&StarDensDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, &StarDensDataOut[Send_offset[recvTask]],
                          Send_count[recvTask] * sizeof(struct stardensdata_out), MPI_BYTE, recvTask, 
                            TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          // add the result to the local particles //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              P[place].SNIINgbNum += StarDensDataOut[j].Ngb;
              
              P[place].Density += StarDensDataOut[j].Rho;
              P[place].DhsmlDensityFactor += StarDensDataOut[j].DhsmlDensity;
              P[place].Pressure += StarDensDataOut[j].Pressure;
            }
            
          myfree(StarDensDataOut);
          myfree(StarDensDataResult);
          myfree(StarDensDataGet);
        }
      while(ndone < NTask);
      
      // The following is the main difference from hydro_force() because now numngb_inbox has to be //
      // reduced to a sphere, which ngbnum returned from ngb_treefind_pairs() does not need doing.  //
      for(i = FirstActiveParticle, npleft = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagSNII < All.SNIIEventNumber && Left[i] >= 0 && stellarage > P[i].SNIIExplosionTime)
            {
              if(P[i].Density > 0.0)
                {
                  P[i].DhsmlDensityFactor *= P[i].Hsml / (NUMDIMS * P[i].Density);
                  
                  // note: this would be -1 if only a single particle at zero lag is found //
                  if(P[i].DhsmlDensityFactor > - 0.9)
                    P[i].DhsmlDensityFactor = 1.0 / (1.0 + P[i].DhsmlDensityFactor);
                  else
                    P[i].DhsmlDensityFactor = 1.0;
                }
                
              // now check whether we had enough neighbours //
              if(P[i].SNIINgbNum < (desnumngb - All.MaxNumNgbDeviation) || (P[i].SNIINgbNum > (desnumngb + All.MaxNumNgbDeviation) && P[i].Hsml > (1.01 * All.MinGasHsml)))
                {
                  // need to redo this particle //
                  npleft++;
                  
                  if(Left[i] > 0 && Right[i] > 0)
                    if((Right[i] - Left[i]) < 1.0e-3 * Left[i])
                      {
                        // this one should be ok //
                        npleft--;
                        Left[i] = -1;// Mark as inactive //
                        continue;
                      }
                      
                  if(P[i].SNIINgbNum < (desnumngb - All.MaxNumNgbDeviation))
                    Left[i] = DMAX(P[i].Hsml, Left[i]);
                  else
                    {
                      if(Right[i] != 0)
                        {
                          if(P[i].Hsml < Right[i])
                            Right[i] = P[i].Hsml;
                        }
                      else
                        Right[i] = P[i].Hsml;
                    }
                    
                  if(iter >= MAXITER - 10)
                    {
                      printf("i = %d task = %d ID = %d Hsml = %g Left = %g Right = %g Ngbs = %d Right - Left = %g\n", i, ThisTask, (int) P[i].ID, P[i].Hsml, Left[i], Right[i], (int) P[i].SNIINgbNum, Right[i] - Left[i]);
                      printf("pos = (%g |%g |%g)\n", P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
                      fflush(stdout);
                    }
                    
                  if(Right[i] > 0 && Left[i] > 0)
                    P[i].Hsml = pow(0.5 * (pow(Left[i], 3) + pow(Right[i], 3)), 1.0 / 3);
                  else
                    {
                      if(Right[i] == 0 && Left[i] == 0)
                        endrun(8188);// can't occur //
                        
                      if(Right[i] == 0 && Left[i] > 0)
                        {
                          if(fabs(P[i].SNIINgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - (P[i].SNIINgbNum - desnumngb) / ((double) NUMDIMS * P[i].SNIINgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac < 1.26)
                                P[i].Hsml *= fac;
                              else
                                P[i].Hsml *= 1.26;
                            }
                          else
                            P[i].Hsml *= 1.26;
                        }
                      
                      if(Right[i] > 0 && Left[i] == 0)
                        {
                          if(fabs(P[i].SNIINgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - (P[i].SNIINgbNum - desnumngb) / ((double) NUMDIMS * P[i].SNIINgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac > 1.0 / 1.26)
                                  P[i].Hsml *= fac;
                              else
                                  P[i].Hsml /= 1.26;
                            }
                          else
                            P[i].Hsml /= 1.26;
                        }
                    }
                    
#ifdef OSAKA_SET_STAR_MINIMUM_HTML
                  if(P[i].Hsml < All.MinGasHsml)
		    P[i].Hsml = All.MinGasHsml;
#endif
                }
              else
                Left[i] = -1;// Mark as inactive //
            }
        }
        
      sumup_large_ints(1, &npleft, &ntot);
      
      if(ntot > 0)
        {
          iter++;
          
          if(iter > MAXITER)
            {
              printf("failed to converge in neighbour iteration in snii_star_density()\n");
              fflush(stdout);
              endrun(1155);
            }
        }
    }
  while(ntot > 0);
  
  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  myfree(Right);
  myfree(Left);
}

void set_snii_star_hsml_guess(void)
{
  int n, no;
  float hsml;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  for(n = FirstActiveParticle; n >= 0; n = NextActiveParticle[n])
    {
      if(All.ComovingIntegrationOn)
        stellarage = time - a2t(P[n].FormationTime);
      else
        stellarage = time - factor_time * P[n].FormationTime;
        
      if(P[n].Type == 4 && P[n].FBflagSNII < All.SNIIEventNumber && stellarage > P[n].SNIIExplosionTime)
        {
          no = Father[n];
          
          while(no >= 0)
            {
              if(Nodes[no].u.d.mass > (double) All.DesNumNgbSNII * P[n].Mass)
                break;
                
              no = Nodes[no].u.d.father;
            }
            
          hsml = P[n].Hsml;
          
          if(no >= 0)
            {
              if(Nodes[no].u.d.mass > (double) All.DesNumNgbSNII * P[n].Mass)
                hsml = pow(3.0 / (4.0 * M_PI) * (double) All.DesNumNgbSNII * P[n].Mass / Nodes[no].u.d.mass, 1.0 / 3.0) * Nodes[no].len;
            }
            
          if(hsml < P[n].Hsml)
            P[n].Hsml = hsml;
            
#ifdef OSAKA_SET_STAR_MINIMUM_HTML
          if(P[n].Hsml < All.MinGasHsml)
            P[n].Hsml = All.MinGasHsml;
#endif
        }
    }
}
#endif

#ifdef OSAKA_SNIA
void snia_star_density(void)
{
  MyFloat *Left, *Right;
  int i, j, ndone, ndone_flag, npleft, dummy, iter = 0;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  long long ntot;
  double dmax1, dmax2, fac;
  double desnumngb;
  double ascale, a3inv;
  double time, stellarage, factor_time;
    
#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif

  if(ThisTask == 0)
    {
      printf("Star density calculation...\n");
      fflush(stdout);
    }
    
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  Left = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  Right = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(All.ComovingIntegrationOn)
        stellarage = time - a2t(P[i].FormationTime);
      else
        stellarage = time - factor_time * P[i].FormationTime;
        
      if(P[i].Type == 4 && P[i].FBflagSNIa < All.SNIaEventNumber && stellarage > P[i].SNIaExplosionTime)
        Left[i] = Right[i] = 0;
      else
        Left[i] = Right[i] = -1;
    }
    
  // allocate buffers to arrange communication //
  
  Ngblist = (int *) mymalloc(NumPart * sizeof(int));
  
  All.BunchSize = (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                    sizeof(struct stardensdata_in) + sizeof(struct stardensdata_out) +
                      sizemax(sizeof(struct stardensdata_in), sizeof(struct stardensdata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
  
  desnumngb = (double) All.DesNumNgbSNIa;
    
  // we will repeat the whole thing for those particles where we didn't find enough neighbours //
  do
    {
      do
        {
          for(j = 0; j < NTask; j++)
            {
              Send_count[j] = 0;
              Exportflag[j] = -1;
            }
            
          // do local particles and prepare export list //
          for(i = FirstActiveParticle, nexport = 0; i >= 0; i = NextActiveParticle[i])
            {
              if(All.ComovingIntegrationOn)
                stellarage = time - a2t(P[i].FormationTime);
              else
                stellarage = time - factor_time * P[i].FormationTime;
                
              if(P[i].Type == 4 && P[i].FBflagSNIa < All.SNIaEventNumber && Left[i] >= 0 && stellarage > P[i].SNIaExplosionTime)
                {
                  if(star_density_evaluate(i, 0, 2, &nexport, Send_count) < 0)
                    break;
                }
            }
            
#ifdef MYSORT
          mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
          qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
          MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
          
          for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
            {
              Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
              nimport += Recv_count[j];
              
              if(j > 0)
                {
                  Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
                  Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
                }
            }
            
          StarDensDataGet = (struct stardensdata_in *) mymalloc(nimport * sizeof(struct stardensdata_in));
          StarDensDataIn = (struct stardensdata_in *) mymalloc(nexport * sizeof(struct stardensdata_in));
          
          // prepare particle data for export //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              StarDensDataIn[j].Pos[0] = P[place].Pos[0];
              StarDensDataIn[j].Pos[1] = P[place].Pos[1];
              StarDensDataIn[j].Pos[2] = P[place].Pos[2];
              StarDensDataIn[j].Hsml   = P[place].Hsml;
              
              memcpy(StarDensDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
            }
            
          // exchange particle data //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // get the particles //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&StarDensDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE,recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&StarDensDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&StarDensDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, &StarDensDataGet[Recv_offset[recvTask]], 
                          Recv_count[recvTask] * sizeof(struct stardensdata_in), MPI_BYTE, recvTask, 
                            TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          myfree(StarDensDataIn);
          
          StarDensDataResult = (struct stardensdata_out *) mymalloc(nimport * sizeof(struct stardensdata_out));
          StarDensDataOut = (struct stardensdata_out *) mymalloc(nexport * sizeof(struct stardensdata_out));
          
          // now do the particles that were sent to us //
          for(j = 0; j < nimport; j++)
            star_density_evaluate(j, 1, 2, &dummy, &dummy);
            
          if(i < 0)
            ndone_flag = 1;
          else
            ndone_flag = 0;
            
          MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
          
          // get the result //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // send the results //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&StarDensDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&StarDensDataOut[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&StarDensDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, &StarDensDataOut[Send_offset[recvTask]],
                          Send_count[recvTask] * sizeof(struct stardensdata_out), MPI_BYTE, recvTask, 
                            TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          // add the result to the local particles //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              P[place].SNIaNgbNum += StarDensDataOut[j].Ngb;
              
              P[place].Density += StarDensDataOut[j].Rho;
              P[place].DhsmlDensityFactor += StarDensDataOut[j].DhsmlDensity;
              P[place].Pressure += StarDensDataOut[j].Pressure;
            }
            
          myfree(StarDensDataOut);
          myfree(StarDensDataResult);
          myfree(StarDensDataGet);
        }
      while(ndone < NTask);
      
      // The following is the main difference from hydro_force() because now numngb_inbox has to be //
      // reduced to a sphere, which ngbnum returned from ngb_treefind_pairs() does not need doing.  //
      for(i = FirstActiveParticle, npleft = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagSNIa < All.SNIaEventNumber && Left[i] >= 0 && stellarage > P[i].SNIaExplosionTime)
            {
              if(P[i].Density > 0.0)
                {
                  P[i].DhsmlDensityFactor *= P[i].Hsml / (NUMDIMS * P[i].Density);
                  
                  // note: this would be -1 if only a single particle at zero lag is found //
                  if(P[i].DhsmlDensityFactor > - 0.9)
                    P[i].DhsmlDensityFactor = 1.0 / (1.0 + P[i].DhsmlDensityFactor);
                  else
                    P[i].DhsmlDensityFactor = 1.0;
                }
                
              // now check whether we had enough neighbours //
              if(P[i].SNIaNgbNum < (desnumngb - All.MaxNumNgbDeviation) || (P[i].SNIaNgbNum > (desnumngb + All.MaxNumNgbDeviation) && P[i].Hsml > (1.01 * All.MinGasHsml)))
                {
                  // need to redo this particle //
                  npleft++;
                  
                  if(Left[i] > 0 && Right[i] > 0)
                    if((Right[i] - Left[i]) < 1.0e-3 * Left[i])
                      {
                        // this one should be ok //
                        npleft--;
                        Left[i] = -1;// Mark as inactive //
                        continue;
                      }
                      
                  if(P[i].SNIaNgbNum < (desnumngb - All.MaxNumNgbDeviation))
                    Left[i] = DMAX(P[i].Hsml, Left[i]);
                  else
                    {
                      if(Right[i] != 0)
                        {
                          if(P[i].Hsml < Right[i])
                            Right[i] = P[i].Hsml;
                        }
                      else
                        Right[i] = P[i].Hsml;
                    }
                    
                  if(iter >= MAXITER - 10)
                    {
                      printf("i = %d task = %d ID = %d Hsml = %g Left = %g Right = %g Ngbs = %d Right - Left = %g\n", i, ThisTask, (int) P[i].ID, P[i].Hsml, Left[i], Right[i], (int) P[i].SNIaNgbNum, Right[i] - Left[i]);
                      printf("pos = (%g |%g |%g)\n", P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
                      fflush(stdout);
                    }
                    
                  if(Right[i] > 0 && Left[i] > 0)
                    P[i].Hsml = pow(0.5 * (pow(Left[i], 3) + pow(Right[i], 3)), 1.0 / 3);
                  else
                    {
                      if(Right[i] == 0 && Left[i] == 0)
                        endrun(8188);// can't occur //
                        
                      if(Right[i] == 0 && Left[i] > 0)
                        {
                          if(fabs(P[i].SNIaNgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - (P[i].SNIaNgbNum - desnumngb) / ((double) NUMDIMS * P[i].SNIaNgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac < 1.26)
                                P[i].Hsml *= fac;
                              else
                                P[i].Hsml *= 1.26;
                            }
                          else
                            P[i].Hsml *= 1.26;
                        }
                      
                      if(Right[i] > 0 && Left[i] == 0)
                        {
                          if(fabs(P[i].SNIaNgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - (P[i].SNIaNgbNum - desnumngb) / ((double) NUMDIMS * P[i].SNIaNgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac > 1.0 / 1.26)
                                  P[i].Hsml *= fac;
                              else
                                  P[i].Hsml /= 1.26;
                            }
                          else
                            P[i].Hsml /= 1.26;
                        }
                    }
                    
#ifdef OSAKA_SET_STAR_MINIMUM_HTML
                  if(P[i].Hsml < All.MinGasHsml)
		    P[i].Hsml = All.MinGasHsml;
#endif
                }
              else
                Left[i] = -1;// Mark as inactive //
            }
        }
        
      sumup_large_ints(1, &npleft, &ntot);
      
      if(ntot > 0)
        {
          iter++;
          
          if(iter > MAXITER)
            {
              printf("failed to converge in neighbour iteration in snia_star_density()\n");
              fflush(stdout);
              endrun(1155);
            }
        }
    }
  while(ntot > 0);
  
  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  myfree(Right);
  myfree(Left);
}

void set_snia_star_hsml_guess(void)
{
  int n, no;
  float hsml;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  for(n = FirstActiveParticle; n >= 0; n = NextActiveParticle[n])
    {
      if(All.ComovingIntegrationOn)
        stellarage = time - a2t(P[n].FormationTime);
      else
        stellarage = time - factor_time * P[n].FormationTime;
        
      if(P[n].Type == 4 && P[n].FBflagSNIa < All.SNIaEventNumber && stellarage > P[n].SNIaExplosionTime)
        {
          no = Father[n];
          
          while(no >= 0)
            {
              if(Nodes[no].u.d.mass > (double) All.DesNumNgbSNIa * P[n].Mass)
                break;
                
              no = Nodes[no].u.d.father;
            }
            
          hsml = P[n].Hsml;
          
          if(no >= 0)
            {
              if(Nodes[no].u.d.mass > (double) All.DesNumNgbSNIa * P[n].Mass)
                hsml = pow(3.0 / (4.0 * M_PI) * (double) All.DesNumNgbSNIa * P[n].Mass / Nodes[no].u.d.mass, 1.0 / 3.0) * Nodes[no].len;
            }
            
          if(hsml < P[n].Hsml)
            P[n].Hsml = hsml;
            
#ifdef OSAKA_SET_STAR_MINIMUM_HTML
          if(P[n].Hsml < All.MinGasHsml)
            P[n].Hsml = All.MinGasHsml;
#endif
        }
    }
}
#endif

#ifdef OSAKA_AGB
void agb_star_density(void)
{
  MyFloat *Left, *Right;
  int i, j, ndone, ndone_flag, npleft, dummy, iter = 0;
  int ngrp, sendTask, recvTask, place, nexport, nimport;
  long long ntot;
  double dmax1, dmax2, fac;
  double desnumngb;
  double ascale, a3inv;
  double time, stellarage, factor_time;
    
#ifdef  USE_ISEND_IRECV
  MPI_Status stat[2];
  MPI_Request mpireq[2];
#endif

  if(ThisTask == 0)
    {
      printf("Star density calculation...\n");
      fflush(stdout);
    }
    
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  Left = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  Right = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
  
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(All.ComovingIntegrationOn)
        stellarage = time - a2t(P[i].FormationTime);
      else
        stellarage = time - factor_time * P[i].FormationTime;
        
      if(P[i].Type == 4 && P[i].FBflagAGB < All.AGBEventNumber && stellarage > P[i].AGBExplosionTime)
        Left[i] = Right[i] = 0;
      else
        Left[i] = Right[i] = -1;
    }
    
  // allocate buffers to arrange communication //
  
  Ngblist = (int *) mymalloc(NumPart * sizeof(int));
  
  All.BunchSize = (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                    sizeof(struct stardensdata_in) + sizeof(struct stardensdata_out) +
                      sizemax(sizeof(struct stardensdata_in), sizeof(struct stardensdata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
  
  desnumngb = (double) All.DesNumNgbAGB;
    
  // we will repeat the whole thing for those particles where we didn't find enough neighbours //
  do
    {
      do
        {
          for(j = 0; j < NTask; j++)
            {
              Send_count[j] = 0;
              Exportflag[j] = -1;
            }
            
          // do local particles and prepare export list //
          for(i = FirstActiveParticle, nexport = 0; i >= 0; i = NextActiveParticle[i])
            {
              if(All.ComovingIntegrationOn)
                stellarage = time - a2t(P[i].FormationTime);
              else
                stellarage = time - factor_time * P[i].FormationTime;
                
              if(P[i].Type == 4 && P[i].FBflagAGB < All.AGBEventNumber && Left[i] >= 0 && stellarage > P[i].AGBExplosionTime)
                {
                  if(star_density_evaluate(i, 0, 3, &nexport, Send_count) < 0)
                    break;
                }
            }
            
#ifdef MYSORT
          mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
          qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
          MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
          
          for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
            {
              Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
              nimport += Recv_count[j];
              
              if(j > 0)
                {
                  Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
                  Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
                }
            }
            
          StarDensDataGet = (struct stardensdata_in *) mymalloc(nimport * sizeof(struct stardensdata_in));
          StarDensDataIn = (struct stardensdata_in *) mymalloc(nexport * sizeof(struct stardensdata_in));
          
          // prepare particle data for export //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              StarDensDataIn[j].Pos[0] = P[place].Pos[0];
              StarDensDataIn[j].Pos[1] = P[place].Pos[1];
              StarDensDataIn[j].Pos[2] = P[place].Pos[2];
              StarDensDataIn[j].Hsml   = P[place].Hsml;
              
              memcpy(StarDensDataIn[j].NodeList, DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
            }
            
          // exchange particle data //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // get the particles //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&StarDensDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE,recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&StarDensDataGet[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&StarDensDataIn[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_in), 
                        MPI_BYTE, recvTask, TAG_DENS_A, &StarDensDataGet[Recv_offset[recvTask]], 
                          Recv_count[recvTask] * sizeof(struct stardensdata_in), MPI_BYTE, recvTask, 
                            TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          myfree(StarDensDataIn);
          
          StarDensDataResult = (struct stardensdata_out *) mymalloc(nimport * sizeof(struct stardensdata_out));
          StarDensDataOut = (struct stardensdata_out *) mymalloc(nexport * sizeof(struct stardensdata_out));
          
          // now do the particles that were sent to us //
          for(j = 0; j < nimport; j++)
            star_density_evaluate(j, 1, 3, &dummy, &dummy);
            
          if(i < 0)
            ndone_flag = 1;
          else
            ndone_flag = 0;
            
          MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
          
          // get the result //
          for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
              sendTask = ThisTask;
              recvTask = ThisTask ^ ngrp;
              
              if(recvTask < NTask)
                {
                  if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                      // send the results //
#ifdef USE_ISEND_IRECV
                      MPI_Isend(&StarDensDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                      MPI_Irecv(&StarDensDataOut[Send_offset[recvTask]], Send_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                      MPI_Waitall(2,mpireq,stat);
#else
                      MPI_Sendrecv(&StarDensDataResult[Recv_offset[recvTask]], Recv_count[recvTask] * sizeof(struct stardensdata_out), 
                        MPI_BYTE, recvTask, TAG_DENS_B, &StarDensDataOut[Send_offset[recvTask]],
                          Send_count[recvTask] * sizeof(struct stardensdata_out), MPI_BYTE, recvTask, 
                            TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
          // add the result to the local particles //
          for(j = 0; j < nexport; j++)
            {
              place = DataIndexTable[j].Index;
              P[place].AGBNgbNum += StarDensDataOut[j].Ngb;
              
              P[place].Density += StarDensDataOut[j].Rho;
              P[place].DhsmlDensityFactor += StarDensDataOut[j].DhsmlDensity;
              P[place].Pressure += StarDensDataOut[j].Pressure;
            }
            
          myfree(StarDensDataOut);
          myfree(StarDensDataResult);
          myfree(StarDensDataGet);
        }
      while(ndone < NTask);
      
      // The following is the main difference from hydro_force() because now numngb_inbox has to be //
      // reduced to a sphere, which ngbnum returned from ngb_treefind_pairs() does not need doing.  //
      for(i = FirstActiveParticle, npleft = 0; i >= 0; i = NextActiveParticle[i])
        {
          if(All.ComovingIntegrationOn)
            stellarage = time - a2t(P[i].FormationTime);
          else
            stellarage = time - factor_time * P[i].FormationTime;
            
          if(P[i].Type == 4 && P[i].FBflagAGB < All.AGBEventNumber && Left[i] >= 0 && stellarage > P[i].AGBExplosionTime)
            {
              if(P[i].Density > 0.0)
                {
                  P[i].DhsmlDensityFactor *= P[i].Hsml / (NUMDIMS * P[i].Density);
                  
                  // note: this would be -1 if only a single particle at zero lag is found //
                  if(P[i].DhsmlDensityFactor > - 0.9)
                    P[i].DhsmlDensityFactor = 1.0 / (1.0 + P[i].DhsmlDensityFactor);
                  else
                    P[i].DhsmlDensityFactor = 1.0;
                }
                
              // now check whether we had enough neighbours //
              if(P[i].AGBNgbNum < (desnumngb - All.MaxNumNgbDeviation) || (P[i].AGBNgbNum > (desnumngb + All.MaxNumNgbDeviation) && P[i].Hsml > (1.01 * All.MinGasHsml)))
                {
                  // need to redo this particle //
                  npleft++;
                  
                  if(Left[i] > 0 && Right[i] > 0)
                    if((Right[i] - Left[i]) < 1.0e-3 * Left[i])
                      {
                        // this one should be ok //
                        npleft--;
                        Left[i] = -1;// Mark as inactive //
                        continue;
                      }
                      
                  if(P[i].AGBNgbNum < (desnumngb - All.MaxNumNgbDeviation))
                    Left[i] = DMAX(P[i].Hsml, Left[i]);
                  else
                    {
                      if(Right[i] != 0)
                        {
                          if(P[i].Hsml < Right[i])
                            Right[i] = P[i].Hsml;
                        }
                      else
                        Right[i] = P[i].Hsml;
                    }
                    
                  if(iter >= MAXITER - 10)
                    {
                      printf("i = %d task = %d ID = %d Hsml = %g Left = %g Right = %g Ngbs = %d Right - Left = %g\n", i, ThisTask, (int) P[i].ID, P[i].Hsml, Left[i], Right[i], (int) P[i].AGBNgbNum, Right[i] - Left[i]);
                      printf("pos = (%g |%g |%g)\n", P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
                      fflush(stdout);
                    }
                    
                  if(Right[i] > 0 && Left[i] > 0)
                    P[i].Hsml = pow(0.5 * (pow(Left[i], 3) + pow(Right[i], 3)), 1.0 / 3);
                  else
                    {
                      if(Right[i] == 0 && Left[i] == 0)
                        endrun(8188);// can't occur //
                        
                      if(Right[i] == 0 && Left[i] > 0)
                        {
                          if(fabs(P[i].AGBNgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - (P[i].AGBNgbNum - desnumngb) / ((double) NUMDIMS * P[i].AGBNgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac < 1.26)
                                P[i].Hsml *= fac;
                              else
                                P[i].Hsml *= 1.26;
                            }
                          else
                            P[i].Hsml *= 1.26;
                        }
                      
                      if(Right[i] > 0 && Left[i] == 0)
                        {
                          if(fabs(P[i].AGBNgbNum - desnumngb) < 0.5 * desnumngb)
                            {
                              fac = 1.0 - (P[i].AGBNgbNum - desnumngb) / ((double) NUMDIMS * P[i].AGBNgbNum) * P[i].DhsmlDensityFactor;
                                
                              if(fac > 1.0 / 1.26)
                                  P[i].Hsml *= fac;
                              else
                                  P[i].Hsml /= 1.26;
                            }
                          else
                            P[i].Hsml /= 1.26;
                        }
                    }
                    
#ifdef OSAKA_SET_STAR_MINIMUM_HTML
                  if(P[i].Hsml < All.MinGasHsml)
		    P[i].Hsml = All.MinGasHsml;
#endif
                }
              else
                Left[i] = -1;// Mark as inactive //
            }
        }
        
      sumup_large_ints(1, &npleft, &ntot);
      
      if(ntot > 0)
        {
          iter++;
          
          if(iter > MAXITER)
            {
              printf("failed to converge in neighbour iteration in agb_star_density()\n");
              fflush(stdout);
              endrun(1155);
            }
        }
    }
  while(ntot > 0);
  
  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);
  myfree(Right);
  myfree(Left);
}

void set_agb_star_hsml_guess(void)
{
  int n, no;
  float hsml;
  double ascale, a3inv;
  double time, stellarage, factor_time;
  
  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      a3inv = 1.0 / (ascale * ascale * ascale);
      factor_time = 1.0;
      time = a2t(All.Time);
    }
  else
    {
      ascale = a3inv = 1.0;
      factor_time =  All.UnitTime_in_s / SEC_PER_GIGAYEAR;
      time = factor_time * All.Time;
    }
    
  for(n = FirstActiveParticle; n >= 0; n = NextActiveParticle[n])
    {
      if(All.ComovingIntegrationOn)
        stellarage = time - a2t(P[n].FormationTime);
      else
        stellarage = time - factor_time * P[n].FormationTime;
        
      if(P[n].Type == 4 && P[n].FBflagAGB < All.AGBEventNumber && stellarage > P[n].AGBExplosionTime)
        {
          no = Father[n];
          
          while(no >= 0)
            {
              if(Nodes[no].u.d.mass > (double) All.DesNumNgbAGB * P[n].Mass)
                break;
                
              no = Nodes[no].u.d.father;
            }
            
          hsml = P[n].Hsml;
          
          if(no >= 0)
            {
              if(Nodes[no].u.d.mass > (double) All.DesNumNgbAGB * P[n].Mass)
                hsml = pow(3.0 / (4.0 * M_PI) * (double) All.DesNumNgbAGB * P[n].Mass / Nodes[no].u.d.mass, 1.0 / 3.0) * Nodes[no].len;
            }
            
          if(hsml < P[n].Hsml)
            P[n].Hsml = hsml;
            
#ifdef OSAKA_SET_STAR_MINIMUM_HTML
          if(P[n].Hsml < All.MinGasHsml)
            P[n].Hsml = All.MinGasHsml;
#endif
        }
    }
}
#endif

int star_density_evaluate(int target, int mode, int fbmode, int *nexport, int *nsend_local)
{
  int j, n;
  int startnode, numngb, numngb_inbox, listindex = 0;
  double h, h2, hinv, hinv3, hinv4;
  MyLongDouble rho, pressure;
  double wk, dwk;
  double dx, dy, dz, r, r2, u, mass_j, rho_j;
  MyLongDouble weighted_numngb, pressure_masswk;
  MyLongDouble dhsmlrho;
  MyDouble *pos;
  rho = weighted_numngb = dhsmlrho = pressure = pressure_masswk = 0.0;
  
  if(mode == 0)
    {
      pos = P[target].Pos;
      h = P[target].Hsml;
    }
  else
    {
      pos = StarDensDataGet[target].Pos;
      h = StarDensDataGet[target].Hsml;
    }
    
  h2 = h * h;
  hinv = 1.0 / h;
#ifndef  TWODIMS
  hinv3 = hinv * hinv * hinv;
#else
  hinv3 = hinv * hinv / boxSize_Z;
#endif
  hinv4 = hinv3 * hinv;
  
  if(mode == 0)
    {
      startnode = All.MaxPart;// root node //
    }
  else
    {
      startnode = StarDensDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;// open it //
    }
    
  numngb = 0;
  
  while(startnode >= 0)
    {
      while(startnode >= 0)
        {
          numngb_inbox = ngb_treefind_variable(pos, h, target, &startnode, mode, nexport, nsend_local);
          
          if(numngb_inbox < 0)
            return -1;
            
          for(n = 0; n < numngb_inbox; n++)
            {
              j = Ngblist[n];
              dx = pos[0] - P[j].Pos[0];
              dy = pos[1] - P[j].Pos[1];
              dz = pos[2] - P[j].Pos[2];
              //  now find the closest image in the given box size  //
#ifdef PERIODIC
              if(dx > boxHalf_X)
                dx -= boxSize_X;
              if(dx < -boxHalf_X)
                dx += boxSize_X;
              if(dy > boxHalf_Y)
                dy -= boxSize_Y;
              if(dy < -boxHalf_Y)
                dy += boxSize_Y;
              if(dz > boxHalf_Z)
                dz -= boxSize_Z;
              if(dz < -boxHalf_Z)
                dz += boxSize_Z;
#endif
              r2 = dx * dx + dy * dy + dz * dz;
              
              if(r2 < h2)
                {
                  numngb++;
                  r = sqrt(r2);
                  u = r * hinv;
                  
#if !defined(QUINTIC_KERNEL) && !defined(WENDLAND_C4_KERNEL)
                  if(u < 0.5)
                    {
                      wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1.0) * u * u);
                      dwk = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
                    }
                  else
                    {
                      wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
                      dwk = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
                    }
#elif QUINTIC_KERNEL
                  if(0.0 <= u && u < ONETHIRD)
                    {
                      wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) + 15.0 * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u);
                      dwk = - 5.0 * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) + 30.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) - 75.0 * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u) * (ONETHIRD - u);
                    }
                  else if(ONETHIRD <= u && u < TWOTHIRD)
                    {
                      wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) - 6.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u);
                      dwk = - 5.0 * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) + 30.0 * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u) * (TWOTHIRD - u);
                    }
                  else
                    {
                      wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u);
                      dwk = - 5.0 * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u);
                    }
                    
                  wk *= KERNEL_NORM * hinv3;
                  dwk *= KERNEL_NORM * hinv4;
#elif WENDLAND_C4_KERNEL
                  wk = (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 + 6.0 * u + 35.0 / 3 * u * u);
                  dwk = - (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (1.0 - u) * (280.0 / 3.0 * u * u + 56.0 / 3.0 * u);
                  wk *= KERNEL_NORM * hinv3;
                  dwk *= KERNEL_NORM * hinv4;
#endif
                  
                  mass_j = P[j].Mass;
                  rho_j = SphP[j].d.Density;
                  rho += FLT(mass_j * wk);
                  weighted_numngb += FLT(NORM_COEFF * wk / hinv3);
                  dhsmlrho += FLT(- mass_j * ((double) NUMDIMS * hinv * wk + u * dwk));
                  pressure += FLT(SphP[j].Pressure * mass_j / rho_j * wk);
                }
            }
        }
        
      if(mode == 1)
        {
          listindex++;
          
          if(listindex < NODELISTLENGTH)
            {
              startnode = StarDensDataGet[target].NodeList[listindex];
              
              if(startnode >= 0)
                startnode = Nodes[startnode].u.d.nextnode;// open it //
            }
        }
    }
    
  if(mode == 0)
    {
      if(fbmode == 0)
        P[target].ESFBNgbNum = (double) numngb;
      else if(fbmode == 1)
        P[target].SNIINgbNum = (double) numngb;
      else if(fbmode == 2)
        P[target].SNIaNgbNum = (double) numngb;
      else if(fbmode == 3)
        P[target].AGBNgbNum = (double) numngb;
      //P[target].NgbNum = weighted_numngb;
      P[target].Density = rho;
      P[target].DhsmlDensityFactor = dhsmlrho;
      P[target].Pressure = pressure;
    }
  else
    {
      StarDensDataResult[target].Ngb = (double) numngb;
      //StarDensDataResult[target].Ngb = weighted_numngb;
      StarDensDataResult[target].Rho = rho;
      StarDensDataResult[target].DhsmlDensity = dhsmlrho;
      StarDensDataResult[target].Pressure = pressure;
    }
    
  return 0;
}

#endif
